# Computational Fluids Management Tool
Computational Fluids Management Tool (cfmt) is a simulation management tool and tracker for running and managing numerical cfd simulations. It is code agnostic and allows for interfaces with existing numerical codes, including ANSYS Fluent and OpenFOAM. It can also work with batch submission systems, including SGE.

## Dependencies

```
python-colored
python-pick
python-pyqt5
```

## Installation
cfmt can be installed locally using `pip`

```
pip install --user /path/to/package
```

To install in editable mode, add `-e`, so:

```
pip install --user -e /path/to/package
```

## Usage
