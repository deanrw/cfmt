#/usr/bin/env bash
# bash-completion script for cfmt

CMDS="
init \
edit \
delete \
info \
run \
utils \
new \
update \
status \
make \
fetch \
sync \
note \
move \
log \
results \
"
init_opts="
--name \
--empty \
--description \
--project-name \
--case \
--code \
--openfoam-example \
--openfoam-list-examples \
--openfoam-solver \
"

_repo_conf() {
    local cur prev prev2

    cur=${COMP_WORDS[COMP_CWORD]}
    prev=${COMP_WORDS[COMP_CWORD-1]}
    prev2=${COMP_WORDS[COMP_CWORD-2]}
    prev3=${COMP_WORDS[COMP_CWORD-3]}
    first=${COMP_WORDS[1]}
    
    case ${COMP_CWORD} in 
        1)
            if [[ ${cur} == -* ]]; then
                WORDS="$OPTS"
            else
                WORDS="$CMDS"
            fi
            COMPREPLY=($(compgen -W "$WORDS" -- $cur))
            ;;
        2) 
            case ${prev} in
                init)
                    COMPREPLY=($(compgen -W "$init_opts" -- $cur))
                    ;; 
                init|enable|list|create|show|edit|check|check-conf|prune)
                    COMPREPLY=($(compgen -W "$REPOS" -- $cur))
                    ;;
                extract)
                    # add :: to each REPO
                    REPO_MOD=$(printf '%s:: ' $REPOS)
                    COMPREPLY=($(compgen -W "$REPO_MOD" -- $cur))
                    ;;
                disable)
                    # only enabled repos
                    ACTIVE=$(find -L $BORGCTL_CONF_DIR/timers/ -xtype l -exec basename {} \;)
                    ACTIVE=$(echo -e "${ACTIVE// /\\n}" | sort -u)
                    COMPREPLY=($(compgen -W "$ACTIVE" -- $cur))
                    ;;
                enable-timer|disable-timer|start-timer|stop-timer)
                    # supposed to be specific but cba to implement that atm
                    COMPREPLY=($(compgen -W "$TIMERS" -- $cur))
            esac
            ;;
        3)
            case ${prev2} in
                enable)
                    COMPREPLY=($(compgen -W "$TIMERS" -- $cur))
                    ;;
                disable)
                    ACTIVE=$(find -L $BORGCTL_CONF_DIR/timers/*/${prev} -xtype l )
                    available_timers=""
                    for act in $ACTIVE; do
                        _timer="$(basename $(dirname $act))"
                        available_timers+=" ${_timer%.d}"
                    done
                    COMPREPLY=($(compgen -W "$available_timers" -- $cur))
                    ;;
                check|check-conf|create|init)
                    COMPREPLY=($(compgen -W "$REPOS" -- $cur))
                    ;;
                enable-timer|disable-timer|start-timer|stop-timer)
                    # supposed to be specific but cba to implement that atm
                    COMPREPLY=($(compgen -W "$TIMERS" -- $cur))
            esac
            ;;
        *)
            case ${first} in
                check|check-conf|create|init)
                    COMPREPLY=($(compgen -W "$REPOS" -- $cur))
                ;;
                enable-timer|disable-timer|start-timer|stop-timer)
                    # supposed to be specific but cba to implement that atm
                    COMPREPLY=($(compgen -W "$TIMERS" -- $cur))
            esac
            ;;
    esac
}

complete -F _repo_conf cfmt