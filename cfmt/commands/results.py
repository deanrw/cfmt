"""
Subparser options for results mode
"""
from .parser import StatusAction
from ..entities.runs import Run
from ..codes import codes
from ..post.controller import AVAILABLE_POST_FORMATS, AVAILABLE_POST_SYSTEMS

def add_parser(subparsers):
    """
    Adds options
    """
    parser_results = subparsers.add_parser('results', help="View run results")
    parser_results.set_defaults(func="results")

    parser_results.add_argument(
        '-r', '--residuals',
        action="store_true",
        help="Display residuals",
        default=False
    )
    parser_results.add_argument(
        '-m', '--mass-flow',
        action="store_true",
        help="Display mass-flow rate",
        default=False
    )
    parser_results.add_argument(
        '-t', '--timeres',
        action="store_true",
        help="Display time residuals",
        default=False
    )
    parser_results.add_argument(
        '--list-monitors',
        action="store_true",
        help="List the monitors available",
        default=False
    )
    parser_results.add_argument(
        '--monitor',
        nargs='?',
        help="Display monitors",
        default=False,
        const=None
    )
    parser_results.add_argument(
        '--plot',
        nargs='?',
        help="Plot files",
        default=False,
        const=None
    )
    parser_results.add_argument(
        '--plot-sequence',
        nargs=1,
        type=str,
        help="Plot a sequence of files from a given DIR",
        metavar="DIR",
        default=None
    )
    parser_results.add_argument(
        '--unsteady',
        action="store_true",
        help="Process time-dependent output",
        default=False,
    )
    parser_results.add_argument(
        '--tout-dir',
        type=str,
        nargs=1,
        default=None,
        help="The directory where time output files have been saved.",
    )
    parser_results.add_argument(
        '--tout-mask',
        type=str,
        nargs=1,
        default=None,
        help="The directory mask to search for where time output files have been saved.",
    )
    parser_results.add_argument(
        '--tout-info',
        action="store_true",
        help="Provides information about any time-output data sets",
        default=None,
    )
    parser_results.add_argument(
        '--force-convert',
        action="store_true",
        help="Convert results even if they are considered up-to-date.",
        default=False,
    )
    parser_results.add_argument(
        '--compare',
        action="store_true",
        help="Compares two runs chosen with --select",
        default=False,
    )
    parser_results.add_argument(
        '--last-n',
        type=int,
        nargs=1,
        default=0,
        help="Only plot the last n points"
    )
    parser_results.add_argument(
        '--tavg-info',
        action="store_true",
        help="Provides information about any time-averaged data sets",
        default=None,
    )
    parser_results.add_argument(
        '--stdout',
        action="store_true",
        help="Follow the stdout of the running job",
        default=None,
    )
    parser_results.add_argument(
        '--stderr',
        action="store_true",
        help="Follow the stderr of the running job",
        default=None,
    )
    parser_results.add_argument(
        '-L',
        "--local-only",
        action="store_true",
        default=False,
        help="Ignores staging status and plots local run directory"
    )
    parser_results_mode = parser_results.add_mutually_exclusive_group()
    parser_results_mode.add_argument(
        '--view',
        nargs=1,
        help="View results in selected postprocessing tool",
        default=None,
        choices=AVAILABLE_POST_SYSTEMS,
    )
    parser_results_mode.add_argument(
        '--batch',
        nargs=2,
        help="Run the provided script with the selected tool",
        default=None,
        metavar=("PROG", "SCRIPT")
    )
    parser_results_mode.add_argument(
        '--real-time', '--live',
        action="store_true",
        dest="live",
        help="Continuous read and plot the data in real-time",
        default=False
    )
    parser_results_mode.add_argument(
        '--convert',
        nargs=1,
        help="Convert code output to specified supported format",
        default=False,
        choices=AVAILABLE_POST_FORMATS,
    )
    parser_results_compare = parser_results.add_mutually_exclusive_group()
    parser_results_compare.add_argument(
        '--runs', '--select',
        nargs='+',
        type=str,
        dest="select",
        help="The run name to select",
        default=None,
    )
    parser_results_compare.add_argument(
        '--status',
        nargs=1,
        action=StatusAction,
        type=str,
        help="Show all runs with this status",
        default=None,
        choices=Run.valid_status_names
    )
    # add code specific arguments
    for code in codes.AVAILABLE_CODES:
        codes.add_arguments(code, 'results', parser_results)
