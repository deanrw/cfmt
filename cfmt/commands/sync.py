"""
Subparser options for sync mode
"""
def add_parser(subparsers, parents=None):
    """
    Adds options
    """
    parser_sync = subparsers.add_parser('sync', help="Sync runs from staging. Runs remain staged.",
                                        parents=parents)
    parser_sync.set_defaults(func="sync")
    parser_sync_select = parser_sync.add_mutually_exclusive_group()
    parser_sync_select.add_argument(
        '-a', '--all',
        action='store_true',
        help="Syncs all cases currently marked as in staging",
        default=False,
    )
    parser_sync_select.add_argument(
        '--select',
        nargs='+',
        type=str,
        help="The run names to sync",
        default=None,
    )
    parser_sync.add_argument(
        '--with-tout',
        action='store_true',
        help="Include timeoutput when fetching",
        default=False
    )
    parser_sync.add_argument(
        '--force',
        action='store_true',
        help="Ignore staging status when syncing. Can lead to data loss",
        default=False
    )
    parser_sync.add_argument(
        '--exclude',
        action='append',
        default=[],
        help="Exclude matching files, matches glob patterns",
    )
    parser_sync.add_argument(
        '--get-tout',
        nargs='?',
        action='append',
        metavar="TOUT",
        help="Retrive all tout or TOUT if provided (tarpipe)",
        default=[],
    )
    parser_sync.add_argument(
        '--move-staging-to',
        nargs=1,
        type=str,
        metavar="DIR",
        help="Move staging to a new root DIR (combined with staging location in cfg)",
        default=None
    )
