"""
Automatic imports for different cfmt modes
"""
from . import upgrade
from . import init
from . import edit
from . import delete
from . import info
from . import batch
from . import run
from . import utils
from . import cmd
from . import new
from . import update
from . import status
from . import make
from . import fetch
from . import sync
from . import note
from . import move
from . import copy
from . import log
from . import results
from . import export
from . import archive
from . import srcdiff
from . import post
