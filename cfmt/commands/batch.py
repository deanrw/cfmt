"""
Subparser options for common batch mode
"""
from argparse import ArgumentParser, REMAINDER
from ..batch_systems import controller as batch

def get_common():
    """
    Common arguments across multiple subparsers
    """
    parser_batch = ArgumentParser(add_help=False)
    parser_run_batch = parser_batch.add_argument_group("Batch system options")
    parser_run_batch.add_argument(
        '--batch-system',
        nargs=1,
        type=str,
        help="The batch scheduler to use",
        choices=batch.AVAILABLE_BATCH_SYSTEMS,
        default=None,
    )
    parser_run_batch.add_argument(
        '--no-batch',
        action = "append_const",
        dest="batch_system",
        const="none",
        help = "Don't use a batch system",
    )
    parser_run_batch.add_argument(
        '-q','--queue',
        nargs=1,
        type=str,
        help="The queue to pass to the batch scheduler",
        default=None,
    )
    parser_run_batch.add_argument(
        '--batch-options',
        nargs=REMAINDER,
        help="Pass remaining options to the batch scheduler",
        default=None
    )
    # -> add batch system specific options
    for batch_name in batch.AVAILABLE_BATCH_SYSTEMS:
        batch.add_arguments(batch_name, 'run', parser_run_batch)

    return parser_batch
