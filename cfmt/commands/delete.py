"""
Subparser options for delete mode
"""
def add_parser(subparsers):
    """
    Adds subparser options for delete mode
    """
    parser_delete = subparsers.add_parser('delete', help="Deletes a study")
    parser_delete.set_defaults(func="delete")
    parser_delete.add_argument(
        '--keep-files',
        action='store_true',
        help="Keep all files, only remove from database",
        default=False
    )
    parser_delete.add_argument(
        '--keep-local',
        action='store_true',
        help="Keep all local data, only remove files in staging",
        default=False
    )
    parser_delete.add_argument(
        '--staging-only',
        action='store_true',
        help="Remove all files within staging dir",
        default=False
    )
    parser_delete.add_argument(
        '--delete-db',
        action='store_true',
        help="When deleting runs, also delete the database entry",
        default=False
    )
    parser_delete.add_argument(
        '-y', '--yes',
        action='store_true',
        help="Say yes to all prompts",
        default=False
    )
    parser_delete_choice = parser_delete.add_mutually_exclusive_group()
    parser_delete_choice.add_argument(
        '-n','--case-names',
        nargs='+',
        type=str,
        help="Names of cases to delete",
        default=None
    )
    parser_delete_choice.add_argument(
        '-p','--case-paths',
        nargs='+',
        type=str,
        help="Paths to cases to delete",
        default=None
    )
    parser_delete_choice.add_argument(
        '-r', '--runs',
        nargs='+',
        type=str,
        help="Names of runs to delete",
        default=None
    )
    parser_delete_choice.add_argument(
        '-j', '--jobs',
        nargs='+',
        type=int,
        help="Ids of jobs to delete",
        default=None
    )
