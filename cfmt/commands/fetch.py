"""
Subparser options for fetch mode
"""
def add_parser(subparsers, parents=None):
    """
    Adds options
    """
    parser_fetch = subparsers.add_parser('fetch', help="Fetch runs from staging",
                                         parents=parents)
    parser_fetch.set_defaults(func="fetch")
    parser_fetch_select = parser_fetch.add_mutually_exclusive_group()
    parser_fetch_select.add_argument(
        '-a', '--all',
        action='store_true',
        help="Fetch all cases currently marked as in staging",
        default=False,
    )
    parser_fetch_select.add_argument(
        '--select',
        nargs='+',
        type=str,
        help="The run names to fetch",
        default=None,
    )
    parser_fetch.add_argument(
        '--preserve-source',
        action='store_true',
        help="Do not delete staging files after transfer",
        default=False
    )
    parser_fetch.add_argument(
        '--with-tout',
        action='store_true',
        help="Include timeoutput when fetching",
        default=False
    )
    parser_fetch.add_argument(
        '--exclude',
        action='append',
        default=[],
        help="Exclude matching files, matches glob patterns",
    )
    parser_fetch.add_argument(
        '--force',
        action='store_true',
        help="Ignore run status when fetching",
        default=False
    )
