"""
Subparser options for update mode
"""
from ..entities.runs import Run
from .parser import str2int,str2bool,valid_date

def add_parser(subparsers):
    """
    Adds options
    """
    parser_update = subparsers.add_parser('update', help="Updates the status of CFMT cases")
    parser_update.set_defaults(func="update")
    parser_update_all = parser_update.add_mutually_exclusive_group()
    parser_update_all.add_argument(
        '-a','--all',
        action='store_true',
        dest="update_all",
        help="Update all cases within the study",
        default=True
    )
    parser_update_all.add_argument(
        '-n','--case-names',
        nargs='+',
        type=str,
        help="Names of cases to update",
        default=None
    )
    parser_update_all.add_argument(
        'directory',
        nargs='?',
        type=str,
        help="Paths to cases to update",
        default=None
    )
    parser_update_all.add_argument(
        '-r', '--runs', '--select',
        nargs='+',
        type=str,
        help="Runs to update",
        default=None
    )
    parser_update.add_argument(
        '--set-status',
        nargs=1,
        type=str,
        help="The status to set",
        choices=Run.valid_status_names,
        default=None,
    )
    parser_update.add_argument(
        '--set-reason',
        nargs=1,
        type=str,
        help="Set a reason",
        default=None,
    )
    parser_update.add_argument(
        '--set-staging',
        nargs=1,
        type=str2bool,
        help="Set the in-staging status",
        default=None,
    )
    parser_update.add_argument(
        '--set-staging-mode',
        nargs=1,
        type=str,
        help="Set the staging mode",
        choices=Run.valid_staging_modes,
        default=None,
    )
    parser_update.add_argument(
        '--set-archived',
        nargs=1,
        type=str2int,
        help="Set the archived status",
        default=None,
    )
    parser_update.add_argument(
        '-t:a',
        '--add-tags',
        nargs='+',
        type=str,
        metavar="TAG",
        help="Tag(s) to add to the specified run",
        default=None
    )
    parser_update.add_argument(
        '-t:r',
        '--remove-tags',
        nargs='+',
        type=str,
        metavar="TAG",
        help="Tag(s) to add to the specified run",
        default=None
    )
    parser_update.add_argument(
        '--set-tags',
        nargs='+',
        type=str,
        metavar="TAG",
        help="Set tags on the specified run",
        default=None
    )
    parser_update.add_argument(
        '--move-to-case',
        nargs=1,
        type=str,
        metavar="CASE",
        help="Move the run to a different (existing) case",
        default=None
    )
    parser_update.add_argument(
        '--since',
        nargs=1,
        type=valid_date,
        help="Update all runs since the specified datetime"
    )
    parser_update.add_argument(
        '--before',
        nargs=1,
        type=valid_date,
        help="Update all runs before the specified datetime"
    )
    parser_update.add_argument(
        '--only-status',
        nargs=1,
        type=str,
        help="Update only those cases with the specified status"
    )
