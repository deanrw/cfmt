"""
Subparser options for info mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_make = subparsers.add_parser('make', help="Compile the code if required")
    parser_make.set_defaults(func="make")
    parser_make.add_argument(
        'directory',
        nargs='?',
        type=str,
        help="The directory of the case to run",
        default='.',
    )
    parser_make.add_argument(
        '--clean',
        action='store_true',
        help="Pass 'clean' to the build script",
    )
