"""
Subparser options for info mode
"""
from .parser import StatusAction
from ..entities.runs import Run

def add_parser(subparsers):
    """
    Adds options
    """
    parser_copy = subparsers.add_parser('copy', help="Copy cases or studies", aliases=['cp'])
    parser_copy.set_defaults(func="copy")
    parser_copy.add_argument(
        'source',
        nargs=1,
        metavar='SRC',
        type=str,
        help="The source path of the study or case to copy",
    )
    parser_copy.add_argument(
        'destination',
        nargs=1,
        metavar='DST',
        type=str,
        help="The destination path of the study or case to copy",
    )
    parser_copy.add_argument(
        '-a','--all',
        action='store_true',
        help="Include all runs and other data",
        default=False,
    )
    parser_copy.add_argument(
        '--with-post',
        action='store_true',
        help="Include contents of post folder",
        default=False,
    )
    parser_copy.add_argument(
        '--no-mesh',
        action='store_true',
        help="Do not include contents of mesh folder",
        default=False,
    )
    parser_copy.add_argument(
        '--with-history',
        action='store_true',
        help="Copy the .history folder",
        default=False,
    )
    parser_copy_opt_group = parser_copy.add_argument_group("Run selection")
    parser_copy_opt_group.add_argument(
        '--with-last',
        action='store_true',
        help="Include the last run",
        default=False,
        dest="last_run",
    )
    parser_copy_opt_group.add_argument(
        '--with-run',
        nargs=1,
        type=str,
        help="Include the specified run names",
        default=None,
        metavar='RUN_NAME',
        action='append',
        dest="run_names",
    )
    parser_copy_opt_group.add_argument(
        '--with-status',
        nargs=1,
        type=str,
        metavar="STATUS",
        help="Include runs with the specified status",
        action=StatusAction,
        choices=Run.valid_status_names,
        dest="run_status"
    )
