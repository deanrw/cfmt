"""
Subparser options for edit mode
"""
from ..codes import codes

def add_parser(subparsers):
    """
    Subparser options for edit mode

    This automates the editing of different CFD code files
    """
    parser_edit = subparsers.add_parser('edit', help="Edits run files")
    parser_edit.set_defaults(func="edit")
    parser_edit.add_argument(
        'file',
        nargs='?',
        type=str,
        help="File to edit",
        default=None,
    )
    parser_edit.add_argument(
        '--select',
        nargs=1,
        help='Run names selected for operations',
        type=str,
        default=None,
    )
    parser_edit.add_argument(
        '--case',
        action='store_true',
        help='Operate on case level files',
        default=False,
    )
    parser_edit.add_argument(
        '-L',
        '--local-only',
        action="store_true",
        help="Edit in local directory, rather than in staging",
        default=False,
    )
    # add code specific arguments
    for code in codes.AVAILABLE_CODES:
        codes.add_arguments(code, 'edit', parser_edit)
