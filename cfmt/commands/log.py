"""
Subparser options for log mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_log = subparsers.add_parser('log', help="View the log")
    parser_log.set_defaults(func="log")
    parser_log.add_argument(
        '-r', '--reverse',
        action='store_true',
        help="Show most recent entries first",
    )
    parser_log.add_argument(
        '-f', '--follow',
        action='store_true',
        help="Follow the log live",
    )
