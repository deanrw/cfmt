"""
Subparser options for utils mode
"""
from argparse import ArgumentParser
from ..utils import SUPPORTED_COMPRESSION

def add_parser(subparsers):
    """
    Adds options
    """
    parser_utils = subparsers.add_parser('utils', help="Run built in utilities on specified runs")
    parser_utils.set_defaults(func="utils")
    parser_common = ArgumentParser(add_help=False)
    parser_common.add_argument(
        '--select',
        nargs='*',
        help='Run to operate on',
        type=str,
        default=None,
    )
    parser_common.add_argument(
        '-L',
        '--local-only',
        action="store_true",
        help="Run in local directory, rather than in staging",
        default=False,
    )
    parser_common.add_argument(
        '-S',
        '--include-scratch',
        action="store_true",
        help="Run in scratch directory, even if run is not staged",
        default=False,
    )
    subparser_utils = parser_utils.add_subparsers(
        help="Utilities available",
        dest="util_choice",
        required=True
    )

    util_compress = subparser_utils.add_parser('compress',
                                          help="Compress runs",
                                          parents=[parser_common])
    util_compress.add_argument(
        '-d','--decompress',
        help="Decompress",
        action="store_true",
        default=False
    )
    util_compress.add_argument(
        '--stdout',
        help="Compress all previously uncompressed stdout files",
        action="store_true",
        default=False
    )
    util_compress.add_argument(
        '--stderr',
        help="Compress all previously uncompressed stderr files",
        action="store_true",
        default=False
    )
    util_compress.add_argument(
        '--logs',
        help="Compress all previously uncompressed log files",
        action="store_true",
        default=False
    )
    util_compress.add_argument(
        '-m', '--monitors',
        help="Compress all previously uncompressed monitor files",
        action="store_true",
        default=False
    )
    util_compress.add_argument(
        '-r', '--residuals',
        help="Compress all previously uncompressed residual files",
        action="store_true",
        default=False
    )
    util_compress.add_argument(
        '-a','--algorithm',
        help="Compression algorithm supported",
        default=None,
        choices=SUPPORTED_COMPRESSION
    )

    util_delete_post = subparser_utils.add_parser('delete-post',
                                                  help="Delete unused or reproducible post files",
                                                  parents=[parser_common])
    util_delete_post.add_argument(
        '--ens',
        help="Delete ensight data for STREAM runs.",
        action="store_true",
        default=False
    )
    util_delete_tout = subparser_utils.add_parser(
        'delete-tout',
        help="Delete timeoutput folders",
        parents=[parser_common]
    )
    util_delete_tout.add_argument(
        '-n','--dry-run',
        help="Dry run, only print actions",
        action="store_true",
        default=False
    )
    util_delete_tout.add_argument(
        '--scratch-only',
        help="Force",
        action="store_true",
        default=False
    )

    util_delete_uncompressed = subparser_utils.add_parser(
        'delete-uncompressed',
        help="Delete uncompressed files that have compressed duplicates",
        parents=[parser_common]
    )
    util_delete_uncompressed.add_argument(
        '--strict',
        help="Uncompresses the file in memory, and checks the hash",
        action="store_true",
        default=False
    )
    util_delete_uncompressed.add_argument(
        '-n','--dry-run',
        help="Dry run, only print actions",
        action="store_true",
        default=False
    )

def compress(run, args):
    """
    Compress the attached run
    """
    run.compress(stdout=args.stdout,
                 stderr=args.stderr,
                 monitors=args.monitors,
                 residuals=args.residuals,
                 logs=args.logs,
                 local_only=args.local_only,
                 algorithm=args.algorithm,
                 decompress=args.decompress)

def delete_post(run, args):
    """
    Delete/tidy post for the attached run
    """
    run.delete_post(ens=args.ens,
                    local_only=args.local_only)

def delete_tout(run, args):
    """
    Delete/tidy post for the attached run
    """
    run.delete_tout(local_only=args.local_only,
                    include_scratch=args.include_scratch)

def delete_uncompressed(run, args):
    """
    Delete/tidy post for the attached run
    """
    run.delete_uncompressed(strict=args.strict,
                            local_only=args.local_only,
                            dry_run=args.dry_run)