"""
Subparser options for info mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_upgrade = subparsers.add_parser('upgrade', help="Upgrade an existing database")
    parser_upgrade.set_defaults(func="upgrade")