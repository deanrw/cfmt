"""
Subparser options for init mode
"""
from ..codes import codes

def add_parser(subparsers):
    """
    Subparser options for init mode

    This initialises a folder for management by cfmt
    """
    parser_init = subparsers.add_parser('init', help="For initializing a new study")
    parser_init.set_defaults(func="init")
    parser_init.add_argument(
        '--name',
        nargs='?',
        type=str,
        help="Name of the study. Defaults to directory",
        default=None,
    )
    parser_init.add_argument(
        '--empty',
        action='store_true',
        help="Do not create any directories",
    )
    parser_init.add_argument(
        '--description',
        nargs='?',
        type=str,
        help="Description of the study.",
        default="",
    )
    parser_init.add_argument(
        '--project-name',
        nargs='?',
        type=str,
        help="Name of the project which the study is part of",
        default="",
    )
    parser_init.add_argument(
        '--case',
        nargs='+',
        type=str,
        help="Cases to generate at initialization",
        default=[],
    )
    parser_init.add_argument(
        '--code',
        nargs='?',
        type=str,
        help="The codes to initialize with the study.",
        choices=codes.AVAILABLE_CODES,
        action='append',
        default=None,
    )
    # add code specific arguments
    for code in codes.AVAILABLE_CODES:
        codes.add_arguments(code, 'init', parser_init)

    parser_init_group = parser_init.add_mutually_exclusive_group()
    parser_init_group.add_argument(
        '--system',
        action='store_true',
        help="Initialize system wide",
    )
    parser_init_group.add_argument(
        '--user',
        action='store_true',
        help="Initialize for current user",
    )
    parser_init_group.add_argument(
        'directory',
        nargs='?',
        type=str,
        help="Directory in which to create study",
        default=".",
    )
