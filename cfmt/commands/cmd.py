"""
Subparser options for info mode
"""
from ..codes import codes

def add_parser(subparsers):
    """
    Adds options
    """
    parser_cmd = subparsers.add_parser('cmd', help="Run commands in a particular run directory")
    parser_cmd.set_defaults(func="cmd")
    parser_cmd.add_argument(
        '--select',
        nargs='*',
        help='Run to operate on',
        type=str,
        default=None,
    )
    parser_cmd.add_argument(
        '-L',
        '--local-only',
        action="store_true",
        help="Run in local directory, rather than in staging",
        default=False,
    )
    parser_cmd.add_argument(
        '-C',
        '--cwd',
        nargs=1,
        help="Change to directory before operating",
        type=str,
        default=None,
    )
    parser_cmd.add_argument(
        'application',
        nargs='*',
        type=str,
        help="The command to run. Arguments are passed.",
        default=None,
    )
    # add code specific arguments
    for code in codes.AVAILABLE_CODES:
        codes.add_arguments(code, 'cmd', parser_cmd)
