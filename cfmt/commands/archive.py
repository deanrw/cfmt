"""
Subparser options for archive mode
"""
from ..entities.archives import AVAILABLE_ARCHIVE_TYPES

def add_parser(subparsers):
    """
    Adds options
    """
    parser_archive = subparsers.add_parser('archive', help="Archive runs")
    parser_archive.set_defaults(func="archive")

    parser_archive_runops = parser_archive.add_argument_group("Run operations")
    parser_archive_runops.add_argument(
        '-c','--create',
        nargs="*",
        metavar="RUNS",
        default=None,
        help="Archive the selected runs"
    )
    parser_archive_runops.add_argument(
        '--withdraw',
        nargs="*",
        metavar="RUNS",
        default=None,
        help="Retrive the marked archive of the selected run, and restore the run state."
    )
    parser_archive_runops.add_argument(
        '--deposit',
        nargs="*",
        metavar="RUNS",
        default=None,
        help="Archive the selected runs, and mark them as archived."
    )

    parser_archive_aops = parser_archive.add_argument_group("Archive operations")
    parser_archive_aops.add_argument(
        '--download',
        nargs="*",
        default=None,
        metavar="RUN|IDs,",
        help="Download all archives with ID or associated with RUN"
    )
    parser_archive_aops.add_argument(
        '-t','--list',
        nargs=1,
        default=None,
        dest="list_contents",
        metavar="ID",
        help="List contents of archive ID"
    )
    parser_archive_aops.add_argument(
        '--delete',
        nargs="*",
        default=None,
        metavar="RUN|ID",
        help="Delete all archives with ID or associated with RUN"
    )
    parser_archive_aops.add_argument(
        '--verify',
        nargs="*",
        default=None,
        metavar="RUN|ID",
        help="Verify all archives with ID or associated with RUN"
    )
    parser_archive_aops.add_argument(
        '--restore',
        nargs=1,
        default=None,
        metavar="ID",
        help="Restore archive with ID to run"
    )

    parser_archive.add_argument(
        '--init',
        action="store_true",
        default=None,
        help="Initialize an archive repository"
    )

    parser_archive.add_argument(
        '-z','--compress',
        action="store_true",
        default=True,
        help="Additionally compress the archive"
    )
    parser_archive.add_argument(
        '--type',
        nargs=1,
        default=None,
        choices=AVAILABLE_ARCHIVE_TYPES,
        help="Type of archive to create"
    )
    parser_archive.add_argument(
        '--keep-source',
        action="store_true",
        default=False,
        help="Keep source files after deposit"
    )
    parser_archive.add_argument(
        '--keep-archive',
        action="store_true",
        default=False,
        help="Keep archive after withdrawing"
    )
    parser_archive.add_argument(
        '--checksum',
        action="store_true",
        default=False,
        help="Compute the checksum when creating"
    )
    parser_archive.add_argument(
        '--all',
        action="store_true",
        default=False,
        help="Select all runs in the study"
    )
    parser_archive.add_argument(
        '--force',
        action="store_true",
        default=None,
        help="Force archive if runs are marked as in staging"
    )
    parser_archive.add_argument(
        '--description',
        type=str,
        nargs=1,
        metavar="DESC",
        help="A description to add when creating/depositing",
        default=None
    )
