"""
Subparser options for export mode
"""
from .parser import StatusAction
from ..entities.runs import Run
from ..entities.studies import Study

def add_parser(subparsers):
    """
    Adds options
    """
    parser_export = subparsers.add_parser('export', help="export output")
    parser_export.set_defaults(func="export")

    parser_export.add_argument(
        '--format',
        nargs=1,
        type=str,
        choices=["json", "xml"],
        default=["json"],
        help="Specific the output format."
    )
    parser_export_run = parser_export.add_argument_group("Run fields")
    parser_export_run.add_argument(
        '--add-db-field',
        nargs=1,
        type=str,
        action='append',
        choices=Run.db_fields,
        help="Specify additional database fields to add to the output."
    )
    parser_export_run.add_argument(
        '--add-field',
        nargs=1,
        type=str,
        action='append',
        metavar="FIELD",
        help="Specify additional empty fields to add to the output."
    )
    parser_export_run.add_argument(
        '--add-field-value',
        nargs=2,
        type=str,
        action='append',
        metavar=("FIELD","VALUE"),
        help="Specify additional field - value pairs to add to the output."
    )
    parser_export_study = parser_export.add_argument_group("Study fields")
    parser_export_study.add_argument(
        '--add-study-db-field',
        nargs=1,
        type=str,
        action='append',
        choices=Study.db_fields,
        help="Specify additional study database fields to add",
    )
    parser_export_study.add_argument(
        '--add-study-field',
        nargs=1,
        type=str,
        action='append',
        metavar="FIELD",
        help="Specify additional empty study fields to add to the output."
    )
    parser_export_study.add_argument(
        '--add-study-field-value',
        nargs=2,
        type=str,
        action='append',
        metavar=("FIELD","VALUE"),
        help="Specify additional study field - value pairs to add to the output."
    )

    parser_export.add_argument(
        '--append',
        nargs=1,
        type=str,
        help="Append/modifies an existing file rather than output to stdout"
    )
    parser_export.add_argument(
        'item',
        nargs='+',
        help="Case or runs to include.",
        default=None,
        metavar="ITEM"
    )
    parser_export.add_argument(
        '--status',
        nargs=1,
        action=StatusAction,
        type=str,
        help="Select all runs with this status",
        default=None,
        choices=Run.valid_status_names
    )
