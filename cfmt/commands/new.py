"""
Subparser options for info mode
"""
from ..codes import codes

def add_parser(subparsers):
    """
    Adds options
    """
    parser_new = subparsers.add_parser('new', help="Creates a new case")
    parser_new.set_defaults(func="new")
    parser_new.add_argument(
        'directory',
        nargs='+',
        type=str,
        help="Cases to generate",
        default=".",
    )
    parser_new.add_argument(
        '--name',
        nargs='+',
        type=str,
        help="Case names to associate",
        default=None,
    )
    parser_new.add_argument(
        '--code',
        nargs=1,
        type=str,
        help="The code to use in the case",
        choices=codes.AVAILABLE_CODES,
        default=None,
    )
