"""
Subparser options for srcdiff mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_diff = subparsers.add_parser('srcdiff', help="View a diff between two runs or jobs")
    parser_diff.set_defaults(func="srcdiff")

    parser_diff.add_argument(
        '--select',
        nargs='+',
        type=str,
        help="The runs to pass to the module",
        default=None,
    )
    parser_diff.add_argument(
        '--difftool','--tool',
        nargs=1,
        type=str,
        help="The tool to use",
        default=None,
    )
    parser_diff.add_argument(
        'args',
        nargs='*'
    )
