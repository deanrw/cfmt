"""
Subparser options for info mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_move = subparsers.add_parser('move', help="Move cases or studies", aliases=['mv'])
    parser_move.set_defaults(func="move")
    parser_move.add_argument(
        'source',
        nargs=1,
        metavar='SRC',
        type=str,
        help="The source path of the study or case to move",
    )
    parser_move.add_argument(
        'destination',
        nargs=1,
        metavar='DST',
        type=str,
        help="The destination path of the study or case to move",
    )
    parser_move.add_argument(
        '--name',
        nargs=1,
        type=str,
        help="The new case name, if different from DST"
    )
    parser_move.add_argument(
        '--no-copy-files',
        help="Do not copy files over.",
        action="store_true",
        default=False,
    )
