"""
Subparser options for note mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_note = subparsers.add_parser('note', help="Add, view or delete notes")
    parser_note.set_defaults(func="note")
    parser_note.add_argument(
        '-a', '--add',
        nargs='?',
        help="Add note",
        default=False,
        const=True,
    )
    parser_note.add_argument(
        '-e', '--edit',
        nargs=1,
        type=int,
        help="Edit note",
        default=None,
    )
    parser_note.add_argument(
        '-c', '--case',
        nargs=1,
        help="Case name to add note to",
        default=None
    )
    parser_note.add_argument(
        '-d', '--delete',
        nargs='+',
        help="Note ids to delete",
        default=None,
    )
    parser_note.add_argument(
        '-y', '--yes',
        action='store_false',
        dest='prompt',
        help="Say yes to all prompts",
        default=True
    )
    parser_note.add_argument(
        '--editor',
        nargs=1,
        type=str,
        help="Path to editor to use",
        default=None
    )
