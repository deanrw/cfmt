"""
Subparser options for run mode
"""
from ..codes import codes

def add_parser(subparsers, parents=None):
    """
    Adds options
    """
    parser_run = subparsers.add_parser('run', help="Runs a case",
                                       parents=parents)
    parser_run.set_defaults(func="run")
    parser_run.add_argument(
        'directory',
        nargs='?',
        type=str,
        help="The directory of the case to run",
        default=['.'],
    )
    parser_run.add_argument(
        '--select',
        nargs='+',
        help='Run names selected for operations',
        type=str,
        default=None,
    )
    parser_run.add_argument(
        '--with-gui',
        action='store_true',
        help="Run the code with a gui",
        default=False,
    )
    parser_run.add_argument(
        '-g',
        '--without-gui',
        action='store_true',
        help="Run the code without a gui",
        default=False,
    )
    parser_run.add_argument(
        '-c', '--continue',
        action='store_true',
        help="Continue the last run, implies --no-build. Pass --rebuild to force a rebuild",
        dest="continue_calc",
        default=False
    )
    parser_run.add_argument(
        '-e', '--edit',
        action='store_true',
        help="Edit the run input file/script before submission",
        dest="edit_input",
        default=False
    )
    parser_run.add_argument(
        '--edit-run-command',
        action='store_true',
        help="Edit the run command before submission",
        dest="edit_command",
        default=False
    )
    parser_run.add_argument(
        '--dev',
        action='store_true',
        help=("Run the code in development mode. Doesn't commit changes to code. "
              "Implies --local, --no-staging"),
        dest="run_dev",
        default=False
    )
    parser_run.add_argument(
        '--print-command',
        action='store_true',
        help="Do not run the job, just print the generate run command",
        default=False
    )
    parser_run.add_argument(
        '--reason',
        nargs=1,
        help="The reason for doing this run",
        default=None
    )
    parser_run.add_argument(
        '-t',
        '--tag',
        nargs=1,
        type=str,
        help="A tag to associate with the run",
        default=None,
        action='append'
    )
    parser_run.add_argument(
        '-n', '--no-submit',
        action='store_true',
        help="Prepare for the run but don't actually submit it",
        default=None
    )
    parser_run.add_argument(
        '--clear-batch-extra',
        action='store_true',
        help="When continuing, clear any previous batch options",
        default=False
    )
    # Initialization options
    parser_run_init = parser_run.add_argument_group("Initialization options")
    parser_run_init.add_argument(
        '--init-from',
        nargs=1,
        help="The run to initialize results from",
        default=None,
        type=str,
    )
    parser_run_init.add_argument(
        '--init-from-home',
        help="Force initialize from the run's home directory rather than scratch",
        action="store_true",
        default=False,
    )
    parser_run_init.add_argument(
        '--init-to-scratch',
        help="Copy runs directly to the new run scratch directory",
        action="store_true",
        default=False,
    )
    parser_run_init.add_argument(
        '--keep-residuals',
        action='store_true',
        help="Keep residuals when initializing",
        default=False,
    )
    parser_run_init.add_argument(
        '--keep-monitors',
        action='store_true',
        help="Keep monitors when initializing",
        default=False,
    )
    parser_run_init.add_argument(
        '--keep-input',
        action='store_true',
        help="Keep run input when initializing. Does not copy case input.",
        default=False,
    )
    parser_run_init.add_argument(
        '--keep-dir',
        type=str,
        nargs=1,
        action="append",
        metavar="DIR",
        help="Copy directory DIR from the src run folder. Path should be relative to the src.",
        default=None,
    )
    parser_run_init.add_argument(
        '--continue-tout',
        action='store_true',
        help="Continue timeoutput",
        default=False,
    )

    # Staging
    parser_run_staging = parser_run.add_argument_group("Staging options")
    parser_run_staging.add_argument(
        '--staging',
        action='store_true',
        help="Stage files before running. Settings are taken from config or command line",
        default=None
    )
    parser_run_staging.add_argument(
        '--no-staging',
        action='store_true',
        help="Do not stage files. Runs the case in the current directory",
        default=None
    )
    parser_run_staging.add_argument(
        '--staging-mode',
        nargs=1,
        type=str,
        help="The mode to use for staging",
        choices=['share', 'remote'],
        default=None
    )
    parser_run_staging.add_argument(
        '--transfer-mode',
        nargs=1,
        type=str,
        help="The mode to use for transferring files",
        choices=['os', 'scp', 'rsync'],
        default=None
    )
    parser_run_staging.add_argument(
        '--staging-root',
        nargs=1,
        type=str,
        help="The root directory to use for staging. This must already exist",
        default=None
    )

    # Build options
    parser_run_build = parser_run.add_argument_group("Build options")
    parser_run_build.add_argument(
        '--no-build',
        action="store_true",
        help="Do not rebuild any code sources",
        dest="no_build",
        default=False
    )
    parser_run_build.add_argument(
        '--rebuild',
        action="store_true",
        help="Rebuild or build the code source",
        dest="rebuild",
        default=False
    )

    # System options
    parser_run_system = parser_run.add_argument_group("System options")
    parser_run_system.add_argument(
        '--remote',
        action='store_true',
        help="Run the case on a remote system",
        default=None,
    )
    parser_run_system.add_argument(
        '--local',
        action='store_true',
        help="Run the case on the local system",
        default=None,
    )

    # Parallel options
    parser_run_mode = parser_run.add_argument_group("Parallel options")
    parser_run_mode.add_argument(
        '-p',
        '--parallel',
        action='store_true',
        dest="parallel",
        help="Run the case in parallel",
        default=None,
    )
    parser_run_mode.add_argument(
        '-s',
        '--serial',
        action='store_false',
        dest="parallel",
        help="Run the case in serial",
        default=None,
    )
    parser_run_mode.add_argument(
        '-np',
        '--slots',
        nargs=1,
        dest="np",
        type=int,
        help="The number of slot\'s to use",
        default=None,
    )
    parser_run_mode.add_argument(
        '--parallel-exec',
        nargs=1,
        type=str,
        help="Specify the executable for launching parallel jobs (i.e. mpiexec)",
        default=None
    )
    # add code specific arguments
    for code in codes.AVAILABLE_CODES:
        codes.add_arguments(code, 'run', parser_run)
