"""
Custom parser actions and checks
"""
from argparse import ArgumentTypeError, Action
from datetime import datetime
from ..entities.runs import Run

def valid_date(datestring):
    """ Checks for a valid datetime string """
    valid_formats = [
        '%Y-%m-%d %H:%M:%S'
    ]
    for date_fmt in valid_formats:
        try:
            return datetime.strptime(datestring, date_fmt)
        except ValueError as exc:
            msg = f"Not a valid date: '{datestring}'\n"
            msg += "Valid formats: \n"
            msg += "\n".join(valid_formats)
            raise ArgumentTypeError(msg) from exc

def str2bool(v):
    """ Returns a boolean based on common strings """
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise ArgumentTypeError("Boolean value expected.")
def str2int(v):
    """ Returns an int based on common strings """
    if v.lower() in ('false', 'f', '0'):
        return 0
    else:
        try:
            i = int(v)
        except ValueError as exc:
            raise ArgumentTypeError("Integer value expected") from exc
        return i

class StatusAction(Action):
    """ Custom action for returning status numbers """
    def __call__(self, parser, args, values, option_string=None):
        # convert to status number
        status_numbers = []
        for status in values:
            level = Run._name_to_level[status]
            status_numbers.append(level)
        setattr(args, self.dest, status_numbers)
