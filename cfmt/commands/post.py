"""
Subparser options for post mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_post = subparsers.add_parser('post', help="Run post-processing modules")
    parser_post.set_defaults(func="post")

    parser_post.add_argument(
        '-m',
        '--module',
        type=str,
        nargs=1,
        metavar="MOD",
        help="Path to a python module file to import",
        required=True,
    )
    parser_post.add_argument(
        '--select',
        nargs='+',
        type=str,
        help="The runs to pass to the module",
        default=None,
    )
    parser_post.add_argument(
        '-f',
        nargs=1,
        type=str,
        dest="user_func",
        help="The function to run",
        default=None,
    )
    parser_post.add_argument(
        'args',
        nargs='*'
    )
