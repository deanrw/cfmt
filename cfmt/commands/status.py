"""
Subparser options for info mode
"""
from ..entities.runs import Run
from .parser import str2bool

def add_parser(subparsers):
    """
    Adds options
    """
    parser_status = subparsers.add_parser('status', help="Status of the study")
    parser_status.set_defaults(func="status")
    parser_status_choice = parser_status.add_mutually_exclusive_group()
    parser_status.add_argument(
        'directory',
        nargs='?',
        type=str,
        help="Print status of cases chosen by path",
        default=None,
    )
    parser_status_choice.add_argument(
        '-S','--study',
        action='store_true',
        help="Print status of the study",
        default=None,
    )
    parser_status_choice.add_argument(
        '-C','--case-names',
        nargs='+',
        type=str,
        help="Print status of cases chosen by name",
        default=None
    )
    parser_status_choice.add_argument(
        '-J', '--jobs',
        nargs='+',
        type=int,
        help="Print status of runs chosen by job number",
        default=None
    )
    parser_status_choice.add_argument(
        '--recent',
        action='store_true',
        help="Print status of runs, most recent first",
        default=False,
    )
    parser_status.add_argument(
        '-d','--dates',
        action='store_true',
        help="Print submitted, start and finish dates",
        default=False,
    )
    parser_status.add_argument(
        '--sizes',
        action='store_true',
        help="Print sizes of case folders",
        default=False,
    )
    parser_status.add_argument(
        '-n', '--notes',
        action='store_true',
        dest="notes",
        help="Print last two notes with cases",
        default=None,
    )
    parser_status.add_argument(
        '-N', '--no-notes',
        action='store_false',
        dest="notes",
        help="Do not print any notes with cases",
        default=None,
    )
    parser_status.add_argument(
        '--detailed',
        action='store_true',
        help="Print more detail",
        default=False,
        dest="details",
    )
    parser_status.add_argument(
        '--machine-readable',
        action='store_true',
        help="Print only matching run names",
        default=False,
        dest="machine_readable",
    )
    parser_status.add_argument(
        '--stats',
        action='store_true',
        help="Print run statistics",
        default=False,
    )
    parser_status.add_argument(
        '--src-ref',
        action='store_true',
        help="Print src references to case history",
        default=False,
    )
    parser_status.add_argument(
        '-j','--list-jobs',
        action='store_true',
        help="List all jobs associated with runs",
        default=False,
    )
    parser_status.add_argument(
        '--job-type',
        nargs=1,
        help="Only show jobs of type",
        metavar="TYPE",
        default=None,
    )
    parser_status.add_argument(
        '-a', '--list-archives',
        action='store_true',
        help="List all archives associated with runs",
        default=False,
    )
    parser_status.add_argument(
        '--with-dev',
        action='store_true',
        help="Include dev runs in results",
        default=False,
    )
    parser_status.add_argument(
        '--with-deleted',
        action='store_true',
        help="Include deleted runs in results",
        default=False,
    )
    parser_status.add_argument(
        '-t',
        '--tags',
        nargs='+',
        metavar="TAG",
        help="Only list runs with the given tags",
        default=None,
        dest="with_tags"
    )
    parser_status.add_argument(
        '--with-status',
        nargs='+',
        metavar="STATUS",
        help="Only list runs with the given status",
        choices=Run.valid_status_names,
        default=None,
    )
    parser_status.add_argument(
        '--limit',
        type=int,
        help="Limit results to most recent x results",
        metavar='N',
        default=10,
    )
    parser_status.add_argument(
        '--with-staging',
        type=str2bool,
        metavar="STAGING",
        help="Only list runs which have staging status STAGING",
        default=None,
    )
    parser_status.add_argument(
        '-l','--local',
        action="store_true",
        help="Only show results for current submit host",
        default=False,
    )
