"""
Subparser options for info mode
"""
def add_parser(subparsers):
    """
    Adds options
    """
    parser_info = subparsers.add_parser('info', help="Information about the current directory")
    parser_info.set_defaults(func="info")
    parser_info.add_argument(
        'directory',
        nargs='?',
        type=str,
        help="Directory to request information about",
        default=".",
    )
    parser_info.add_argument(
        '--select',
        nargs='+',
        help='Run names selected for operations',
        type=str,
        default=None,
    )
