"""
    cft: A management tool for Computational Fluid Dynamics

    plotting.py: Functions for plotting
"""
import csv
import time
import collections
import math
import os, sys
import signal, socket
from itertools import cycle
from colour import Color, color_scale
from PyQt5 import QtNetwork, QtGui, QtCore, QtWidgets
import pyqtgraph as pg
#from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
import pyqtgraph.dockarea as dock

from cfmt import utils

class FileSequencePlot(QtWidgets.QMainWindow):
    """
    An application that plots a sequence of files, allowing play/plause functionality
    """
    def __init__(self, reader):
        """
        Initialize

        (array) flist - The list of files to load

        """
        self.app = QtWidgets.QApplication([])

        super().__init__()

        #for path in QtGui.QIcon.themeSearchPaths():
        #    print (path, QtGui.QIcon.themeName())

        # set an interval (in ms)
        self._interval = 1000
        
        # QT timer
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.timer_fire)

        # store the reader
        self.reader = reader

        # default play direction
        self.play_forward = True

        # Setup the toolbar
        tb = self.addToolBar("File")

        tb_first_frame = QtWidgets.QAction(QtGui.QIcon.fromTheme('media-skip-backward'), "first-frame", self)
        tb_first_frame.setStatusTip("First frame")
        tb_first_frame.triggered.connect(self.first_frame)
        tb.addAction(tb_first_frame)
        
        tb_frame_back = QtWidgets.QAction(QtGui.QIcon.fromTheme('media-seek-backward'), "frame-back", self)
        tb_frame_back.setStatusTip("Frame back")
        tb_frame_back.triggered.connect(self.frame_back)
        tb.addAction(tb_frame_back)
        
        tb_reverse = QtWidgets.QAction(QtGui.QIcon.fromTheme('media-playback-start-rtl'), "reverse", self)
        tb_reverse.setStatusTip("Start playback in reverse")
        tb_reverse.triggered.connect(self.reverse)
        self.tb_reverse = tb_reverse
        #tb_start.setEnabled(False)
        tb.addAction(tb_reverse)

        tb_play = QtWidgets.QAction(QtGui.QIcon.fromTheme('media-playback-start'), "play", self)
        tb_play.setStatusTip("Start playback")
        tb_play.triggered.connect(self.play)
        self.tb_play = tb_play
        #tb_start.setEnabled(False)
        tb.addAction(tb_play)
        
        #tb_pause = QtGui.QAction(QtGui.QIcon.fromTheme('media-playback-pause'), "pause", self)
        #tb_pause.setStatusTip("Pause playback")
        #tb.addAction(tb_pause)
        
        
        tb_frame_forward = QtWidgets.QAction(QtGui.QIcon.fromTheme('media-seek-forward'), "frame-forward", self)
        tb_frame_forward.setStatusTip("Frame forward")
        tb_frame_forward.triggered.connect(self.frame_forward)
        tb.addAction(tb_frame_forward)

        tb_last_frame = QtWidgets.QAction(QtGui.QIcon.fromTheme('media-skip-forward'), "last-frame", self)
        tb_last_frame.setStatusTip("Last frame")
        tb_last_frame.triggered.connect(self.last_frame)
        tb.addAction(tb_last_frame)
        

        tb.addSeparator()
        # set interval
        tb_nt = QtWidgets.QWidget(parent=self)
        nt_layout = QtWidgets.QHBoxLayout()

        # time label
        label = QtWidgets.QLabel('Time:')
        label.setFixedWidth(40)
        nt_layout.addWidget(label)

        # time viewport
        el = QtWidgets.QLineEdit(parent=tb_nt)
        el.setAlignment(QtCore.Qt.AlignLeft)
        el.setValidator(QtGui.QDoubleValidator(0.0, 1.0E12, 4))
        el.setText("0.0")
        el.setFixedWidth(150)
        nt_layout.addWidget(el)

        label = QtWidgets.QLabel('NT: ')
        label.setFixedWidth(20)
        nt_layout.addWidget(label)
        
        # integer box to hold 
        nt_input_box = QtWidgets.QSpinBox(parent=tb_nt)
        nt_input_box.setMinimum(0)
        nt_input_box.setMaximum(100)
        nt_input_box.setValue(0)
        nt_input_box.valueChanged.connect(self.nt_input_changed)
        nt_input_box.setFixedWidth(75)
        nt_layout.addWidget(nt_input_box)
        self.nt_input_box = nt_input_box
        
        tb_nt.setLayout(nt_layout)
        tb_nt.setFixedWidth(350)
        tb.addWidget(tb_nt)


        # create the liveplot app
        plot = SequencePlot(
            reader=reader, 
            live=False, 
            terminate=reader.terminate,
            title="TEST",
            xlabel="",
            ylabel="",
        )
        plot.create(self)
        plotWidget = plot.widget

        plotWidget.setAlignment(QtCore.Qt.AlignCenter)
        self.widget = plotWidget
        self.plot = plot
        self.setCentralWidget(self.widget)
        
        # status bar
        self.status_bar = QtWidgets.QStatusBar()
        self.setStatusBar(self.status_bar)
        
        self.setGeometry(100,100,1200,800)
        self.setWindowTitle("CFMT File Sequence Plotter")

        
    def timer_fire(self):
        if self.play_forward:
            self.next_frame()
        else:
            self.previous_frame()

    def update(self):
        """ Update all the elements """
        #self.status_bar.showMessage("Updating...")

        self.plot.update()
        self.update_toolbar()
        #self.status_bar.clearMessage()
        #self.status_bar.showMessage("Running")
        #self.app.processEvents()
    
    def update_toolbar(self, max_nt=None):
        """ Updates the toolbar """
        # get the latest nt values
        if max_nt is not None:
            self.nt_input_box.setMaximum(max_nt)

        nt = self.reader.fint
        self.nt_input_box.setValue(nt)

    def nt_input_changed(self):
        """ Fires when the nt input box changes """
        nt = self.nt_input_box.value()
        if nt > self.reader.fint:
            self.frame_forward()
        elif nt < self.reader.fint:
            self.frame_back()

    def frame_back(self, checked=False):
        """ Reverse frame """
        self.update_play_icons()
        
        self.timer.stop()
        self.previous_frame()
    
    def frame_forward(self, checked=False):
        """ Forward frame """
        self.update_play_icons()
        
        self.timer.stop()
        self.next_frame()

    def next_frame(self):
        """ Advance to the next frame """
        try:
            self.reader.forward()
            self.update()
        except StopIteration:
            self.update_play_icons()
            self.timer.stop()

    def previous_frame(self):
        """ Advance to the previous frame """
        try:
            self.reader.previous()
            self.update()
        except StopIteration:
            self.update_play_icons()
            self.timer.stop()

    def first_frame(self, checked=False):
        """ Load first frame """
        self.update_play_icons()
        
        self.reader.first_file()
        self.update()

    def last_frame(self, checked=False):
        """ Last frame """
        self.update_play_icons()
        
        self.reader.last_file()
        self.update()
    
    def stop(self, checked=False):
        """ Stops live capture """
        # stop the timer
        self.status_bar.showMessage("Stopping...")
        self.timer.stop()
        
        #self.tb_start.setEnabled(True)
        #self.tb_stop.setEnabled(False)
        self.status_bar.showMessage("Stopped")

    def update_play_icons(self):
        """
        Updates the play icons
        """
        if self.play_forward:
            self.tb_play.setIcon(QtGui.QIcon.fromTheme('media-playback-start'))
        else:
            self.tb_reverse.setIcon(QtGui.QIcon.fromTheme('media-playback-start-rtl'))

    def play(self, checked=False):
        """ 
        Toggle for forward playback
        """
        if self.timer.isActive():
            # change the icon back to play
            if not self.play_forward:
                self.tb_reverse.setIcon(QtGui.QIcon.fromTheme('media-playback-start-rtl'))
                self.play_forward = True
                self.tb_play.setIcon(QtGui.QIcon.fromTheme('media-playback-pause'))
            else:
                self.tb_play.setIcon(QtGui.QIcon.fromTheme('media-playback-start'))
                self.timer.stop()
        else:
            self.tb_play.setIcon(QtGui.QIcon.fromTheme('media-playback-pause'))
            self.play_forward = True
            self.timer.start()
        
        #self.tb_start.setEnabled(False)
        #self.tb_stop.setEnabled(True)
    def reverse(self, checked=False):
        """ Plays in reverse """
        if self.timer.isActive():
            # change the icon back to play
            if self.play_forward:
                self.tb_play.setIcon(QtGui.QIcon.fromTheme('media-playback-start'))
                self.play_forward = False
                self.tb_reverse.setIcon(QtGui.QIcon.fromTheme('media-playback-pause'))
            else: 
                self.tb_reverse.setIcon(QtGui.QIcon.fromTheme('media-playback-start-rtl'))
                self.timer.stop()
        else:
            self.tb_reverse.setIcon(QtGui.QIcon.fromTheme('media-playback-pause'))
            self.play_forward = False
            self.timer.start()
    
    def clean_up(self):
        """ Clean up on exit """
        # tell updatables we're going to kill
        self.plot.clean_up()

    def run(self):
        """ Run the app """
        # initialize the plot
        self.plot.init()

        # release the signals
        self.widget.blockSignals(False)
        
        # set the app up
        self.show()
        
        # start the timer 
        #self.timer.start(self._interval)
        #self.stop()
        # update the time and nt values
        max_nt = self.reader.get_max_fnumber()
        self.update_toolbar(max_nt=max_nt) 
        
        self.status_bar.showMessage("Ready")

        self.app.aboutToQuit.connect(self.clean_up) 
        self.app.exec_()
        self.app.deleteLater()
        self.app.quit()

    def quit(self):
        """ Exit """
        self.app.quit()


class PlotComparisonApp(QtWidgets.QMainWindow):
    """ """
    def __init__(self, runs, fname, live=False, local_only=False):

        self.app = QtWidgets.QApplication([])

        super().__init__()
        
        # create the readers 
        readers = []
        for r in runs:
            fpath = os.path.join(r.get_run_dir(local_only=local_only), fname)
            reader =  r.code.plot_reader(fpath)
            readers.append(reader)

        # add the top toolbar
        tb = self.addToolBar("Runs")

        run_list = QtWidgets.QWidget(parent=self)
        layout = QtWidgets.QGridLayout()
        layout.setSpacing(10)

        for i, r in enumerate(runs):
            label = QtWidgets.QLabel(f"Run: {str(i+1)}", parent=run_list)
            run_name = QtWidgets.QLineEdit(parent=run_list)
            run_name.setAlignment(QtCore.Qt.AlignLeft)
            run_name.setText(r.name + " " + '"' + r.reason + '"')
            run_name.setReadOnly(True)
            layout.addWidget(label, i, 0)
            layout.addWidget(run_name, i, 1)

        run_list.setLayout(layout)
        bg_color = run_list.palette().color(QtGui.QPalette.Background)
        tb.addWidget(run_list)

        # create the liveplot
        plot = ComparisonPlot(
            readers,
            live=live,
        )
        plot.create(self)
        plotWidget = plot.widget
        self.plot = plot
        self.widget = plotWidget

        plotWidget.setAlignment(QtCore.Qt.AlignCenter)
        self.setCentralWidget(plotWidget)

        self.setGeometry(100,100,1200,800)
        self.setWindowTitle("CFMT Run Plot Comparison")

        # add the left toolbar
        tb_left = QtWidgets.QToolBar("Series", parent=self)
        tb_left.setAutoFillBackground(True)
        tb_left.setStyleSheet(r"QToolBar {border: none; background : " + bg_color.name() + r";}")
        self.addToolBar(QtCore.Qt.LeftToolBarArea, tb_left)
        series_tree = QtWidgets.QTreeWidget(self)
        series_tree.setColumnCount(1)
        series_tree.setHeaderLabels(["Series"])
        series_tree.header().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        series_tree.header().setStretchLastSection(False)
        series_tree.setAutoFillBackground(True)
        #w.setPalette(p)
        series_tree.setStyleSheet(r"QTreeWidget {background : " + bg_color.name() + r";}")
        #color = w.palette().color(w.backgroundRole())

        tb_left.addWidget(series_tree)
        self.series_tree = series_tree

        self.runs = runs


    def clean_up(self):
        self.plot.clean_up()

    def run(self):
        """ Runs the app """

        # initialize the plot
        self.plot.init()

        # populate the series widget
        self.createSeriesTree(self.plot, self.series_tree) 

        # connect the plot series toggle to the tree

        # release the signals
        self.widget.blockSignals(False)

        # setup the app
        self.show()

        #self.app.aboutToQuit.connect(self.clean_up)
        self.app.exec_()
        #self.app.deleteLater()
        #self.app.quit()

    def createSeriesTree(self, plot, tree):
        """
        Given a LivePlot, popular the series tool bar
        """
        readers = plot.data_readers
        runs = self.runs
        curveSelectMenus = plot.curveSelectMenus
        
        i = 1
        for reader, run, curveSelectMenu in zip(readers, runs, curveSelectMenus):
            parent = QtWidgets.QTreeWidgetItem(tree)
            parent.setText(0, f"Run {i}")
            parent.setFlags(parent.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)
            i += 1
            for action in curveSelectMenu.actions():
                child = QtWidgets.QTreeWidgetItem(parent)
                child.setFlags(child.flags() | QtCore.Qt.ItemIsUserCheckable)
                child.setText(0, action.text())
                if action.isChecked():
                    child.setCheckState(0, QtCore.Qt.Checked)
                else:
                    child.setCheckState(0, QtCore.Qt.Unchecked)

                # add a new connect
                action.triggered.connect(self.toggleSeries)
                
                # store the action
                child.menuAction = action
                # store the treeItem
                action.treeItem = child
            tree.expandItem(parent)
        
        tree.itemChanged.connect(self.treeClicked)

    @QtCore.pyqtSlot(QtWidgets.QTreeWidgetItem, int)
    def treeClicked(self, it, col):
        Checked = QtCore.Qt.Checked
        Unchecked = QtCore.Qt.Unchecked
        PartiallyChecked = QtCore.Qt.PartiallyChecked
        
        # trigger the action
        if it.childCount() > 0:
            parentState = it.checkState(col)

            if parentState == PartiallyChecked:
                return
            # trigger actions on all the children
            for i in range(it.childCount()):
                child = it.child(i)
                action = child.menuAction
                childState = Checked if action.isChecked() else Unchecked
                if parentState != childState:
                    child.menuAction.trigger()
        else:
            it.menuAction.trigger()

    def toggleSeries(self, it):
        """ Fires when a series changes """
        Checked = QtCore.Qt.Checked
        Unchecked = QtCore.Qt.Unchecked
        
        sender = self.sender()
        if isinstance(sender, QtWidgets.QAction):
            treeItem = sender.treeItem
            state = Checked if it else Unchecked

            treeItem.setCheckState(0, state)

    def quit(self):
        self.app.quit()



class PlotApp(QtWidgets.QMainWindow):
    """ The QT application to handle the plot inputs """
    def __init__(self, live=False, tabs=None):
        self.app = QtWidgets.QApplication([])

        super().__init__()
        
        # set an interval (in ms)
        self._interval = 500
        
        # QT timer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        
        # toolbar
        tb = self.addToolBar("File")
        tb_start = QtGui.QAction(QtGui.QIcon.fromTheme('media-playback-start'), "start", self)
        tb_start.setStatusTip("Start monitoring")
        tb_start.triggered.connect(self.start)
        tb_start.setEnabled(False)
        tb.addAction(tb_start)
        self.tb_start = tb_start
        
        tb_pause = QtGui.QAction(QtGui.QIcon.fromTheme('media-playback-pause'), "pause", self)
        tb.addAction(tb_pause)
        
        tb_stop = QtGui.QAction(QtGui.QIcon.fromTheme('media-playback-stop'), "stop", self)
        tb_stop.setStatusTip("Stop monitoring")
        tb_stop.triggered.connect(self.stop)
        tb.addAction(tb_stop)
        self.tb_stop = tb_stop

        tb.addSeparator()
        # set interval
        tb_interval = QtGui.QWidget(self)

        tb_interval_label = QtGui.QLabel('Update Interval')

        self.interval_input = QtGui.QDoubleSpinBox(parent=self)
        self.interval_input.setSuffix('s')
        self.interval_input.setMinimum(0.1)
        self.interval_input.setMaximum(99.0)
        self.interval_input.setSingleStep(0.1)
        self.interval_input.setValue(float(self._interval)/1000)
        self.interval_input.valueChanged.connect(self.update_interval)
        
        tb_interval_layout = QtGui.QHBoxLayout()
        tb_interval_layout.addWidget(tb_interval_label)
        tb_interval_layout.addWidget(self.interval_input)
        
        tb_interval.setLayout(tb_interval_layout)
        
        tb.addWidget(tb_interval)

        # create tab area
        self.tabbed_widget = QtGui.QTabWidget(parent=self)

        # connect changed signal but block until loaded
        self.tabbed_widget.blockSignals(True)
        self.tabbed_widget.currentChanged.connect(self.on_change)

        # set as central widget
        self.setCentralWidget(self.tabbed_widget)


        # handle tabs
        self.tabs = tabs
        for tab in self.tabs:
            tab.create(self)
            self.tabbed_widget.addTab(tab.widget, tab.title)


        # set a signal handler to catch keyboard
        # things to update
        #self.to_update = []

        ## all widgets
        #self.widgets = []


        ## set docks
        #self.dockarea = dock.DockArea()
        #self.setCentralWidget(self.dockarea)
        
        #if residuals:
        #    # add the dock
        #    d1 = dock.Dock("Residuals")
        #    self.dockarea.addDock(d1, 'top')

        #    # create the widget
        #    residuals.create(self)
        #    d1.addWidget(residuals.plt)
        #    self.to_update.append(residuals)
        #    self.widgets.append(residuals)

        ## stack texts
        #if texts:
        #    # do the first one
        #    text = texts.pop()
        #    d1 = dock.Dock(text.title)
        #    self.dockarea.addDock(d1, 'bottom')
        #    
        #    text.create(self)
        #    d1.addWidget(text.widget)
        #    self.to_update.append(text)
        #    self.widgets.append(text)

        #    # loop and stack the rest
        #    for text in texts:
        #        d2 = dock.Dock(text.title)
        #        self.dockarea.addDock(d2, 'above', d1)

        #        text.create(self)

        #        d2.addWidget(text.widget)
        #        self.to_update.append(text)
        #        self.widgets.append(text)
        
        
        ## widgets
        #if monitors:
        #    for monitor in monitors:
        #        monitor.create(self)
        #        self.widgets.append(monitor.plt)



        # set the layout
        #layout = QtGui.QVBoxLayout()



        # set up the central widget
        #centralWidget.setLayout(layout)
        # status bar
        self.status_bar = QtGui.QStatusBar()
        self.setStatusBar(self.status_bar)
        
        self.setGeometry(100,100,1200,800)
        self.setWindowTitle("CFMT Live Run Monitor")
        
    
    def update(self):
        """ Update all the elements """
        self.status_bar.showMessage("Updating...")
        index = self.current_tab_index
        self.tabs[index].update()

        self.status_bar.clearMessage()
        self.status_bar.showMessage("Running")
        self.app.processEvents()
    
    def update_interval(self):
        """ Fires when the interval is changed """
        new_interval = int(self.interval_input.value()*1000)

        self.timer.setInterval(new_interval)

    def items_to_update(self):
        """ Returns a generator """
        if self.tabs:
            for tab in self.tabs:
                yield tab
        else:
            for widget in self.widgets:
                yield widget
    
    def stop(self, checked=False, tab_index=None):
        """ Stops live capture """
        # stop the timer
        self.status_bar.showMessage("Stopping...")
        self.timer.stop()
        
        # current tab if not provided
        if tab_index is not None:
            index = tab_index
        else:
            index = self.tabbed_widget.currentIndex()

        self.tabs[index].stop()

        self.tb_start.setEnabled(True)
        self.tb_stop.setEnabled(False)
        self.status_bar.showMessage("Stopped")
    
    def start(self, checked=False, tab_index=None):
        """ Stops live capture """
        self.status_bar.showMessage("Starting...")

        # current tab if not provided
        if tab_index is not None:
            index = tab_index
        else:
            index = self.tabbed_widget.currentIndex()

        self.tabs[index].restart()

        # start the timer
        self.timer.start()
        
        self.tb_start.setEnabled(False)
        self.tb_stop.setEnabled(True)

        self.status_bar.showMessage("Started")

    def on_change(self, tab_index):
        """ Fires when the current tab is changed """
        # stop the old tab
        #self.stop(tab_index=self.current_tab_index)

        # start the new tab
        #self.start(tab_index=tab_index)

        # store the activated tab
        self.current_tab_index = tab_index

    def clean_up(self):
        """ Clean up on exit """
        # tell updatables we're going to kill
        for item in self.items_to_update():
            item.clean_up()

    def signal_handler(self, *args):
        """ Handler for the SIGINT signal """
        print("SIGINT caught ... terminating")
        self.clean_up()
        self.quit()
        print("QUITING")
        
        
    def run(self):
        """ Run the app """
        # only init the current tab
        self.current_tab_index = self.tabbed_widget.currentIndex()
        index = self.current_tab_index
        
        self.tabs[index].init()
        
        # release the signals
        self.tabbed_widget.blockSignals(False)
        
        # set the app up
        self.show()
        
        # start the timer 
        #self.timer.start(self._interval)

        self.stop()
        
        self.status_bar.showMessage("Ready")

        self.app.aboutToQuit.connect(self.clean_up) 
        self.app.exec_()
        self.app.deleteLater()
        self.app.quit()

    def quit(self):
        """ Exit """
        self.app.quit()

class LiveBase(object):
    """ Base class for Live objects """
    # to hold the data reader which will provide the data
    data_reader = None

    # whether we're waiting for data
    waiting = False

    def create(self, parent):
        """ Creates the widget """
        raise NotImplementedError 

    def init(self):
        """ Start the reader and return the first reading """
        raise NotImplementedError
    
    def update(self):
        """ Everything required to update the plot """
        raise NotImplementedError

    def clean_up(self):
        """ Called on exit """
        raise NotImplementedError
    
    def stop(self):
        """ Stop the widget processing """
        self.data_reader.stop()
    
    def restart(self):
        """ Restart the widget """
        # reset the file reader
        self.data_reader.reset()

        # re-init
        self.init()
    
    def get_data(self):
        """ Tell our reader to get data """
        self.data_reader.get()

class LiveFile(LiveBase):
    """ 
    Class for live file reading

    Takes a threaded reader which reads a file asynchronously, and returns a widget which
    tails the screen.
    
    """
    def __init__(self, live=False, reader=None, terminate=None, title=None):
        """ Constructor """
        self.live = live

        # whether we've actually created the widget 
        self.created = False
        
        self.updateable = True if self.live else False

        # placeholder for the widget
        self.widget = None

        self.idx = 0
        
        # title
        self.title = title if title else "textwidget"

        # if we havea reader set it here
        if reader is not None:
            self.data_reader = reader
            # reader also has the max length
            self.maxlen = self.data_reader.maxlen
        else:
            raise NotImplementedError("Live Data widget requires a reader")

        # set the terminate event
        self.terminate = terminate

    def create(self, parent):
        """ Create the widget """
        size = (800,600) # will be changed
        
        # we use the QPlainTextEdit widget
        self.widget = QtGui.QPlainTextEdit(parent=parent)
        self.widget.resize(*size)

        # set the size
        self.widget.setMaximumBlockCount(self.maxlen)

        # disable text interaction
        self.widget.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)

        # set the font
        font = QtGui.QFont("monospace")
        self.widget.setFont(font)

        # set the background
        self.widget.setStyleSheet("background-color: black;")

        # supports insertPlainText(), appendPlainText(), setPlainText()
        # we start with nothing
        self.widget.setPlainText("")

        self.created = True

    def init(self):
        """ Initialize """
        # start the reader
        self.data_reader.start()

        # wait for initial read
        self.data_reader.ready.wait()

        # update the window
        self.update()

    
    def process_data(self, data):
        # process it 
        [self.databuffer.append(i) for i in data]

        # return one giant string
        text = "".join(self.databuffer)

        # empty the deque
        self.databuffer.clear()
        return text

    
    def update(self):
        """ Updates the plot """
        for line in self.data_reader.readlines():
            self.widget.moveCursor(QtGui.QTextCursor.End)
            self.widget.insertPlainText(line)
            self.widget.moveCursor(QtGui.QTextCursor.End)

    def clean_up(self):
        """ Kill the data reader """
        self.stop()

    def stop(self):
        """ Kill the data reader """
        self.data_reader.stop()
    
    def status(self):
        """ Print status """
        return self.data_reader.status()

class LivePlot(LiveBase):
    """ Class for live plotting """
    # line styles stolen from https://github.com/Gnuplotting/gnuplot-palettes
    PEN_WIDTH = 1
    _line_pallete = {
        'set1': [
            pg.mkPen(color='#E41A1C', width=PEN_WIDTH),     # red
            pg.mkPen(color='#377EB8', width=PEN_WIDTH),     # blue
            pg.mkPen(color='#4DAF4A', width=PEN_WIDTH),     # green
            pg.mkPen(color='#984EA3', width=PEN_WIDTH),     # purple
            pg.mkPen(color='#FF7F00', width=PEN_WIDTH),     # orange
            pg.mkPen(color='#FFFF33', width=PEN_WIDTH),     # yellow
            pg.mkPen(color='#A65628', width=PEN_WIDTH),     # brown
            pg.mkPen(color='#F781BF', width=PEN_WIDTH),     # pink
            pg.mkPen(color='#9ED2E5', width=PEN_WIDTH),     # light blue
            pg.mkPen(color='#423396', width=PEN_WIDTH),     # violet
            pg.mkPen(color='#305532', width=PEN_WIDTH),     # fern
            pg.mkPen(color='#78797B', width=PEN_WIDTH),     # grey
            pg.mkPen(color='#FFFFFF', width=PEN_WIDTH),     # white
            pg.mkPen(color='#DD991E', width=PEN_WIDTH),     # buttercup
        ],
        'set2': [
            pg.mkPen(color='#66C2A5', width=PEN_WIDTH),     # teal
            pg.mkPen(color='#FC8D62', width=PEN_WIDTH),     # orange
            pg.mkPen(color='#8DA0CB', width=PEN_WIDTH),     # lilac
            pg.mkPen(color='#E78AC3', width=PEN_WIDTH),     # magenta
            pg.mkPen(color='#A6D854', width=PEN_WIDTH),     # lime green
            pg.mkPen(color='#FFD92F', width=PEN_WIDTH),     # banana
            pg.mkPen(color='#E5C494', width=PEN_WIDTH),     # tan
            pg.mkPen(color='#B3B3B3', width=PEN_WIDTH),     # grey
        ],
    }
    
    def __init__(self, live=False, reader=None, terminate=None, 
                 line_pallete=None, title="Live Plot", xlabel=None,
                 ylabel=None, xunitlabel=None, yunitlabel=None, standalone=False,
                 logmode=(False, False),
                 readers=None):
        """ Class constructor"""
        self.live = live
        self.updateable = True if self.live else False
        if standalone:
            # create an app
            self.app = QtWidgets.QApplication([])

            if self.live:
                #set timers
                self.timer = QtCore.QTimer()

                # connect to update function
                self.timer.timeout.connect(self.update)

            # set a signal handler
            signal.signal(signal.SIGINT, self.signal_handler)
        
        # simple counter
        self.idx = 0

        # created
        self.created = False 

        self.widget = None

        # default title
        self.title = title

        self.xlabel = "X" if xlabel is None else xlabel
        self.xunitlabel = "" if xunitlabel is None else xunitlabel
        self.ylabel = "Y" if ylabel is None else ylabel
        self.yunitlabel = "" if yunitlabel is None else yunitlabel

        # logmode
        self.logmode = logmode

        # default pallete
        
        #self.line_pallete = self._line_pallete['set1']
        lp = self._line_pallete['set1']

        if line_pallete:
            try:
                lp = self._line_pallete[line_pallete]
            except KeyError:
                pass
        
        # setup the line pallete as a cycle
        self.line_pallete = cycle(lp)

        sampleinterval = 0.25

        # sampling interval
        self._interval = int(sampleinterval*1000)
        self.databuffers = []
        #self.databuffer = []

        # set the application
        #self.app = QtWidgets.QApplication([])

        self.data_readers = []        
        # if we have a custom data reader we set it here
        if reader is not None:
            self.data_reader = reader

        #if readers is not None:
        #    self.data_readers = readers

        # set the terminate event
        self.terminate = terminate

        # mouse tracker
        #self.plt.
        #proxy = pg.SignalProxy(self.plt.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
        #self.mouse_label = pg.LabelItem()
        #self.plt.scene().sigMouseMoved.connect(self.mouseMoved)
        #self.vb = self.plt.getViewBox()

        # cross hairs
        #self.vLine = pg.InfiniteLine(pos=-1000, angle=90, movable=False, pen='w')
        #self.hLine = pg.InfiniteLine(pos=10, angle=0, movable=False, pen='w')
        #self.plt.addItem(self.vLine, ignoreBounds=True)
        #self.plt.addItem(self.hLine, ignoreBounds=True)

        self.monkeyPatch()

        # QT timer
        #if live:
    def create(self, parent):
        """ Create the widgets """
        #self.plt = pg.plot(title=self.title)
        size=(800,600)
        self.widget = pg.PlotWidget(parent=parent, title=self.title)
        # set size 
        self.widget.resize(*size)
        # set grid
        self.widget.showGrid(x=True, y=True)

        # set axes labels
        self.widget.setLabel('bottom', self.xlabel, self.xunitlabel)
        self.widget.setLabel('left', self.ylabel, self.yunitlabel)

        # set axes
        self.widget.setLogMode(x=self.logmode[0],y=self.logmode[1])

        # set legend
        self.legend = HorizontalLegendItem(size=None, offset=(30,30))
        self.widget.getPlotItem().legend = self.legend
        self.legend.setParentItem(self.widget.getPlotItem().vb)


        #self.legend = self.widget.addLegend()
        #self.legend.setParentItem(self.widget)
        #print(self.widget.boundingRect().bottomLeft().x(), self.widget.boundingRect().bottomLeft().y())
        self.legend.anchor((0.9,0), (0.9,0))
        #self.legend.setPos(self.widget.boundingRect().topRight())
        #self.legend.autoAnchor(self.widget.boundingRect().topLeft(), relative=True)

        # set downsampling
        self.widget.setDownsampling(ds=False, auto=False, mode='subsample')

        # add curve options context menu
        self.curveSelectMenu = CurveQMenu("Series Options")
        self.widget.getPlotItem().ctrlMenu = [self.curveSelectMenu, self.widget.getPlotItem().ctrlMenu]

        # add series selector
        xAxisUi = self.widget.getViewBox().menu.ctrl[0]
        label = QtWidgets.QLabel("Series:")
        self.xSeriesSelectMenu = QtWidgets.QComboBox()
        xAxisUi.gridLayout.addWidget(label, 8,0,1,2)
        xAxisUi.gridLayout.addWidget(self.xSeriesSelectMenu, 8,2,1,2)

        # block signals until after initialization 
        self.xSeriesSelectMenu.blockSignals(True)
        # connect combo box
        self.xSeriesSelectMenu.currentIndexChanged.connect(self.xSeriesChanged)

        self.curves = []
        
        # set the event filter
        self.curveSelectMenu.installEventFilter(self.curveSelectMenu)
        
        # fix exports
        #exporter = pg.exporters.ImageExporter.params.param('width').setValue(size[0], blockSignal=exporter.widthChanged)
        # See https://github.com/pyqtgraph/pyqtgraph/issues/538

        self.created = True

    def init(self):
        """ Initialize everything required """
        # start the reader
        self.data_reader.start()

        # wait for initial read
        idx = 0
        waiting_msg = "Reading " + self.data_reader.get_filename() + " ..."
        while self.data_reader.ready.is_set() is not True:
            utils.waiting(idx, waiting_msg)
            idx += 1
            time.sleep(0.8)

        print(waiting_msg + " OK")

        #if not self.live:
            # tell the data readers to stop
        #    self.data_reader.join()

        #for reader in self.data_readers:
        #    reader.start()

        #for reader in self.data_readers:
        #    reader.ready.wait()

        self.update()
    
    def update_downsampling(self):
        """ Update the downsampling"""

        # adjust the downsampling based on the length
        num_points = len(self.data_reader.databuffers[0])
        current_ds = self.widget.getPlotItem().downsampleMode()
        ds = int(math.ceil(num_points/50000))
        
        if current_ds != ds and ds != 1:
            self.widget.setDownsampling(ds)

    
    def process_data(self, data):
        """ Process the data to the databuffer """
        
        # for each header we create a deque to hold the data
        # append the new data to the buffer
        [self.databuffer.append(i) for i in data]


        # return plotable data
        test = collections.deque(zip(*self.databuffer))
    
    def initialise_curves(self):
        """ Initialize curves if they don't exit """
        # self.xaxis_assign provides valid x axes values
        # self.yaxis_assign provides valid y axes values

        # we pick the first xaxis
        xi = self.data_reader.xaxis_assign
        x = self.data_reader.databuffers[xi]

        # assign data label
        self.xlabel = self.data_reader.headers[xi]
        self.widget.setLabel('bottom', self.xlabel, self.xunitlabel)

        # assign a curve for each header, but only show those in yaxis_assign
        # loop through y values
        # here i represents the column_index: the column in the line.split() 
        # j is a counter than represents the curves
        j=0
        for i in self.data_reader.column_index:
            header = self.data_reader.headers[i]
            y = self.data_reader.databuffers[i]
            curve = self.widget.plot(x,y, pen=next(self.line_pallete), name=header, connect="all")
            selected=True
            
            if i not in self.data_reader.yaxis_assign:
                # hide (set pen to None)
                curve.curve.hide()
                # destroy legend
                self.legend.removeItem(header)
                selected=False
            
            # store the column index
            curve.column_index = i
            self.curves.append(curve)

            # add toggle option in menu
            action = QtWidgets.QAction(header, self.curveSelectMenu, checkable=True)
            action.setChecked(selected)
            action.index = j
            action.triggered.connect(self.toggleCurve)
            self.curveSelectMenu.addAction(action)
            
            # add to x series options
            self.xSeriesSelectMenu.addItem(header, userData=i)
            j += 1
        
        # unblock signal
        self.xSeriesSelectMenu.blockSignals(False)
        
    
    def toggleCurve(self):
        """ Toggles the visibility of the curve """
        sender = self.widget.sender()
        # determine whether to show or hide
        curve = self.curves[sender.index].curve
        if curve.isVisible():
            self.legend.removeItem(sender.text())
            self.curves[sender.index].curve.hide()
        else:
            self.legend.addItem(curve, sender.text())
            self.curves[sender.index].curve.show()
    
    def update_curves(self):
        """ Update curves """
        xi = self.data_reader.xaxis_assign
        # these should correspond to 
        for curve in self.curves:
#            if curve.column_index in self.data_reader.yaxis_assign:
            x = self.data_reader.databuffers[xi]
            y = self.data_reader.databuffers[curve.column_index]

            curve.setData(x,y)
        
        
    
    def xSeriesChanged(self):
        """ Triggered when the x-seris combo changes """
        sender = self.widget.sender()
        bi = sender.currentIndex()
        xi = sender.itemData(bi)
        
        # change data label
        self.xlabel = self.data_reader.headers[xi]
        self.widget.setLabel('bottom', self.xlabel, self.xunitlabel)

        # update curves
        self.data_reader.xaxis_assign = xi

        self.update_curves()
    
    def update(self):
        """ Update the plot """
        if not self.curves:
            self.initialise_curves()

        if len(self.data_reader.databuffers[0]) == 0:
            # we're waiting
            self.waiting = True
            utils.waiting(self.idx, "Waiting for data ...")
            self.idx += 1
            return
        elif self.waiting:
            self.waiting = False
            print("Waiting for data ... OK")
        
        # check we have curves
        if self.curves:
            self.update_downsampling()
            self.update_curves()

    def run(self):
        """ Run the LivePlot """
        self.init()

        # set the app up
        self.widget.show()
        
        # start the timer
        if self.live:
            self.timer.start(self._interval)
        
        self.app.aboutToQuit.connect(self.clean_up) 
        self.app.exec_()
    
    def signal_handler(self, *args):
        """ Handler for the SIGINT signal """
        print("SIGINT caught ... terminating")
        self.clean_up()
        self.quit()
    
    def clean_up(self):
        """ Clean up on exit """
        # kill the data_reader
        self.data_reader.stop()
    
    def quit(self):
        """ Exit """
        self.app.quit()

    def stop(self):
        """ Pause capture """
        self.data_reader.stop()

    def restart(self):
        """ Restart capture """

        # reset the file reader
        self.data_reader.reset()

        # re init this 
        self.init()

    def status(self):
        """ Print status """
        return self.data_reader.status()

    def mouseMoved(self, pos):
        """ Event which fires when the mouse moves """
        if self.plt.sceneBoundingRect().contains(pos):
            mousePoint = self.vb.mapSceneToView(pos)
            self.vLine.setPos(mousePoint.x())
            self.hLine.setPos(mousePoint.y())
            #self.mouse_label.setText("Mouse: ({},{})".format(mousePoint.x(), mousePoint.y()))
        else:
            if self.vLine.isVisible():
                self.vLine.setVisible(False)
                self.hLine.setVisible(False)
    
    def monkeyPatch(self):
        """
        Monkey patch: Remove the show/hide lines in the updateItems method
        of PlotDataItem.

        When we update the downsampling value, the curve automatically calls
        the updateItems method. This sets the new x/y values but then also
        shows/hides the curves depending on visual attributes like Pen. Since
        we may have explicitely hidden a curve, this will automatically show it.
        
        Here we monkey patch the function to remove this limtation
        """
        # Monkey patch the updateItems function in PlotDataItem
        pg.PlotDataItem.updateItems = overwriteUpdateItems

class SequencePlot(LivePlot):
    """ For plotting files in sequence """

    def update(self):
        """ Updating the plot """
        if not self.curves:
            self.initialise_curves()

        # wait for the ready switch to be set
        if self.data_reader.ready.wait():
            self.update_downsampling()
            self.update_curves()


class ComparisonPlot(LivePlot):
    """ For comparing two or more runs on the same plot """
    DEFAULT_PALLETE = "set1"
    DEFAULT_SAMPLE = 0.25
        
    DEFAULT_WINDOW_SIZE = (800,600)
    
    def __new__(cls, *args, **kwargs):
        """ 
        Define additional color palletes
        """
        cls._line_palletes = [cls._line_pallete]
        
        # number of colors to add 
        n_colors = 5
        LIGHTNESS = 0.9
        # extend the palletes
        cls._line_palletes.extend([{} for i in range(n_colors-1)])

        # Here we extend the existing colour palettes monochromatically
        for s,pal in cls._line_pallete.items():
            # for each set we add an empty list
            for i in range(n_colors-1):
                #print(type(i))
                cls._line_palletes[i+1][s] = []
            
            # for each pen we get the colour and create a colour range
            for p in pal:
                # QColor returns (H, S, L, alpha)
                *hsl, alpha = p.color().getHslF()
                start_color = Color(hsl=hsl)
                # end colour is the same hue, sat but 90% lightness
                end_color = Color(hsl=(start_color.hue, start_color.saturation, LIGHTNESS))
                _s, *new_colors = cls._monochromatic_range(start_color, end_color, n_colors)

                for i, c in enumerate(new_colors):
                    # create a new pen
                    pen = pg.mkPen(color=c.hex, width=cls.PEN_WIDTH)
                    cls._line_palletes[i+1][s].append(pen)

        return super().__new__(cls)
                
    @classmethod
    def _monochromatic_range(cls, s, e, nb):
        """
        Uses the colour module to compute a range of monochromatic colours
        s and e are expected 
        """
        return list(s.range_to(e, nb))


    def __init__(self, readers, live=False, title="Comparison Plot",
                       logmode=(False, False), standalone=True):
        """ 
        Constructor

        Differs from the parent
        TODO: This should supercede the LivePlot eventually
        """
        # whether this plot is to be continually updated
        self.live = live
        self.updateable = True if self.live else False

        # whether we have finished
        self.created = False

        if standalone:
            self.app = self.create_app()

        # titles and axes
        self.title = title
        self.xlabel = "X"
        self.xunitlabel = ""
        self.ylabel = "Y"
        self.yunitlabel = ""

        # axes transformations
        self.logmode = logmode

        # pallete handling
        # -> each pallete is set-up as a cycle
        self.line_palletes = [cycle(lp[self.DEFAULT_PALLETE]) for lp in self._line_palletes]
        
        self.databuffers = []
        
        self.data_readers = readers

        # monkey patch
        self.monkeyPatch()

    def create(self, parent):
        """ Creates the widgets """
        widget = pg.PlotWidget(parent=parent, title=self.title)

        widget.resize(*self.DEFAULT_WINDOW_SIZE)

        # set grid
        widget.showGrid(x=True, y=True)
        
        # set axes labels
        widget.setLabel('bottom', self.xlabel, self.xunitlabel)
        widget.setLabel('left', self.ylabel, self.yunitlabel)

        # set axes
        widget.setLogMode(x=self.logmode[0],y=self.logmode[1])

        # set legend
        legend = HorizontalLegendItem(size=None, offset=(30,30))
        widget.getPlotItem().legend = legend
        legend.setParentItem(widget.getPlotItem().vb)
        legend.anchor((0.9,0),(0.9,0))
        self.legend = legend

        # set downsampling
        widget.setDownsampling(ds=False, auto=False, mode="subsample")

        # add curve options for each reader
        curveSelectMenus = []
        for i, r in enumerate(self.data_readers):
            menu = CurveQMenu(f"Series ({i+1}) Options")
            curveSelectMenus.append(menu)
        widget.getPlotItem().ctrlMenu = curveSelectMenus + [widget.getPlotItem().ctrlMenu]
        self.curveSelectMenus = curveSelectMenus

        # add an x-series selector
        xAxisUi = widget.getViewBox().menu.ctrl[0]
        label = QtWidgets.QLabel("Series:")
        self.xSeriesSelectMenu = QtWidgets.QComboBox()
        xAxisUi.gridLayout.addWidget(label, 8,0,1,2)
        xAxisUi.gridLayout.addWidget(self.xSeriesSelectMenu, 8,2,1,2)

        # block signals until after initialization 
        self.xSeriesSelectMenu.blockSignals(True)
        # connect combo box
        self.xSeriesSelectMenu.currentIndexChanged.connect(self.xSeriesChanged)

        self.curves = []
        
        # set the event filter
        for menu in self.curveSelectMenus:
            menu.installEventFilter(menu)

        self.widget = widget
        self.created = True

    def init(self):
        """ Initialize """
        # start the readers
        for reader in self.data_readers:
            reader.start()

        # wait for initial read
        idx = 0
        waiting_msg = "Reading " + ", ".join([r.get_filename() for r in self.data_readers]) + " ..."

        while True:
            # poll for ready
            ready = [r.ready.is_set() for r in self.data_readers]
            if not all(ready):
                utils.waiting(idx, waiting_msg)
                idx += 1
                time.sleep(0.8)
            else:
                break
            
            # check incase one of the readers has died
            alive = [r.is_alive() for r in self.data_readers]
            if not any(alive):
                break
        
        print(waiting_msg + "OK")
        
        self.update()
    
    def create_app(self):
        """
        For running a standalone app
        """
        app = QtWidgets.QApplication([])

        if self.live:
            # we need our own timer
            self.timer = QtCore.QTimer()

            # connect to the on_timer function
            self.timer.timeout.connect(self.on_timer)
        
        # set a signal handler
        signal.signal(signal.SIGINT, self.signal_handler)

        return app
    
    def clean_up(self):
        """
        perform clean up when exit has been requested
        """
        # stop the data readers
        [r.stop() for r in self.data_readers]

    def initialise_curves(self):
        """ 
        Initialize curves
        
        We may have more than one databuffer here, and each has their own x-assign.
        This has to be common between them
        """
        widget = self.widget
        legend = self.legend
        xMenu = self.xSeriesSelectMenu

        # -> set the first reader as primary (where we take axis values from)
        p_reader = self.data_readers[0]
        xi = p_reader.xaxis_assign

        self.xlabel = p_reader.headers[xi]
        widget.setLabel("bottom", self.xlabel, self.xunitlabel)

        # assign a curve for each header, but only show those in yaxis_assign
        # loop through y values
        # here i represents the column_index: the column in the line.split() 
        # j is a counter than represents the curves
        # -> x is unique to each data reader

        # also add references to the curves to a datareader specific 
        self.curvesByDataReader = []

        for ri, r in enumerate(self.data_readers):
            x = r.databuffers[xi]
            pallete = self.line_palletes[ri]
            
            curveSelectMenu = self.curveSelectMenus[ri]
            _curves = []
            j = 0
            for i in r.column_index:
                header = r.headers[i] + f" ({str(ri + 1)})"
                y = r.databuffers[i]
                curve = widget.plot(x, y, pen=next(pallete), name=header, connect="all")
                selected = True

                if i not in r.yaxis_assign:
                    # hide
                    curve.curve.hide()
                    legend.removeItem(header)
                    selected = False
                    # destroy legend
                
                # store the column index
                curve.column_index = i
                curve.databuffer_index = ri
                self.curves.append(curve)
                _curves.append(curve)

                # add toggle option in menu
                action = QtWidgets.QAction(header, curveSelectMenu, checkable = True)
                action.setChecked(selected)
                action.index = j
                action.triggered.connect(self.toggleCurve)
                curveSelectMenu.addAction(action)

                # add to x series options
                self.addXSeriesMenu(xMenu, header, userData=i)
                j += 1
            
            self.curvesByDataReader.append(_curves)

        # unblock menu signal
        xMenu.blockSignals(False)


    def update(self):
        if not self.curves:
            self.initialise_curves()
        
        if self.curves:
            #self.update_downsampling()
            self.update_curves()

    def update_curves(self):
        """ Updates the curves """
        xi = self.data_readers[0].xaxis_assign

        for curve in self.curves:
            ri = curve.databuffer_index 
            r = self.data_readers[ri]
            x = r.databuffers[xi]
            y = r.databuffers[curve.column_index]

            curve.setData(x,y)

    def xSeriesChanged(self):
        """ Triggered when the x-series combo box changes """
        sender = self.widget.sender()
        bi = sender.currentIndex()
        xi = sender.itemData(bi)

        # change the data label
        self.xlabel = self.data_readers[0].headers[xi]
        self.widget.setLabel("bottom", self.xlabel, self.xunitlabel)

        # update curves
        self.data_readers[0].xaxis_assign = xi

        self.update_curves()

    def toggleCurve(self):
        """ 
        Toggles the visibility of curves 
        
        Fires when a series button is ticked/unticked.

        Here its possible we have more than one sender, so we have to ensure
        we indentigy the right curve
        """
        sender = self.widget.sender()
        parentMenu = sender.parentWidget()
        legend = self.legend
        

        # get the databuffer index
        ri = self.curveSelectMenus.index(parentMenu)
        i = sender.index

        # determine whether to show or hide
        curve = self.curvesByDataReader[ri][i].curve
        if curve.isVisible():
            legend.removeItem(sender.text())
            curve.hide()
        else:
            legend.addItem(curve, sender.text())
            curve.show()


    @classmethod
    def addXSeriesMenu(cls, menu, header, userData):
        """
        We maintain one xseries selection for each 
        """
        if header not in [menu.itemText(i) for i in range(menu.count())]:
            menu.addItem(header, userData=userData)


# monkey patch
def overwriteUpdateItems(self, styleUpdate=True):
    curveArgs = {}
    for k,v in [('pen','pen'), ('shadowPen','shadowPen'), ('fillLevel','fillLevel'), ('fillBrush', 'brush'), ('antialias', 'antialias'), ('connect', 'connect'), ('stepMode', 'stepMode')]:
        curveArgs[v] = self.opts[k]
    
    scatterArgs = {}
    for k,v in [('symbolPen','pen'), ('symbolBrush','brush'), ('symbol','symbol'), ('symbolSize', 'size'), ('data', 'data'), ('pxMode', 'pxMode'), ('antialias', 'antialias')]:
        if k in self.opts:
            scatterArgs[v] = self.opts[k]
    
    x,y = self.getData()
    #scatterArgs['mask'] = self.dataMask
    
    self.curve.setData(x=x, y=y, **curveArgs)
    
    if scatterArgs['symbol'] is not None:
        self.scatter.setData(x=x, y=y, **scatterArgs)
 

# multi-run live monitor app
def live_monitor_app(runs, args):
    """ Runs a live monitor app """
    # get the stdout of the running process
    # for each run we initialize the readers
    
    # we set maximum number of lines for texts
    maxlen_text = 5000 if args.last_n is None else args.last_n
    tabs = []
    for run in runs:
        # texts
        texts = []
        stdout_file = run.get_stdout_path()
        if os.path.isfile(stdout_file):
            stdout_reader = utils.file_reader(stdout_file, maxlen=maxlen_text)
            stdout_widget = LiveFile(
                reader=stdout_reader,
                live=True,
                terminate=stdout_reader.terminate,
                title="stdout",
            )
            texts.append(stdout_widget)

        stderr_file = run.get_stderr_path()
        if os.path.isfile(stderr_file):
            stderr_reader = utils.file_reader(stderr_file, maxlen=maxlen_text)
            stderr_widget = LiveFile(
                reader=stderr_reader,
                live=True,
                terminate=stderr_reader.terminate,
                title="stderr",
            )
            texts.append(stderr_widget)

        # get the residuals
        residual_reader = run.code.residual_reader(run.get_run_dir())
        residual_widget = LivePlot( 
            reader=residual_reader, 
            live=True, 
            terminate=residual_reader.terminate,
            title=run.code.name + " Residuals",
            xlabel="Residual",
            ylabel="IT",
            logmode=(False, True)
        )
        monitors = []
        # get the massflow rate 
        #if run.code.name == 'stream':
        #    fpath = run.code.get_massflow_file(run.get_run_dir()).pop()
        #    reader = run.code.monitor_reader(fpath)
        #    monitor_widget = LivePlot(
        #        reader=reader, 
        #        live=True, 
        #        terminate=reader.terminate,
        #        title=run.code.name + " Monitor: " + os.path.basename(fpath),
        #        xlabel="Mass Flow",
        #        ylabel="IT",
        #    )
        #    monitors.append(monitor_widget)
        
        for monitor in run.code.get_monitor_files(run.get_run_dir()):
            fpath = monitor
            reader = run.code.monitor_reader(fpath)
            monitor_widget = LivePlot(
                reader=reader, 
                live=True, 
                terminate=reader.terminate,
                title=os.path.basename(fpath),
                xlabel="",
                ylabel="IT",
            )
            monitors.append(monitor_widget)

        
        tabs.append(LiveAppTab(title=run.name, monitors=monitors, residuals=residual_widget, texts=texts))
    
    
    
    
    #app = plotting.PlotApp(live=True, residuals=residual_widget, monitors=monitors, texts=texts)
    app = PlotApp(live=True, tabs=tabs)
    
#    SignalWakeupHandler(app.app)
#    signal.signal(signal.SIGINT, lambda sig,_: app.signal_handler())
    

    # start the signal handler
    #signal_handler = utils.SignalHandler(residual_reader.terminate, residual_reader.thread)
    #signal_handler.set_handler()
    
    sys.exit(app.run())

class LiveAppTab():
    """ Class to store the elements required for Live App Tabs """
    def __init__(self, title=None, residuals=None, monitors=None, texts=None):
        """ Construct and store the tab """
        self.widgets = []
        
        self.residuals = residuals
        self.monitors = monitors
        self.texts = texts

        self.title = title if title else "Tab"
        
        # defaults
        self.dockarea = None
        self.widget = None

        self.initialized = False
    
    def create(self, parent=None):
        """ Create the tab """
        # set docks
        self.dockarea = dock.DockArea()
        if self.residuals:
            # add the dock
            d1 = dock.Dock("Residuals")
            self.dockarea.addDock(d1, "top")

            # create the widget
            self.residuals.create(d1)

            # add to relevent places
            d1.addWidget(self.residuals.widget)
            self.widgets.append(self.residuals)
        
        if self.monitors:
            # place to the right of the residuals
            monitor = self.monitors.pop()
            d1 = dock.Dock(monitor.title)
            self.dockarea.addDock(d1, 'right')

            monitor.create(d1)
            d1.addWidget(monitor.widget)
            self.widgets.append(monitor)

            # loop and stack the rest
            for monitor in self.monitors:
                d2 = dock.Dock(monitor.title)
                self.dockarea.addDock(d2, 'above', d1)

                monitor.create(d2)
                d2.addWidget(monitor.widget)
                self.widgets.append(monitor)
        
        if self.texts:
            # stack the texts
            # do the first
            text = self.texts.pop()
            d1 = dock.Dock(text.title)
            self.dockarea.addDock(d1, 'bottom')
            
            text.create(d1)
            d1.addWidget(text.widget)
            self.widgets.append(text)

            # loop and stack the rest
            for text in self.texts:
                d2 = dock.Dock(text.title)
                self.dockarea.addDock(d2, 'above', d1)

                text.create(d2)

                d2.addWidget(text.widget)
                self.widgets.append(text)
        

        # consistency
        self.widget = self.dockarea

    def update(self):
        """ update the tab """
        for widget in self.widgets:
            if widget.updateable:
                widget.update()

    def init(self):
        """ Initialize the tab """
        if not self.initialized:
            for widget in self.widgets:
                widget.init()
            self.initialized = True

    def restart(self):
        """ Stop the tab """
        for widget in self.widgets:
            widget.restart()

    def stop(self):
        """ Stop the tab """
        for widget in self.widgets:
            widget.stop()

    def status(self):
        """ Status """
        statuses = []
        for widget in self.widgets:
            statuses.append(str(widget.status()))
        
        return statuses
    
    def clean_up(self):
        """ Clean up on exit """
        for widget in self.widgets:
            widget.clean_up()


class CurveQMenu(QtWidgets.QMenu):
    """ Subclass to implement an event filter """
    def eventFilter(self, obj, ev):
        """ Event filter """
        if ev.type() in [QtCore.QEvent.MouseButtonRelease]:
            if isinstance(obj, QtWidgets.QMenu):
                obj.activeAction().trigger()
                return True
        
        return QtCore.QObject.eventFilter(self, obj, ev)
        
class HorizontalLegendItem(pg.graphicsItems.LegendItem.LegendItem):
    """ Add legend items to make them horizontal """
    
    def __init__(self, size=None, offset=None):
        super().__init__()
        self.layout.setVerticalSpacing(0.0)
        self.layout.setHorizontalSpacing(3.0)
        #self.layout.setContentsMargins(0.0, 0.0, 0.0, 0.0)

    def addItem(self, item, name):
        """
        Add a new entry to the legend. 

        ==============  ========================================================
        **Arguments:**
        item            A PlotDataItem from which the line and point style
                        of the item will be determined or an instance of
                        ItemSample (or a subclass), allowing the item display
                        to be customized.
        title           The title to display for this item. Simple HTML allowed.
        ==============  ========================================================
        """
        label = pg.graphicsItems.LabelItem.LabelItem(name)
        label.setMaximumHeight(18)
        if isinstance(item, pg.graphicsItems.LegendItem.ItemSample):
            sample = item
        else:
            #sample = pg.graphicsItems.LegendItem.ItemSample(item)
            sample = LegendItemSample(item)
        row = self.layout.rowCount()
        self.items.append((sample, label))
        self.layout.addItem(label, row, 0, alignment=QtCore.Qt.AlignCenter)
        self.layout.setColumnAlignment(0, QtCore.Qt.AlignVCenter)
        self.layout.addItem(sample, row, 1, alignment=QtCore.Qt.AlignCenter)
        self.layout.setColumnAlignment(1, QtCore.Qt.AlignVCenter)
        #print(self.layout.columnStretchFactor(0), self.layout.rowStretchFactor(row))
        self.updateSize()
    
    def updateSize(self):
        if self.size is not None:
            return
            
        height = 0
        width = 0
        #print("-------")
        for sample, label in self.items:
            sample_width = sample.boundingRect().width()
            sample_height = sample.boundingRect().height()
            height += max(sample_height, label.height())
            width = max(width, sample_width + label.width())
        self.setGeometry(0, 0, width + 25, height)

class LegendItemSample(pg.graphicsItems.LegendItem.ItemSample):
    """ Overwrite the legend sample """
    def paint(self, p, *args):
        #p.setRenderHint(p.Antialiasing)  # only if the data is antialiased.
        opts = self.item.opts

        fn = pg.functions
        
        if opts.get('fillLevel',None) is not None and opts.get('fillBrush',None) is not None:
            p.setBrush(fn.mkBrush(opts['fillBrush']))
            p.setPen(fn.mkPen(None))
            p.drawPolygon(QtGui.QPolygonF([QtCore.QPointF(2,18), QtCore.QPointF(18,2), QtCore.QPointF(18,18)]))
        
        if not isinstance(self.item, pg.graphicsItems.ScatterPlotItem.ScatterPlotItem):
            p.setPen(fn.mkPen(opts['pen']))
            #p.drawLine(2, 18, 18, 2)
            p.drawLine(0,8,15,8)
        
        symbol = opts.get('symbol', None)
        if symbol is not None:
            if isinstance(self.item, pg.graphicsItems.PlotDataItem.PlotDataItem):
                opts = self.item.scatter.opts
                
            pen = fn.mkPen(opts['pen'])
            brush = fn.mkBrush(opts['brush'])
            size = opts['size']
            
            p.translate(10,10)
            path = drawSymbol(p, symbol, size, pen, brush)
    
    def boundingRect(self):
        return QtCore.QRectF(0, 0, 15, 18)


class SignalWakeupHandler(QtNetwork.QAbstractSocket):
    """
    Implementation of a wakeup handler to handle signals
    when low-level code is running
    See https://stackoverflow.com/questions/4938723/what-is-the-correct-way-to-make-my-pyqt-application-quit-when-killed-from-the-co/4939113#4939113
    """

    def __init__(self, parent=None):
        super().__init__(QtNetwork.QAbstractSocket.UdpSocket, parent)
        self.old_fd = None

        # create a socket pair
        self.wsock, self.rsock = socket.socketpair(type=socket.SOCK_DGRAM)
    
        # let QT listen on the one end
        self.setSocketDescriptor(self.rsock.fileno())
    
        # and let python write on the other end
        self.wsock.setblocking(False)
        self.old_fd = signal.set_wakeup_fd(self.wsock.fileno())
    
        # first python code executed gets any exception from the 
        # signal handler, so add a dummy handler first
        self.readyRead.connect(lambda: None)
    
        # Second handler does the real handling
        self.readyRead.connect(self._readSignal)

    def __del__(self):
        """ Restore any old handler on deletion """
        if self.old_fd is not None and signal and signal.set_wakeup_fd:
            signal.set_wakeup_fd(self.old_fd)

    def _readSignal(self):
        """ Read the written byte """
        # Note that readyRead is blocked from occuring again until readData()
        # is called, so call it, even if you don't need the value
    
        data = self.readData(1)
    
        # emit a Qt signal for convinience 
        self.signalReceived.emit(data[0])
    
    signalReceived = QtCore.pyqtSignal(int)
    
