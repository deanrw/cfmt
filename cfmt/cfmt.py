"""
    cft: A computational Fluids Simulations tool
"""
import sys
import textwrap
import argparse
import os
from datetime import datetime
import logging
import logging.handlers
from cfmt import config as config
import cfmt.batch_systems.controller as batch
import cfmt.post.controller as post
from cfmt.entities import cases, studies, runs, archives
from cfmt.entities.runs import load_run
from cfmt.db import controller as dbc
from cfmt import exceptions
from cfmt.codes import codes
from cfmt import utils
from cfmt import logging as customLog
from . import commands
#from .commands.init import add_init_parser

def main(argv=sys.argv):
    """ Runs the CFT program """
    command = Cfmt(argv)
    return command._run()

def fatal(message=""):
    """ Prints a message and exists """
    print("ERROR: " + message)
    sys.exit(2)

def stdout(message=""):
    """ Writes to stdout """
    sys.stdout.write(message + "\n")


class Cfmt(object):
    """
    The main cmft class
    """
    welcome_msg = """
                  Welcome to CFMT, a Computational Fluids Management Tool
                  A configuration file has been installed to '{0}'. Please complete it.
                  All options will be copied as defaults to new studies
                  """
    description = """\
    cfmt is a tool for managing computational fluid dynamics simulations
    """
    available_codes = codes.AVAILABLE_CODES
    available_batch = batch.AVAILABLE_BATCH_SYSTEMS
    available_post = post.AVAILABLE_POST_SYSTEMS
    available_post_format = post.AVAILABLE_POST_FORMATS
    available_status = runs.Run.valid_status_names
    available_fields = runs.Run.db_fields
    available_archive_types = archives.AVAILABLE_ARCHIVE_TYPES

    parser = argparse.ArgumentParser(
        prog="cfmt",
        description = textwrap.dedent(description),
    )
    parser.add_argument(
        '--version',
        action='version',
        version=config.PROGVERSION,
    )
    parser.add_argument(
        '-f', '--force',
        action='store_true',
        help="Accept all confirmation prompts",
        dest='override'
    )
    parser.add_argument(
        '-c','--config',
        nargs=1,
        help="Path to configuration file",
        default=None
    )
    parser.add_argument(
        '-p','--profile',
        nargs=1,
        help="Configuration profile to load of form cfmt-profile.cfg",
        default=None
    )
    parser.add_argument(
        '-d', '--debug',
        help="Print debug output to log",
        action='store_const',
        dest='loglevel_d',
        const=logging.DEBUG,
        default=logging.INFO
    )
    parser.add_argument(
        '-v', '--verbose',
        help="Be verbose",
        action='store_const',
        dest='loglevel_v',
        const=logging.INFO,
        default=logging.WARNING
    )
    parser.add_argument(
        '-vv', '--vverbose',
        help="Be very verbose",
        action='store_const',
        dest='loglevel_v',
        const=logging.DEBUG,
        default=logging.WARNING
    )
    subparsers = parser.add_subparsers(
        help="sub-command help",
        dest='mode')
    
    # ==================================================
    # some common arguments shared between subparsers
    # ==================================================
    # Batch system
    parser_batch = commands.batch.get_common()
    # ==================================================
    # cfmt upgrade
    # ==================================================
    commands.upgrade.add_parser(subparsers)
    # ==================================================
    # cfmt init
    # ==================================================
    commands.init.add_parser(subparsers)
    # ==================================================
    # cfmt edit
    # ==================================================
    commands.edit.add_parser(subparsers)
    # ==================================================
    # cfmt delete
    # ==================================================
    commands.delete.add_parser(subparsers)
    # ==================================================
    # cfmt info
    # ==================================================
    commands.info.add_parser(subparsers)
    # ==================================================
    # cfmt run
    # ==================================================
    commands.run.add_parser(subparsers, parents=[parser_batch])
    # ==================================================
    # cfmt utils
    # ==================================================
    commands.utils.add_parser(subparsers)
    # ==================================================
    # cfmt cmd
    # ==================================================
    commands.cmd.add_parser(subparsers)
    # ==================================================
    # cfmt new
    # ==================================================
    commands.new.add_parser(subparsers)
    # ==================================================
    # cfmt update
    # ==================================================
    commands.update.add_parser(subparsers)
    # ==================================================
    # cfmt status
    # ==================================================
    commands.status.add_parser(subparsers)
    # ==================================================
    # cfmt make
    # ==================================================
    commands.make.add_parser(subparsers)
    # ==================================================
    # cfmt fetch
    # ==================================================
    commands.fetch.add_parser(subparsers, parents=[parser_batch])

    # Batch system queue
    #parser_run_batch = parser_run.add_argument_group("Batch system options")
    # ==================================================
    # cfmt sync
    # ==================================================
    commands.sync.add_parser(subparsers, parents=[parser_batch])
    # ==================================================
    # cfmt note
    # ==================================================
    commands.note.add_parser(subparsers)
    # ==================================================
    # cfmt move
    # ==================================================
    commands.move.add_parser(subparsers)
    # ==================================================
    # cfmt copy
    # ==================================================
    commands.copy.add_parser(subparsers)
    # ==================================================
    # cfmt log
    # ==================================================
    commands.log.add_parser(subparsers)
    # ==================================================
    # cfmt results
    # ==================================================
    commands.results.add_parser(subparsers)
    # ==================================================
    # cfmt export
    # ==================================================
    commands.export.add_parser(subparsers)
    # ==================================================
    # cfmt archive
    # ==================================================
    commands.archive.add_parser(subparsers)
    # ==================================================
    # cfmt srcdiff
    # ==================================================
    commands.srcdiff.add_parser(subparsers)
    # ==================================================
    # cfmt post
    # ==================================================
    commands.post.add_parser(subparsers)

    # tell config the name of our program
    config.PROGNAME = parser.prog
    # do the autocomplte
    #argcomplete.autocomplete(parser)

    def __init__(self, argv):
        """ Class constructor """
        
        # initialize parser
        self.args = self.parser.parse_args(argv[1:])
        
        # setup logging
        self._setLogging()

        # load database object

        # load version control
    
    def _setLogging(self):
        """
        Sets up the loggers
        """
        # activate debug mode if required
        console_level = logging.DEBUG if self.args.loglevel_d == logging.DEBUG else self.args.loglevel_v

        #  set up the console logger
        self.console = logging.getLogger('console')
        self.console.setLevel(console_level)
        
        # --> set up a stream handler
        console = logging.StreamHandler()
        console.setLevel(console_level)
        
        # --> create a Formatter for formatting 
        formatter = customLog.CfmtFormatter()

        # --> add the formatter to the handler
        console.setFormatter(formatter)

        # --> add the handler to the Logger
        self.console.addHandler(console)

        # set up the file logger
        self.logger = logging.getLogger('file')
        self.logger.setLevel(self.args.loglevel_d)

        # make sure the root logger also has the correct level set
        root_logger = logging.getLogger()
        root_logger.setLevel(self.args.loglevel_d)

        # --> set up a streamhandler, we can replace this when we have
        #     a file name to use
        log_handler = logging.StreamHandler()

        # --> create a Formatter
        formatter = logging.Formatter(
            fmt=config.LOG['fmt'],
            datefmt=config.LOG['datefmt'],
        )

        # --> add the formatter to the handler
        log_handler.setFormatter(formatter)
        
        # --> add the handler to the logger
        self.logger.addHandler(log_handler)
        
    def _run(self):
        """ Responsible for actually running the script """
        # handle global options
        if self.args.config is not None:
            # config has been provided on the cmd
            config_path = self.args.config.pop()
        else:
            config_path = None

        # attempt to load the config
        
        try:
            config.load(config_path=config_path, from_user=True)
        except IOError as err:
            fatal(str(err))
        except config.configparser.ParsingError as err:
            fatal(str(err))
        except exceptions.NoUserConfig as err:
            # user config file doesn't exist, so we install it
            fname = config.save(to_user=True)
            msg = self.welcome_msg.format(fname)
            print(utils.remove_left_margin(msg))
            sys.exit(0)

        if self.args.func is None:
            self.parser.print_help()
            sys.exit(0)

        try:
            run_mode = getattr(self, self.args.func)
            self.console.info("Running %s in '%s' mode ...", self.parser.prog, self.args.mode)
        except AttributeError:
            self.parser.error("Mode not found")

        try:
            run_mode()
        except:
            raise
        finally:
            # close the logs
            logging.shutdown()

    def init(self):
        """ Initialize a study """

        args = self.args

        # are we initializing a system/user database#
        if args.system:
            # install a config file to /etc/cfmt
            raise NotImplementedError
        elif args.user:
            # install a config file to $XDG_CONFIG_DIR
            raise NotImplementedError
        
        # initialze the codes
        # --> args.code should contain a list of code names
        # parse any config command line arguments
        if args.code is not None:
            # we've been passed a list of codes to initialize. 
            _codes = args.code 
            # set the first as the default
            config.configtool.set('defaults','code', next(iter(args.code or []), None))
        else:
            # we initialize without specifying a code
            #codes = [config.configtool.get('defaults','code')]
            _codes = []
        
        try:
            for code in _codes:
                _code = codes.get(code)
                config.add_config(_code.get_default_config(), overwrite=False)
                _code.init_check(args)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)
        
        # check whether this is an existing project
        try:
            study = studies.load_study(args.directory)

            if args.code is not None:
                # see if we're adding codes
                #codes = 
                for c in args.code:
                    if c in study.codes:
                        msg = "Code '{}' already attached to study".format(c)
                        print(msg)
                        self.logger.info(msg)
                        continue

                    study.add_code(c, args=args)
                study.save()
                sys.exit(0)
            
            msg = "A cmft study alread exists in directory '{0}'."
            self.parser.error(msg.format(study.path))
        except exceptions.NotAStudy:
            pass
        
        # make the study directory if we need
        abs_directory = os.path.abspath(args.directory)
        if abs_directory != os.getcwd():
            if not os.path.exists(abs_directory):
                os.makedirs(abs_directory)

        # make the CFT_DIR_NAME directory
        config_dir = os.path.join(abs_directory, config.CFMT_DIR_NAME)
        if not os.path.exists(config_dir):
            os.mkdir(config_dir)
        
        # create the log file
        log_fname = os.path.join(config_dir, config.LOG['filename'])
        
        # logger
        log_handler = customLog.getLogHandler(log_fname)
        customLog.replaceHandler(self.logger, log_handler)

        # initialize the database
        db = dbc.DBController(config.DATABASE['type'], abs_directory)

        # get the study object
        study = studies.Study(
            path=abs_directory,
            db=db,
            name=args.name,
            empty=args.empty,
            description=args.description,
            project_name=args.project_name,
            code_names=_codes,
        )
        # initialize (databases are written here)
        study.init(args)

        # write config file
        config.save(directory=config_dir)

        # save
        study.save()

        # initialize any cases
        for case_name in args.case:
            # get a case object
            default_code = config.configtool.get('defaults','code')
            case = cases.Case(name=case_name, path=case_name, study=study, code=default_code)
            
            try:
                case.create()
            except exceptions.CaseExists as err:
                print("WARNING :: " + str(err) + str(" ... skipping case"))
                continue
            except exceptions.ReservedName as err:
                print("WARNING :: " + str(err) + str(" ... skipping case"))
                continue
            except Exception:
                raise
            else:
                name = os.path.normpath(case.name)
                msg = "  -> Created case '{}'".format(name)
                print(msg)

        
        if args.empty:
            msg = ":: Initialized empty cmft study in '{0}'."
        else:
            msg = ":: Initialized cmft study in '{0}'."

        stdout(msg.format(abs_directory))
        exit()

        
            #self.study = Study(self.args.study)
            #self.with_study = True
            #self.study.create()

        ## case names take precedence
        #if self.args.case is not None:
        #    # flatten the list of case names
        #    case_names = self.args.case
        #    case_names = [item for sublist in case_names for item in sublist]
        #    
        #    # make sure they're unique
        #    case_names = list(set(case_names))

        #    print(case_names)
        #    for case in case_names:
        #        self.case.append(Case(case, self.study))

        #for case in self.case:
        #    case.create()



        #attrs = vars(self)
        #print(','.join("%s: %s" % item for item in attrs.items()))
    
    def delete(self):
        """ 
        Deletes an entity
        """
        args = self.args
        
        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)

        # see what we need to delete
        try:
            study.delete(args)
        except exceptions.DeleteFailed as err:
            utils.print_exception_info(str(err))
            sys.exit(1)
        
        #try:
        #    studies.delete_study(args.directory, args.delete_files)
        #except IOError as err:
        #    self.parser.error(str(err))
        #except:
        #    raise
    
    def info(self):
        """ Output information about the current directory """
        
        tree_template = """

        """
        args = self.args
        
        # determine whether this is a study, case or run
        # we always return the study if we're in a CFMT controlled study
        msg = ""
        directory = os.path.normpath(args.directory)
        abs_directory = os.path.abspath(directory)

        # first check the the passed entity actually exists
        if not os.path.isdir(abs_directory):
            msg = "{}: {}: No such file or directory"
            msg = msg.format(self.parser.prog, os.path.basename(abs_directory))
            fatal(msg)
        
        try:
            study = studies.load_study(directory)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        
        # --> check if we've been passed something specific to look for 
        if args.select is not None:
            _cases = study.get_cases(select=args.select)
            
            if _cases:
                # loop and print run info
                for case in _cases:
                    if case.runs:
                        for run in case.runs:
                            if run.jobs:
                                job = run.jobs[0]
                                print(job.info())
                                print('')
                            else:
                                print(run.info())
                                print('')
                    else:
                        print("No matching runs found in case '{}'".format(case.name))
            else:
                print("No matching cases found")
        else:
            # --> we definitely have a study
            msg = "Directory '{}' is a cfmt managed study"
        
            # --> check if that's what we requested.
            if study.path == abs_directory:
                msg = msg.format(os.path.basename(abs_directory))
                print(msg + "\n")
                print(study.info())
                sys.exit(0)

            # we have a study, but we need to determine where we are.
            # --> we traverse up to the study directory 
            path = os.path.abspath(directory)
            while path != study.path:
                # --> do we have a case
                try:
                    case = cases.load_case(path, study)
                except exceptions.CaseDoesNotExist:
                    msg = "Directory '{}' is unmanaged but placed within study {} located at '{}'"
                    msg = msg.format(directory, study.name, study.path)
                except IOError as err:
                    fatal(str(err))
                else:
                    case_abs_path = os.path.join(study.path, case.path)
                    if os.path.abspath(directory) == case_abs_path:
                        # exact match, directory we requested is a case
                        msg = "Directory '{}' is a case and part of study {} located at '{}'"
                        msg = msg.format(os.path.basename(abs_directory), study.name, study.path)
                        print(msg + "\n")
                        print(case.info())
                        sys.exit(0)
                    else:
                        # we're within a case directory so wecheck for a matching run directory
                        try:
                            run = load_run(directory, study=study)
                        except exceptions.RunDoesNotExist:
                            rel_path_to_case = os.path.relpath(abs_directory, path)
                            msg = "Directory '{}' is".format(rel_path_to_case)
                            if rel_path_to_case in config.CASE_DIRS:
                                print(msg + " a managed directory within case '{}'".format(case.name))
                            else:
                                print(msg + " not a managed directory but within case '{}'".format(case.name))
                        except IOError as err:
                            self.logger.error("Cannot load run", exc_info=err)
                            sys.exit(1)
                        else:
                            # get the relative path to the study
                            print(run.info())

                finally:
                    oldpath, path = path, os.path.dirname(path)
            
    def run(self):
        """ Runs a code """
        
        args = self.args
        path = args.directory.pop()
        
        directory = os.path.normpath(path)
        abs_directory = os.path.abspath(directory)

        # remove redundants
        if args.batch_system is not None:
            args.batch_system = list(set(args.batch_system))
            if len(args.batch_system) > 1:
            # if they're all the same remove redundant
                msg = "More than one batch system specified"
                self.parser_run.error(msg)

        # check the study is valid
        try:
            study = studies.load_study(directory, profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        else:
            # try and load the valid case, we could have:
            # -> current directory
            # -> a specific "select" i.e. a run
            # -> a job_id
            # note it may be multiple cases
            try:
                if args.select is not None:
                    _cases = study.get_cases(select=args.select)
                    # we have jobs
                else:
                    # assume the current directory
                    _cases = [cases.load_case(abs_directory, study)]
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(directory)
                fatal(msg)
            except IOError as err:
                fatal(str(err))
        
        for case in _cases:
            # --> we have a valid case
            print("Preparing to run case '{}'...".format(case.name))
            self.logger.info("Preparing to run case '%s'", case.name)

            # run the case
            try:
                case.run(args=args)
            except Exception as err:
                utils.print_exception_info(err)
                pass
    
    def utils(self):
        """ Runs pre-defined utility functions """
        args = self.args
        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        except exceptions.DBVersionError as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
            # try and load the valid case, we could have:
            # -> current directory
            # -> a specific "select" i.e. a run
            # -> a job_id
        else:
            try:
                if args.select is not None:
                    _cases = study.get_cases(select=args.select)
                    # we have jobs
                else:
                    # assume the current directory
                    _cases = [cases.load_case(os.getcwd(), study)]
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(os.getcwd())
                fatal(msg)
            except IOError as err:
                fatal(str(err))
            
        if not _cases:
            utils.print_exception_info("No cases found")
            sys.exit(1)
        
        # check the utility exists
        if not args.util_choice:
            utils.print_exception_info("No utility provided")
            sys.exit(1)

        # determine the util we have (sanitize)
        util_choice = args.util_choice.replace("-","_")
        for c in _cases:
            if not c.runs:
                if args.select is None:
                    # get the latest run
                    c.set_last_run()
                else:
                    print(utils.WARNING + "No runs found")
                    continue
            for r in c.runs:
                if hasattr(commands.utils, util_choice):
                    getattr(commands.utils, util_choice)(r, args)
                else:
                    print(utils.ERROR + f" Utility {util_choice} not found")
                    sys.exit(0)

    def cmd(self):
        """ Runs commands """
        args = self.args

        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        except exceptions.DBVersionError as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
            # try and load the valid case, we could have:
            # -> current directory
            # -> a specific "select" i.e. a run
            # -> a job_id
        else:
            try:
                if args.select is not None:
                    _cases = study.get_cases(select=args.select)
                    # we have jobs
                else:
                    # assume the current directory
                    _cases = [cases.load_case(os.getcwd(), study)]
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(os.getcwd())
                fatal(msg)
            except IOError as err:
                fatal(str(err))
            
        if not _cases:
            utils.print_exception_info("No cases found")
            sys.exit(1)
        
        # check the utility exists
        if not args.application:
            utils.print_exception_info("No application provided")
            sys.exit(1)
        
        # check for cwd, and that the destination exists
        _cwd = args.cwd.pop() if args.cwd is not None else None
        
        for c in _cases:
            if not c.runs:
                if args.select is None:
                    # get the latest run
                    c.set_last_run()
                else:
                    print(utils.WARNING + "No runs found")
                    continue

            for r in c.runs:
                if _cwd is not None:
                    if _cwd.startswith("/"):
                        cwd = _cwd
                    else:
                        cwd = os.path.join(r.get_run_dir(local_only=args.local_only), _cwd)
                    if not os.path.isdir(cwd):
                        print(utils.WARNING + " Not a directory: '{}'".format(cwd))
                        continue
                else:
                    cwd = r.get_run_dir(local_only=args.local_only)

                utils.run(args.application, cwd, shell=True, bash=False)


    def new(self):
        """ Creates a new case """
        args = self.args
        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)

        # parse any command line arguments
        if args.code is not None:
            code = args.code.pop()
        else:
            code = config.configtool.get('defaults','code')

        # loop over directory list
        for directory in args.directory:
            # cannot create a case in an existing directory
            _dir = os.path.normpath(directory)
            if (os.path.exists(_dir)):
                msg = "WARNING :: Directory '{}' exists ... skipping"
                msg = msg.format(_dir)
                stdout(msg)
                continue

            case_name = _dir            
            # get a case object
            case = cases.Case(name=case_name,
                              path=case_name,
                              study=study,
                              code=code,
                              )

            try:
                case.create()
            except exceptions.CaseExists as err:
                print("WARNING :: " + str(err) + str(" ... skipping case"))
                continue
            except exceptions.ReservedName as err:
                print("WARNING :: " + str(err) + str(" ... skipping case"))
                continue
            except Exception:
                raise
            else:
                name = os.path.normpath(case.name)
                msg = "  -> Created case '{}'".format(name)
                self.logger.info("Created new case '%s'",name)
                print(msg)

    def update(self):
        """
        Updates the current state of the study
        """
        args = self.args
        
        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
        
        try:
            study.update(args)
        except exceptions.UpdateFailed as err:
            utils.print_exception_info(str(err))
            sys.exit(1)
        # cmd options

    def edit(self):
        """
        Edits files in the case/run
        """
        args = self.args

        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        except exceptions.DBVersionError as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
            # try and load the valid case, we could have:
            # -> current directory
            # -> a specific "select" i.e. a run
            # -> a job_id
        else:
            try:
                if args.select is not None:
                    _cases = study.get_cases(select=args.select)
                    # we have jobs
                else:
                    # assume the current directory
                    _cases = [cases.load_case(os.getcwd(), study)]
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(os.getcwd())
                fatal(msg)
            except IOError as err:
                fatal(str(err))

        # we should only have 1, but just incase
        if not _cases:
            utils.print_exception_info("No cases found")
            sys.exit(1)

        case = _cases.pop()
        flist = []
        if args.file is not None:
            flist = args.file
        
        if args.case:
            # ask the code to edit the files
            case.code.edit(flist, cwd=case.abspath, args=args)
        else:
            # check for a specific run
            if not case.runs:
                case.set_last_run()

            if not case.runs:
                utils.print_exception_info("No runs found")
                sys.exit(1)

            run = case.runs.pop()
            run.code.edit(flist, cwd=run.get_run_dir(local_only=args.local_only), args=args)

    
    def status(self):
        """
        Prints the status of the cases or runs requested
        """
        args = self.args

        args.update_all = True
        
        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        except exceptions.DBVersionError as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)



        # print the status of the study
        study.print_status(args)
    
    def upgrade(self):
        """
        Upgrades an existing database to the newest version
        """
        # try and load the database
        study_path = os.getcwd()
        requested_path = study_path
        while not os.path.isdir(os.path.join(study_path, config.CFMT_DIR_NAME)):
            oldpath, study_path = study_path, os.path.dirname(study_path)
            if study_path == oldpath:
                utils.print_exception_info("No cfmt study exists in directory '{}' or above it".format(requested_path))
                sys.exit(1)

        # load the database            
        try:
            db = dbc.load_database(study_path)
        except IOError:
            raise
        except:
            raise
        #raise IOError("Cannot load database file")

        # check if an update is required 
        db_version = db.get_version()
        if db_version == config.DBVERSION:
            print("Database is up to date")
            sys.exit(0)

        # make a backup of the database before we operate
        try:
            print("Backing up database ... ", end='\r')
            db.backup_db()
        except IOError as err:
            print("Backing up database ... FAILED", end='\n')
            utils.print_exception_info(err)
            print("Upgrade failed")
            sys.exit(1)
        except:
            utils.print_exception_info(err)
            print("Upgrade failed")
            sys.exit(1)
        else:
            db.db_backed_up = True
            print("Backing up database ... OK", end='\n')

        # do the upgrade 
        try:
            db.upgrade_db()
        except exceptions.UpgradeFailed as err:
            # restore the database 
            print("Upgrading database ... FAILED", end="\n")
            utils.print_exception_info(err)
            try:
                print("Restoring database backup ...", end="\r")
                db.restore_db_backup()
            except Exception as err:
                print("Restoring database backup ... FAILED", end="\n")
                utils.print_exception_info(str(err))
                print("Database backup is located at '{}'".format(db.db_file))
            else:
                print("Restoring database backup ... OK", end="\n")

        else:
            print("Upgrading database ... OK", end="\n")
            #study.logger.info("Database upgraded to version: '%s'",str(config.DBVERSION))
            print("Database upgraded to version: {}".format(str(config.DBVERSION)))


    def make(self):
        """
        Attempts to build the code associated with a particular case
        """
        args = self.args
        path = args.directory
        
        directory = os.path.normpath(path)
        abs_directory = os.path.abspath(directory)
        #check the study is valid
        try:
            study = studies.load_study(directory)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
        else:
            # try and load the case
            try:
                case = cases.load_case(abs_directory, study)
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(directory)
                fatal(msg)
            except IOError as err:
                fatal(str(err))

        # --> we have a valid case
        # --> check the code in the case
        if not case.code.can_build:
            print("Code '{}' cannot be built".format(case.code.name))
            exit(0)
        
        print(" -> Building code '{}'".format(case.code.name))
        self.logger.info("Building code '{}'".format(case.code.name))
        case.code.build(case, print_output=True, rebuild=False)

        #try:
        #    # tell the code what case we're running
        #    print("Building code '{}'".format(case.code.name))
        #    self.logger.info("Building code '{}'".format(case.code.name))
        #    case.code.build(print_output=True)
        #except exceptions.BuildFailed as err:
        #    utils.print_exception_info(str(err))
        #    sys.exit(1)
    def fetch(self):
        """
        Fetch the selected runs from the remote staging directory
        """
        args = self.args
        
        # check the study is valid
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        study.fetch(args)
    
    def sync(self):
        """
        Sync the selected runs with the remote staging directory
        """
        args = self.args
        
        # check the study is valid
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        
        if args.get_tout:
            if len(args.get_tout) > 1 and None in args.get_tout:
                self.parser.error("Missing argument for --get-tout")

        study.fetch(args, sync=True)

    def note(self):
        """
        Manage notes for the selected entity
        """
        args = self.args

        # current directory
        cwd = os.getcwd()

        # check the study is valid
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)
        
        if study.path == cwd:
            case = None
        else:
            path = cwd
            while path != study.path:
                # --> check for a case
                try:
                    case = cases.load_case(path, study)
                except exceptions.CaseDoesNotExist:
                    msg = "Cannot find a suitable case"
                except IOError as err:
                    utils.print_exception_info(str(err))
                    sys.exit(1)
                else:
                    break
                    # add the note
                finally:
                    oldpath, path = path, os.path.dirname(path)

        if args.add:
            # we're adding a note
            # work out if we're under a case
            if case is None:
                study.console.error("Can only add notes to cases")
            else:
                # prompt if we don't have anything passed
                case.add_note(text=args.add, editor=args.editor)
        
        elif args.edit is not None:
            # we have some notes to edit
            note_ids = args.edit
            
            study.edit_note(note_ids, editor=args.editor)
        
        elif args.delete is not None:
            # we have some notes to delete
            note_ids =  args.delete
            
            # --> theyre integers, so clean up the input
            valid_ids = [int(x) for x in note_ids if x.isdigit()]
            rejected = [x for x in note_ids if not x.isdigit()]

            if rejected:
                study.console.warning("Ignoring ids '{}'. Note ids must be numeric".format(", ".join(rejected)))
            # delete the note
            study.delete_notes(valid_ids, prompt=args.prompt)
        
        else:
            # we view the entire note history
            #text = study.print_notes()
            if case is None:
                text = study.format_notes()
                #text = utils.cstr("THIS IS A TESST", fg=4)
            else:
                # we want just the case
                text = case.format_notes()

            utils.less(text)
    
    def copy(self):
        """ Copies a study or case """

        args = self.args

        # check the arguments are as we expect them
        assert isinstance(args.source, list)
        assert isinstance(args.destination, list)
        source = args.source.pop()
        abssource = os.path.abspath(source)
        destination = args.destination.pop()

        # check the source exists and is a cfmt study
        try:
            study = studies.load_study(source)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        moving_study = True if study.path == abssource else False
        moving_case = False

        if not moving_study:
            # check if it's a case
            path = abssource
            while path != study.path:
                # --> do we have a case 
                try:
                    case = cases.load_case(path, study)
                except exceptions.CaseDoesNotExist:
                    msg = "Directory '{}' is not a case or study"
                    msg = msg.format(source, study.name, study.path)
                    moving_case = False
                    print(msg)
                    break
                except IOError as err:
                    utils.print_exception_info(str(err))
                    sys.exit(0)
                else:
                    case_abs_path = os.path.join(study.path, case.path)
                    if abssource == case_abs_path:
                        moving_case = True
                        break
                    else:
                        moving_case = False
                        continue
                finally:
                    oldpath, path = path, os.path.dirname(path)
        if moving_study:
            raise NotImplementedError
        elif moving_case:
            print("Preparing to copy case '{}'".format(case.name))
            try:
                case.copy(destination, args=args)
            except FileExistsError as err:
                utils.print_exception_info(str(err))
                sys.exit(0)


    def move(self):
        """ Moves a case or study """

        args = self.args

        assert isinstance(args.source, list)
        assert isinstance(args.destination, list)

        source = args.source.pop()
        abssource = os.path.abspath(source)
        destination = args.destination.pop()

        # check the source exists and is a cfmt study
        try:
            study = studies.load_study(source)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        moving_study = True if study.path == abssource else False
        moving_case = False

        if not moving_study:
            # check if it's a case
            path = abssource
            while path != study.path:
                # --> do we have a case 
                try:
                    case = cases.load_case(path, study)
                except exceptions.CaseDoesNotExist:
                    msg = "Directory '{}' is unmanaged but placed within study {} located at '{}'"
                    msg = msg.format(source, study.name, study.path)
                    moving_case = False
                    print(msg)
                    break
                except IOError as err:
                    utils.print_exception_info(str(err))
                    sys.exit(0)
                else:
                    case_abs_path = os.path.join(study.path, case.path)
                    if abssource == case_abs_path:
                        moving_case = True
                        break
                    else:
                        moving_case = False
                        continue
                finally:
                    oldpath, path = path, os.path.dirname(path)

        if moving_study:
            try:
                study.move(destination, copy_files=not args.no_copy_files)
            except FileExistsError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)

        if moving_case:
            try:
                case.move(destination, args=args)
            except FileExistsError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)

    def log(self):
        """ Views the log """
        args = self.args
    
        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        if args.follow:
            # we tail it
            try:
                utils.tail(study.log_fname)
            except KeyboardInterrupt:
                print("")
                pass
        else:
            # load the log file
            with open(study.log_fname, 'r') as f:
                lines = f.readlines()
            
            if args.reverse:
                lines.reverse()

            text = "".join(lines)
                # reverse it
            # reverse if -r 
            utils.less(text)
    
    def results(self):
        """ View results """
        args = self.args

        path = os.getcwd()
        case = None
        runs = None

        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        # parse the command line selection if there is one
        if args.select:
            # we've been passed something by name or id
            _cases = study.get_cases(select=args.select)
        elif args.status:
            # we're after run with select status
            _cases = study.get_cases(with_run_status=args.status)
        else:
            # we assume it's the current directory
            try:
                _cases = [cases.load_case(path, study)]
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(path)
                utils.print_exception_info(msg)
                sys.exit(1)
            except IOError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)
        
        
        # we should have a list of cases, we need to translate that to runs
        runs = []
        if _cases:
            for case in _cases:
                # do we have runs
                if not case.runs:
                    case.set_last_run()

                if not case.runs:
                    # case has not been run yet, so nothin to show
                    print("Case '{}' has no runs".format(case.name))
                    sys.exit(0)
                else:
                    # add 
                    runs += case.runs
        else:
            msg = "No cases found"
            print(msg)
            sys.exit(0)

        # check we still have something left
        if not runs:
            print("Cases do not have any runs")
            sys.exit(0)
        else:
            # add the last job if we don't have it
            for run in runs:
                if not run.jobs:
                    run.set_last_job()

        # if maxlen is 0, default to 0
        args.last_n = None if args.last_n == 0 else args.last_n.pop()
        # do we want the monitor app rather than just plots
        monitor_app = True if (args.live and (not args.residuals and not args.mass_flow and not args.monitor and not (args.stdout or args.stderr))) else False
        
        # have we set the local only flag
        local_only = args.local_only

        if args.list_monitors:
            try:
                for run in runs:
                    print(" " + utils.cstr("==>", fg='green', attr=['bold']) + " run: " + utils.cstr(run.name, fg='white', attr=['bold']))
                    if run.is_archived:
                        print("  " + utils.cstr("->", fg='red') + " Run archived, skipping")
                        continue
                    monitors = run.code.get_monitor_files(run.get_run_dir(local_only=local_only))
                    if monitors:
                        for monitor in monitors:
                            mon_fname = os.path.relpath(monitor, run.get_run_dir(local_only=local_only))
                            print("  " + utils.cstr("-->", fg='yellow') + " " + mon_fname)
                    else:
                        print("  " + utils.cstr("->", fg='red') + " No monitors found")
                    print()
            except FileNotFoundError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)
            else:
                sys.exit(0)
        elif monitor_app:
            from cfmt import plotting
            try:
                plotting.live_monitor_app(runs, args)
            except FileExistsError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)

        elif args.monitor is not False or args.mass_flow:
            # warn that we only support 1 run, unless the compare option is selected
            if args.compare:
                if len(runs) < 2:
                    self.console.error("Two runs needed for --compare")
                    sys.exit(1)
            elif len(runs) > 1:
                self.console.warning("Monitors only supports 1 run. Loading '%s'",runs[0].name)

            run = runs.pop()
            if run.is_archived:
                self.console.error("Run '%s' is archived", run.name)
                sys.exit(1)

            if args.monitor is None:
                # prompt for selection
                monitor = run.select_monitors(local_only=local_only)
                if not monitor:
                    sys.exit(0)
                else:
                    monitor = [monitor]
            else:
                monitor = [args.monitor]
            
            try:
                compare_with = runs if runs else None
                run.display_monitors(
                    live=args.live, 
                    monitors=monitor, 
                    mass_flow=args.mass_flow, 
                    compare_with=compare_with,
                    local_only=local_only
                )
            except FileNotFoundError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)
        elif args.plot is not False:
            if len(runs) > 1:
                if args.plot is None:
                    self.parser.error("A plot file is required when comparing runs")
                study.comparison_plot(runs, args.plot, local_only=local_only)
                sys.exit(0)
            
            run = runs.pop()
            if run.is_archived:
                self.console.error("Run '%s' is archived", run.name)
                sys.exit(1)
            
            if args.plot is None:
                # prompt for selection
                plot = run.select_plots(local_only=local_only)
                if not plot:
                    sys.exit(0)
                else:
                    plot = [plot]
            else:
                plot = [os.path.join(run.get_run_dir(local_only=local_only), args.plot)]
            
            try:
                run.display_plot(plot)
            except FileNotFoundError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)

        elif args.plot_sequence:
            if len(runs) > 1:
                self.console.warning("Monitors only supports 1 run. Loading '%s'",runs[0].name)
            
            run = runs.pop()
            if run.is_archived:
                self.console.error("Run '%s' is archived", run.name)
                sys.exit(1)
            
            plot = [os.path.join(run.get_run_dir(local_only=local_only), args.plot_sequence[0])]

            try:
                run.display_plot(plot, file_sequence=True)
            except FileNotFoundError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)
            

        elif args.residuals or args.timeres:
            # warn that we only support 1 run 
            if len(runs) > 1:
                self.console.warning("Monitors only supports 1 run. Loading '%s'",runs[0].name)
            
            run = runs.pop()
            if run.is_archived:
                self.console.error("Run '%s' is archived", run.name)
                sys.exit(1)
            
            try:
                run.display_monitors(
                    live=args.live, 
                    residuals=args.residuals, 
                    mass_flow=args.mass_flow, 
                    timeres=args.timeres,
                    maxlen=args.last_n,
                    local_only=local_only,
                )
            except FileExistsError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)

        elif args.stdout or args.stderr:
            # warn that we only support 1 run 
            if len(runs) > 1:
                self.console.warning("Output can only be viewed for 1 run. Loading '%s'",runs[0].name)
            
            run = runs.pop()
            if run.is_archived:
                self.console.error("Run '%s' is archived", run.name)
                sys.exit(1)
            
            try:
                if args.stdout:
                    if len(run.jobs) > 1:
                        self.console.warning("Output can only be viewed for 1 job. Loading '%s'",str(run.jobs[0].id))
                    run.display_stdout(live=args.live, local_only=local_only)
                elif args.stderr:
                    run.display_stderr(live=args.live, local_only=local_only)
            except FileNotFoundError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)
        elif args.convert is not False:
            run = runs.pop()
            if run.is_archived:
                self.console.error("Run '%s' is archived", run.name)
                sys.exit(1)
            
            try:
                data_format = args.convert.pop()
                print(" -> Converting {} output to {} ...".format(run.code.name, data_format), end='\r')
                self.logger.info("Converting %s output to %s", run.code.name, data_format)
                run.convert_output(
                    args,
                    fmt=data_format,
                )
            except NotImplementedError as err:
                print(" -> Converting {} output to {} ... FAILED".format(run.code.name, data_format), end='\n')
                utils.print_exception_info(str(err))
                sys.exit(1)
            else:
                print(" -> Converting {} output to {} ... OK".format(run.code.name, data_format), end='\n')

        elif args.tavg_info is not None:
            try:
                for run in runs:
                    if run.is_archived:
                        print(" -> Run '{}' archived, SKIPPING".format(run.name))
                        continue
                    run.print_tavg_info()
                    print("")

            except NotImplementedError as err:
                utils.print_exception_info(err)
                sys.exit(1)
        elif args.tout_info is not None:
            try:
                for run in runs:
                    if run.is_archived:
                        print(" -> Run '{}' archived, SKIPPING".format(run.name))
                        continue
                    run.print_tout_info()
                    print("")

            except NotImplementedError as err:
                utils.print_exception_info(err)
                sys.exit(1)
        
        elif args.batch:
            raise NotImplementedError("Batch post processing not implemented yet")
        else:
            # load the run and determine which program
            
            # default is we view the results
            try:
                run = runs.pop()
                    
                if run.is_archived:
                    self.console.error("Run '%s' is archived", run.name)
                    sys.exit(1)

                if args.view is not None:
                    post_name = args.view.pop()
                else:
                    post_name = config.configtool.get(run.code.name, 'post_utility')
            
                if post_name == run.code.name:
                    # we're loading the code
                    print("Loading results in {} ...".format(post_name))
                    run.code.results(run)
                else:
                    post_prog = post.Post(post_name)
                    # preprare the run for the post_program
                    run.prepost(post_prog, args)
                
                    print("Loading results in {} ...".format(post_name))
                    post_prog.run()

            except FileExistsError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)

    
    def export(self):
        """
        Generates readable output
        """
        args = self.args

        # current directory
        path = os.getcwd()

        # check the study is valid
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        study.export(args)

    def archive(self):
        """
        Archives runs(s)
        """
        args = self.args

        # argument handling
        r_ops = ["create", "deposit", "withdraw"]   # operations which expect a RUN
        ra_ops = ["download", "delete", "verify"]   # operations which expect either a RUN or ID
        a_ops = ["list_contents", "restore"]        # operations which expect an ID
        o_ops = ["init"]                            # operations which operate once
        given = []
        mutually_exclusive = r_ops + ra_ops + a_ops + o_ops
        # check for mutually exclusive arguments
        check = [getattr(args,x) is not None for x in mutually_exclusive]
        if utils.multiple_true(check):
            for i, val in enumerate(check):
                if val:
                    given.append(mutually_exclusive[i])

            self.parser.error("{} are mutually exclusive".format(", ".join(("--" + str(i) for i in given))))
        else:
            # what was passed is the only True value
            given = mutually_exclusive[check.index(True)]
        
        # default is no RUNS or IDS
        args.RUNS, args.IDS = ([],[])
        # set the mode for convinience
        args.archive_mode = given

        _args = getattr(args, given)
        # for r_ops, we expect a list of RUNS - checked later when we search
        if given in r_ops and not args.all:
            if not getattr(args, given):
                self.parser.error("No RUNS passed to {}".format("--"+given))
            setattr(args, "RUNS", _args)
        elif given in a_ops:
            # we expect a single int
            try:
                _ints = [int(i) for i in _args]
            except ValueError:
                self.parser.error("IDS are integers")
            else:
                setattr(args, "IDS", _ints)
        elif given in o_ops:
            # we don't expect anything
            pass
        else:
            # we can have IDS, RUNS or nothing
            # we use the fact that IDS _must_ be all integers and we can't mix
            if _args:
                try:
                    _ints = [int(i) for i in _args]
                except ValueError:
                    # they're runs
                    setattr(args, "RUNS", getattr(args, given))
                else:
                    # they're IDS, and now sanitized
                    setattr(args, "IDS", _ints)
            elif not args.all:
                self.parser.error("No RUNS/IDS selected. Pass --all to operate on all")
        
        # if we have a type make sure its not a list
        if args.type is not None:
            args.type = args.type.pop()

        self.logger.debug("Archive arguments parsed: {}".format(str(args)))

        # check the study is valid
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.DBVersionError as err:
            print(err)
            sys.exit(1)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        study.archive(args, cwd=os.getcwd())
    
    def srcdiff(self):
        """
        Loads a diff between two sets of src's
        """
        args = self.args
        
        path = os.getcwd()
        case = None
        runs = None
        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        # parse the command line selection if there is one
        if args.select:
            # we've been passed something by name or id
            _cases = study.get_cases(select=args.select)
        else:
            # we assume it's the current directory
            try:
                _cases = [cases.load_case(path, study)]
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(path)
                utils.print_exception_info(msg)
                sys.exit(1)
            except IOError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)
        
        # we should have a list of cases, we need to translate that to runs
        runs = []
        if _cases:
            for case in _cases:
                # do we have runs
                if not case.runs:
                    case.set_last_run()

                if not case.runs:
                    # case has not been run yet, so nothin to show
                    print("Case '{}' has no runs".format(case.name))
                    sys.exit(0)
                else:
                    # add 
                    runs += case.runs
        else:
            msg = "No cases found"
            print(msg)
            sys.exit(0)

        # check we still have something left
        if not runs:
            print("Cases do not have any runs")
            sys.exit(0)
        else:
            # add the last job if we don't have it
            for run in runs:
                if not run.jobs:
                    run.set_last_job()

        if len(runs) == 1:
            # then we're diffing with the case level src
            # location of src is code_dependent
            run = runs[0]
            # have we specified a job
            src1 = run.code.get_src(run)
            src2 = run.code.get_src(run, case_src=True)
        else:
            run1 = runs[0]
            run2 = runs[1]
            src1 = run1.code.get_src(run1)
            src2 = run2.code.get_src(run2)
            
        if args.difftool is None:
            difftool = config.configtool.get('defaults', 'difftool', fallback="vim")
        else:
            difftool = args.difftool[0]
        
        if difftool == "vim":
            _arg = " ".join(["DirDiff",src1,src2]) 
            cmd = [difftool, "-c", '"' + _arg + '"']
        else:
            cmd = [difftool, src1, src2]
        utils.run(cmd)
    
    def post(self):
        """
        Archives runs(s)
        """
        args = self.args
        
        path = os.getcwd()
        case = None
        runs = None

        # load the study
        try:
            study = studies.load_study(profile=args.profile)
        except exceptions.NotAStudy as err:
            utils.print_exception_info(str(err), no_log=True)
            sys.exit(1)

        # parse the command line selection if there is one
        if args.select:
            # we've been passed something by name or id
            _cases = study.get_cases(select=args.select)
        else:
            # we assume it's the current directory
            try:
                _cases = [cases.load_case(path, study)]
            except exceptions.CaseDoesNotExist:
                msg = "'{}' is not a valid case directory"
                msg = msg.format(path)
                utils.print_exception_info(msg)
                sys.exit(1)
            except IOError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)
        
        # we should have a list of cases, we need to translate that to runs
        runs = []
        if _cases:
            for case in _cases:
                # do we have runs
                if not case.runs:
                    case.set_last_run()

                if not case.runs:
                    # case has not been run yet, so nothin to show
                    print("Case '{}' has no runs".format(case.name))
                    sys.exit(0)
                else:
                    # add 
                    runs += case.runs
        else:
            msg = "No cases found"
            print(msg)
            sys.exit(0)

        # check we still have something left
        if not runs:
            print("Cases do not have any runs")
            sys.exit(0)
        else:
            # add the last job if we don't have it
            for run in runs:
                if not run.jobs:
                    run.set_last_job()

        # check 
        mfile = args.module.pop()
        print(f" -> Loading user module from '{mfile}' ... ", end="\r")
        user_post = post.UserPostModule(mfile)
        print(f" -> Loading user module from '{mfile}' ... " + utils.OK, end="\n")
        func = args.user_func.pop() if isinstance(args.user_func, list) else args.user_func
        func_args = args.args
        print(f" -> Executing on selected runs ... ")

        # pass all the runs to the module, rather than processing separately
        user_post.run(runs, func, func_args)
