"""
 cfmt: A Computational Fluids Management Tool

 db/models.py: Table (models) definitions
"""

import peewee as pw
from cfmt import config as config
import uuid
# db is defined as a proxy, since it can change at run-time
DB_PROXY = pw.Proxy()
DB_VERSION = config.DBVERSION

# for when we need to initialize
def initialize(database):
    """ Initializes the database at runtime"""
    DB_PROXY.initialize(database)

class BaseModel(pw.Model):
    """ Base model which specifies the database to use """
    class Meta:
        """ Meta class to store database """
        database = DB_PROXY


class Study(BaseModel):
    """ Study Model """
    name = pw.CharField()
    path = pw.TextField()
    created = pw.DateTimeField()
    last_modified = pw.DateTimeField()
    description = pw.TextField()
    project_name = pw.CharField()
    creator = pw.CharField()
    if DB_VERSION >= 3:
        uuid = pw.UUIDField(default=uuid.uuid4)

class Code(BaseModel):
    """ Code Model """
    name = pw.CharField()
    identifier = pw.CharField()
    url = pw.CharField()
    description = pw.CharField()
    maintainer = pw.CharField()
    path = pw.CharField(null=True)

class Case(BaseModel):
    """ Case Model to implement the case table """
    name = pw.CharField()
    path = pw.CharField()
    study = pw.ForeignKeyField(Study, related_name="has_cases")
    code_db = pw.ForeignKeyField(Code, null=True, related_name="used_in_cases")
    #status = pw.CharField()
    created = pw.DateTimeField()
    last_modified = pw.DateTimeField()
    creator = pw.CharField()
    if DB_VERSION >= 3:
        description = pw.CharField(null=True)

class Run(BaseModel):
    """ Run Model to implement the run table """
    path = pw.CharField()
    name = pw.CharField()
    status = pw.SmallIntegerField()
    case = pw.ForeignKeyField(Case, related_name="has_runs")
    reason = pw.TextField(null=True)
    code_db = pw.ForeignKeyField(Code, null=True, related_name="used_in_runs")
    is_dev = pw.BooleanField(default=False)
    staging_mode = pw.FixedCharField(max_length=10)
    in_staging = pw.BooleanField()
    transfer_mode = pw.FixedCharField(max_length=10, null=True)
    staging_dir = pw.CharField(null=True)
    submit_host = pw.CharField(null=True)
    created = pw.DateTimeField()
    last_modified = pw.DateTimeField()
    code_src_ref = pw.FixedCharField(null=True, max_length=40)
    case_src_ref = pw.FixedCharField(null=True, max_length=40)
    if DB_VERSION >= 3:
        is_archived = pw.IntegerField(default=0)
    
    if DB_VERSION >= 5:
        tags = pw.CharField(default="||")

#        batch_system = pw.FixedCharField(null=True, max_length=8)
#        batch_queue = pw.CharField(null=True)
#        run_command = pw.TextField(null=True)
#        job_number = pw.BigIntegerField(null=True)
#        submitted = pw.DateTimeField(null=True)
#        started = pw.DateTimeField(null=True)
#        finished = pw.DateTimeField(null=True)

if DB_VERSION >= 3:    
    class Archive(BaseModel):
        """ Archive Model to implement the archive table """
        # name column
        if DB_VERSION >= 4: 
            name = pw.CharField(default='')
        
        # path/filepath
        if DB_VERSION < 4:
            filepath = pw.CharField()
        else:
            path = pw.CharField()

        run = pw.ForeignKeyField(Run, backref="has_archives")
        hostname = pw.CharField()
        size = pw.BigIntegerField() 
        
        if DB_VERSION >= 4: 
            type = pw.FixedCharField(max_length=32, default='tar')

        # hash/filehash
        if DB_VERSION < 4:
            filehash = pw.FixedCharField(null=True, max_length=32)
        else:
            hash = pw.CharField(null=True)

        created = pw.DateTimeField()
        last_modified = pw.DateTimeField()
        description = pw.CharField(null=True)

    

class Job(BaseModel):
    """ Job Model """
    name = pw.CharField()
    batch_system = pw.FixedCharField(null=True, max_length=8)
    batch_queue = pw.CharField(null=True)
    run = pw.ForeignKeyField(Run, related_name="has_jobs", on_delete="CASCADE")
    sys_command = pw.CharField(null=True)
    working_dir = pw.CharField()
    job_id = pw.BigIntegerField(null=True)
    submit_host = pw.CharField(null=True)
    execution_host = pw.CharField(null=True)
    status = pw.SmallIntegerField()
    user = pw.CharField()
    submitted = pw.DateTimeField(null=True)
    started = pw.DateTimeField(null=True)
    finished = pw.DateTimeField(null=True)
    pe = pw.CharField(null=True)
    slots = pw.SmallIntegerField(null=True)
    exit_status  = pw.SmallIntegerField(null=True)
    cpu_time = pw.FloatField(null=True)
    vmem = pw.BigIntegerField(null=True)
    mem = pw.BigIntegerField(null=True)
    rusage = pw.CharField(null=True)
    created = pw.DateTimeField()
    last_modified = pw.DateTimeField()
    code_src_ref = pw.FixedCharField(null=True, max_length=40)
    case_src_ref = pw.FixedCharField(null=True, max_length=40)

    if DB_VERSION > 5 and DB_VERSION < 9:
        batch_options = pw.CharField(default="{}")

    if DB_VERSION > 6:
        allocation_units = pw.FloatField(null=True)

    if DB_VERSION > 7:
        type = pw.SmallIntegerField(default=0)
        stdout = pw.CharField(null=True)
        stderr = pw.CharField(null=True)
    
    if DB_VERSION > 8:
        options = pw.CharField(default="{}")

class Parameter(BaseModel):
    """ Parameter Model """
    name = pw.CharField()
    type = pw.CharField()
    units = pw.CharField()
    value = pw.FloatField()

class Notes(BaseModel):
    """ Notes Model """
    case = pw.ForeignKeyField(Case, related_name="has_notes")
    last_modified = pw.DateTimeField()
    created = pw.DateTimeField()
    text = pw.TextField()
