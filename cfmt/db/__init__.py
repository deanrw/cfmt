#!/usr/bin/env python
"""
   Storage module

   Provides an abstraction layer for storing data

   @author: Dean Wilson
   @since: 0.1
   @package cft
"""