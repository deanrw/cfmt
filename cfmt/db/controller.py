"""
    cfmt: A Computational Fluids simulations management Tool

    db/controller.py: implements an abstract database controller

    We utilize the peewee ORM interface    
"""
import os
import sys
import shutil
import importlib
import peewee as pw
from datetime import datetime
from cfmt import config
from cfmt import exceptions
from cfmt import utils
import cfmt.db.models as db_models

def get_database_file_path(path=None):
    """ returns the path to the database file """
    path = os.getcwd if path is None else path
    return os.path.join(path, config.CFMT_DIR_NAME, config.DATABASE['filename'])

def load_database(study_path, type=None):
    """ Attempts to load the database by automatically detects type """
    db_file = get_database_file_path(path=study_path)
    # does the file exists
    if not os.path.isfile(db_file):
        raise IOError("Database file does not exist")
    
    # can we detect the type
    if type is None:
        for db_type in DBController.AVAILABLE_DB:
            _db = DBController(db_type, study_path)
            try:
                _db.connect()
            except:
                continue
            else:
                db = _db
                break
    else:
        db = DBController(type, study_path)
        db.connect()

    # attempt to read tables as a test 
    tables = db.db.get_tables()
    db.db.close()
    return db


class DBController(object):
    """ Controller for the DB """

    # Available DB Engines
    AVAILABLE_DB = {
        'sqlite': pw.SqliteDatabase,
    }

    # Models to include
    MODELS = ['Study', 'Case', 'Run', 'Code', 'Notes', 'Job', 'Archive']

    SUPPORTED_SORT = ["order_by", "group_by"]

    def __init__(self, db_type, study_path):
        """ Initializes the database object """
        # set db filename
        self.db_file = get_database_file_path(study_path)

        # store the study path for reference
        self.study_path = study_path

        # store the version
        self.version = config.DBVERSION

        # connected
        self.connected = False

        if db_type in self.AVAILABLE_DB:
            _db = self.AVAILABLE_DB[db_type]
            # initialize the object, but defer initilization
            self.db = _db(self.db_file)
            
        else:
            raise NotImplementedError()

        self.db_backed_up = False
        self.db_backup = self.db_file + ".bak"
    
    def connect(self, **kwargs):
        """ Connect to the database """
        # return if we already have a connection
        if not self.connected:
            # set the correct db for the models
            db_models.initialize(self.db)
            self.db.connect()
            self.connected = True

    def close(self):
        """ Closes the db connection """
        if self.connected:
            self.db.close()
            self.connected = False
    
    def reload(self):
        """ Reloads the db connection """
        self.close()
        self.connect()

    def get_pragma(self, pragma):
        """ Returns a PRAGMA value """
        return next(self.db.execute_sql('PRAGMA ' + pragma + ';'))[0]

    def set_pramga(self, expression):
        """ Sets a PRAGMA value """
        self.db.execute_sql('PRAGMA ' + expression + ';')
    
    def get_version(self):
        """ Return the version number of the database """
        return self.get_pragma('user_version')

    def set_version(self, version):
        """ Sets the version number of the database """
        self.set_pramga('user_version=' + str(version))
    
    def create(self):
        """ Creates a new database """
        # check if we have a connection
        if not self.connected:
            self.connect()

        for model in self.MODELS:
            # create our tables
            _model = getattr(db_models, model)
            _model.create_table()

        # save the version
        self.set_version(self.version)
    
    def info(self):
        """ Prints information about the database """
        pass

    def delete(self, db_record, cascade=False):
        """ 
        Deletes a record
        """
        db_record.delete_instance(recursive=cascade)

    def delete_notes(self, ids=None):
        """
        Delete notes
        """
        query = db_models.Notes.delete()
        if ids is not None:
            assert isinstance(ids, list)
            query = query.where(db_models.Notes.id << ids)

        # execute the query
        try:
            rows = query.execute()
        except pw.PeeweeException as err:
            raise exceptions.DeleteFailed(str(err))
        else:
            return rows


    def get_notes(self, case=None, limit=None, ids=None, order_by=None, order="DESC"):
        """ Returns notes from the db """
        notes = db_models.Notes.select()
        
        if case is not None:
            #notes = db_models.Notes.select().where(case == case).order_by(+db_models.Notes.last_modified).limit(limit)
            notes = notes.where(db_models.Notes.case == case)

        if ids is not None:
            notes = notes.where(db_models.Notes.id << ids)

        if order_by is not None:
            _order_by = getattr(db_models.Notes, order_by)

            if order == "ASC":
                notes = notes.order_by(_order_by.asc())
            else:
                notes = notes.order_by(_order_by.desc())

        if limit is not None:
            notes = notes.limit(limit)

        return notes 

    def get_study(self, id=1):
        """ Returns a study record from the database """
        return db_models.Study.get(db_models.Study.id == id)

    def get_studies(self):
        """ Returns all the study records """
        return db_models.Study.select()

    def new_study(self, **kwargs):
        """ Returns a new study record """
        return db_models.Study(**kwargs)

    def new_case(self, **kwargs):
        """ Returns a new case record """
        return db_models.Case(**kwargs)
    
    def new_run(self, **kwargs):
        """ Returns a new run record """
        return db_models.Run(**kwargs)
    
    def new_note(self, **kwargs):
        """ Returns a new run record """
        return db_models.Notes(**kwargs)

    def new_job(self, **kwargs):
        """ Returns a new job record """
        return db_models.Job(**kwargs)

    def get_run(self, path=None, run_id=None):
        """ Returns a run record from the database """
        if path is not None:
            try:
                run = db_models.Run.get(db_models.Run.path == path)
            except pw.DoesNotExist:
                raise exceptions.RunDoesNotExist
            else:
                return run
        elif run_id is not None:
            try:
                run = db_models.Run.get(db_models.Run.id == run_id)
            except pw.DoesNotExist:
                raise exceptions.RunDoesNotExist
            else:
                return run

    def get_case(self, path=None, case_id=None):
        """ Returns a case record from the database """
        if path is not None:
            try:
                case = db_models.Case.get(db_models.Case.path == path)
            except pw.DoesNotExist:
                raise exceptions.CaseDoesNotExist
            else:
                return case
        elif case_id is not None:
            try:
                case = db_models.Case.get(db_models.Case.id == case_id)
            except pw.DoesNotExist:
                raise exceptions.CaseDoesNotExist
            else:
                return case
    def get_cases(self, paths=None, case_ids=None, names=None, tags=None,
                  has_run_ids=None, has_run_names=None,
                  order_by=None, order='ASC', limit=None, group_by=None):
        """ Returns case records from the database """
        """ Return run query from the database """
        cases = []
        model = db_models.Case
        query = model.select()

        if paths is not None:
            query = query.where(model.path << paths)
        if case_ids is not None:
            query = query.where(model.id << case_ids)
        if names is not None:
            query = query.where(model.name << names)

        if has_run_ids is not None:
            assert isinstance(has_run_ids, list)
            query = query.distinct().join(db_models.Run).where(db_models.Run.id << has_run_ids)
        
        if has_run_names is not None:
            assert isinstance(has_run_names, list)
            query = query.distinct().join(db_models.Run).where(db_models.Run.name << has_run_names)


        #if has_run_staging is not None:


        if group_by is not None:
            _group_by = getattr(model, group_by) 
            query = query.group_by(_group_by)

        if order_by is not None:
            _order_by = getattr(model, order_by)

            if order == 'ASC':
                query = query.order_by(_order_by.asc())
            else:
                query = query.order_by(_order_by.desc())

        if limit is not None:
            assert isinstance(limit, int)
            query = query.limit(limit)

        return query

        try:
            if paths is not None:
                cases = db_models.Case.select().where(db_models.Case.path << paths)
            elif case_ids is not None:
                cases = db_models.Case.select().where(db_models.Case.id << case_ids)
            elif names is not None:
                cases = db_models.Case.select().where(db_models.Case.name << names)
        except pw.DoesNotExist:
            utils.print_exception_info("Error loading case")
        except:
            raise
        
        return cases

    
    def get_runs(self, query=None, **kwargs):
        """ 
        Return run query from the database 

        Supported kwargs:
        case
        status
        names
        in_staging
        has_job_ids
        has_archives
        tags
        job_type
        
        """
        model = db_models.Run


        if query is None:
            query = model.select()
        
        if "case" in kwargs and kwargs["case"] is not None:
            case = kwargs["case"]
            assert isinstance(case, db_models.Case)
            query = query.where(model.case == case)

        if "status" in kwargs and kwargs["status"] is not None:
            query = query.where(model.status << kwargs["status"])

        if "names" in kwargs and kwargs["names"] is not None:
            query = query.where(model.name << kwargs["names"])
        
        if "in_staging" in kwargs and kwargs["in_staging"] is not None:
            in_staging = kwargs["in_staging"]
            query = query.where(model.in_staging == int(in_staging))
        
        if "has_job_ids" in kwargs and kwargs["has_job_ids"] is not None:
            job_ids = kwargs["has_job_ids"]
            assert isinstance(job_ids, list)
            query = query.distinct().join(db_models.Job).where(db_models.Job.id << job_ids)
        
        if "has_archives" in kwargs and kwargs["has_archives"] is not None:
            archive_ids = kwargs["has_archives"]
            assert isinstance(archive_ids, list)
            query = query.distinct().join(db_models.Archive).where(db_models.Archive.id << archive_ids)

        if "tags" in kwargs and kwargs["tags"] is not None:
            tags = kwargs["tags"]
            assert isinstance(tags, list)
            for tag in tags:
                _tag = "|{}|".format(tag)
                query = query.where(model.tags.contains(_tag))

        # handle sorts
        query = self._sort(query, model, **kwargs)

        # handle limit
        query = self._limit(query, model, **kwargs)

        return query
    
    # TODO: This function is deprecated and has changed named
    def get_runs_old(self, case=None, status=None, names=None, job_number=None, 
                       has_job_ids=None, in_staging=False, recent=False, 
                       with_dev=False):
        """ Returns run records from the database """
        runs = []
        
        try:
            runs = db_models.Run.select()
            if case is not None:
                runs = runs.where(db_models.Run.case == case)
            if status is not None:
                runs = runs.where(db_models.Run.status << status)
            if names is not None:
                runs = runs.where(db_models.Run.name << names)
            # TODO DEPRECATED
            if job_number is not None:
                runs = runs.where(db_models.Run.job_number << job_number)
            if in_staging:
                runs = runs.where(db_models.Run.in_staging == 1)
            if has_job_ids is not None:
                runs = runs.distinct().join(db_models.Job).where(db_models.Job.id << has_job_ids)
        except:
            raise
        
        return runs

    def get_jobs(self, run=None, status=None, names=None, job_id=None, batch_system=None,
                       ids=None, order_by=None, order="ASC", limit=None, has_run=None,
                       group_by=None, recent=False, with_dev=False, type_=None, submit_host=None):
        """ Returns job records from the database """

        query = db_models.Job.select()
        if run is not None:
            query = query.where(db_models.Job.run == run)
        if status is not None:
            query = query.where(db_models.Job.status << status)
        if names is not None:
            query = query.where(db_models.Job.name << names)
        if job_id is not None:
            query = query.where(db_models.Job.job_id << job_id)
        if batch_system is not None:
            query = query.where(db_models.Job.batch_system << batch_system)
        if ids is not None:
            query = query.where(db_models.Job.id << ids)
        if has_run is not None:
            assert isinstance(has_case, db_models.Run)
            query = query.where(db_models.Job.job_id << has_run.has_runs)
        if type_ is not None:
            query = query.where(db_models.Job.type << type_)
        if submit_host is not None:
            query = query.where(db_models.Job.submit_host << submit_host)

        if group_by is not None:
            _group_by = getattr(db_models.Job, group_by)
            query = query.group_by(_group_by)
        
        if order_by is not None:
            _order_by = getattr(db_models.Job, order_by)

            if order == "ASC":
                query = query.order_by(_order_by.asc())
            else:
                query = query.order_by(_order_by.desc())
        
        if recent:
            query = query.join(db_models.Run).where(db_models.Run.is_dev == with_dev).group_by(db_models.Run.id).having(pw.fn.Max(db_models.Job.finished)).order_by(db_models.Job.finished.desc())
        if limit is not None:
            assert isinstance(limit, int)
            query = query.limit(limit)

        return query

    def _limit(self, query, model, **kwargs):
        """ Handles limit """
        if "limit" in kwargs:
            limit = kwargs["limit"]
            assert isinstance(limit, int)
            query = query.limit(limit)
        
        return query
    
    def _sort(self, query, model, **kwargs):
        """ Handles ordering """

        # apply in the order provided
        for key, value in kwargs.items():
            if key not in self.SUPPORTED_SORT:
                continue 
            
            if key == "group_by":
                group_by = getattr(model, value)
                query = query.group_by(group_by)
            elif key == "order_by":
                order_by = getattr(model, value)
                if "order" in kwargs and kwargs["order"] == "ASC":
                    query = query.order_by(order_by.asc())
                else:
                    query = query.order_by(order_by.dsc())
        
        return query

    
    def get_archives(self, query=None, **kwargs):
        """ Returns archive records from the database """
        model = db_models.Archive
        
        if query is None:
            query = model.select()
        
        if "ids" in kwargs and kwargs["ids"] is not None:
            ids = kwargs["ids"]
            assert all((isinstance(id, int) for id in ids))
            query = query.where(model.id << ids)
        
        if "run" in kwargs and kwargs["run"] is not None:
            run = kwargs["run"]
            assert isinstance(run, db_models.Run)
            query = query.where(model.run == run)

        if "type" in kwargs and kwargs["type"] is not None:
            _type = kwargs["type"]
            assert isinstance(_type, str)
            query = query.where(model.type == _type)

        # handle sorts
        query = self._sort(query, model, **kwargs) 

        # handle limit
        query = self._limit(query, model, **kwargs)

        return query


    def get_last_job(self, run=None, batch_system=None, type_=None):
        """ Returns the most recent job from the database, optionally in the provided run or batch system """
        if not isinstance(type_, list):
            type_ = [type_]
        try:
            job = self.get_jobs(run=run, batch_system=batch_system, order_by="last_modified", order="DESC", limit=1, type_=type_)
        except:
            raise
        return job

    def get_last_run(self, case=None, is_dev=None):
        """ Returns the most recent run from the database, optionally in the provided case """

        query = db_models.Run.select()
        if case is not None:
            assert isinstance(case, db_models.Case)
            query = query.where(db_models.Run.case == case.id)
            # we add a where case == case clause
        if is_dev is not None:
            assert isinstance(is_dev, bool)
            query = query.where(db_models.Run.is_dev == is_dev)
        
        # order by last modified and limit to 1
        query = query.order_by(-db_models.Run.last_modified).limit(1)

        return query

    def get_number_of_cases(self, db_record):
        """ Given a study db_record, return the number of cases """
        assert isinstance(db_record, db_models.Study)
        return db_record.has_cases.count()
    
    def get_all_cases(self, db_record):
        """ Given a study db_record, select all cases """
        assert isinstance(db_record, db_models.Study)
        
        cases = []
        
        for case in db_models.Case.select().where(db_models.Case.study == db_record.id):
            cases.append(case)

        return cases
    
    def get_number_of_runs(self, db_record):
        """ Given a study db_record, return the number of runs """
        if isinstance(db_record, db_models.Case):
            return db_record.has_runs.count()

    def get_code_names(self, db_record):
        """ Given a study db_record, return a list of codes associated with the study """

        # --> if we have a study, then return a list of names in the code table
        if isinstance(db_record, db_models.Study):
            names = []
            for code in db_models.Code.select():
                names.append(code.name)
            
            return names
        # --> if we have a case, then return the code associated with the case entry
        elif isinstance(db_record, db_models.Case):
            return db_record.code_db.name

    def new_code(self, **kwargs):
        """ Returns a new code record """
        return db_models.Code(**kwargs)

    def get_code(self, identifier=None):
        """ Returns a code entry from the database """
        if identifier is not None:
            return db_models.Code.get(db_models.Code.identifier == identifier)

    def new_archive(self, **kwargs):
        """ Returns a new archive record """
        return db_models.Archive(**kwargs)

    def save(self, db_record):
        """ Given a db_record, save its contents to the db """
        if isinstance(db_record, pw.Model):
            db_record.save()

    def backup_db(self, dest=None):
        """ Backups the database to the given destination """
        dest = self.db_backup if dest is None else dest

        # copy append .bak and copy file
        self.db_backup = dest
        
        shutil.copy2(self.db_file, self.db_backup)
        self.db_backed_up = True

    def restore_db_backup(self, dest=None):
        """ Restore a db backup """
        if dest is None:
            dest = self.db_file

        if self.db_backed_up:
            shutil.copy2(self.db_backup, self.db_file)
            os.unlink(self.db_backup)
            self.db_backed_up = False

    def upgrade_db(self):
        """
        Upgrades the db in a cascading fashion
        """
        import playhouse.migrate as pwmigrate
        
        target_version = config.DBVERSION
        current_version = self.get_version()

        if not self.db_backed_up:
            raise exceptions.UpdateFailed("Database not backed up")

        # from 0 to 1
        if self.get_version() < 1:
            try:
                # create new table
                db_models.Notes.create_table(True)
                
                # update version
                self.set_version(1)
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)

        # from 1 to 2
        if 1 <= self.get_version() < 2:
            raise NotImplementedError("Models table need adjusting")
            print("Upgrading database from version 1 to 2 ... ", end='\n')
            try:
                # create the jobs table
                db_models.Job.create_table(True)
                
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)

                # -> first we add the column we need
                pwmigrate.migrate(
                    migrator.add_column('run', 'is_dev', db_models.Run.is_dev),
                )

                # temporarily add back the model columns

                batch_system = pw.FixedCharField(null=True, max_length=8)
                db_models.Run._meta.add_field('batch_system', batch_system)
                
                batch_queue = pw.CharField(null=True)
                db_models.Run._meta.add_field('batch_queue', batch_queue)
                
                job_number = pw.BigIntegerField(null=True)
                db_models.Run._meta.add_field('job_number', job_number)

                run_command = pw.TextField(null=True)
                db_models.Run._meta.add_field('run_command', run_command)

                submitted = pw.DateTimeField(null=True)
                db_models.Run._meta.add_field('submitted', submitted)
                
                started = pw.DateTimeField(null=True)
                db_models.Run._meta.add_field('started', started)
                
                finished = pw.DateTimeField(null=True)
                db_models.Run._meta.add_field('finished', finished)

                # for each run, add the most recent job
                # -> get all runs
                runs = self.get_runs()

                print("Processing runs ... ", end="\r")
                for run in runs:
                    # convert status
                    if run.status == 20:
                        # ASSIGNED -> CREATED
                        status = 10
                    elif run.status == 70:
                        # CLOSED -> COMPLETED
                        status = 55
                    elif run.status in [75,80,85]:
                        # ATTENTION -> ERROR
                        # INVALID -> ERROR
                        # FAILED -> ERROR
                        status = 90
                    elif run.status == 100:
                        # RESOLVED -> COMPLETED
                        status = 55
                    else:
                        # unchanged
                        status = run.status

                    # get working directory
                    if run.staging_mode == 'share':
                        # job was run in staging
                        working_dir = run.staging_dir
                    else:
                        # job was run locally
                        # we need the study path
                        working_dir = os.path.join(self.study_path,run.path)

                    # determine if this is a dev run and update
                    is_dev = True if "#dev" in str(run.name) else False
                    run.is_dev = is_dev
                    run.save()
                    
                    record = self.new_job(
                        name=run.name,
                        job_id=run.job_number,
                        run=run,
                        status=status,
                        batch_system=run.batch_system,
                        batch_queue=run.batch_queue,
                        last_modified=datetime.today(),
                        created=run.created,
                        user=utils.get_username(),
                        code_src_ref = run.code_src_ref,
                        case_src_ref = run.case_src_ref,
                        sys_command = run.run_command,
                        submit_host = run.submit_host,
                        submitted = run.submitted,
                        started=run.started,
                        finished=run.finished,
                        working_dir=working_dir,
                    )

                    # save the job record
                    record.save()
                
                print("Processing runs ... OK", end="\n")

                # remove columns we don't want
                pwmigrate.migrate(
#                    migrator.drop_column('run', 'code_src_ref'),
#                    migrator.drop_column('run', 'case_src_ref'),
                    migrator.drop_column('run', 'submitted'),
                    migrator.drop_column('run', 'started'),
                    migrator.drop_column('run', 'finished'),
                    migrator.drop_column('run', 'job_number'),
                    migrator.drop_column('run', 'batch_queue'),
                    migrator.drop_column('run', 'batch_system'),
                    migrator.drop_column('run', 'run_command'),
                )

                # update version
                self.set_version(2)
            except BaseException as err:
                raise exceptions.UpgradeFailed("")
            
        # from 2 to 3
        if 2 <= self.get_version() < 3:
            print("Upgrading database from version 2 to 3 ... ", end='\n')
            
            import uuid

            # reload the db_models as per DB_VERSION 2
            config.DBVERSION = 3
            importlib.reload(db_models)
            self.reload()

            try:
                # create the archive table
                db_models.Archive.create_table(True)
                
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)

                # -> process the studies
                # add the uuid column
                pwmigrate.migrate(
                    migrator.add_column('study', 'uuid', db_models.Study.uuid),
                    migrator.add_column('case', 'description', db_models.Case.description),
                    migrator.add_column('run', 'is_archived', db_models.Run.is_archived)
                )
                
                print("Processing studies ... ", end="\r")
                for study in self.get_studies():
                    # generate and add the uuid
                    study.uuid = uuid.uuid1(clock_seq=study.id)
                    
                    # save the record
                    study.save() 
                
                print("Processing studies ... OK", end="\n")

                # update version
                self.set_version(3)
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)
        
        # from 3 to 4
        if 3 <= self.get_version() < 4: 
            print("Upgrading database from version 3 to 4 ... ", end='\n')
            
            # reload the db_models as per DB_VERSION 4
            config.DBVERSION = 4
            importlib.reload(db_models)
            self.reload()
            
            try:
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)
                
                pwmigrate.migrate(
                    migrator.add_column('archive', 'type', db_models.Archive.type),
                    migrator.add_column('archive', 'name', db_models.Archive.name),
                    migrator.alter_column_type('archive', 'filehash', pw.CharField()),
                    migrator.rename_column('archive', 'filepath', 'path'),
                    migrator.rename_column('archive', 'filehash', 'hash'),
                )

                print("Processing archives ... ", end="\r")
                # split the filepath into name and path
                archives = self.get_archives()
                for archive in archives:
                    basename = os.path.basename(archive.path)
                    dirname = os.path.dirname(archive.path)
                    archive.path = dirname
                    archive.name = basename
                    archive.save()
                
                print("Processing archives ... OK", end="\n")
                # update version
                self.set_version(4)
            
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)
        
        # from 4 to 5
        if 4 <= self.get_version() < 5:
            print("Upgrading database from version 4 to 5 ... ", end='\n')
            
            # reload the db_models as per DB_VERSION 4
            config.DBVERSION = 5
            importlib.reload(db_models)
            self.reload()
            
            try:
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)
                
                pwmigrate.migrate(
                    migrator.add_column('run', 'tags', db_models.Run.tags),
                )
                
                # update version
                self.set_version(5)
            
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)
        
        # from 5 to 6
        if 5 <= self.get_version() < 6:
            import json
            print("Upgrading database from version 5 to 6 ... ", end='\n')

            # reload thje db_models as per DB_VERSION 5
            config.DBVERSION = 6
            importlib.reload(db_models)
            self.reload()

            try:
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)

                pwmigrate.migrate(
                    migrator.add_column('job', 'batch_options', db_models.Job.batch_options),
                )
                # fill the column with some things we know
                for job in self.get_jobs():
                    args = {}
                    if job.batch_queue is not None:
                        args["queue"] = str(job.batch_queue)
                    if job.pe is not None:
                        args["pe"] = str(job.pe)
                    
                    if args:
                        job.batch_args = json.dumps(args)
                        job.save()

                # update version
                self.set_version(6)
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)
        
        # from 6 to 7
        if 6 <= self.get_version() < 7:
            print("Upgrading database from version 6 to 7 ... ", end='\n')

            # reload thje db_models as per DB_VERSION 5
            config.DBVERSION = 7
            importlib.reload(db_models)
            self.reload()

            try:
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)

                pwmigrate.migrate(
                    migrator.add_column('job', 'allocation_units', db_models.Job.allocation_units),
                )
                
                # update version
                self.set_version(7)
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)

        # from 7 to 8
        if 7 <= self.get_version() < 8:
            print("Upgrading database from version 7 to 8 ... ", end='\n')

            # reload the db_models as per DB_VERSION 5
            config.DBVERSION = 8
            importlib.reload(db_models)
            self.reload()

            try:
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)

                pwmigrate.migrate(
                    migrator.add_column('job', 'type', db_models.Job.type),
                    migrator.add_column('job', 'stdout', db_models.Job.stdout),
                    migrator.add_column('job', 'stderr', db_models.Job.stderr)
                )
                from cfmt.entities.jobs import Job 
                # fill the column with some things we know
                for job in self.get_jobs():
                    # all jobs should be of solver type 
                    job.type = Job.SOLVER
                    job.save()

                # update version
                self.set_version(8)
            
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)
        
        # from 8 to 9
        if 8 <= self.get_version() < 9:
            print("Upgrading database from version 8 to 9 ... ", end='\n')

            # reload thje db_models as per DB_VERSION 5
            config.DBVERSION = 8
            importlib.reload(db_models)
            self.reload()
            
            try:
                import json
                
                # load the migrator
                migrator = pwmigrate.SqliteMigrator(self.db)

                # all existing options are batch options
                for job in self.get_jobs():
                    options = json.loads(str(job.batch_options))
                    new_options = {
                        "batch": options
                    }
                    job.batch_options = json.dumps(new_options)

                    job.save()
                
                pwmigrate.migrate(
                    migrator.rename_column('job', 'batch_options', 'options'),
                )

                # update version
                self.set_version(9)
            
            except BaseException as err:
                raise exceptions.UpgradeFailed(err)


            
    def move_study(self, destination, db_record=None):
        """ Facilitates the moving of a study """
        if db_record is None:
            # fetch the record
            db_record = db_models.Study.get(db_models.Study.path == self.study_path)

        # modify the path
        db_record.path = destination

        # modify the name
        db_record.name = os.path.basename(destination)

        # save
        try:
            db_record.save()
        except pw.PeeweeException as err:
            raise exceptions.DatabaseException("Error saving new study path: {}".format(str(err)))
    
    def copy_case(self, destination, db_record=None, run_records=None):
        """ Faciliates the copying of a case, including copying the runs """

