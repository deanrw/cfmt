"""
    cft: A management tool for Computational Fluid Dynamics
    
    batch: Batch system interfaces
    BatchSystem: The base class to implement difference batch systems
"""
import shutil
import sys
import os
import logging

from cfmt.entities.jobs import Job
from cfmt.config import configtool as config
from cfmt import utils

# constants for pre/post execution scripts
PRE_EXEC="pre"
POST_EXEC="post"


REQUIRED_SUBCLASS_ATTRIBUTES = [
    "STDOUT_MASK", "STDERR_MASK",
    "SCRIPT_STDOUT_MASK", "SCRIPT_STDERR_MASK",
    "system_name"
]

class BatchSystem(object):
    """ Base class to implement different batch systems """
    mpi_binaries = [
        'mpiexec'
    ]

    rusage_fields = [
        'ru_utime',
        'ru_stime',
        'ru_maxrss',
        'ru_ixrss',
        'ru_idrss',
        'ru_isrss',
        'ru_minflt',
        'ru_majflt',
        'ru_nswap',
        'ru_inblock',
        'ru_oublock',
        'ru_msgsnd',
        'ru_msgrcv',
        'ru_nsignals',
        'ru_nvcsw',
        'ru_nivcsw',
    ]
    
    # file masks for pre/post execution
    DEFAULT_EXEC_SRC_MASK="{stage}exec-src.sh"
    DEFAULT_EXEC_MASK="{stage}exec.sh"
    DEFAULT_EXEC_DIR="input"       # directory containing study/case level scripts

    DEFAULT_PARALLEL_EXEC = "mpiexec"
    DEFAULT_MODE = "serial"
    DEFAULT_NP = 1
    # switch to allow a code to hook into the command creation process
    code_sets_command = False
    code = None

    supported_batch_options = ["np", "mode"]

    def __init__(self, batch_options=None, parallel=None, np=None, parallel_exec=None):
        """ The constructor 

        parallel (bool|None) : Whether or not a parallel job is requested
        np (int|None)     : The number of processes requested
        parallel_exec (str|None) : The parallel executable used for launching parallel jobs

        Specifying np == 1 will fix the mode as serial.

        If None is specified, then the batch_system configuration is read followed by the run configuration
        """
        
        # store the loggers
        self.logger = logging.getLogger('file')

        # update set pre/post execution masks
        self.EXEC_SRC_MASK = config.get(self.system_name, "prepost_exec_src_mask", fallback=self.DEFAULT_EXEC_SRC_MASK)
        self.EXEC_MASK = config.get(self.system_name, "prepost_exec_mask", fallback=self.DEFAULT_EXEC_MASK)

        # check the subclass is implemented properly
        self._check_subclass()

        # load the batch options passed if we have them 
        # handle the number of processes
        if np is not None:
            try:
                np = int(np[-1]) if isinstance(np, list) else int(np)
            except ValueError:
                self.logger.error(f"Invalid value for np '{np}'")
                raise
            # explicitely set parallel as well
            parallel = True if np > 1 else False
        elif batch_options is not None and "np" in batch_options:
            np = batch_options["np"]
        else:
            np = int(config.get(self.system_name, "slots", fallback=config.get("run", "slots", fallback=self.DEFAULT_NP)))
        
        # handle the mode
        if parallel is not None:
            assert isinstance(parallel, bool)
            mode = "parallel" if parallel else "serial"
        elif batch_options is not None and "mode" in batch_options:
            mode = batch_options["mode"]
        else:
            mode = config.get(self.system_name, "mode", fallback=config.get("run", "mode", fallback=self.DEFAULT_MODE))

        if not (mode == "serial" or mode == "parallel"):
            raise ValueError(f"Incorrect mode specified: {mode}")

        # check for conflicting options
        if np > 1 and mode == "serial":
            # warn
            self.logger.warning(f"Serial mode selected with np == {np}")
            np = 1

        if parallel_exec is not None:
            assert isinstance(parallel_exec, str)
        elif batch_options is not None and "parallel_exec" in batch_options:
            parallel_exec = batch_options["parallel_exec"]
        else:
            parallel_exec = config.get(self.system_name, "parallel_exec", fallback=config.get("run", "parallel_exec", fallback=self.DEFAULT_PARALLEL_EXEC))
        
        # process system default modules if we have any
        m = []
        env_modules_r = config.get("run", "env_modules", fallback="")
        if env_modules_r:
            m += env_modules_r.split()
        env_modules_b = config.get(self.system_name, "env_modules", fallback="")
        if env_modules_b:
            m += env_modules_b.split()

        
        self.env_modules = m

        self.mode = mode
        self.parallel = (mode == "parallel")
        self.serial = (mode == "serial")
        self.np = np
        self.parallel_exec = parallel_exec
        self.additional_options = None

    
    def _check_subclass(self):
        """
        Some checks to ensure the subclass is implemented correctly
        """
        for a in REQUIRED_SUBCLASS_ATTRIBUTES:
            if not hasattr(self, a):
                raise NotImplementedError(f"Missing Required Attribute '{a}'")

    def load_exec(self, src_path, wd, stage=None):
        """
        Add/load a existing script to the run_script
        """
        from shutil import copy2
       
        if stage not in [PRE_EXEC, POST_EXEC]:
            raise ValueError("Invalid stage in call to load_exec")
        
        lines = []
        
        # scripts
        fname = self.EXEC_MASK.format(stage=stage)
        fpath = os.path.join(src_path, fname)

        if os.path.isfile(fpath):
            # copy to the working directory
            dst = os.path.join(wd,fname)
            copy2(fpath, dst)

            # add a line running the code
            lines.append(f"# CFMT: running {stage} script {fname}")
            lines.append(f"{dst}")
            lines.append("")

        # files to source (implant)
        fname = self.EXEC_SRC_MASK.format(stage=stage)
        fpath = os.path.join(src_path, fname)

        if os.path.isfile(fpath):
            # read the file
            with open(fpath, 'r') as f:
                content = f.read().splitlines()

            # append to our existing lines
            lines.append(f"# CFMT: added from script {fname}")
            lines += content
            lines.append("")

        return lines
    
    def get_module_lines(self, code=None):
        """
        Add batch and code modules lines if present
        """
        lines = []

        # purge if we have some module files
        if self.env_modules or (code and code.env_modules):
            lines.append("module purge")
            lines.append("")

        # load any enviromental modules
        if self.env_modules:
            module_str = " ".join(self.env_modules)
            lines.append("# load batch module files")
            lines.append("module load " + module_str)
            lines.append("")
        
        # check the code for any enviromental modules
        if code and code.env_modules:
            module_str = " ".join(code.env_modules)
            lines.append("# load code module files")
            lines.append("module load " + module_str)
            lines.append("")

        return lines
    
    
    def get_exec_lines(self, cwd, base_dirs, stage, code=None):
        """
        For each base directory, we load the script if it exists
        and return the combined lines
        """
        lines = []
        
        locations = []
        for b in base_dirs:
            d = os.path.join(b, self.DEFAULT_EXEC_DIR)
            locations.append(d)
        
        if code is not None:
            for b in base_dirs:
                _loc = code.prepost_exec_locations(base_dir=b)
                if _loc: locations.append(_loc)

        for loc in locations:
            _lines = self.load_exec(loc, cwd, stage=stage)
            if _lines:
                lines += _lines
        
        return lines
    
    def get_command(self, run, batch, cmd=None):
        raise NotImplementedError

    #def get_parallel_exec(self):
    #    """ Returns the executable used to pararellize the code """
    #    if self.parallel_system == "openmpi":
    #        return self._mpiexec
    #    else:
    #        raise NotImplementedError
    def _get_option(self, options, key):
        """
        Returns the option if the key is present, else uses the attribute
        """
        if key in options:
            return options[key]
        elif hasattr(self, key):
            return getattr(self, key)
        else:
            msg = f"Option with {key} not found in batch_options"
            self.logger.error(msg)
            raise ValueError(msg)
    
    def create_job(self, run, type_=Job.SOLVER, task_cmd=None, name=None):
        """ Creates a job for submissions """ 

        # check if type is a string
        if isinstance(type_, str):
            assert type_ in Job.valid_type_names, "Job type not supported"
            type_ = Job._name_to_type[type_]
        else:
            assert type_ in Job._type_to_name, "Job type not supported" 
        
        # ask the batch system to generate the command 
        cmd = self.generate_command(run, run.code, task_cmd=task_cmd, name=name)
        
        options = {
            "batch": self.get_batch_options(run)
        } 
        if type_ is Job.SOLVER:
            options["code"] = run.code.get_code_options(run)
        
        # instantiate the job
        job = Job(
            run=run, 
            batch_system=self.system_name, 
            options=options,
            slots=self.np,
            command=cmd,
            type_=type_,
            job_name=name,
        )
        
        # store a reference to the batch object
        job.batch = self

        return job

    def parse_submission_output(self):
        """ Parses output from the submission command """
        pass

    def submit(self, command):
        """ Submit a command, returning the Subprocess """
        raise NotImplementedError

        #assert isinstance(command, list)

        #return subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

    def set_job(self, job):
        """ 
        Assigns a job object to the batch system 
        TODO: Deprecate
        """
        self.job = job
        
        # assign job names
        self.job_name = self.job.name

        # assign working directory
        self.working_dir = self.job.working_dir

        # assign the command
        self.set_command()

    def write_submission_script(self, fname, lines):
        """ Given a submission script name and lines, write the file to disk """
        try:
            self.logger.debug("Writing submission script %s",fname)
            with open(fname, 'w') as f:
                f.writelines(lines)
        except IOError as err:
            utils.print_exception_info(str(err), debug=True)
            sys.exit(1)
    
    
    def get_rusage(self, rusage_dict=None, **kwargs):
        """ Takes either kwargs set or a dict and returns a resource.rusage_struct """
        import resource

        rusage_val = []
        if rusage_dict is not None:
            rusage = rusage_dict
        else:
            rusage = kwargs

        # loop over the fields, order is important
        for field in self.rusage_fields:
            if field in rusage:
                try:
                    value = int(rusage[field])
                except ValueError:
                    try:
                        value = float(rusage[field])
                    except ValueError:
                        value = 0
            else:
                value = 0
            
            rusage_val.append(value)
        
        # convert to structure and return
        return resource.struct_rusage(rusage_val)
    
    def process_args(self, args):
        """
        For processing command line arguments specific to the batch system
        """
        if args.batch_options is not None:
            self.additional_options = args.batch_options
        
        return

    @classmethod 
    def get_stdout_fname(cls, job):
        """ Given the job name, return the stdout file """
        MASK = cls.STDOUT_MASK

        return MASK.format(job=job)

    @classmethod    
    def get_stderr_fname(cls, job):
        """ Given the job name, return the stderr file """
        MASK = cls.STDERR_MASK

        return MASK.format(job=job)
    
    @classmethod
    def get_stdout_script_name(cls):
        """ Returns a format of the stderr name suitable for a job script """
        return cls.SCRIPT_STDOUT_MASK

    @classmethod
    def get_stderr_script_name(cls):
        """ Returns a format of the stderr name suitable for a job script """
        return cls.SCRIPT_STDERR_MASK
        


# DEPRECATED
#class BatchPopen(subprocess.Popen):
#    """
#    Overwrite the Popen class to reimplement wait as os.wait4()
#    See https://stackoverflow.com/questions/26475636/measure-elapsed-time-amount-of-memory-and-cpu-used-by-the-extern-program
#    """
#
#    def _try_wait(self, wait_flags):
#        """All callers to this function MUST hold self._waitpid_lock """
#        try:
#            (pid, sts, res) = os.wait4(self.pid, wait_flags)
#        except ChildProcessError:
#            pid = self.pid
#            sts = 0
#        else:
#            self.rusage = res
#        return (pid, sts)
