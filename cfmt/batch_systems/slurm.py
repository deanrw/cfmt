"""
    cft: A management tool for Computational Fluid Dynamics
    
    batch: Batch system interfaces
    slurm: 
"""
import shutil
import sys
import os
import time
from datetime import datetime,timedelta
from cfmt.config import configtool as config
from cfmt.batch_systems.base import BatchSystem, PRE_EXEC, POST_EXEC
from cfmt import exceptions, utils
    

class SLURM(BatchSystem):
    """ 
    SLURM Submission Plugin
    
    Provides an interface to the SLURM submission system using system binaries

    Options supported
    --workdir=[dir] Execute the job from the current directory
    --export=[ALL|NONE|variable] 
                    Export environmental variables
    --job-name=[name]
                    Sets the job name
    -o file/dir     Path to directory or file for stdout
    -e file/dir     Path to directory or file for stderr
    -hold_jid JOBID Job is conditional upon completion of job jobid
    --wrap=""       Wraps the enclosed into a simple submission script
    -p PARTITION    Submit to specific partition (queue)
    """
    
    # list of binary names to search for
    binaries = [
        'sbatch',
        'sinfo',
        'sstat',
        'sacct'
    ]
    time_fmt = {
        'qacct': '%a %b %d %H:%M:%S %Y',
        'qstat': '%Y-%m-%dT%H:%M:%S'
    }
    DEFAULT_SHELL = "/bin/bash"
    DEFAULT_SCRIPT_NAME = "sbatch.sh"
    DEFAULT_PARTITION = None
    DEFAULT_PASS_ENV = False
    DEFAULT_TIME_FORMAT = None
        
        
    STDOUT_MASK="{job.name}" + ".om" + "{job.job_id}"
    STDERR_MASK="{job.name}" + ".em" + "{job.job_id}"
    SCRIPT_STDOUT_MASK="$SLURM_JOB_NAME" + ".om" + "$SLURM_JOB_ID" 
    SCRIPT_STDERR_MASK="$SLURM_JOB_NAME" + ".em" + "$SLURM_JOB_ID"

    system_name = "slurm"


    def __init__(self, batch_options=None, parallel=None, np=None, parallel_exec=None, cmdline_args=None):
        """ Constructor """

        # call parent
        super().__init__(batch_options=batch_options, parallel=parallel, np=np, parallel_exec=parallel_exec)

        # slurm binary locations
        for binary in self.binaries:
            path = shutil.which(binary)
            if path is None:
                msg = f"Cannot find '{self.system_name}' binary '{binary}'"
                self.logger.error(msg)
                self.console.error(msg)
                sys.exit(0)
            else:
                # set as private attributes
                setattr(self, "_" + binary, path)

        # process the queue
        if cmdline_args is not None and cmdline_args.queue is not None:
            partition = cmdline_args.queue[-1]
        elif batch_options is not None and "partition" in batch_options:
            partition = batch_options["partition"]
        else:
            partition = config.get(self.system_name, self.mode + "_partition", fallback=config.get(self.system_name, "partition", fallback=self.DEFAULT_PARTITION))

        # process the script name
        if cmdline_args is not None and cmdline_args.slurm_script_name is not None:
            script_name = cmdline_args.slurm_script_name[-1]
        elif batch_options is not None and "script_name" in batch_options:
            script_name = batch_options["script_name"]
        else:
            script_name = config.get(self.system_name, "script_name", fallback=self.DEFAULT_SCRIPT_NAME)
        
        # sge shell doesn't have a cmdline option
        if batch_options is not None and "shell" in batch_options:
            shell = batch_options["shell"]
        else:
            shell = config.get(self.system_name, "shell", fallback=self.DEFAULT_SHELL)

        if batch_options is not None and "pass_env" in batch_options:
            pass_env = batch_options["pass_env"]
        else:
            pass_env = config.getboolean(self.system_name, "pass_env", fallback=self.DEFAULT_PASS_ENV)
        
        # process any additional batch options (additive here)
        additional_options = []
        if cmdline_args is not None and cmdline_args.batch_options is not None:
            additional_options = cmdline_args.batch_options
        
        if batch_options is not None and "extra" in batch_options:
            if not cmdline_args.clear_batch_extra:
                additional_options += batch_options["extra"]
        else:
            _options = config.get(self.system_name, "additional_options", fallback="")
            additional_options += _options.split()

        self.partition = partition
        self.script_name = script_name
        self.shell = shell
        self.pass_env = pass_env
        self.additional_options = additional_options
    
    def get_batch_options(self, run):
        """
        Creates an ordered dict of batch arguments from the current attributes
        """
        batch_options = {
            "mode": self.mode,
            "partition": self.partition,
            "shell": self.shell,
            "script_name": self.script_name,
            "np": self.np,
            "pass_env": self.pass_env,
            "extra": self.additional_options,
        }
        return batch_options

    def generate_command(self, run, code, task_cmd=None, name=None):
        """ 
            Compiles and returns the command as a list 
            Submission command is sbatch, full options: https://slurm.schedmd.com/sbatch.html 
        """
        #

        # generate the submission script
        fscript = self.generate_submission_script(run, code, task_cmd=task_cmd, name=name)
        command = []
        # the sbatch binary
        assert self._sbatch is not None
        command.append(self._sbatch)

        # append the script
        command.append(fscript)
        self.submission_script = fscript
        
        self.command = command
        return command


    def generate_submission_script(self, run, code, task_cmd=None, name=None):
        """
        Generate a simple shell script to submit, located in the working directory
        Script will be copied and kept
        """
        study_dir = run.study.path
        case_dir = run.case.abspath
        working_dir = run.working_dir
        job_name = name if name is not None else run.name
        
        assert isinstance(working_dir, str)
        assert isinstance(job_name, str)

        lines = []
        # shebang
        lines.append("#!" + self.shell)

        # commend and empty line
        lines.append("# sbatch submission script generated by cfmt")
        lines.append("")

        
        # add the --parsable option which restricts output to jobid and cluser only
        lines.append(self.get_sbatch_script_line("--parsable"))
        
        # run in the current working directory
        lines.append(self.get_sbatch_script_line("--chdir=" + str(working_dir)))

        # pass all environmental variables
        if self.pass_env:
            lines.append(self.get_sbatch_script_line("--export=ALL"))

        # set the job name
        lines.append(self.get_sbatch_script_line("--job-name=\"" + str(job_name) + "\""))
        
        # set number of slots (called tasks)
        lines.append(self.get_sbatch_script_line("--ntasks=" + str(self.np)))
        
        # set the queue; these are called partitions
        if self.partition is not None:
            line = self.get_sbatch_script_line("--partition=")
            line += self.partition
            lines.append(line) 

        # set the output and error format
        lines.append(self.get_sbatch_script_line("--output=\"" + job_name + ".om%j\""))
        lines.append(self.get_sbatch_script_line("--error=\"" + job_name + ".em%j\""))

        # handle any additional options
        # these should be in the form
        # --option=value
        # -option=value
        # multiple values are supported but are not space-separated. 
        for a in self.additional_options:
            lines.append(self.get_sbatch_script_line(a))

        # empty line
        lines.append("")
        
        # add modules
        m_lines = self.get_module_lines(code=code)
        lines += m_lines

        # hook for pre-exec lines
        p_lines = self.get_exec_lines(
            working_dir, 
            [study_dir, case_dir],
            PRE_EXEC,
            code
        )
        lines += p_lines
        
        command = []
        
        # we ask the code if required
        if task_cmd is not None:
            command = task_cmd
        elif code.has_tight_integration(self.system_name):
            command = code.get_command(run, self, cmd=command)
        else:
            job_binary = code.get_bin_path(wd=working_dir, batch_system=self)
            
            if job_binary is None:
                msg = f"Binary for code {code.name} not available"
                self.logger.error(msg)
                raise ValueError(msg)

            if self.mode == "parallel":
                command.append(self.parallel_exec)
                command.append("-np")
                command.append(str(self.np))

            # append the binary at the end     
            if isinstance(job_binary, list):
                command += job_binary
            else:
                command.append(job_binary)
        
        lines.append(" ".join(command))
        
        # hook for post-exec lines
        p_lines = self.get_exec_lines(
            working_dir, 
            [study_dir, case_dir],
            POST_EXEC,
            code
        )
        lines += p_lines
        
        # append line break to each line
        lines = [l + "\n" for l in lines]
        fname = os.path.join(working_dir, self.script_name)
        
        self.write_submission_script(fname, lines)
        return fname

    def get_sbatch_script_line(self, line, prefix="#SBATCH"):
        """
        Prepends the SBATCH prefix to the line
        """
        return prefix + " " + line
    
    def parse_submission_output(self, proc):
        """ 
        Parse the sbatch output
        
        proc the subprocess Popen return
        """
        # wait for the process to finish
        returncode = proc.wait()
        
        if returncode != 0:
            # error with the submission, log the problem and return
            raise exceptions.JobFailed(self.system_name + ":" + proc.stderr.read())
        else:
            # success and we have the job_id
            stdout = proc.stdout.read()

            # submissions generated with this file will have the parsable switch active
            # so we should just be able to fetch it straight from the screen
            try:
                job_id = int(stdout)
            except ValueError as err:
                utils.print_exception_info(err)
                sys.exit(1)
                # try to see if terse has been missed
            return job_id

    def update(self, jobs, detailed=False):
        """ Fetches the most recent information for the runs specified in runs """
        
        # check jobs is a list
        assert isinstance(jobs, list)

        # fields to return
        fields = ["JobID", "JobName", "State", "Start", "End", "ExitCode", "CPUTimeRAW"]
        delimiter="|"

        time_fmt = config.get(self.system_name, "time_fmt", fallback=self.DEFAULT_TIME_FORMAT)

        # if not empty
        if jobs:
            import subprocess

            # get a list of job ids
            # sort the job list
            jobs = sorted(jobs, key=lambda j: j.job_id)
            job_ids = []
            for _job in jobs:

                job_ids.append(str(_job.job_id))


            # issue a sacct command
            # -> -P Parsable 2: only 
            # -> -X only prints the allocations, so MaxVMSIZE will not exist
            # -> returns in numerical order

            ids = ",".join(job_ids)
            format_str = ",".join(fields)
            command = [self._sacct, "-P", '-j', ",".join(job_ids), "--format="+ format_str, "--noheader", "-X", "--delimiter", delimiter]
            self.logger.debug("Running command '%s'", " ".join(command))
            proc = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            if proc.returncode == 0:
                # success
                try:
                    # attemp to parse the output
                    output = proc.stdout.splitlines()
                    if len(jobs) != len(output):
                        raise ValueError("Extra output received")
                    # sort the output
                    output = sorted(output, key=lambda x : x.split(delimiter)[0])
                    # zip the output with the jobs and loop through
                    for job,sacct_out in zip(jobs, output):
                        # parse the line
                        parsed = dict(zip(fields, sacct_out.split(delimiter)))
                        if str(job.job_id) != str(parsed['JobID']):
                            raise ValueError("Mismatch between sacct and job list")
                        
                        # handle the state
                        if "State" in fields:
                            status = self.parse_state(parsed["State"], job)
                        else:
                            raise ValueError("Status missing from sacct output")

                        # update the start time
                        if "Start" in fields:
                            try:
                                # account for custom formats
                                if time_fmt is None:
                                    start_time = datetime.fromisoformat(parsed["Start"])
                                else:
                                    start_time = datetime.strptime(parsed["Start"], time_fmt)
                            except ValueError:
                                if status in [job.RUNNING, job.COMPLETED]:
                                    self.logger.warning("Job: '%s': could not update start time", job.job_id)
                            else:
                                job.started = start_time
                        
                        # parse the execution host
                        if 'NodeList' in fields:
                            execution_host = parsed['NodeList']
                            job.execution_host = str(execution_host)

            
                        # if the job has ended
                        if status in [job.COMPLETED, job.ABORTED, job.ERROR]: 
                            if "ExitCode" in fields:
                                # we expect and exit code
                                # format is exitcode:signal where signal is what caused the exist code if terminated by signal
                                try:
                                    exit_code, exit_signal = tuple(parsed["ExitCode"].split(":"))
                                except ValueError:
                                        self.logger.warning("Job: '%s': Error parsing exit code", job.job_id)
                                else:
                                    job.exit_status = exit_code
                            # update the end time
                            if "End" in fields:
                                try:
                                    # account for custom formats
                                    if time_fmt is None:
                                        end_time = datetime.fromisoformat(parsed["End"])
                                    else:
                                        end_time = datetime.strptime(parsed["End"], time_fmt)
                                except ValueError:
                                    self.logger.warning("Job: '%s': could not update end time", job.job_id)
                                else:
                                    job.finished = end_time

                            # parse the CPU usage
                            if "CPUTime" in fields:
                                try:
                                    cpu_time = int(parsed['CPUTimeRAW'])

                                except ValueError:
                                    self.logger.warning("Job: '%s': could not parse CPU TIME", job.job_id)
                                else:
                                    job.cpu_time = cpu_time
                            
                            # parse the VMEM usage
                            if "MaxVMSize" in fields:
                                # max vmem has units at the end
                                try:
                                    max_vmem = utils.human2bytes(str(parsed['MaxVMSize']))
                                except ValueError:
                                    self.logger.warning("Job: '%s': could not update max vmem", job.job_id)
                                else:
                                    job.vmem = max_vmem
                        
                        # update the job
                        job.update_status(status, update_run=True)
                except Exception as err:
                    utils.print_exception_info(err)
                    raise exceptions.UpdateFailed("Error parsing sacct output")
        
            else:
                # either it does not exist or there has been a problem
                self.logger.error(proc.stdout)
                self.logger.error(proc.stderr)
                raise exceptions.UpdateFailed("sacct returned a non-zero exit code")

    def parse_state(self, state, job):
        """
        Provided a job object, update according to the state string
        
        See https://slurm.schedmd.com/sacct.html for more details on state codes
        """
        abnormal_state = [job.ERROR, job.ABORTED, job.SUSPENDED]
        assert isinstance(state, str)

        if state == "BOOT_FAIL":
            msg = "Job terminated due to launch failure, typically due to a hardware failure (e.g. unable to boot the node or block and the job can not be requeued)."
            status = job.ERROR
        elif state == "CANCELLED":
            msg = "Job was explicitly cancelled by the user or system administrator. The job may or may not have been initiated."
            status = job.ABORTED
        elif state == "COMPLETED":
            msg = "Job has terminated all processes on all nodes with an exit code of zero."
            status = job.COMPLETED
        elif state == "DEADLINE":
            msg = "Job terminated on deadline."
            status = job.ABORTED
        elif state == "FAILED":
            msg = "Job terminated with non-zero exit code or other failure condition."
            status = job.ABORTED
        elif state == "NODE_FAIL":
            msg = "Job terminated due to failure of one or more allocated nodes."
            status = job.ABORTED
        elif state == "OUT_OF_MEMORY":
            msg = "Job experienced out of memory error"
            status = job.ABORTED
        elif state == "PENDING":
            msg = "Job is awaiting resource allocation."
            status = job.QUEUING
        elif state == "PREEMPTED":
            msg = "Job terminated due to preemption."
            status = job.ABORTED
        elif state == "RUNNING":
            msg = "Job currently has an allocation."
            status = job.RUNNING
        elif state == "REQUEUED":
            msg = "Job was requeued."
            status = job.QUEUING
        elif state == "RESIZING":
            msg = "Job is about to change size."
            status = job.QUEUING
        elif state == "REVOKED":
            msg = "Sibling was removed from cluster due to other cluster starting the job."
            status = job.ABORTED
        elif state == "SUSPENDED":
            msg = "Job has an allocation, but execution has been suspended and CPUs have been released for other jobs."
            status = job.SUSPENDED
        elif state == "TIMEOUT":
            msg = "Job terminated upon reaching its time limit."
            status = job.ABORTED
        else:
            msg = "Unknown error"
            # something else, mark as attention
            status = job.ERROR
        
        # log the message
        if status in abnormal_state:
            if job.status != status:
                log_msg = "Job {} status updated {} -> {}: " + msg
                self.logger.info(log_msg)
        
        # update the job and run
        return status

    def submit(self, job):
        """ 
        Recieves a job object and executes
        Handles output
        """
        import subprocess
        
        if not isinstance(job.command_list, list):
            msg = f"Command list missing for Job: {job.id}"
            self.logger.error(msg)
            raise ValueError(msg)
        
        print(" -> Submitting job using {} ... ".format(self.system_name), end="\r")
        self.logger.info("Submitting job using %s", self.system_name)
        try:
            proc = subprocess.Popen(job.command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

            # parse to get job_id
            job_id = self.parse_submission_output(proc)
        except exceptions.JobFailed as err:
            print(" -> Submitting job using {} ... FAILED".format(self.system_name))
            self.logger.info("Run '%s' failed", job.name)
            utils.print_exception_info(msg=str(err))
            job.update_status(job.ERROR, update_run=True)
        except Exception as err:
            print(" -> Submitting job using {} ... FAILED".format(self.system_name))
            self.logger.error("Run '%s' failed", job.name)
            utils.print_exception_info(msg="Unknown Error", debug=str(err))
            job.update_status(job.ERROR, update_run=True)
        else:
            print(" -> Submitting job using {} ... OK JOB: {}".format(self.system_name, job_id))
            job.update_status(job.SUBMITTED, update_run=True)
            job.update(
                job_id=job_id, 
                submitted=datetime.now(),
            )
            # we need the job_id to be present in order to update the fnames
            job.update(
                stdout=self.get_stdout_fname(job),
                stderr=self.get_stderr_fname(job),
            )

            self.logger.info("Run '%s' submitted as JOB: %s", job.name,job_id)
