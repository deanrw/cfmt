"""
    cft: A management tool for Computational Fluid Dynamics
    
    batch: Batch system interfaces
    SGE: Sun Grid Engine
"""
import shutil
import sys
import os
from datetime import datetime

from cfmt.config import configtool as config
from cfmt.batch_systems.base import BatchSystem, PRE_EXEC, POST_EXEC
from cfmt import exceptions, utils

class SGE(BatchSystem):
    """ 
    Grid Engine submission system 
    
    Provides an interface to the SGE submission system using system binaries

    Options supported
    -cwd            Execute the job from the current directory
    -V              All environmental variables active within the environment will be copied
    -j y            Merge the stderr and stdout streams to the same .o file
    -l resource     Specify a resource to modifiy
    -S /bin/bash    Indicate your job script is written in /bin/bash syntax
    -N name         Sets the job name
    -o file/dir     Path to directory or file for stdout
    -e file/dir     Path to directory or file for stderr
    -hold_jid JOBID Job is conditional upon completion of job jobid
    -m bea          Causes an email to be sent when the job (b)egins, (e)nds or (a)borts
    -M EMAIL        The address to which the email is sent
    -b y            Indicates the filename on the cmd is a binary rather than a script
    -q QUEUE        Submit to specific queue
    """
    
    # list of binary names to search for
    binaries = [
        'qsub',
        'qalter',
        'qstat',
        'qacct'
    ]
    time_fmt = {
        'qacct': '%a %b %d %H:%M:%S %Y',
        'qstat': '%Y-%m-%dT%H:%M:%S'
    }
    DEFAULT_SHELL = "/bin/bash"
    DEFAULT_SCRIPT_NAME = "sgebatch.sh"
    DEFAULT_PE = "smp.pe"
    DEFAULT_QUEUE = None
    DEFAULT_PASS_ENV = False
    modes_supported = ["serial", "parallel"]
    

    system_name = "sge"
    
    STDOUT_MASK="{job.name}" + ".o" + "{job.job_id}"
    STDERR_MASK="{job.name}" + ".e" + "{job.job_id}"
    SCRIPT_STDOUT_MASK="$JOB_NAME" + ".o" + "$JOB_ID" 
    SCRIPT_STDERR_MASK="$JOB_NAME" + ".e" + "$JOB_ID"


    def __init__(self, batch_options=None, parallel=None, np=None, parallel_exec=None, cmdline_args=None):
        """ Constructor """
        
        super().__init__(batch_options=batch_options, parallel=parallel, np=np, parallel_exec=parallel_exec)
        
        # grid engine binary locations
        for binary in self.binaries:
            path = shutil.which(binary)
            if path is None:
                msg = f"Cannot find required {self.system_name} binary '{binary}'"
                self.logger.error(msg)
                self.console.error(msg)
                sys.exit(0)
            else:
                # set as private attributes
                setattr(self, "_" + binary, path)

        # process the queue
        if cmdline_args is not None and cmdline_args.queue is not None:
            queue = cmdline_args.queue[-1]
        elif batch_options is not None and "queue" in batch_options:
            queue = batch_options["queue"]
        else:
            queue = config.get(self.system_name, self.mode + "_queue", fallback=config.get(self.system_name, "queue", fallback=self.DEFAULT_QUEUE))

        # process the parallel environment
        if cmdline_args is not None and cmdline_args.sge_pe is not None:
            pe = cmdline_args.sge_pe[-1]
        elif batch_options is not None and "pe" in batch_options:
            pe = batch_options["pe"]
        else:
            pe = config.get(self.system_name, "pe", fallback=self.DEFAULT_PE)
        
        # process the script name
        if cmdline_args is not None and cmdline_args.sge_script_name is not None:
            script_name = cmdline_args.sge_script_name[-1]
        elif batch_options is not None and "script_name" in batch_options:
            script_name = batch_options["script_name"]
        else:
            script_name = config.get(self.system_name, "script_name", fallback=self.DEFAULT_SCRIPT_NAME)
        
        if batch_options is not None and "pass_env" in batch_options:
            pass_env = batch_options["pass_env"]
        else:
            pass_env = config.getboolean(self.system_name, "pass_env", fallback=self.DEFAULT_PASS_ENV)

        # sge shell doesn't have a cmdline option
        if batch_options is not None and "shell" in batch_options:
            shell = batch_options["shell"]
        else:
            shell = config.get(self.system_name, "shell", fallback=self.DEFAULT_SHELL)
        
        # process cmdline passed job hold ids if we have them
        if cmdline_args is not None and cmdline_args.sge_hold is not None:
            hold_jid = cmdline_args.sge_hold[0]
            hold_jid = [int(x) for x in hold_jid.split(",")]
        else:
            hold_jid = None

        # process any additional batch options (additive here)
        additional_options = []
        if cmdline_args is not None and cmdline_args.batch_options is not None:
            additional_options = cmdline_args.batch_options
        
        if batch_options is not None and "extra" in batch_options:
            additional_options += batch_options["extra"]
        else:
            _options = config.get(self.system_name, "additional_options", fallback="")
            additional_options += _options.split()

        self.queue = queue
        self.script_name = script_name
        self.pe = pe
        self.shell = shell
        self.pass_env = pass_env
        self.additional_options = additional_options
        self.hold_jid = hold_jid

    @classmethod
    def default_config(cls):
        """ 
        Method for writing default config to the user configuration
        """
        pass
    
    def load_job(self, job):
        """ Given a job, load the batch object that submitted it """

    
    def get_batch_options(self, run):
        """
        Creates an ordered dict of batch arguments from the current attributes
        """
        batch_options = {
            "mode": self.mode,
            "pe": self.pe,
            "queue": self.queue,
            "shell": self.shell,
            "script_name": self.script_name,
            "pass_env": self.pass_env,
            "np": self.np,
            "extra": self.additional_options,
        }
        return batch_options

    def generate_command(self, run, code, task_cmd=None, name=None):
        """
        Given a run and code, generate the command that would run a job
        using this batch system
        """

        # generate the submission script
        fscript = self.generate_submission_script(run, code, task_cmd=task_cmd, name=name)
        command = []

        # the qsub binary
        assert self._qsub is not None
        command.append(self._qsub)

        # the job name (since names in the script are split by the hash)
        command.append("-N")
        command.append(self.job_name)

        # append the script
        command.append(fscript)
        self.submission_script = fscript

        self.command = command
        return command 


    def generate_submission_script(self, run, code, task_cmd=None, name=None) :
        """
        Generate a simple shell script to submit, located in the run wd
        Script will be copied and kept
        """
        study_dir = run.study.path
        case_dir = run.case.abspath
        working_dir = run.working_dir
        job_name = name if name is not None else run.name
        
        assert isinstance(working_dir, str)
        assert isinstance(job_name, str)

        lines = []
        # shebang
        lines.append("#!" + self.shell + " --login")

        # commend and empty line
        lines.append("# sge submission script generated by cfmt")
        lines.append("")

        # only request the job number back
        lines.append(self.get_batch_script_line("-terse"))
        
        # run in the current working directory
        lines.append(self.get_batch_script_line("-wd" + " " + str(working_dir)))

        # if we don't have any env modules to load, pass all environmental variables
        if self.pass_env:
            lines.append(self.get_batch_script_line("-V"))

        # any existing jobs to wait for ?
        if isinstance(self.hold_jid, list):
            line = "-hold_jid" + " " + ",".join([str(x) for x in self.hold_jid])
            lines.append(self.get_batch_script_line(line))

        # NOTE: The name is cut off by the hash irrespective of the quotes
        #         so we've moved this to be an explicit qsub option
        #lines.append(self.get_batch_script_line("-N" + " " + "'" + job_name + "'"))

        # use the bash shell
        lines.append(self.get_batch_script_line("-S" + " " + self.shell))
        
        if self.mode == "parallel":
            if self.pe is not None:
                line = "-pe" + " " + self.pe + " " + str(self.np)
                lines.append(self.get_batch_script_line(line))
                
        if self.queue is not None and len(self.queue) > 0:
            lines.append(self.get_batch_script_line("-q" + " " + self.queue))
        
        # handle any additional options
        # -> we should end up with a dict, with each key being a unique cmd and 
        #    the value being a list of options 
        _a = {}
        b = None
        for a in self.additional_options:
            if a.startswith("-"):
                b = a
                _a[b] = []
            else:
                if b is None: 
                    _str = " ".join(self.additional_options)
                    raise ValueError(f"Invalid addidional SGE options '{_str}'")
                else:
                    _a[b].append(a)
        
        for k, v in _a.items():
            lines.append(self.get_batch_script_line(k + " " +  " ".join(v)))

        # empty line
        lines.append("")

        # add modules
        m_lines = self.get_module_lines(code=code)
        lines += m_lines

        # hook for pre-exec lines
        p_lines = self.get_exec_lines(
            working_dir, 
            [study_dir, case_dir],
            PRE_EXEC,
            code
        )
        lines += p_lines
        
        command = []

        # we ask the code if required
        if task_cmd is not None:
            command += task_cmd
        elif code.has_tight_integration(self.system_name):
            # we ask the code if required
            command = code.get_command(run, self, cmd=command)
        else:
            job_binary = code.get_bin_path(wd=working_dir, batch_system=self)
            if job_binary is None:
                msg = f"Binary for code {code.name} not available"
                self.logger.error(msg)
                raise ValueError(msg)
            
            if self.mode == "parallel":
                command.append(self.parallel_exec)
                # bodge to disable core binding
                if self.np < 4:
                    command.append("--bind-to none")
                command.append("-np")
                command.append(str(self.np))

            # append the binary at the end     
            if isinstance(job_binary, list):
                command += job_binary
            else:
                command.append(job_binary)

        lines.append(" ".join(command))  

        lines.append("")
        
        # hook for post-exec lines
        p_lines = self.get_exec_lines(
            working_dir, 
            [study_dir, case_dir],
            POST_EXEC,
            code
        )
        lines += p_lines
        
        # append line breaks to each line 
        lines = [l + "\n" for l in lines]
        fname = os.path.join(working_dir, self.script_name)
        
        self.write_submission_script(fname, lines)

        # save the job name
        self.job_name = job_name

        return fname


    def get_batch_script_line(self, line, prefix="#$"):
        """
        Prepends the SBATCH prefix to the line
        """
        return prefix + " " + line
        
    def parse_submission_output(self, proc):
        """ 
        Parse the qsub output
        
        proc the subprocess Popen return
        """
        # wait for the process to finish
        returncode = proc.wait()
        
        if returncode != 0:
            # error with the submission, log the problem and return
            raise exceptions.JobFailed(self.system_name + ":" + proc.stderr.read())
        else:
            # success and we have the job_id
            stdout = proc.stdout.read()
            # submissions generated with this file will have the terse switch is active
            # so we should just be able to fetch it straight from the screen
            try:
                job_id = int(stdout)
            except ValueError as err:
                utils.print_exception_info(err)
                sys.exit(1)
                # try to see if terse has been missed
            return job_id

    def update(self, jobs, detailed=False):
        """ Fetches the most recent information for the runs specified in runs """
        import subprocess
        import xml.etree.ElementTree as ET
        
        # check jobs is a list
        assert isinstance(jobs, list)

        # if not empty
        if jobs:

            # issue a qstat command
            command = [self._qstat, '-xml']
            proc = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        
            if proc.returncode == 0:
                # success
                try:
                    # attemp to parse the output
                    root = ET.fromstring(proc.stdout)

                    # loop over our jobs and check for them
                    for __job in jobs:
                        # fetch the tag from the tree if it exists
                        xpath = ".//job_list[JB_job_number='" + str(__job.job_id) + "']"
                        job = root.findall(xpath)
                        if job:
                            _job = job.pop()
                            
                            # handle the state
                            self.parse_state(_job.find('state').text, __job)
                            
                            # update the start time if required
                            start_time = _job.find('JAT_start_time')
                            if start_time is not None:
                                __job.started = datetime.strptime(start_time.text, self.time_fmt['qstat'])
                                __job.set_db(started=__job.started)

                        else:
                            # we need to check qacct for finished jobs
                            try:
                                self.parse_qacct(__job)
                            except exceptions.BatchNotReady as err:
                                # raised when the batch system is not ready
                                print(f"WARNING: JOB {__job.id} not updated as batch system not ready")
                                self.logger.warning("JOB: '%s': " + str(err), __job.name)
                                continue
                            except exceptions.UpdateFailed as err:
                                # raised when qacct gives a non-zero exit code
                                self.logger.warning("JOB: '%s': " + str(err), __job.name)

                                # mark the job as in an error state
                                __job.update_status(__job.ERROR, update_run=True)
                                continue


                    
                except ET.ParseError:
                    if self.console.isEnabledFor(self.console.DEBUG):
                        raise
                    else:
                        raise exceptions.UpdateFailed("Cannot parse XML output from qstat")
            else:
                # fail
                raise exceptions.UpdateFailed("Error with qstat command '{}'".format(" ".join(command)))

        # 
    def parse_qacct(self, job):
        """
        Provided a job object, parse qacct and update accordingly

        qacct provides no xml output, so we rely on more primative methods
        """
        import subprocess
        
        # get the qacct output for the run specified 
        assert isinstance(job.job_id, int)

        command = [self._qacct, '-j', str(job.job_id)]

        proc = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        if proc.returncode == 0:
            # success
            # qacct splits multiple entries by ====
            parsed = []
            entry = {}
            for line in proc.stdout.splitlines():
                # split by space
                _line = line.split(" ")
                
                # remove consecutive empty spaces
                _line = list(filter(None, _line))

                # each item should have 2 things
                if len(_line) >= 2:
                    # store in a dict
                    entry[_line[0]] = _line[1:]
                elif line.startswith('==='):
                    # we're starting something
                    parsed.append(entry)
                    entry = {}

            # append the last entry
            parsed.append(entry)
            # remove non-existance entries
            parsed = list(filter(None, parsed))
            
            # qacct may return multiple entries if they exist, in a list match the name
            if len(parsed) > 1:
                result = next((item for item in parsed if job.name in item['jobname']), False)
                #result = list(filter(lambda item: item['jobname'] == run.name, parsed))
            else:
                result = parsed.pop()
            
            assert isinstance(result, dict)

            # throw an error if we now have nothing
            if not result:
                raise exceptions.UpdateFailed("qacct returned an entry but it was filtered out")

            # deduce what we have/require. rusage fields in accordance with https://docs.python.org/3/library/resource.html#resource.getrusage
            items_to_parse = [
                'exit_status', 
                'qsub_time', 
                'start_time', 
                'end_time', 
                'failed', 
                'cpu', 
                'maxvmem', 
                'qname', 
                'hostname',
                'granted_pe'
            ]

            # add on the rusage fields
            items_to_parse += self.rusage_fields

            found_items = []
            for item in items_to_parse:
                if item in result:
                    found_items.append(item)
                else:
                    self.console.warning("Run: '%s': sge did not return '%s', update may be incomplete", job.name, item)
                    self.logger.warning("Run: '%s': sge did not return '%s', update may be incomplete", job.name, item)

            # handle rusage first, since we may overwrite some 
            self.parse_rusage(result, job)
            
            # handle exit_status
            if 'exit_status' in found_items:
                # determine state
                if isinstance(result['exit_status'], list):
                   exit_status = result['exit_status'][0]
                else:
                   exit_status = result['exit_status']

                try:
                   exit_status = int(exit_status)
                except ValueError:
                   exit_status = None
                job.exit_status = exit_status
                if exit_status == 0:
                    job.update_status(job.COMPLETED, update_run=True)
                else:
                    job.update_status(job.ERROR, update_run=True)

            if 'qname' in found_items:
                # parse the queue name
                queue_name = result['qname'].pop() if isinstance(result['qname'], list) else result['qname']
                job.batch_queue = str(queue_name)

            if 'hostname' in found_items:
                # parse the execution host
                execution_host = result['hostname'].pop() if isinstance(result['hostname'], list) else result['hostname']
                job.execution_host = str(execution_host)

            if 'granted_pe' in found_items:
                # parse the pe
                granted_pe = result['granted_pe'].pop() if isinstance(result['granted_pe'], list) else result['granted_pe']
                if granted_pe == 'NONE':
                    job.pe = None
                else:
                    job.pe = str(granted_pe)
            
            if 'qsub_time' in found_items:
                # parse the submission time
                time = " ".join(result['qsub_time'])
                try:
                    time = datetime.strptime(time, self.time_fmt['qacct'])
                    job.submitted = time
                except ValueError:
                    self.logger.warning("Job: '%s': could not update submitted time", job.job_id)

            start_time = None
            if 'start_time' in found_items:
                # update the start time
                time = " ".join(result['start_time'])
                try:
                    start_time = datetime.strptime(time, self.time_fmt['qacct'])
                    job.started = start_time
                except ValueError:
                    self.logger.warning("Job: '%s': could not update start time", job.job_id)

            end_time = None
            if 'end_time' in found_items:
                # update the end time
                time = " ".join(result['end_time'])
                try:
                    end_time = datetime.strptime(time, self.time_fmt['qacct'])
                    job.finished = end_time
                except ValueError:
                    self.logger.warning("Job: '%s': could not update end time", job.job_id)

            if 'cpu' in found_items:
                # record the cpu seconds
                cpu_time = result['cpu'].pop() if isinstance(result['cpu'], list) else result['cpu']
                cpu_time = float(cpu_time.rstrip('s'))
                if cpu_time:
                    job.cpu_time = cpu_time
                else:
                    self.logger.warning("Job: '%s': could not update cpu time", job.job_id)
            
            if 'maxvmem' in found_items:
                # record the cpu seconds
                max_vmem = result['maxvmem'].pop() if isinstance(result['maxvmem'], list) else result['maxvmem']
                # convert human readble format to bytes
                try:
                    max_vmem = utils.human2bytes(str(max_vmem))
                except ValueError:
                    max_vmem = False
                
                if max_vmem:
                    job.vmem = max_vmem
                else:
                    self.logger.warning("Job: '%s': could not update max vmem", job.job_id)

        else:
            # try and detect when it doesn't exist, so we can leave it untouched
            # -> there can be a delay when writing the accounting information
            # -> job id {} not found
            import re
            pattern = re.compile(r"job\sid\s[0-9]+\snot\sfound")
            match = pattern.search(proc.stderr)
            if match:
                # raise an
                raise exceptions.BatchNotReady("qacct likely not been updated")

            # either it does not exist or there has been a problem
            raise exceptions.UpdateFailed("qacct returned a non-zero exit code")


    def parse_rusage(self, result, job):
        """
        Provided a job object and parsed qacct, we form a structured rusage object and
        update the job as required
        """
        rusage = {}
        for field in self.rusage_fields:
            # check we have it and make a dict
            if field in result:
                # print
                rusage[field] = result[field].pop() if isinstance(result[field], list) else result[field]
            else:
                self.logger.warning("Job: '%s': sge did not return '%s', update may be incomplete", job.name, field)
                continue

        # convert to struct
        rusage_obj = self.get_rusage(rusage_dict=rusage)
        
        # tell the job
        job.set_rusage(rusage_obj)
    
    def parse_state(self, state, job):
        """
        Provided a job object, update according to the state string
        
        See http://www.softpanorama.org/HPC/Grid_engine/Queues/queue_states.shtml 
        for more details on state codes
        """
        assert isinstance(state, str)
        
        if state == 'qw':
            # pending
            status = job.QUEUING
        elif state == 'hqw':
            # pending, hold
            status = job.HOLDING
        elif state == 'hRwq':
            # pending, hold, requeue
            status = job.HOLDING
        elif state == 'r':
            # running
            status = job.RUNNING
        elif state == 't':
            # transferering
            status = job.RUNNING
        elif state ==  'Rr':
            # running resubmit
            status = job.RUNNING
        elif state in ['s','ts']:
            # suspended
            status = job.SUSPENDED
        elif state in ['S', 'tS']:
            # suspended by queue
            status = job.SUSPENDED
        elif state in ['Rs', 'Rts', 'RS', 'RtS', 'RT', 'RtT']:
            # all suspending with re-submit
            status = job.RUNNING
        elif state in ['Eqw', 'Ehqw', 'EhRqw']:
            # all running and suspended states with deletion
            status = job.ERROR
        elif state in ['dr','dt','dRr','dRt','ds', 'dS', 'dT','dRs', 'dRS', 'dRT']:
            # all running and suspended states with deletion
            status = job.ABORTED
        else:
            # something else, mark as attention
            status = job.ERROR

        # update the job and run
        job.update_status(status, update_run=True)


    def parse_qacct_line(self, line):
        """
        Given a valid line from an SGE accounting file, return a dictionary
        of each of the fields as a key. Values are parsed as described in
        http://manpages.ubuntu.com/manpages/lucid/man5/sge_accounting.5.html
        """

        try:
            return {
                "qname": fields[0],
                "hostname": fields[1],
                "group": fields[2],
                "owner": fields[3],
                "jobname": fields[4],
                "jobnumber": int(fields[5]),
                "account": fields[6],
                "priority": float(fields[7]),
                "qsub_time": int(fields[8]),
                "start_time": int(fields[9]),
                "end_time": int(fields[10]),
                "failed": int(fields[11]),
                "exit_status": int(fields[12]),
                "ru_wallclock": float(fields[13]),
                "ru_utime": float(fields[14]),
                "ru_stime": float(fields[15]),
                "ru_maxrss": float(fields[16]),
                "ru_ixrss": float(fields[17]),
                "ru_ismrss": float(fields[18]),
                "ru_idrss": float(fields[19]),
                "ru_isrss": float(fields[20]),
                "ru_minflt": float(fields[21]),
                "ru_majflt": float(fields[22]),
                "ru_nswap": float(fields[23]),
                "ru_inblock": float(fields[24]),
                "ru_oublock": float(fields[25]),
                "ru_msgsnd": float(fields[26]),
                "ru_msgrcv": float(fields[27]),
                "ru_nsignals": float(fields[28]),
                "ru_nvcsw": float(fields[29]),
                "ru_nivcsw": float(fields[30]),
                "project": fields[31],
                "department": fields[32],
                "granted_pe": fields[33],
                "slots": int(fields[34]),
                "taskid": int(fields[35]),
                "cpu": float(fields[36]),
                "mem": float(fields[37]),
                "io": float(fields[38]),
                "category": self.parse_category(fields[39]),
                "iow": float(fields[40]),
                "pe_taskid": int(fields[41]) if fields[41] != "NONE" else None,
                "maxvmem": float(fields[42]),
                "arid": fields[43],
                "ar_submission_time": fields[44]
            }
        except IndexError:
            sys.stderr.write("[WARN] Seemingly invalid job line encountered. Skipping.\n")
            return None
    
    def submit(self, job):
        """ 
        Recieves a job object and executes
        Handles output
        """
        import subprocess
        
        if not isinstance(job.command_list, list):
            msg = f"Command list missing for Job: {job.id}"
            self.logger.error("msg")
            raise ValueError(msg)
        
        print(" -> Submitting job using {} ... ".format(self.system_name), end="\r")
        self.logger.info("Submitting job using %s", self.system_name)
        try:
            proc = subprocess.Popen(job.command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

            # parse to get job_id (checks return code as well)
            job_id = self.parse_submission_output(proc)
        except exceptions.JobFailed as err:
            print(" -> Submitting job using {} ... FAILED".format(self.system_name))
            self.logger.info("Run '%s' failed", job.name)
            utils.print_exception_info(msg=str(err))
            job.update_status(job.ABORTED, update_run=True)
        except Exception as err:
            print(" -> Submitting job using {} ... FAILED".format(self.system_name))
            self.logger.error("Run '%s' failed", job.name)
            utils.print_exception_info(msg="Unknown Error", debug=str(err))
            job.update_status(job.ERROR, update_run=True)
        else:
            print(" -> Submitting job using {} ... OK JOB: {}".format(self.system_name, job_id))
            job.update_status(job.SUBMITTED, update_run=True)
            job.update(job_id=job_id, submitted=datetime.now())
            
            # we need the job_id to be present in order to update the fnames
            job.update(
                stdout=self.get_stdout_fname(job),
                stderr=self.get_stderr_fname(job),
            )

            self.logger.info("Run '%s' submitted as JOB: %s", job.name,job_id)
