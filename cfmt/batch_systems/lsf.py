"""
    cft: A management tool for Computational Fluid Dynamics
    
    lsf: Interface for the intel LSF batch system
"""
from json.decoder import JSONDecodeError
from logging import exception
import shutil
import sys
import os
from sys import exc_info
import time
from datetime import datetime,timedelta

from peewee import Value
from cfmt.config import configtool as config
from cfmt.batch_systems.base import BatchSystem, PRE_EXEC, POST_EXEC
from cfmt import exceptions, utils
    
NAME="LSF"
BPREFIX="#BSUB"
            

class LSF(BatchSystem):
    """ 
    LSF Submission Plugin
    
    Provides an interface to the SLURM submission system using system binaries

    Options supported
    --workdir=[dir] Execute the job from the current directory
    --export=[ALL|NONE|variable] 
                    Export environmental variables
    --job-name=[name]
                    Sets the job name
    -o file/dir     Path to directory or file for stdout
    -e file/dir     Path to directory or file for stderr
    -hold_jid JOBID Job is conditional upon completion of job jobid
    --wrap=""       Wraps the enclosed into a simple submission script
    -p PARTITION    Submit to specific partition (queue)
    """
    
    # list of binary names to search for
    binaries = [
        'bsub',     # submit a job for execution
        'bjobs',    # get information about jobs
        'bhist',    # get the history of jobs in the batch system
    ]
    TIME_FMT = {
        'bjobs': '%b %d %H:%M',
    }
    # regex to match wallclock time input, see https://regex101.com/r/68hG8U/1
    wallclock_time_re = r"^([0-9]+:)?(?(1)[0-9]{2}|[0-9]+)$"
    cpu_time_re = r"^([0-9.]+) second\(s\)"
    RUN_TIME_RE = cpu_time_re
            
            # See https://regex101.com/r/68hG8U/1
    DEFAULT_SHELL = "/bin/bash"
    DEFAULT_SCRIPT_NAME = "lsfbatch.sh"
    DEFAULT_QUEUE = None
    DEFAULT_RESOURCE = ""
    DEFAULT_WTIME = "01:00"     # default wall clock time (1hr)

    PARSE_BSUB_RE = r"Job <([0-9]+)> is submitted to queue <[a-zA-z0-9]+>"
    
    system_name = "lsf"
    
    STDOUT_MASK="{job.name}" + ".oi" + "{job.job_id}"
    STDERR_MASK="{job.name}" + ".ei" + "{job.job_id}"
    SCRIPT_STDOUT_MASK="$LSF_JOBNAME" + ".om" + "$LSF_JOBID" 
    SCRIPT_STDERR_MASK="$LSF_JOBNAME" + ".em" + "$LSF_JOBID"


    def __init__(self, batch_options=None, parallel=None, np=None, parallel_exec=None, cmdline_args=None):
        """ Constructor """

        # call parent
        super().__init__(batch_options=batch_options, parallel=parallel, np=np, parallel_exec=parallel_exec)

        # binary locations
        for binary in self.binaries:
            #path = shutil.which(binary)
            #if path is None:
            #    msg = f"Cannot find '{self.system_name}' binary '{binary}'"
            #    self.logger.error(msg)
            #    print(msg)
            #    sys.exit(0)
            #else:
                # set as private attributes
            setattr(self, "_" + binary, binary)

        # process the queue
        if cmdline_args is not None and cmdline_args.queue is not None:
            queue = cmdline_args.queue[-1]
        elif batch_options is not None and "queue" in batch_options:
            queue = batch_options["queue"]
        else:
            queue = config.get(self.system_name, self.mode + "_queue", fallback=config.get(self.system_name, "queue", fallback=self.DEFAULT_QUEUE))

        # process the script name
        if cmdline_args is not None and cmdline_args.lsf_script_name is not None:
            script_name = cmdline_args.lsf_script_name[-1]
        elif batch_options is not None and "script_name" in batch_options:
            script_name = batch_options["script_name"]
        else:
            script_name = config.get(self.system_name, "script_name", fallback=self.DEFAULT_SCRIPT_NAME)
        
        # process any resource flags, these are additive
        resources = []
        if cmdline_args is not None and cmdline_args.lsf_resource is not None:
            assert isinstance(cmdline_args.lsf_resource, list)
            resources = cmdline_args.lsf_resource
            # argparse puts them as a list of lists
            resources = [a.pop() for a in resources]
        
        if batch_options is not None and "resources" in batch_options:
            resources += batch_options["resources"]
        else:
            _resources = config.get(self.system_name, "resources", fallback=self.DEFAULT_RESOURCE)
            # they return as a space separated list
            resources += _resources.split(" ")
        
        # process the wallclock time
        if cmdline_args is not None and cmdline_args.lsf_wtime is not None:
            wallclock_time = self.valid_timedelta(str(cmdline_args.lsf_wtime[-1]))
        elif batch_options is not None and "wallclock_time" in batch_options:
            wallclock_time = batch_options["wallclock_time"]
        else:
            wallclock_time = config.get(self.system_name, "wallclock_time", fallback=self.DEFAULT_WTIME)
            # check its valid, trimming quotes if they exist
            wallclock_time = self.valid_timedelta(wallclock_time.strip(r'"\''))
        
        # sge shell doesn't have a cmdline option
        if batch_options is not None and "shell" in batch_options:
            shell = batch_options["shell"]
        else:
            shell = config.get(self.system_name, "shell", fallback=self.DEFAULT_SHELL)

        
        # process any additional batch options (additive here)
        additional_options = []
        if cmdline_args is not None and cmdline_args.batch_options is not None:
            additional_options = cmdline_args.batch_options
        
        if batch_options is not None and "extra" in batch_options:
            additional_options += batch_options["extra"]
        else:
            _options = config.get(self.system_name, "additional_options", fallback="")
            additional_options += _options.split()

        self.queue = queue
        self.script_name = script_name
        self.shell = shell
        self.wallclock_time = wallclock_time
        self.additional_options = additional_options

        # set this to None
        self.submission_script = None
        
        # remove duplicate resources
        self.resources = utils.make_list_unique(resources)

    @classmethod
    def valid_timedelta(cls, timestring):
        """ Checks for a valid time string

        LSF takes the form [hour:]minutes with optional minutes, see:
        https://www.ibm.com/support/knowledgecenter/SSWRJV_10.1.0/lsf_command_ref/bsub.man_top.1.html

        Since minutes can be > 60 and the hours is optional, we use a regex string to match

        """
        import argparse
        import re

        # compile 
        p = re.compile(cls.wallclock_time_re)

        match = p.match(timestring)

        if match:
            return timestring
        else:
            msg = "Not a valid time format: '{}'\n".format(timestring)
            msg += "Valid format is [hours:]minutes, where minutes can be > 60 if specified alone\n"
            raise argparse.ArgumentTypeError(msg)
    
    def get_batch_options(self, run):
        """
        Creates an ordered dict of batch arguments from the current attributes
        """
        batch_options = {
            "mode": self.mode,                          # parallel|serial
            "queue": self.queue,                 
            "shell": self.shell,
            "script_name": self.script_name,
            "np": self.np,                              # note this is -n in the submission script
            "resources": self.resources,                # list of -R resources to request
            "wallclock_time": self.wallclock_time,     # the -W switch, requested wall-clock time
            "extra": self.additional_options,
        }
        return batch_options

    def generate_command(self, run, code, task_cmd=None, name=None):
        """ 
            Compiles and returns the command as a list 
        """
        # generate the submission script
        fscript = self.generate_submission_script(run, code, task_cmd=task_cmd, name=name)

        command = []
        
        # the _bsub binary
        assert self._bsub is not None
        command.append(self._bsub)

        # store the script - this is fed as stdin
        self.submission_script = fscript
        
        self.command = command
        return command


    def generate_submission_script(self, run, code, task_cmd=None, name=None):
        """
        Generate a simple shell script to submit, located in the working directory
        Script will be copied and kept
        """
        study_dir = run.study.path
        case_dir = run.case.abspath
        working_dir = run.working_dir
        job_name = name if name is not None else run.name
        
        assert isinstance(working_dir, str)
        assert isinstance(job_name, str)

        lines = []
        # shebang
        lines.append("#!" + self.shell)

        # commend and empty line
        lines.append("# LSF submission script generated by cfmt")
        lines.append("# ---------------------------------------")

        # set the job name
        lines.append(self.get_batch_script_line("-J \"" + str(job_name) + "\""))
        
        # set the output and error format
        lines.append(self.get_batch_script_line("-o " + job_name + ".oi%J"))
        lines.append(self.get_batch_script_line("-e " + job_name + ".ei%J"))
        
        # set number of processors
        lines.append(self.get_batch_script_line("-n " + str(self.np)))

        # handle any resource requirementis
        for r in self.resources:
            lines.append(self.get_batch_script_line("-R \"" + str(r) + "\""))

        # handle any additional options
        # -> we should end up with a dict, with each key being a unique cmd and 
        #    the value being a list of options 
        _a = {}
        b = None
        for a in self.additional_options:
            if a.startswith("-"):
                b = a
                _a[b] = []
            else:
                if b is None: 
                    _str = " ".join(self.additional_options)
                    raise ValueError(f"Invalid addidional LSF options '{_str}'")
                else:
                    _a[b].append(a)
        
        for k, v in _a.items():
            lines.append(self.get_batch_script_line(k + " " +  " ".join(v)))

        # wallclock time 
        lines.append(self.get_batch_script_line("-W " + str(self.wallclock_time)))
        
        # set the queue; these are called partitions
        if self.queue is not None:
            lines.append(self.get_batch_script_line("-q " + str(self.queue)))

        lines.append("")
        
        # hook for pre-exec lines
        pre_lines = self.get_exec_lines(
            working_dir, 
            [study_dir, case_dir],
            PRE_EXEC,
            code
        )
        lines += pre_lines

        command = []
        
        # we ask the code if required
        if task_cmd is not None:
            command += task_cmd
        elif code.has_tight_integration(self.system_name):
            command = code.get_command(run, self, cmd=command)
        else:
            job_binary = code.get_bin_path(wd=working_dir, batch_system=self)
            
            if job_binary is None:
                msg = f"Binary for code {code.name} not available"
                self.logger.error(msg)
                raise ValueError(msg)

            if self.mode == "parallel":
                command.append(self.parallel_exec)
                command.append("-n")
                command.append(str(self.np))
            # append the binary at the end     
            if isinstance(job_binary, list):
                command += job_binary
            else:
                command.append(job_binary)
        
        lines.append(" ".join(command))

        lines.append("")
        
        # hook for post-exec lines
        pre_lines = self.get_exec_lines(
            working_dir, 
            [study_dir, case_dir],
            POST_EXEC,
            code
        )
        lines += pre_lines
        
        # append line break to each line
        lines = [l + "\n" for l in lines]
        fname = os.path.join(working_dir, self.script_name)

        # write the script
        self.write_submission_script(fname, lines)
        
        return fname

    def get_batch_script_line(self, line, prefix=BPREFIX):
        """
        Prepends the default prefix to the line
        """
        return prefix + " " + line
    
    def parse_submission_output(self, stdout, stderr):
        """ 
        Parse the bsub output
        """
        import re
        p = re.compile(self.PARSE_BSUB_RE)
        # regex
        match = False
        for l in stdout.splitlines():
            match = p.match(l)
            if match:
                break
        
        if not match:
            raise ValueError("Error parsing bsub output")
        
        # job number should be the first match
        return int(match.group(1))

    def update(self, jobs, detailed=False):
        """ 
        Fetches the most recent information for the jobs specified in jobs
        
        See bjobs manual for details 
        https://www.ibm.com/support/knowledgecenter/SSWRJV_10.1.0/lsf_command_ref/bjobs.o.1.html
        """
        
        # check jobs is a list
        assert isinstance(jobs, list)

        # get the datetime format
        datetime_fmt = self.TIME_FMT["bjobs"]


        # bjobs -o -json
        # fields to return
        fields = ["jobid", "job_name", "stat", "start_time", "finish_time", "first_host", "exit_code", "cpu_used", "max_mem", "run_time"]
        # if not empty
        if jobs:
            import json
            import subprocess

            # get a list of job ids
            job_ids = []
            for _job in jobs:
                job_ids.append(str(_job.job_id))

            # issue a bjobs command
            format_str = " ".join(fields)
            command = [self._bjobs, "-o", format_str, "-json"] + job_ids
            self.logger.debug("Running command '%s'", " ".join(command))
            proc = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

            jobs_not_found = [] 

            # if just one job is not found, then bjobs returns an error code of 255 
            if proc.returncode == 0 or proc.returncode == 255:
                # success
                try:
                    # load the json
                    parsed = json.loads(proc.stdout)

                    # get the RECORDS
                    records = parsed.get("RECORDS", None)

                    if records is None:
                        raise exceptions.UpdateFailed("No RECORDS returned from jobs") 
                    
                    # sort the original jobs array
                    jobs.sort(key=lambda i: i.job_id)

                    # records should be sorted but we do it anyway
                    records.sort(key=lambda i: i["JOBID"])
                    
                    for job,record in zip(jobs, records):
                        if str(job.job_id) != str(record['JOBID']):
                            raise exceptions.UpdateFailed("Mismatch between bjobs output and job list")

                        # if the job cannot be found, we need to check the stdout in the run_dir
                        if "ERROR" in record:
                            jobs_not_found.append(job)
                            continue

                        # parsed the records
                        try:
                            status = self.parse_record(job, record)
                        except (ValueError, KeyError) as err:
                            self.logger.warning("Failed to update Job: '%s', SKIPPING", job.id)
                            continue
                        else:
                            # otherwise, update the job_status
                            if job.status != status:
                                job.update_status(status, update_run=True)
                            
                except JSONDecodeError as err:
                    utils.print_exception_info(err)
                    raise exceptions.UpdateFailed("Error decoding bjobs output")
                except Exception as err:
                    utils.print_exception_info(err)
                    raise exceptions.UpdateFailed("Error parsing bjobs output")
        
            else:
                # either it does not exist or there has been a problem
                self.logger.error(proc.stdout)
                self.logger.error(proc.stderr)
                raise exceptions.UpdateFailed(f"bjobs returned a non-zero exit code: {proc.returncode}")


            # if we have jobs not found in the output, then it may be been purged from bjobs
            # -> lsf writes a job summary to the stdout, so we attempt to parse it
            if jobs_not_found:
                for job in jobs_not_found:
                    # get the batch stdout file
                    fname = self.get_stdout_fname(job)
                    fpath = os.path.join(job.run.get_run_dir(), fname)

                    if not os.path.isfile(fpath):
                        # then the job must have failed before the batch ran it
                        self.logger.debug("Count not find stdout file '%s' from '%s'. Possible the job did not run.", fpath, self.system_name)
                        job.update_status(job.ABORTED, update_run=True, update_code=True)
                        continue
                    
                    # parse the stdout file if it exists
                    try:
                        status = self.parse_stdout(fpath, job)
                    except exceptions.UpdateFailed as err:
                        self.logger.error(str(err), exc_info=err)
                        continue
                    else:
                        # otherwise, update the job_status
                        if job.status != status:
                            job.update_status(status, update_run=True)

    def parse_stdout(self, path, job):
        """
        Open and parse the given path for LSF output. Here we deduce:
            - 
        """
        import re
        SUBJECT_RE = re.compile(r"^Subject\:\sJob\s([0-9]+)\:\s<(.+?)>\sin\scluster\s<(.+?)>\s([a-zA-Z]+)\s*$", re.M)
        FIRST_HOST_RE = re.compile(r"^Job was executed on host\(s\)\s<(.+?)>", re.M)
        START_TIME_RE = re.compile(r"^Started\sat\s(.+)$", re.M)
        FINISH_TIME_RE = re.compile(r"^Terminated\sat\s(.+)$", re.M)

        BASE_TIME_RE = r"\s:\s*(([0-9.]+)\ssec\.|\-)"   # matches float time
        CPU_TIME_RE = re.compile(r"CPU time" + BASE_TIME_RE, re.M)
        #RUN_TIME_RE = re.compile(r"Run time" + BASE_TIME_RE, re.M)
        #TURN_TIME_RE = re.compile(r"Turnaround time" + BASE_TIME_RE, re.M)
        
        BASE_MEM_RE = r"\s:\s*([0-9.]+\s[A-Z]*|-)"
        MAX_MEM_RE = re.compile(r"Max Memory" + BASE_MEM_RE, re.M)
        #AVERAGE_MEM_RE = re.compile(r"Average Memory" + BASE_MEM_RE, re.M)
        #TOTAL_REQ_MEM_RE = re.compile(r"Total Requested Memory" + BASE_MEM_RE, re.M)
        #DELTA_MEM = re.compile(r"Delta Memory" + BASE_MEM_RE, re.M)
        #MAX_SWAP_MEM = re.compile(r"Max Swap" + BASE_MEM_RE, re.M)

        #BASE_INT_RE = r"s:\s*([0-9]+|-)"
        #MAX_PROC_RE = re.compile(r"Max Processes" + BASE_INT_RE, re.M)
        #MAX_THREADS_RE = re.compile(r"Max Threads" + BASE_INT_RE, re.M)

        EXIT_CODE_RE = re.compile(r"Exited with exit code\s([0-9]+).", re.M)
        
        END_MARKER = r"The output (if any) follows:"
        TIME_FMT = "%a %b %d %H:%M:%S %Y" # e.g. Thu Nov 12 15:57:23 2020

        # we read all the lines until we match the END_MARKER
        lines = []
        with open(path, 'r') as f:
            for line in f:
                _line = line.rstrip()
                if _line == END_MARKER:
                    break
                lines.append(_line)

        # recombine
        text = "\n".join(lines)

        # search our text
        # SUBJECT - job_id, job_name, cluster_name, status
        match = re.search(SUBJECT_RE, text)
        if not match:
            raise exceptions.UpdateFailed("Failed to parse lsf output in '%s'", path)
            # check the jobid is correct
        else:
            job_id = int(match.group(1))
            # check that we have the right job
            if job_id != job.job_id:
                raise exceptions.UpdateFailed(f"Job id: '{job_id}' in lsf out does not match current job '{job.job_id}'")

            #job_name = match.group(2)
            #cluster_name = match.group(3)
            # only two statuses recognized to date
            status_str = match.group(4)
            if status_str.lower() == "done":
                status = job.COMPLETED
            elif status_str.lower() == "exited":
                status = job.ERROR
            else:
                self.logger.warning(f"Unknown status returned from lsf output: '{status_str}'")
                status = job.ERROR

        # START TIME
        match = re.search(START_TIME_RE, text)
        if not match:
            self.logger.warning("Job: '%s': Error matching REGEX '%s' from lsf output", job.job_id, START_TIME_RE)
        else:
            time_str = match.group(1)
            try:
                time = datetime.strptime(time_str, TIME_FMT)
            except ValueError:
                self.logger.warning("Job: '%s': Failed to parse time string '%s' from lsf output", job.job_id, time_str)
            else:
                job.started = time

        # FINISH TIME
        match = re.search(FINISH_TIME_RE, text)
        if not match:
            self.logger.warning("Job: '%s': Error matching REGEX '%s' from lsf output", job.job_id, FINISH_TIME_RE)
        else:
            time_str = match.group(1)
            try:
                time = datetime.strptime(time_str, TIME_FMT)
            except ValueError:
                self.logger.warning("Job: '%s': Failed to parse time string '%s' from lsf output", job.job_id, time_str)
            else:
                job.finished = time

        # CPU TIME
        # -> if there is 1 match only, we got a "-"
        match = re.search(CPU_TIME_RE, text)
        if not match:
            self.logger.warning("Job: '%s': Error matching REGEX '%s' from lsf output", job.job_id, CPU_TIME_RE)
        elif match.group(1).strip() == "-":
            # it wasn't reported 
            self.logger.warning("Job: '%s': 'CPU Time' not reported in lsf output", job.job_id)
        else:
            try:
                cpu_time = float(match.group(2))
            except ValueError:
                self.logger.warning("Job: '%s': Failed to parse CPU_TIME from lsf output", job.job_id)
            else:
                job.cpu_time = cpu_time

        # MAX MEMORY
        match = re.search(MAX_MEM_RE, text)
        if not match:
            self.logger.warning("Job: '%s': Error matching REGEX '%s' from lsf output", job.job_id, MAX_MEM_RE)
        elif match.group(1).strip() == "-":
            # it wasn't reported 
            self.logger.warning("Job: '%s': 'Max Memory' not reported in lsf output", job.job_id)
        else:
            try:
                # try and trim off the B
                mem_str = match.group(1).strip("B")
                max_mem = utils.human2bytes(mem_str)
            except ValueError as err:
                self.logger.warning("Job: '%s': Failed to parse CPU_TIME from lsf output", job.job_id, exc_info=err)
            else:
                job.max_vmem = max_mem

        # EXIT CODE
        if status == job.COMPLETED:
            job.exit_status = 0
        else:
            match = re.search(EXIT_CODE_RE, text)
            if not match:
                self.logger.warning("Job: '%s': Error matching REGEX '%s' from lsf output", job.job_id, EXIT_CODE_RE)
            else:
                job.exit_status = int(match.group(1))
        
        return status 

    
    def parse_state(self, state, job):
        """
        Provided a job object, update according to the state string
        
        See https://www.ibm.com/support/knowledgecenter/SSWRJV_10.1.0/lsf_command_ref/bjobs.zz4category.description.1.html 
            for more details on state codes
        """
        abnormal_state = [job.ERROR, job.ABORTED, job.SUSPENDED]
        assert isinstance(state, str)
        # unchanged
        status = job.status
        msg = ""
        
        if state == "PEND":
            status = job.QUEUING
            msg = "The job is pending. That is, it has not yet been started."
        elif state == "PROV":
            status = job.QUEUING
            msg = "The job has been dispatched to a power-saved host that is waking up. Before the job can be sent to the sbatchd, it is in a PROV state."
        elif state == "PSUSP":
            status = job.SUSPENDED
            msg = "The job has been suspended, either by its owner or the LSF administrator, while pending."
        elif state == "RUN":
            status = job.RUNNING
            msg = "The job is currently running."
        elif state == "USUSP":
            status = job.SUSPENDED
            msg = "The job has been suspended, either by its owner or the LSF administrator, while running."
        elif state == "SSUSP":
            status = job.SUSPENDED
            msg = "The job has been suspended by LSF. The following are examples of why LSF suspended the job:\n"
            msg += "The load conditions on the execution host or hosts have exceeded a threshold according to the loadStop vector defined for the host or queue.\n"
            msg += "The run window of the job's queue is closed. See bqueues(1), bhosts(1), and lsb.queues(5)."
        elif state == "DONE":
            status = job.COMPLETED
            msg = "The job has terminated with status of 0."
        elif state == "EXIT":
            status = job.ERROR
            msg = "The job has terminated with a non-zero status – it may have been aborted due to an error in its execution, or killed by its owner or the LSF administrator.\n"
            msg += "For example, exit code 131 means that the job exceeded a configured resource usage limit and LSF killed the job."
        elif state == "UNKWN":
            msg = "mbatchd has lost contact with the sbatchd on the host on which the job runs."
        elif state == "WAIT":
            status = job.QUEUING
            msg = "For jobs submitted to a chunk job queue, members of a chunk job that are waiting to run."
        elif state == "ZOMBI":
            status = job.ERROR
            msg = "A job becomes ZOMBI if:\n"
            msg += "A non-rerunnable job is killed by bkill while the sbatchd on the execution host is unreachable and the job is shown as UNKWN.\n"
            msg += "After the execution host becomes available, LSF tries to kill the ZOMBI job. Upon successful termination of the ZOMBI job, the job's status is changed to EXIT.\n"
            msg += "With the LSF multicluster capability, when a job running on a remote execution cluster becomes a ZOMBI job, the execution cluster treats the job the same way as local ZOMBI jobs. In addition, it notifies the submission cluster that the job is in ZOMBI state and the submission cluster requeues the job."
        
        # log the message
        if job.status in abnormal_state:
            if job.status != status:
                log_msg = "Job {} in abnormal state: " + msg
                self.logger.info(log_msg)
        
        # update the job and run
        #if job.status != status:
        #    job.update_status(status, update_run=True)
        
        return status

    def parse_record(self, job, record):
        """
        Parses an individual JSON record returned from bjobs

        Raises an exception if we cannot update
        """
        import re
        
        # get the datetime format
        DATETIME_FMT = self.TIME_FMT["bjobs"]
        
        # bjobs doesn't return the year, assume its the current year
        YEAR = datetime.now().year

        # REGEX for CPU
        CPU_TIME_RE = self.cpu_time_re
        RUN_TIME_RE = self.RUN_TIME_RE
        
        # handle the state
        field = "STAT"
        if field not in record:
            raise KeyError(f"Missing '{field}' from bjobs record, cannot process Job: {job.id}")
        
        status = self.parse_state(record[field], job) # note the status is updated here

        # update the start time
        field = "START_TIME"
        if field in record:
            try:
                start_time = datetime.strptime(record[field], DATETIME_FMT).replace(year=YEAR)
            except ValueError:
                if job.status in [job.RUNNING, job.COMPLETED]:
                    self.logger.warning("Job: '%s': Error parsing field '%s' from bjobs record", job.job_id, field)
            else:
                job.started = start_time
        else:
            self.logger.warning(f"Missing '{field}' from bjobs record")
        
        # get the first execution host
        field = "FIRST_HOST"
        if field in record:
            execution_host = record[field]
            if execution_host:
                job.execution_host = str(execution_host)
        else:
            self.logger.warning(f"Missing '{field}' from bjobs record")
            
        # if the job has ended
        if status in [job.COMPLETED, job.ABORTED, job.ERROR]: 
            field = "EXIT_CODE"
            if field in record:
                # we expect an exit code, but it will be blank if the job completed OK
                try:
                    if status == job.COMPLETED:
                        exit_code = 0
                    else:
                        exit_code = int(record[field])
                except ValueError:
                    self.logger.warning("Job: '%s': Error parsing field '%s' from bjobs record", job.job_id, field)
                else:
                    job.exit_status = exit_code
            else:
                self.logger.warning(f"Missing '{field}' from bjobs record")
            
            # update the end time
            # -> we compute from the run time, since the finish time is only accurate to minutes
            field = "RUN_TIME"
            if field in record:
                try:
                    # compile 
                    p = re.compile(RUN_TIME_RE)
                    
                    match = p.match(record[field])
                    
                    if match:
                        run_time = float(match.group(1))
                    else:
                        raise ValueError("Failed to match regex '%s' on record '%s'", RUN_TIME_RE, record[field])
                    
                    # the finish_time sometimes has an " L" at the end, not clear what that is so we strip it
                    end_time = job.started + timedelta(seconds=run_time)
                except ValueError:
                    self.logger.warning("Job: '%s': Error parsing field '%s' from bjobs record", job.job_id, field)
                else:
                    job.finished = end_time
            else:
                self.logger.warning(f"Missing '{field}' from bjobs record")
            #field = "FINISH_TIME"
            #if field in record:
            #    try:
            #        # the finish_time sometimes has an " L" at the end, not clear what that is so we strip it
            #        end_time = record[field].strip(" L")
            #        end_time = datetime.strptime(end_time, DATETIME_FMT).replace(year=YEAR)
            #    except ValueError:
            #        self.logger.warning("Job: '%s': Error parsing field '%s' from bjobs record", job.job_id, field)
            #    else:
            #        job.finished = end_time
            #else:
            #    self.logger.warning(f"Missing '{field}' from bjobs record")

            # parse the CPU usage
            field = "CPU_USED"
            if field in record:
                try:
                    # compile 
                    p = re.compile(CPU_TIME_RE)
                    
                    match = p.match(record[field])
                    
                    if match:
                        cpu_time = float(match.group(1))
                    else:
                        raise ValueError("Failed to match regex '%s' on record '%s'", CPU_TIME_RE, record[field])

                except ValueError as err:
                    self.logger.warning("Job: '%s': Error parsing field '%s' from bjobs record", job.job_id, field, exc_info=err)
                else:
                    job.cpu_time = cpu_time
        
            # parse the VMEM usage
            field = "MAX_MEM"
            if field in record:
                try:
                    # max_mem example: "1.4 Gbytes", "1.4 Mbytes"
                    # human2bytes cannot deal with the bytes so we remote it
                    max_vmem = str(record[field]).rstrip("bytes")
                    max_vmem = utils.human2bytes(max_vmem)
                except ValueError as err:
                    self.logger.warning("Job: '%s': Error parsing field '%s' from bjobs record", job.job_id, field, exc_info=err)
                else:
                    job.vmem = max_vmem

        return status
    
    
    def submit(self, job):
        """ 
        Submits the job to lsf

        -> The bsub command expects the submission script to be passed as stdin
        -> LSF also does not have a working directory option, so we need to submit in
           the correct working dir
        """
        import subprocess
        
        if not isinstance(job.command_list, list):
            msg = f"Command list missing for Job: {job.id}"
            self.logger.error(msg)
            raise ValueError(msg)
        
        # submission script
        script = self.submission_script
        
        if script is None:
            # the job may be assigned previously, find the script
            script = os.path.join(job.working_dir, self.script_name)

        if not os.path.isfile(script):
            msg = f"Submission script missing for Job: {job.id}"
            self.logger.error(msg)
            raise ValueError(msg)
        
        # open the script for reading
        fscript = open(script, 'r')

        print(" -> Submitting job using {} ... ".format(self.system_name), end="\r")
        self.logger.info("Submitting job using %s", self.system_name)
        try:
            proc = subprocess.Popen(
                job.command_list, 
                stdout=subprocess.PIPE, 
                stderr=subprocess.PIPE, 
                stdin=fscript, 
                universal_newlines=True, 
                cwd=job.working_dir)
            
            # fetch the stdin, stdout and wait
            stdout, stderr = proc.communicate() 

            if proc.returncode != 0:
                msg = stdout + "\n" + stderr
                raise exceptions.JobFailed(msg)
            else:
                # parse to get job_id
                job_id = self.parse_submission_output(stdout, stderr)
        except exceptions.JobFailed as err:
            print(" -> Submitting job using {} ... FAILED".format(self.system_name))
            self.logger.info("Run '%s' failed", job.name)
            utils.print_exception_info(msg=str(err))
            job.update_status(job.ERROR, update_run=True)
        except Exception as err:
            print(" -> Submitting job using {} ... FAILED".format(self.system_name))
            self.logger.error("Run '%s' failed", job.name)
            utils.print_exception_info(msg="Unknown Error", debug=str(err))
            job.update_status(job.ERROR, update_run=True)
            raise
        else:
            print(" -> Submitting job using {} ... OK JOB: {}".format(self.system_name, job_id))
            # print the stdout for reference
            print(" -> " + stdout.strip())
            print(" -> " + stderr.strip())
            job.update_status(job.SUBMITTED, update_run=True)
            job.update(job_id=job_id, submitted=datetime.now())
            
            # we need the job_id to be present in order to update the fnames
            job.update(
                stdout=self.get_stdout_fname(job),
                stderr=self.get_stderr_fname(job),
            )

            self.logger.info("Run '%s' submitted as JOB: %s", job.name,job_id)
    