"""
    cft: A management tool for Computational Fluid Dynamics
    
    batch: Batch system interfaces
"""
import os 
import psutil
import signal
import time
import logging
import asyncio
from datetime import datetime
from contextlib import contextmanager

from cfmt.batch_systems.base import BatchSystem
from cfmt.config import configtool as config
from cfmt.utils import AsyncRun
from cfmt import exceptions

class Local(BatchSystem):
    """
    Local submission through shell
    """
    system_name = "none"
    MPIEXEC = "mpiexec"
    modes_supported = ["serial", "parallel"]

    STDOUT_MASK="{job.name}" + ".ol" + "{job.id}"
    STDERR_MASK="{job.name}" + ".el" + "{job.id}"

    binaries = []
    
    # nothing to set the name here so we simply match everything
    # TODO: we could set some env variables ourseleves
    SCRIPT_STDOUT_MASK="*" + ".om" + "*" 
    SCRIPT_STDERR_MASK="*" + ".em" + "*"

    """ Local submission engine """
    def __init__(self, cmdline_args=None, **kwargs):
        """ The constructor """
        # call parent
        super().__init__(**kwargs)

        # store the command line arguments
        self.args = cmdline_args

    def get_batch_options(self, run):
        """
        Creates an ordered dict of batch arguments from the current attributes
        """
        batch_options = {
            "mode": self.mode,
            "np": self.np,
        }
        return batch_options

    def generate_command(self, run, code, task_cmd=None, name=None): 
        """
        Given a run and a code, generate the command that 
        would run the job
        """
        command = [] 

        if task_cmd is not None:
            return task_cmd
        elif code.has_tight_integration(self.system_name):
            return code.get_command(run, self, cmd=command, cmdline_args=self.args)
        
        job_binary = code.get_bin_path(wd=run.working_dir, batch_system=self)
        if job_binary is None:
            msg = f"Binary path for code '{code}' not set"
            self.logger.error(msg)
            raise ValueError(msg)

        # are we parallel
        if self.mode == "parallel":
            command.append(self.parallel_exec)
            command.append("--bind-to=none")
            command.append("-np")
            command.append(str(self.np))
    
        # append the binary
        if isinstance(job_binary, list):
            command += job_binary
        else:
            command.append(job_binary)

        return command 
    

    def get_args(self):
        """
        Returns the batch arguments
        """
        return {}

    def get_command(self):
        """ Generates the command and returns a list """
        return self.command

    def parse_submission_output(self):
        """ Parse the job output """

        # combine stdout and stdin 
    def submit(self, job):
        """ Submit a command, returning the Subprocess """
        assert job.working_dir is not None
        print(" -> Running '{}' ... ".format(job.run.code.name), end="\n")
        
        stdout_fpath = os.path.join(job.working_dir, self.get_stdout_fname(job))
        stderr_fpath = os.path.join(job.working_dir, self.get_stderr_fname(job))

        job.update(
            stdout=self.get_stdout_fname(job),
            stderr=self.get_stderr_fname(job),
        )
        job.save()

        runner = AsyncJobRunner(
            job,
            cwd=job.working_dir,
            stdout_f=stdout_fpath,
            stderr_f=stderr_fpath,
        )
        
        try:
            proc = runner.execute()
            #proc = subprocess.run(job.command_list, cwd=self.working_dir)
        except KeyboardInterrupt:
            self.logger.info("Caught SIGINT. Terminating code '%s'", job.run.code.name)
            try:
                print(" -> Caught SIGINT. Terminating '{}' ...".format(job.run.code.name))
                runner.terminate()
            except OSError:
                pass
            status = job.ABORTED
            print(" -> Running '{}' ... ABORTED".format(job.run.code.name), end="\n")
        except Exception as err:
            print(" -> Running '{}' ... ERROR".format(job.run.code.name), end="\n")
            print(str(err))
            self.logger.error(f"Job {job.id} terminated unexpectedly", exc_info=err)
            status = job.ERROR
        else:
            if proc.returncode is None:
                # process hasn't exited yet
                proc.wait()
            
            job.exit_status = proc.returncode
            status = job.ERROR
            if job.exit_status == 0:
                print(" -> Running '{}' ... OK".format(job.run.code.name), end="\n")

                # get rusage
                job.set_rusage(runner.rusage)
                # hook for completed
                status = job.COMPLETED
            else:
                print(" -> Running '{}' ... FAILED".format(job.run.code.name), end="\n")
        finally:
            job.finished =datetime.now()
            job.update_status(status, update_run=True)
            job.save(update=True)
       
    def update(self, jobs, detailed=False):
        """ 
        Fetches the most recent information for the jobs specified in jobs
        """

        # check jobs is a list
        assert isinstance(jobs, list)

        # if not empty
        if jobs:
            # loop through them
            for _job in jobs:
                pid = getattr(_job, 'job_id', False)
                if pid is False:
                    raise exceptions.JobDoesNotExist("No job_id")
                
                # we have a pid, but it may have been reused.
                # -> check it's running
                if psutil.pid_exists(pid):
                    # yes the pid exists, is it still the same?
                    # check the binary name?
                    proc = psutil.Process(pid)
                    if proc.cwd() == _job.working_dir:
                        # we have the right process, parse the status
                        try:
                            self.parse_proc_status(_job, proc.status())
                        except exceptions.UpdateFailed as err:
                            # raise when status
                            msg = "Cannot read process status for Job ID: '%s'".format(_job.id)
                            self.console.warning(msg)
                            self.logger.warning(msg)
                else:
                    # it's finished, we do not need to do anything
                    continue
    
    def parse_proc_status(self, job, status_string):
        """
        Given a status string from proc.status, intepret and set the job status accordingly
        See https://psutil.readthedocs.io/en/latest/index.html?highlight=cwd#psutil.STATUS_RUNNING
        """
        # status for jobs which are potentially still running
        running = [
            psutil.STATUS_RUNNING,
            psutil.STATUS_IDLE,
            psutil.STATUS_SLEEPING,
        ]
        if status_string in running:
            # job is running
            job.update_status(job.RUNNING, update_run=True)
            # job is sleeping, which it shoudn't
            #job.update_status(job.SUSPENDED, update_run=True)

            # job is in idle
            #job.update_status(job.SUSPENDED, update_run=True)

        if status_string is psutil.STATUS_DISK_SLEEP:
            return
            # 
        if status_string is psutil.STATUS_DEAD:
            # job is dead
            job.update_status(job.ABORTED, update_run=True)
        
        if status_string is psutil.STATUS_ZOMBIE:
            # job is zombie state
            job.update_status(job.ERROR, update_run=True)

        if status_string is psutil.STATUS_STOPPED:
            # job has stopped
            job.update_status(job.HOLDING, update_run=True)

        return

class AsyncJobRunner(AsyncRun):
    """ 
    Subclass for the AsyncRun to enable job statistics to be captured in
    real time
    """

    def __init__(self, job, cwd=None, stdout_f=None, stderr_f=None):
        """
        Arguments
        job - The job object
        """
        # store the job object
        self.job = job
        
        # child watcher initialized here to avoid odd bug with 3.7
        child_watcher = SafeChildWatcherWithRusage()
        asyncio.set_child_watcher(child_watcher)
        
        super().__init__(job.command_list, cwd, stdout_f, stderr_f)
        
        # set debug
        self.loop.set_debug(True)

    async def post_launch(self):
        """
        Runs post launch
        """
        
        self.job.update_status(self.job.RUNNING, update_run=True)
        self.job.submitted = self.submitted
        self.job.started = self.started
        self.job.job_id = self.proc.pid
        self.job.execution_host = self.execution_hostname
        self.job.save(update=True, cascade=True)
        return
    
    async def _stream_subprocess(self, cmd, stdout_fd, stderr_fd, shell=False):
         
        self.proc = await asyncio.create_subprocess_exec(*cmd,
            cwd=self.cwd,
            stdout=asyncio.subprocess.PIPE, 
            stderr=asyncio.subprocess.PIPE
        )
        
        self.started = datetime.now()

        await self.post_launch()

        with SafeChildWatcherWithRusage.monitor(self.proc) as results:
            loop = asyncio.get_event_loop()
            # create the tasks
            read_stdout = loop.create_task(
                self._read_stream(self.proc.stdout, stdout_fd, _print=self.stdout_print, log=self.stdout_log)
            )
            read_stderr = loop.create_task(
                self._read_stream(self.proc.stderr, stderr_fd, _print=self.stderr_print, log=self.stderr_log)
            )

            # wait for the tasks to complete
            await asyncio.wait([read_stdout,read_stderr])
        
        # gather any remaining stdout, stderr (avoid blocking)
        stdout, stderr = await self.proc.communicate()
        returncode = self.proc.returncode
        self.rusage = results.rusage

        return returncode

#    def clean_up(self):
#        """
#        Perform any cleanup. Runs after the loop has been closed
#        """
#        # call parent method
#        super().clean_up():
class ExtendedResults:
    """
    Class to hold RUSAGE results
    """
    def __init__(self):
        self.rusage = None
        self.returncode = None

def _compute_returncode(status):
    """
    Function copied from asyncio/unix_events.py for compatibility with Python 3.7
    """
    if os.WIFSIGNALED(status):
        # The child process died because of a signal.
        return -os.WTERMSIG(status)
    elif os.WIFEXITED(status):
        # The child process exited (e.g sys.exit()).
        return os.WEXITSTATUS(status)
    else:
        # The child exited, but we don't understand its status.
        # This shouldn't happen, but if it does, let's just
        # return that status; perhaps that helps debug it.
        return status

class SafeChildWatcherWithRusage(asyncio.SafeChildWatcher):
    """
    An implementation of SafeChildWatcher that uses os.wait4 to also get rusage
    information.

    See https://www.enricozini.org/blog/2019/debian/getting-rusage-of-child-processes-on-python-s-asyncio/
    """
    rusage_results = {}
    logger = logging.getLogger(__name__)

    @classmethod
    @contextmanager
    def monitor(cls, proc):
        """
        Return an ExtendedResults that gets filled when the process exits
        """
        assert proc.pid > 0
        pid = proc.pid
        extended_results = ExtendedResults()
        cls.rusage_results[pid] = extended_results
        try:
            yield extended_results
        finally:
            cls.rusage_results.pop(pid, None)

    def _do_waitpid(self, expected_pid):
        # The original is in asyncio/unix_events.py; on new python versions, it
        # makes sense to check changes to it and port them here
        assert expected_pid > 0
        try:
            pid, status, rusage = os.wait4(expected_pid, os.WNOHANG)
        except ChildProcessError:
            # The child process is already reaped
            # (may happen if waitpid() is called elsewhere).
            pid = expected_pid
            returncode = 255
            self.logger.warning(
                "Unknown child process pid %d, will report returncode 255",
                pid)
        else:
            if pid == 0:
                # The child process is still alive.
                return

            returncode = _compute_returncode(status)
            if self._loop.get_debug():
                self.logger.debug('process %s exited with returncode %s',
                             expected_pid, returncode)

        extended_results = self.rusage_results.get(pid)
        if extended_results is not None:
            extended_results.rusage = rusage
            extended_results.returncode = returncode

        try:
            callback, args = self._callbacks.pop(pid)
        except KeyError:  # pragma: no cover
            # May happen if .remove_child_handler() is called
            # after os.waitpid() returns.
            if self._loop.get_debug():
                self.logger.warning("Child watcher got an unexpected pid: %r",
                               pid, exc_info=True)
        else:
            callback(pid, returncode, *args)
