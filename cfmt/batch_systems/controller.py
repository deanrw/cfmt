"""
    cft: A management tool for Computational Fluid Dynamics
    
    batch: Batch system interfaces
"""
import importlib

# function to return the correct instance
AVAILABLE_BATCH_SYSTEMS = {
    'slurm': 'SLURM',
    'sge': 'SGE',
    'lsf': 'LSF',
    'none': 'Local'
}

def get_batch_cls(name):
    """ Returns the correct batch class """
    if name in AVAILABLE_BATCH_SYSTEMS:
        # bodge for "none"
        _names = "local" if name == "none" else name
        module = importlib.import_module('cfmt.batch_systems.' + str(_names))
        return getattr(module, str(AVAILABLE_BATCH_SYSTEMS[name]))
    else:
        raise NotImplementedError("Batch system '%s' not available", name)


class Batch(object):
    """
    Parent Controller
    Implements the singleton pattern
    """
    def __new__(cls, desc, *args, **kwargs):
        if cls is Batch:
            _class = get_batch_cls(desc)
            to_return = _class(*args, **kwargs)
            return to_return

def add_arguments(batch_name, mode, argparser):
    """ Adds batch specific arguments """
    arg = argparser
    
    if batch_name == "slurm":
        if mode == 'run':
            arg.add_argument(
                '--slurm',
                action='append_const',
                dest='batch_system',
                const="slurm",
                help="Use the slurm batch system",
            )
            arg.add_argument(
                '--slurm-partition',
                '--slurm-q',
                nargs=1,
                dest="queue",
                type=str,
                help="The partition (queue) to use with SLRUM (same as -q)",
                default=None,
            )
            arg.add_argument(
                '--slurm-script-name',
                nargs=1,
                type=str,
                help="Name of the generated submission script",
                default=None,
            )
    elif batch_name == "sge":
        if mode == "run":
            arg.add_argument(
                '--sge-q',
                nargs=1,
                dest="queue",
                type=str,
                help="The queue to use with SGE (same as -q)",
                default=None,
            )
            arg.add_argument(
                '--sge-pe',
                nargs=1,
                type=str,
                help="The parallel environment to use with SGE",
                default=None,
            )
            arg.add_argument(
                '--sge',
                action='append_const',
                dest='batch_system',
                const="sge",
                help="Use the sge batch system",
            )
            arg.add_argument(
                '--sge-script-name',
                nargs=1,
                type=str,
                help="Name of the generated submission script",
                default=None,
            )
            arg.add_argument(
                '--sge-hold',
                nargs=1,
                type=str,
                help="Ask SGE to hold until these JOB(S) have finished",
                default=None,
            )
    elif batch_name == "lsf":
        if mode == 'run':
            arg.add_argument(
                '--lsf',
                action='append_const',
                dest='batch_system',
                const="lsf",
                help="Use the lsf batch system",
            )
            arg.add_argument(
                '--lsf-q',
                nargs=1,
                dest="queue",
                type=str,
                help=f"The queue to use with lsf",
                default=None,
            )
            arg.add_argument(
                '--lsf-script-name',
                nargs=1,
                type=str,
                help="Name of the generated submission script",
                default=None,
            )
            arg.add_argument(
                "--lsf-resource",
                nargs=1,
                type=str,
                help="A resource to request (can be specified more than once)",
                default=None,
                action='append'
            )
            arg.add_argument(
                "--lsf-wtime",
                nargs=1,
                type=str,       # checked in __init__ for lsf
                help="The requested wall-clock time",
                default=None,
            )
