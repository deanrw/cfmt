"""
    cft: A computational Fluids Simulations tool

    base.py: Base class for all post processing interfaces
"""

import logging
import sys
import os

from cfmt import utils, config

class PostProcessor(object):
    """ Base class to implememt different post processing tools """
    batch_support = False
    code_data_formats = None
    supported_input_formats = []
    format_mapping = {}
    default_format = 'none'
    bin_name = None

    def __init__(self):
        """ The constructor """

        # store the loggers 
        self.logger = logging.getLogger('file')
        self.console = logging.getLogger('console')

        # initialize run
        self._run = None

        self.cmd = None

        self.cwd = None

        self.argv = []

        # check the bin is present
        if self.bin_name:
            # we trust the system
            # do we have a valid file passed?
            binary = self.bin_name

            if binary is None:
                msg = "Cannot find binary for '{}'".format(self.name)
                utils.print_exception_info(msg)
                sys.exit(1)
            else:
                self.binary = binary
        else:
            raise NotImplementedError("No binary provided")

    def set_args(self, post_args, args=None):
        """ Set the command line arguments requires to run the program """

        # handle format specific options
        self.argv = post_args
    
    def set_cwd(self, cwd):
        """
        Sets the cwd
        """ 
        assert isinstance(cwd, str), "cwd must be a list"

        self.cwd = cwd

    def set_run(self, run):
        """ sets the run """
        self._run = run
    
    def run(self, cmd=None):
        """ Run """
        # get the case post directory
        if self.cwd is not None:
            cwd = self.cwd
        else:
            # use the current working directory
            cwd = os.getcwd()

        # we prepend the binary 
        if cmd is None:
            cmd = [self.binary]

            if self.argv:
                cmd+= self.argv
        
        utils.run(cmd, shell=True, cwd=cwd)

    def run_binary(self, cmd, cwd=None):
        """ Runs the binary """
        import subprocess

        if cwd is None:
            cwd = os.getcwd()
        try:
            proc = subprocess.Popen(cmd, cwd=cwd)
            proc.wait()
        except KeyboardInterrupt:
            self.logger.info("Caught SIGINT. Terminating '%s'", self.name)
            try:
                print(" -> Caught SIGINT. Terminating '{}' ... ".format(self.name))
                proc.terminate()
            except OSError:
                pass
            proc.wait()

        
