"""
    cft: A management tool for Computational Fluid Dynamics
    
    batch: Controller to the batch system interface
"""

from cfmt.post.paraview import Paraview
from cfmt.post.visit import Visit
from cfmt.post.cfdpost import Cfdpost
from cfmt.post.tecplot import Tecplot
from cfmt.post.user_module import UserPostModule

# function to return the correct name
AVAILABLE_POST_SYSTEMS = {
    'paraview': Paraview,
    'visit': Visit,
    'cfdpost': Cfdpost,
    'tecplot': Tecplot,
    'user_module': UserPostModule
}
AVAILABLE_POST_FORMATS = [
    'ensight',
    'cgns',
]
class Post(object):
    """
    Parent controller
    """
    def __new__(cls, desc):
        if cls is Post:
            if desc in AVAILABLE_POST_SYSTEMS:
                to_return = AVAILABLE_POST_SYSTEMS[desc]()
                return to_return
            else:
                raise NotImplementedError