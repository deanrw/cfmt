"""
    cft: A computational Fluids Simulations tool

    visit.py: Visit support
"""
from cfmt.post.base import PostProcessor
class Visit(PostProcessor):
    """ Visit post processing """
    name = "visit"
    # find the binary
    def __init__(self):
        """ Constructor """

        # find the binary
        self.bin_name = 'visit'
        
        # call parent
        super().__init__()

        # hard coded defaults
        self.batch_support = True

        # format mappings
        self.format_mapping = {
            'stream' : 'ensight',
        }

        # default format
        self.default_format = 'cgns'
    
    def run(self):
        """ Run """
        # get the case post directory
        self.cmd = [self.binary, '-o', self._run.output_post_file]

        super().run()