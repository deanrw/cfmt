"""
    cft: A computational Fluids Simulations tool

    visit.py: Visit support
"""
from cfmt.post.base import PostProcessor
class Tecplot(PostProcessor):
    """ Visit post processing """
    name = "tecplot"
    # find the binary
    def __init__(self):
        """ Constructor """

        # find the binary
        self.bin_name = 'tec360'
        
        # call parent
        super().__init__()

        # hard coded defaults
        self.batch_support = True

        # format mappings
        self.format_mapping = {
            'stream' : 'ensight',
        }

        # default format
        self.default_format = 'ensight'
    