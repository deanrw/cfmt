"""
    cft: A computational Fluids Simulations tool

    paraview.py: Paraview support
"""
import os
import sys
import logging
import importlib.util

from cfmt.post.base import PostProcessor

DEFAULT_ENTRY_POINT = "cfmt"

class UserPostModule(PostProcessor):
    """ A class to wrap user written modules """
    name = "user_module"
    entry_point = DEFAULT_ENTRY_POINT

    def __init__(self, mfile):
        """ Constructor """

        # get the loggers
        self.logger = logging.getLogger(__name__)
        
        # check the module file
        if not os.path.isfile(mfile):
            raise FileNotFoundError(mfile)

        # try and import the module
        # See https://stackoverflow.com/a/67692
        self.logger.info(f"Loading user module from file {mfile}")
        mname = os.path.basename(mfile.strip(".py"))
        spec = importlib.util.spec_from_file_location(mname, mfile)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)

        if not hasattr(module, self.entry_point):
            raise AttributeError(f"User module does not contain required entry point '{self.entry_point}'")
        else:
            self._entry_point = getattr(module, self.entry_point)

    def run(self, runs, func=None, func_args=None):
        """
        Executes the function func on the passed run
        """
        ep = self._entry_point

        return ep(runs, func, func_args)