"""
    cft: A computational Fluids Simulations tool

    visit.py: Visit support
"""
from cfmt.post.base import PostProcessor
class Cfdpost(PostProcessor):
    """ ANSYS cfdpost post processing """
    name = "cfdpost"
    # find the binary
    def __init__(self):
        """ Constructor """

        # find the binary
        self.bin_name = 'cfdpost'
        
        # call parent
        super().__init__()

        # hard coded defaults
        self.batch_support = True

        # format mappings
        self.format_mapping = {
            'stream' : 'cgns',
        }

        # default format
        self.default_format = 'cgns'
    
    def run(self):
        """ Run """
        # get the case post directory
        self.cmd = [self.binary, '-gui', self._run.output_post_file]

        super().run()