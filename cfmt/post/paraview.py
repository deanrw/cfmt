"""
    cft: A computational Fluids Simulations tool

    paraview.py: Paraview support
"""
import shutil, os

from cfmt.post.base import PostProcessor
from cfmt import utils
from cfmt import config
class Paraview(PostProcessor):
    """ Paraview post processing """
    name = "paraview"

    supported_input_formats = [
        'ensight',
        'vtk',
        'openfoam',
        'stream'        # requires plugin
    ]

    def __init__(self):
        """ Constructor """

        # find the binary
        self.bin_name = 'paraview'
        
        # call parent
        super().__init__()

        # hard coded defaults
        self.batch_support = True

        # format mappings
        self.format_mapping = {
            'stream' : 'ensight',
            'fluent' : 'cgns',
        }

        # default format
        self.default_format = 'cgns'

    
