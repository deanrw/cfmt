"""
Git inteface
"""
import os
import sys
import logging
from cfmt import config, exceptions, utils
from cfmt.vcs.base import VCS
from git import Repo
from git import exc
from git import RemoteProgress
from progress.bar import Bar as Bar

class GitPrinter(RemoteProgress):
    """ Print git updates live """
    def line_dropped(self, line):
        print(line)

    def update(self, *args):
        print(self._cur_line, end='\n')

class ProgressBar(RemoteProgress):
    '''Nice looking progress bar for long running commands'''
    transferred = 0
    previous = 0
    total = 0
    prev_count = 0
    def setup(self, repo_name):
        from tqdm import tqdm
        #self.progress_bar = Bar(message='Cloning from {}'.format(repo_name), fill='#', suffix='%(percent)d%%')
        self.progress_bar = tqdm(desc="Cloning from '{}'".format(repo_name), ascii=True, unit='ops')

    def update(self, op_code, cur_count, max_count=None, message=''):
        #log.info("{}, {}, {}, {}".format(op_code, cur_count, max_count, message))
        if not max_count:
            return
        if self.progress_bar.total is None:
            # we need a total
            max_count = int(max_count)
            self.progress_bar.total = 0

        if self.prev_count != max_count:
            self.progress_bar.total = self.progress_bar.total + int(max_count)
            self.prev_count = int(max_count)
            self.previous = 0
            #self.progress_bar.total = self.progress_bar.total + max_count
        self.transferred = int(cur_count) 
        
        delta = self.transferred - self.previous
        self.total = self.total + delta
        self.previous = self.transferred
        self.progress_bar.update(delta)

class Git(VCS):
    """ Git repository VCS """
    #  store a reference to the exceptions
    exceptions = exc
    def __init__(self):
        """ Constructor """
        
        # store loggers
        self.console = logging.getLogger('console')
        self.logger = logging.getLogger('file')

    def init(self, path, bare=False):
        """ Initialize a repository """
        Repo.init(path, bare=bare)

    def commit(self, repo_path, msg, name=None, add_untracked=False):
        """ 
        Commits any checked out srcs with msg
        Returns the sha1 of the commit
        """
        # default commit message if we're not passed one
        if msg is None:
            msg = config.RUN['commit_msg']

        # we require a repository path
        try:
            self.console.info("Loading git repository at '{}'".format(repo_path))
            repo = self.load(repo_path)
        except self.exceptions.GitError as err:
            if isinstance(err, self.exceptions.NoSuchPathError):
                msg = "Cannot find git repository at '{}'".format(repo_path)
            elif isinstance(err, self.exceptions.InvalidGitRepositoryError):
                msg = "Git repository at '{}' is invalid".format(repo_path)
            else:
                msg = "Git Error whilst working with '{}'".format(repo_path)

            utils.print_exception_info(err)
            raise exceptions.VCException(msg) 

        # check it's not a bare repo
        assert not repo.bare

        # set the name if we haven't been provided one
        if name is None:
            name = os.path.basename(repo_path)

        if add_untracked and repo.untracked_files:
            # add the files
            repo.index.add(repo.untracked_files)

        # check if the repo is dirty
        if repo.is_dirty():
            self.logger.debug("Working directory '{}' dirty".format(repo_path))

            # get modified files
            changed_files = [item.a_path for item in repo.index.diff(None)]
            file_list = ", ".join("'{0}'".format(w) for w in changed_files)
            self.logger.debug("Changed files to commit {}".format(file_list))
            
            # add the files to the index
            repo.index.add(changed_files)

            # commit the changes
            self.logger.debug("Committing to repo {}...".format(repo_path))
            commit = repo.index.commit(msg)
            sha1 = str(commit)
            self.logger.info("Committed changes to '{}' ref: {}".format(name, sha1))
            #print(" -> Committed changes to '{}' ref: {}".format(name, sha1))

        else:
            # get the sha1 of the most recent commit
            self.logger.debug("Working directory '{}' for '{}' clean".format(repo_path, name))

            # make sure we have a valid branch to commit to
            valid_branch = repo.active_branch.is_valid()
            if not valid_branch:
                # we don't have a valid branch to commit to
                self.logger.warning("Repository '{}' has no valid branch".format(repo_path))

                # try an empty commit
                try:
                    commit = repo.index.commit("CFMT: Initial commit")
                except self.exceptions.GitError:
                    msg = "Cannot make initial commit to repository"
                    self.logger.debug(msg)
                    self.console.debug(msg)
                    self.logger.error(msg)
                    self.console.error(msg)
                    sys.exit(1)
                except:
                    raise

            sha1 = str(repo.head.commit)
            self.logger.info("Saving reference to current commit '{}'".format(sha1))
        
        # warn about untracked changes if we have them
        untracked_files = repo.untracked_files
        if untracked_files:
            self.logger.warning("Repository '{}' contains untracked files".format(repo_path))

        # return the sha1
        return sha1


    def clone(self, url, to_path, progress=False, evn=None, **kwargs):
        """ Clone an existing repository """
        if not progress:
            pb = None
        elif progress is True:
            pb = ProgressBar()
            pb.setup(url)
        else:
            pb = progress

        Repo.clone_from(url, to_path, pb, evn, **kwargs)
    
    def load(self, working_dir):
        """ Load an existing repository """
        return(Repo(working_dir))

    def getExceptions(self):
        """ Returns a reference to the exceptions """
        return self.exceptions

