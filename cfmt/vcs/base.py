"""
    Base class for version control software
"""

class VCS(object):
    """ Base class for VSC """
    def __init__(self):
        """ Class constructor """
        pass
    
    def init(self, path):
        """ Initialize the repository """
        raise NotImplementedError()
    
    def commit(self, files=None, message=None):
        """ Commit to the repository """
        raise NotImplementedError()

    def load(self, working_dir):
        """ Loads an existing repository located at working_dir """
        raise NotImplementedError()