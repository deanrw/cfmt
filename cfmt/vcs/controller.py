"""
    cft: A computational Fluids Simulations tool

    Module for vcs support
"""
from cfmt.vcs.git import Git

class VCSController(object):
    """ Controller for the VCS """
    AVAILABLE_VCS = {
        'git': Git
    }

    @classmethod
    def get_vcs(cls, vcs):
        """ Returns the correct VCS """
        if vcs in cls.AVAILABLE_VCS:
            _vcs = cls.AVAILABLE_VCS[vcs]
            return _vcs()
        else:
            raise NotImplementedError()

 