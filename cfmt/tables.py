"""
    cft: A management tool for Computational Fluid Dynamics
    
    tables: Pretty tables
"""
from tabulate import tabulate

def print_table(*args,**kwargs):
    print(tabulate(*args, **kwargs))