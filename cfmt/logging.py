"""
    cft: A management tool for Computational Fluid Dynamics
    
    logging: Set's up logging
"""
import logging
from cfmt import config as config

def getLogHandler(fname):
    """ 
    Given a fname, return a RotatingFileHandler for logging
    """
    log_handler = logging.handlers.RotatingFileHandler(
        filename=fname,
        mode='a',
        maxBytes=config.LOG['maxBytes'],
        backupCount=config.LOG['backupCount'],
        delay=True
    )
    return log_handler

def replaceHandler(logger, handler):
    """
    Given a logger, replace the attached handler with the passed handler
    """
    # add the formatter to the new handler if we have one
    formatter = None
    if logger.handlers:
        formatter = logger.handlers[0].formatter

    # replace the existing handler with the new one
    logger.handlers = [handler]

    # add the formatter to the new handler
    if formatter:
        logger.handlers[0].setFormatter(formatter)


# Custom formatter
class CfmtFormatter(logging.Formatter):
    """ 
    Defines a custom formatter for printing to the console
    - Different log levels are assigned different formats
    - See https://stackoverflow.com/questions/14844970/modifying-logging-message-format-based-on-message-logging-level-in-python3

    """
    dbg_fmt = "[%(relativeCreated)d] %(levelname)s: %(filename)s#%(lineno)d in %(module)s: %(message)s"
    info_fmt = "%(message)s"
    standard_fmt = "%(levelname)s: %(message)s"

    def __init__(self):
        """ Class constructor """
        super().__init__(fmt="%(levelno)d: %(msg)s", datefmt=None, style='%')

    def format(self, record):
        """ The method that does the actual formatting """

        # save the original format configured by the user
        format_orig = self._style._fmt

        # replace the original format with one customised according to log level
        if record.levelno == logging.DEBUG:
            self._style._fmt = CfmtFormatter.dbg_fmt
        elif record.levelno == logging.INFO:
            self._style._fmt = CfmtFormatter.info_fmt
        else:
            self._style._fmt = CfmtFormatter.standard_fmt
        
        # call the original formatter method to do the grunt work
        result = logging.Formatter.format(self, record)

        # restore the original format configured by the user
        self._style._fmt = format_orig

        return result