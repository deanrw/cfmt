"""
    cft: A management tool for Computational Fluid Dynamics

    config.py

    Configuration for the tool
"""
import configparser
import os
import appdirs
from cfmt import exceptions
from cfmt.codes import codes
from cfmt.batch_systems.controller import AVAILABLE_BATCH_SYSTEMS

configtool = configparser.ConfigParser(allow_no_value=True, interpolation=configparser.ExtendedInterpolation())
configtool.optionxform = str.lower
PROGNAME = "cfmt"
PROGVERSION = "0.2"

# database version
DBVERSION = 9

# Non user editable configuration
CFMT_DIR_NAME = '.cfmt'
CFMT_CONFIG_NAME = 'cfmt.cfg'

# Template for additional config profles
CFMT_PROFILE_TEMPLATE = 'cfmt-{profile}.cfg'

# Debug mode
DEBUG = False

STUDY_DIRS = {
    'src': {},
    'mesh': {},
    'post': {},
    'input': {},
}
CASE_DIRS = {
    'src': {
        'vcs': 'git',
        },
    'mesh' : {},
    'post' : {},
    'runs' : {},
    '.history': {
        'vcs': 'git',
    }
}
DATABASE = {
    'type': 'sqlite',
    'filename': 'cfmt.db'
}
LOG = {
    'filename': 'cfmt.log',
    'fmt': '%(asctime)s %(levelname)-8s %(message)s',
    'datefmt': '%Y-%m-%d %H:%M:%S',
    'level': 'INFO',
    'maxBytes': 50*1024*1024,
    'backupCount': 5,
}
RUN = {
    'dir_fmt': '%Y%m%d-%H%M%S',
    'commit_msg': 'CFMT autocommit',
}
STUDY_CONFIG = {
    'defaults' : {
        '# the default code to use': None,
        'code': 'stream',
        '# whether to save code history per case': None,
        'save_history': 'True',
        '# the editor command to use for notes': None,
        'editor': '/usr/bin/gvim -f',
        '# whether to compute job allocation quotas': None,
        'compute_allocation': False,
        '# name of script where job allocation is computed': None,
        'compute_allocation_script_name': 'compute_allocation.sh',
        '# print study summary with status ': None,
        'study_summary_with_status': False
    },
    'notes': {
        '# display notes with status': None,
        'print_with_status': False,
        '# number of notes to print with status': None,
        'number_with_status': 2,
    },
    'run' : {
        '# the run mode {serial|parallel}': None,
        'mode': 'serial',
        '# the batch system to use {none|sge}': None,
        'batch_system': 'none',
        '# the machine to use {local|remote}': None,
        'machine': 'local',
        'slots': '2',
        'pe': 'smp.pe',
        'env_modules': ''
    },
    'staging' : {
        '# the stating mode to use {none|share|remote}': None,
        'mode': 'share',
        'transfer_mode': 'rsync',
        'location': '',
        '# the address of the staging machine': None,
        'host': '',
    },
    'logging' : {
        'level': 'WARNING'
    },
    'archive': {
        '# the host where the path is': None,
        'host': None,
        '# the path to archive to': None,
        'path': None,
        '# the type of archive to create {tar|borg}': None,
        'type': 'borg',
        '# whether to compress': None,
        'compress': True,
        '# the default compression function' : None,
        '# compress_function': 'pigz',
        '# the archive name format': None,
        'name_format': 'archive--{name}--{created}',
    }
    #
}

VALID_CONFIG = {
    'defaults': {
        'code': "'{}' in list(codes.AVAILABLE_CODES.keys())"
    },
    'run': {
        'mode': "'{}' in ['parallel', 'serial']",
        'batch_system': "'{}' in list(AVAILABLE_BATCH_SYSTEMS.keys())",
        'machine': "'{}' in ['local', 'remote']",
    },
    'staging': {
        'mode': "'{}' in ['none', 'share', 'remote']",
        'transfer_mode': "'{}' in ['os', 'scp', 'rsync']",
    },
    'archive': {
        'type': "'{}' in ['borg', 'tar']",
    }

}
# read in hard coded defaults from here
configtool.read_dict(STUDY_CONFIG)


# handle mo


def load(from_user=False, study_path=None, config_path=None, profile=None, config_dict=None):
    """ Responsible for loading the config
    Loads the study level configuration 
    study_path: Path to the study, where we search
    config_path: Directly provided location
    """
    files = []
    
    # user config ($HOME/.config/cfmt)
    if from_user:
        # raise an exception if this file does not exist
        fname = os.path.join(appdirs.user_config_dir(appname=PROGNAME), CFMT_CONFIG_NAME)
        if os.path.isfile(fname):
            files.append(os.path.join(appdirs.user_config_dir(appname=PROGNAME), CFMT_CONFIG_NAME))
        else:
            raise exceptions.NoUserConfig
    
    # check for study config
    if study_path:
        # study level config ()
        files.append(os.path.join(study_path, CFMT_DIR_NAME, CFMT_CONFIG_NAME))
        
        # check for profile configs
        if profile or os.environ.get('CFMT_PROFILE'):
            # explicit command line supercedes environmental variable 
            # we only support one profile 
            if profile:
                profile = profile.pop() if isinstance(profile, list) else profile
            else:
                profile = os.environ.get('CFMT_PROFILE')

            # get the file name from the template
            fname = CFMT_PROFILE_TEMPLATE.format(profile=profile)
            fpath = os.path.join(study_path, CFMT_DIR_NAME, fname)
            if os.path.isfile(fpath):
                files.append(fpath)
            else:
                msg = f"Configuration file for profile '{profile}' does not exist"
                raise FileNotFoundError(msg)
    
    # check for direct config
    if config_path:
        if not os.path.exists(config_path):
            msg = "Configuration file '{}' does not exist".format(config_path)
            raise IOError(msg)
        files.append(config_path)


    # attempt to read files
    configtool.read(files)

    # read directly from the supplied dict
    if config_dict:
        configtool.read_dict(config_dict)

    # once we've read we need to check the options
    validate_config()

def save(path=None, to_user=False, directory=None):
    """ Responsible for saving the config """
    if to_user:
        fname = os.path.join(appdirs.user_config_dir(appname=PROGNAME), CFMT_CONFIG_NAME)
        dirname = os.path.dirname(fname)
        # try to create all folders required
        os.makedirs(dirname, exist_ok=True)
    
    if path:
        fname = path
    elif directory:
        fname = os.path.join(directory, CFMT_CONFIG_NAME)

    with open(fname, 'w') as configfile:
        configtool.write(configfile)

    return fname

def append_config(section, path=None, to_user=False, directory=None):
    """ 
    Appends the passed section to the written config file
    """
    if to_user:
        fname = os.path.join(appdirs.user_config_dir(appname=PROGNAME), CFMT_CONFIG_NAME)
        dirname = os.path.dirname(fname)
        # try to create all folders required
        os.makedirs(dirname, exist_ok=True)
    
    if path:
        fname = path
    elif directory:
        fname = os.path.join(directory, CFMT_CONFIG_NAME)

    # warn the user if a duplicate section already exists
    with open(fname, 'r') as f:
        current_config = configparser.ConfigParser(allow_no_value=True, interpolation=configparser.ExtendedInterpolation())
        current_config.read_file(f)
        if current_config.has_section(section):
            msg = f"WARNING: Existing configuration file already has section: {section}. Manual merge required."
            print(msg)

    items = configtool.items(section)
    new_config = configparser.RawConfigParser()
    new_config.add_section(section)

    for k, v in items:
        new_config.set(section, k, v)

    with open(fname, 'a') as configfile:
        new_config.write(configfile)

def add_config(config, overwrite=True):
    """
    Adds the passed dict to the loaded config

    If overwrite is False, then we don't update values that already
    exist
    """
    #for code in codes.AVAILABLE_CODES:
    #    code_class = codes.get(code)
    #    if code_class.DEFAULT_CONFIG:
    if not isinstance(config, dict):
        raise TypeError("config is not a dict")

    if overwrite: 
        configtool.read_dict(config)
    else:
        for section, values in config.items():
            if not configtool.has_section(section):
                configtool.read_dict(config)
            else:
                # we have to loop through the config one by one
                for key, value in values.items():
                    if not configtool.has_option(section, key):
                        configtool.set(section, key, str(value))

def init_user():
    # write the config to the user directory
    config['INSTALL'] = {'type': 'user'}
    config['DATABASE'] = DATABASE

def init_study(path):
    """ Writes a study based config file """
    for section, values in STUDY_CONFIG.items():
        configtool[section] = values

    fname = os.path.join(path, CFMT_CONFIG_NAME) 
    with open(fname, 'w') as configfile:
        configtool.write(configfile)

def init_case():
    """ Writes a case based config file """
    pass

def validate_config():
    """ Validates the final configuration that has been loaded """
    for section, keys in VALID_CONFIG.items():
        if configtool.has_section(section):
            for key, values in keys.items():
                value = configtool.get(section,key)
                teststring = values.format(value)
                try:
                    this = eval(teststring)
                    if not this:
                        msg = "Value '{}' for key '{}' in section '{}' incorrect."
                        msg = msg.format(value, key, section)
                        raise exceptions.ConfigError(msg)
                except ValueError:
                    msg = "Value '{}' for key '{}' in section '{}' incorrect."
                    msg = msg.format(value, key, section)
                    raise
                except:
                    raise

def get_editor():
    """ 
    Return an editor command 
    This first searches the config and then the user environment
    """
    return configtool.get("defaults", "editor", fallback=os.environ.get('EDITOR', 'vim'))

def get_supported_batch():
    """
    Returns the list of supported batch systems, if any
    """
    supported = configtool.get("update", "batch_systems", fallback=None)

    if supported is None:
        return None
    elif isinstance(supported, str):
        return supported.split()

def get_supported_submithost():
    """
    Returns the list of supported submit hosts, if any
    """
    supported = configtool.get("update", "submit_host", fallback=None)

    if supported is None:
        return None
    elif isinstance(supported, str):
        return supported.split()
