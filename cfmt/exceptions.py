"""
    cfmt: A Computational Fluids Management Tool

    cfmt/exceptions.py: Exceptions
"""

class NotAStudy(Exception):
    """ Raised when object is not a study """
    pass

class NotACase(Exception):
    """ Raised when object is not a case """
    pass

class NotARun(Exception):
    """ Raised when object is not a run """
    pass

class CaseExists(Exception):
    """ Raised when we're trying to create a case but it already exists """
    pass

class ReservedName(Exception):
    """ Raised when a name is reserved for internal use """

class NoDB(Exception):
    """ Raised when a database object should have been passed """
    pass

class NoStudy(Exception):
    """ Raised when a study object should be present """
    pass

class CaseDoesNotExist(Exception):
    """ Raised when a case does not exist in the database """
    pass
class RunExists(Exception):
    """ Raised when a run already exists in the database """
    pass
class InvalidCode(Exception):
    """ Raised when a code is invalid """
    pass

class RunDoesNotExist(Exception):
    """ Raised when a run does not exist in the database """
    pass
class JobDoesNotExist(Exception):
    """ Raised when a job does not exist in the database """
    pass

class NoUserConfig(Exception):
    """ Raised when the user config doesn't exist """
    pass
class CheckFailed(Exception):
    """ Raised when a required check has failed """
    pass
class RunFailed(Exception):
    """ Raised when a run fails """
    pass
class BuildFailed(Exception):
    """ Raised when a build fails """
    pass
class UnknownCode(Exception):
    """ Raised when the provided code is unknown """
    pass
class ConfigError(Exception):
    """ Raised when a configuration value is incorrect """
    pass
class NoCode(Exception):
    """ Raised when we expect a code object but don't get one """
    pass
class JobFailed(Exception):
    """ Raised when a job has failed to run """
    pass
class UpdateFailed(Exception):
    """ Raised when an update has failed """
    pass
class BatchNotReady(UpdateFailed):
    """ Raised when the batch system hasn't updated something """
    pass
class DeleteFailed(Exception):
    """ Raised when a delete has failed """
    pass
class DBVersionError(Exception):
    """ Raised when a version mis-match is detected """
    pass
class TransferFailed(Exception):
    """ Raised when a staging Transfer fails """
    pass
class UpgradeFailed(Exception):
    """ Raised when a db upgrade fails """
    pass
class DatabaseException(Exception):
    """ Raised when a database error occurs """
    pass
class PostFailure(Exception):
    """ Raised when a post processing failure occurs """
    pass
class VCException(Exception):
    """ Raised when an exception within the version control routine occurs """
    pass
class EmptyFile(Exception):
    """ Raised when a file read fails due to an empty file """
    pass
class ArchiveException(Exception):
    """ Raised when an archive operation fails """
    pass
class ArchiveFailed(ArchiveException):
    """ Raised when an archive operation fails """
    pass
class ArchiveRepoMissing(ArchiveException):
    """ Raised when an archive repository is missing """ 
    pass
class ArchiveRepoExists(ArchiveException):
    """ Raised when an archive repository already exists """
    pass
class ArchiveMissing(ArchiveException):
    """ Raised when an archive does not exist """
    pass