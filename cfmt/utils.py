"""
Utilities module for cmft
"""
import os
import shutil
import re
import logging
import threading
import lzma
import asyncio
import collections
import time
import signal
import queue
import datetime
import functools
from contextlib import contextmanager
from colored import stylize
from cfmt import logging as customLog
from cfmt import exceptions, archives

# see https://gist.github.com/leepro/9694638
HSYMBOLS = {
    'customary'     : ('B', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'),
    'customary_ext' : ('byte', 'kilo', 'mega', 'giga', 'tera', 'peta', 'exa',
                       'zetta', 'iotta'),
    'iec'           : ('Bi', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi'),
    'iec_ext'       : ('byte', 'kibi', 'mebi', 'gibi', 'tebi', 'pebi', 'exbi',
                       'zebi', 'yobi'),
}
LOADING_SYM = r"\|/-\|/-"

SUPPORTED_COMPRESSION={
    "lzma": "xz"
}

def confirm(prompt=None, default=False, override=False):
    """
    prompts for yes or no response from the user.
    Returns True for yes and False for no

    'resp' should be set to the default value assumed by the caller
    when the user simply presses ENTER
    'override' is used to shortcircuit
    """
    if override:
        return True

    prompt_accepted = ['y', 'Y', 'n', 'N']
    if prompt is None:
        prompt = 'Confirm'

    if default:
        prompt = "{} [{}]|{}: ".format(prompt, 'y', 'n')
    else:
        prompt = "{} {}|[{}]: ".format(prompt, 'y', 'n')

    while True:
        ans = input(prompt)
        if not ans:
            return default
        if ans not in prompt_accepted:
            print("Please enter y or n")
            continue
        if ans == 'y' or ans == 'Y':
            return True
        if ans == 'n' or ans == 'N':
            return False

def run(exe, cwd=None, shell=False, alias=False, bash=True, **kwargs):
    """ 
    Runs the provided exe in the cwd 
    If alias is true, attempts to resolve the alias before running
    """
    import subprocess
    # -> exe must be a full path or in the users path, but
    # we leave this to bash
    if cwd is None:
        cwd = os.getcwd()
    
    if not isinstance(exe, list):
        exe = [exe]
    
    if alias:
        raise NotImplementedError
    # use /bin/bash -i
    if bash:
        exe = ['/bin/bash','-i','-c'] + [" ".join(exe)]
    
    return subprocess.run(exe, cwd=cwd, **kwargs)

def rmdir(path):
    """ Removes a directory """
    if not os.path.isdir(path):
        raise NotADirectoryError

    shutil.rmtree(path)

def delete_dir_contents(path=None, callback=None, remove_symlinks=True):
    """ Deletes the contents of a directory """
    if path is None:
        return False
    if not os.path.isdir(path):
        return False
    for f in os.scandir(path):
        file_path = os.path.join(path, f)
        if callback is not None:
            callback(file_path)

        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.islink(file_path) and remove_symlinks:
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except:
            msg = "Failed to remove contents of directory '{0}'"
            raise IOError(msg.format(path))

def print_datetime(datetime, timeago=False):
    """ Prints a datetime object in isoformat """
    if timeago:
        import humanize
        return humanize.naturaltime(datetime)
    else:
        return datetime.isoformat(sep=' ', timespec='seconds')

def diff_datetime_str(delta):
    """ 
    Takes a datetime diff object and returns human readable string 
    This currently removes microseconds
    """
    return str(delta - datetime.timedelta(microseconds=delta.microseconds))

def remove_left_margin(string):
    """ Removes the left margin when using a docstring """
    lines = string.strip().split('\n')
    return "\n".join(line.strip() for line in lines)

def indent_string(string, indent=5):
    """ Indents a string by indent spaces """
    # split the string
    s = string.split('\n')
    s = [(indent * ' ') + line.lstrip() for line in s]
    s = "\n".join(s)
    return s

def remove_suffix(input_string, suffix):
    """
    Removes a suffix, to replace built in removesuffix, only valid for Python 3.9+
    """
    if suffix and input_string.endswith(suffix):
        return input_string[:-len(suffix)]
    return input_string

def multiple_true(l):
    """ Returns true if an iterable has more than 1 true value """
    return True if sum(l) > 1 else False

def single_true(l):
    """ Returns true if a list has one, and only one, truthy value """
    i = iter(l)
    return any(i) and not any(i)

def get_username():
    """ Returns the username of the current user """
    import getpass
    return getpass.getuser()

@contextmanager
def named_pipes(n=1):
    """ Create n named pipes """
    import tempfile
    dirname = tempfile.mkdtemp()
    try:
        paths = [os.path.join(dirname, 'named_pipe' + str(i)) for i in range(n)]
        for path in paths:
            os.mkfifo(path)
        yield paths
    finally:
        shutil.rmtree(dirname)

def get_named_pipe():
    """ Returns n named pipes """
    import tempfile
    dirname = tempfile.mkdtemp()
    path = os.path.join(dirname, "named_pipe")
    os.mkfifo(path)
    return path

def cstr(string, fg=None, bg=None, attr=None, reset=True):
    """ Stylises the string """
    import colored
    args = []
    assert not isinstance(fg, list)
    assert not isinstance(bg, list)

    if fg is not None:
        args.append(colored.fg(fg))

    if bg is not None:
        args.append(colored.bg(bg))

    if attr is not None:
        if not isinstance(attr, list):
            attr = [attr]
        for  _attr in attr:
            args.append(colored.attr(_attr))

    # add the arguments together
    if args:
        arg = "".join(args)
    else:
        arg = ""
    
    return stylize(string, arg, reset)

# some shortcuts
SKIPPING = cstr("SKIPPING", fg=3)
FAILED = cstr("FAILED", fg=1)
OK = cstr("OK", fg=2)
ERROR = cstr("ERROR", fg=1)
WARNING = cstr("WARNING", fg=3)
PARTIAL = cstr("PARTIAL", fg=3)

def cprint(string, fg=None, bg=None, attr=None, reset=True):
    """ Stylises and prints the string """
    print(cstr(string, fg=fg, bg=bg, attr=attr, reset=reset))

def str_remove_ansi(string):
    """ Strips the string of ansi escape characters """
    ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]')
    return ansi_escape.sub('', string)

def print_exception_info(msg=None, debug=True, critical=None, no_log=False, no_print=False):
    """ Logs exception information """
    
    logger = logging.getLogger('file')
    console = logging.getLogger('console')

    if no_log:
        # we grab the handler and replace with a null one
        l_log_handler = logger.handlers[0]
        null_handler = logging.NullHandler()
        customLog.replaceHandler(logger, null_handler)

    if no_print:
        # we grab the handler and replace with a null one
        c_log_handler = console.handlers[0]
        null_handler = logging.NullHandler()
        customLog.replaceHandler(console, null_handler)

        
    if msg is None:
        msg = "Unspecified error"
    
    logger.error(str(msg))
    console.error(str(msg))
    
    if isinstance(debug, bool) and debug:
        logger.debug(str(msg), exc_info=True)
        console.debug(str(msg), exc_info=True)
    elif isinstance(debug, str):
        logger.debug(debug, exc_info=True)
        console.debug(debug, exc_info=True)

    if isinstance(critical,bool) and critical:
        logger.critical(str(msg))
        console.critical(str(msg))
    elif isinstance(critical, str):
        logger.critical(critical)
        console.critical(critical)

    # swap the handler back
    if no_log:
        customLog.replaceHandler(logger, l_log_handler)
    
    # swap the handler back
    if no_print:
        customLog.replaceHandler(console, c_log_handler)

def list_select(options, **kwargs):
    """
    Small wrapper for the pick library to allow a curses file selection
    """
    import pick

    mapped = None 
    # extra kwargs
    if "default_index" not in kwargs and "default_option" in kwargs:
        # convert to default index
        d_o = kwargs["default_option"]
        if d_o in options:
            kwargs["default_index"] = options.index(d_o)
        else:
            raise ValueError("Default option not present in list")
        del kwargs["default_option"]

    if "title" not in kwargs:
        kwargs['title'] = "Please select one of the following: "

    if "options_map_func" in kwargs:
        func = kwargs["options_map_func"]
        del kwargs["options_map_func"]
        mapped = [func(i) for i in options]

    if "multi_select" in kwargs and kwargs["multi_select"]:
        if mapped is not None:
            selection = pick.pick(mapped, **kwargs)
            return [(options[i],i) for f, i in selection]
        else:
            selection = pick.pick(options, **kwargs)
            return selection
    else:
        options = mapped if mapped is not None else options
        option, index = pick.pick(options, **kwargs)
        return index

def tarpipe(src, host=None, cwd=None, cd=None, compression=False, src_size=None, 
            base_desc=None, final_desc=None):
    """
    Wrapper for tar pipe using subprocesses (local only)
    """
    import math
    import subprocess
    from tqdm import tqdm 
    
    assert isinstance(src, list)
    proc = []

    if cwd is None:
        cwd = os.getcwd()

    if src_size is None:
        guess_size = True
        src_size = 10
    else:
        guess_size = False

    if base_desc is None:
        base_desc = " --> Transferring "
    
    check_dir = [cd, cwd]
    for d in check_dir:
        if d is not None and not os.path.isdir(d):
            raise exceptions.TransferFailed("Not a directory: {}".format(d))
    
    # tar
    if host:
        # use ssh
        cmd = ["\'cd\'", "\'" + cwd + "\'", "&&"]
        cmd += ["\'tar\'", "\'-c\'"]
        if compression: cmd.append("\'-z\'")
        for s in src:
            cmd.append("\'" + s + "\'")

        ssh_proc = archives.base.Archive.get_ssh_proc
        tar_create = ssh_proc(archives.base.Archive, host, cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        cmd = ["tar", "-c"]
        if compression: cmd.append("-z")
        for s in src:
            cmd.append(s)
    
        tar_create = subprocess.Popen(cmd, cwd=cwd ,stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    
    proc.append(tar_create)

    # pv
    cmd = ["pv", "--bytes", "-n"]
    pv_proc = subprocess.Popen(cmd, stdin=tar_create.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.append(pv_proc)
    
    # tar extract
    cmd = ["tar", "-xvf", "-"]
    if cd is not None:
        cmd.append("-C")
        cmd.append(cd)
    
        tar_extract = subprocess.Popen(cmd, cwd=cwd, stdin=pv_proc.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.append(tar_extract)

    # launch Asynchronous readers for feedback
    pv_queue = get_queue()
    pv_reader = AsynchronousFileReader(pv_proc.stderr, pv_queue)

    tar_queue = get_queue()
    tar_reader = AsynchronousFileReader(tar_extract.stdout, tar_queue)

    bar_fmt = "{l_bar}{bar}{r_bar}"
    with tqdm(total=src_size, desc=base_desc, position = 1, leave=True, ascii=True,
              unit_divisor=1024, unit_scale=True, unit='B', bar_format=bar_fmt) as pbar:
            
        total = 0
        while not pv_reader.eof() or not tar_reader.eof():
            # what have we got from pv
            while not pv_queue.empty():
                line = pv_queue.get()
                try:
                    new_total = int(line.decode('utf-8'))
                except ValueError as err:
                    pbar.write(ERROR + ":" + str(err))
                else:
                    pbar.update(new_total - total)
                    total = new_total
                    if guess_size:
                        pbar.total = 2.5**(math.ceil(math.log(total, 2.5)))

            # what have we got from tar
            while not tar_queue.empty():
                line = tar_queue.get().decode('utf-8').rstrip()
                fname = os.path.basename(line)
                if len(fname) > 20:
                    fname = '{}~{}'.format(fname[:3], fname[-16:])
                desc = base_desc + "{:<20}".format(fname)
                pbar.set_description(desc=desc)
            
            time.sleep(0.1)
        
        # set the final total and pbar
        pbar.total = new_total

        # join the threads
        pv_reader.join()
        tar_reader.join()

        # make sure all processes are finished
        [p.wait() for p in proc]

        # check error codes
        msg = ""
        error = False
        for p in proc:
            if p.returncode != 0:
                # grab any remaning stderr
                stderr = p.stderr.read().decode('utf-8')
                if stderr:
                    msg += stderr + "\n"

                error = True 
        
        if final_desc is None:
            desc = base_desc
        else:
            desc = final_desc
    
        if error:
            desc = desc + " " + ERROR
        else:
            desc = desc + " " + OK
        
        pbar.set_description(desc=desc)
    
    if error:
        raise exceptions.TransferFailed(msg)





class AsyncRun(object):
    """
    Class to implement an asyncio based process runner.
    This optionally captures stdout and stderr to files

    Based on https://kevinmccarthy.org/2016/07/25/streaming-subprocess-stdin-and-stdout-with-asyncio-in-python/
    """
    # signals to catch
    #signals = [signal.SIGINT]
    signals = [signal.SIGINT, signal.SIGQUIT, signal.SIGTERM]


    def __init__(self, cmd, cwd=None, stdout_f=None, stderr_f=None, shell=False):
        """ Constructor """
        self.cmd = cmd
        self.stdout_f = stdout_f
        self.stderr_f = stderr_f
        self.stdout_fd = None
        self.stderr_fd = None

        
        if cwd is None:
            # default to current dir
            cwd = os.getcwd()
        self.cwd = cwd

        # time
        self.started = None
        self.submitted = None
        self.finished = None

        # execution
        self.execution_hostname = self.get_execution_hostname()
        self.shell = shell
    
        # logging
        self.logger = logging.getLogger('file')
        self.console = logging.getLogger('console')
        
        self.stdout_log = False
        self.stdout_print = True
        self.stderr_log = False
        self.stderr_print = True

        self.proc = None

        # check the paths and open the file descriptiors
        if self.stdout_f is not None:
            self.stdout_fd = open(self.stdout_f, 'wb')
        if self.stderr_f is not None:
            self.stderr_fd = open(self.stderr_f, 'wb')

        asyncio.set_event_loop(None)
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

    def handle_signal(self, sig):
        """ Handler for signals received """
        # pass the signal to the subprocess if it exists
        if self.proc is None:
            print("Proc is not running...")
            # pass the signal up
            if sig is signal.SIGINT:
                # stop the loop
                self.loop.close()
                raise KeyboardInterrupt
        else:
            if sig is signal.SIGQUIT:
                # terminate the underlying process
                print("Recieved SIGQUIT, terminating ...")
                self.logger.warning("SIGQUIT recevied, terminating PID= %i", self.proc.pid)
                self.terminate()
            else:
                print("Forwarding signal %s" % sig.name)
                self.proc.send_signal(sig)
    
    def terminate(self):
        """ Termiate the underlying process """
        self.proc.terminate()
    
    async def _read_stream(self, stream, fd, write_to_file=True, _print=True, log=False):
        while True:
            line = await stream.readline()
            if line:
                if _print:
                    print(line.decode("utf-8"), end="")
                if fd is not None and write_to_file:
                    fd.write(line)
                if log:
                    self.logger.info(line.decode("utf-8").rstrip())
            else:
                break
        
    async def _stream_subprocess(self, cmd, stdout_fd, stderr_fd, shell=False):
        
        if shell:
            _cmd = " ".join(cmd)
            self.proc = await asyncio.create_subprocess_shell(_cmd,
                cwd=self.cwd,
                stdout=asyncio.subprocess.PIPE, 
                stderr=asyncio.subprocess.PIPE
                )
        else:
            self.proc = await asyncio.create_subprocess_exec(*cmd,
                cwd=self.cwd,
                stdout=asyncio.subprocess.PIPE, 
                stderr=asyncio.subprocess.PIPE
                )
        
        self.started = datetime.datetime.now()

        await self.post_launch()
        
        loop = asyncio.get_event_loop()
        
        # create the tasks
        read_stdout = loop.create_task(
            self._read_stream(self.proc.stdout, stdout_fd, _print=self.stdout_print, log=self.stdout_log)
        )
        read_stderr = loop.create_task(
            self._read_stream(self.proc.stderr, stderr_fd, _print=self.stderr_print, log=self.stderr_log)
        )

        # wait for the tasks to complete
        await asyncio.wait([read_stdout,read_stderr])

        return await self.proc.wait()

    async def post_launch(self):
        """
        Runs after the subprocess has been launched
        """
        pass

    def execute(self):
        # add signals to the loop
        for s in self.signals:
            self.loop.add_signal_handler(
                s,
                functools.partial(self.handle_signal, s)
            )
        # register submitted
        self.submitted = datetime.datetime.now()
        # run the event loop
        rc = self.loop.run_until_complete(
            self._stream_subprocess(
                self.cmd,
                self.stdout_fd,
                self.stderr_fd,
                shell=self.shell,
            )
        )
        
        self.loop.close()
        self.clean_up()
        
        # return the completed process
        return self.proc
    
    def clean_up(self):
        """
        Perform any cleanup before exiting
        """
        # close the file descriptors
        if self.stdout_fd is not None:
            self.stdout_fd.close()
        if self.stderr_fd is not None:
            self.stderr_fd.close()
    
    def get_execution_hostname(self):
        """ Returns the execution hostname """
        import socket
        return socket.getfqdn()



class AsynchronousFileReader(threading.Thread):
    """
    Helper class to implement asynchronous reading of a file in a separate thread.
    Pushes read lines on a aqueue to be consumed in another thread.
    From https://github.com/soxofaan/asynchronousfilereader
    """
    def __init__(self, fd, _queue=None, autostart=True, wait=False, terminate=None, name=None):
        self._fd = fd
        if _queue is None:
            _queue = queue.Queue()
        
        self.queue = _queue

        # wait for IO
        self.wait = wait

        # ready event
        self.ready = threading.Event()

        # kill switch
        if terminate:
            self.terminate = terminate
        else:
            self.terminate = threading.Event()

        threading.Thread.__init__(self, name=name)

        if autostart:
            self.start()

    def run(self):
        """
        The body of the tread: read lines and put them on the queue.
        """
        while not self.terminate.is_set():
            line = self._fd.readline()
            if not line:
                self.ready.set()
                if self.wait:
                    time.sleep(0.1)
                    continue
                else:
                    break
            
            self.queue.put(line)
            
    def eof(self):
        """
        Check whether there is no more content to expect.
        """
        return not self.is_alive() and self.queue.empty()

    def readlines(self):
        """
        Get currently available lines.
        """
        while not self.queue.empty():
            yield self.queue.get()

def get_queue():
    """ Returns a queue object """
    return queue.Queue()

def get_thread(*args, **kwargs):
    """ Returns a thread """
    return threading.Thread(*args, **kwargs)

def get_hash(fpath, callback=None):
    """ Generates a hash of the file """
    import hashlib

    assert os.path.isfile(fpath)

    BUF_SIZE = 65536  # lets read stuff in 64kb chunks!

    md5 = hashlib.md5()
    with open(fpath, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            if callback is not None:
                callback(BUF_SIZE)
            
            md5.update(data)

    return md5.hexdigest()

def get_dir_size(start_path = '.'):
    """ Returns the size the folder in bytes """
    import subprocess
    
    # python only implementation
    if os.name == 'nt':
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                # skip if it is a symbolic link
                if not os.path.islink(fp):
                    total_size += os.path.getsize(fp)
    else:
        # we call du
        cmd = ['du', '-bs', start_path]
        p = subprocess.run(cmd, capture_output=True)
        if p.returncode == 0:
            total_size = p.stdout.split()[0].decode('utf-8')
        else:
            #self.logger.warning("Error returned for '%s'", " ".join(cmd))
            raise OSError(p.stderr.decode('utf-8').rstrip())
        #total_size = subprocess.check_output(['du', '-bs', start_path]).split()[0].decode('utf-8')
    
    return int(total_size)

def edit_file(fpath, cwd=None):
    """ Loads the provided files in the default editor """
    import subprocess
    editor = os.environ.get('EDITOR', 'vim')

    if not isinstance(fpath, list):
        fpath = [fpath]

    cmd = [editor] + fpath

    if cwd is not None:
        # check cwd is valid
        assert os.path.isdir(cwd)
    
    subprocess.call(cmd, cwd=cwd)

def editor_prompt(initial_msg=None, strip_comments=False, 
                  remove_initial_msg=False, editor=None):
    """ Prompts for a note and returns the text """
    import tempfile
    import subprocess
    
    if editor is None:
        editor = [os.environ.get('EDITOR', 'vim')]

    if isinstance(editor, str):
        editor = editor.split()

    assert isinstance(editor, list)

    with tempfile.NamedTemporaryFile(mode="r+", suffix=".tmp") as tf:
        # write the initial msg if we have one
        if isinstance(initial_msg, str):
            tf.write(initial_msg)
            tf.flush()

        # open the file
        cmd = editor + [tf.name]
        subprocess.call(cmd)

        # do the parsing with 'tf' using regular File operations.
        tf.seek(0)
        edited_message = tf.read()
        
        # strip final newline
        edited_message = edited_message.rstrip()
    
    if remove_initial_msg and (edited_message == initial_msg):
        return ""
    
    if strip_comments:
        # strip all lines beginning with '#'
        edited_message = re.sub(r'(?m)^\#.*\n?', '', edited_message)
        
    return edited_message

def concatenate_files(src, dst, remove_header=0):
    """
    Given a single or list of src files, append them to dst
    Optionally remove n headers from the src files
    """
    if isinstance(src, str):
        src = [src]

    # copy the first file if the destination doesn't exist
    if not os.path.isfile(dst):
        s = src.pop()
        shutil.copy2(s, dst)

    # loop and cat the rest (if there are any)
    with open(dst, 'a') as fdst:
        for s in src:
            with open(s, 'r') as fsrc:
                # discard headers
                [fsrc.readline() for i in range(remove_header)]

                for line in fsrc:
                    fdst.write(line)

def compare_compressed(compressed, uncompressed, algorithm, chunk_size=1024*1024):
    """
    Takes two files, one compressed and one uncompressed, and
    does a byte to byte comparison
    """
    if algorithm not in SUPPORTED_COMPRESSION:
        raise NotImplementedError("Compression algorithm %s not supported", algorithm)
    
    if algorithm == "lzma":
        with open(uncompressed, 'rb') as f1, lzma.open(compressed, 'rb') as f2:
            while True:
                chunk1 = f1.read(chunk_size)
                chunk2 = f2.read(chunk_size)
                if chunk1 != chunk2:
                    return False
                if not chunk1:
                    return True


def check_for_compressed(path):
    """
    Checks SUPPORTED_COMPRESSION to determine if a filename has
    a compressed version
    If so, returns the corrected path, else false
    """
    for alg, ext in SUPPORTED_COMPRESSION.items():
        _path = path + "." + ext
        if os.path.exists(_path):
            return _path

    return False

def compress(src, dst=None, algorithm="lzma", decompress=False, force=False, pbar=None):
    """
    Compresses an individual file, by default using the lzma algorithm

    By default, the default extension is added to the src. This can be overridden.
    
    - force [True|False] : Whether to skip detection of already applied compression
    """
    if algorithm not in SUPPORTED_COMPRESSION:
        raise NotImplementedError("Compression algorithm '%s' not supported", algorithm)

    ext = SUPPORTED_COMPRESSION[algorithm]

    # print or pbar write
    if pbar is not None:
        pfunc = pbar.write
    else:
        pfunc = print

    if dst is None:
        dst = src + "." + ext

    if decompress:
        # src may not contain correct extension, add if not present
        if not src.endswith("." + ext):
            src += "." + ext

    # do not compress symbolics links
    if os.path.islink(src):
        pfunc(" -> " + WARNING + f": Refusing to compress symbolic link '{os.path.basename(src)}'")
        return

    if not os.path.exists(src):
        if os.path.exists(src + "." + ext):
            # already compressed
            pfunc(" -> " + WARNING + f":'{os.path.basename(src)}' already compressed")
            return

        raise FileNotFoundError(src)

    if not force:
        if not decompress and src.endswith("." + ext):
            return

    if algorithm == "lzma":
        # check if system tool exists, as it's much quicker
        xz = shutil.which("xz")

        if xz is not None:
            import subprocess
            cmd = [xz]
            if decompress:
                cmd += ["-d"]

            cmd += [src]
            subprocess.call(cmd)
        else:
            # use the lzma library
            try:
                with open(src, 'rb') as fin, lzma.open(dst, 'wb') as fout:
                    shutil.copyfileobj(fin, fout)
            except lzma.LZMAError as err:
                raise err
            else:
                os.remove(src)

def less(text=None, fname=None):
    """ 
    Page text with a less like viewer 
    NOTE: pydoc was used here, but its actually just a poor wrapper to less
          so we use that directly here. 
    """
    import subprocess
    import io
    
    if fname is not None:
        # we load a file
        with open(fname, 'r') as f:
            text = f.read()
    
    proc = subprocess.Popen('less -r', shell=True, stdin=subprocess.PIPE)
    try:
        with io.TextIOWrapper(proc.stdin, errors='backslashreplace') as pipe:
            try:
                pipe.write(text)
            except KeyboardInterrupt:
                # We've now abandoned writing the text, but the pager
                # is still in control of the terminal
                pass
    except OSError:
        pass    # ignore broken pipes casued by quitting the pager program

    while True:
        try:
            proc.wait()
            break
        except KeyboardInterrupt:
            # Ignore Ctrl-C like the pager itself does. Otherwise pager is 
            # left running and the terminal is in raw mode and unusable
            pass


def tail(fname=None):
    """ Tail text """
    import select
    import subprocess
    
    proc = subprocess.Popen(['tail', '-F', fname], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    sel = select.poll()
    sel.register(proc.stdout)

    while True:
        if sel.poll(1):
            print(proc.stdout.readline(), end="")

def get_last_line(f, lines=1, block_size=1024):
    """
    Utility to efficiently read the last line of a file
    See: https://stackoverflow.com/questions/136168/get-last-n-lines-of-a-file-with-python-similar-to-tail
    """   
    # place holder for the lines found
    lines_found = []

    # block counter will be multiplied by buffer
    # to get the block size from the end
    block_counter = -1
    # loop until we find X lines
    while len(lines_found) < lines:
        try:
            f.seek(block_counter * block_size, os.SEEK_END)
        except IOError:  # either file is too small, or too many lines requested
            f.seek(0)
            lines_found = f.readlines()
            break

        lines_found = f.readlines()

        # we found enough lines, get out
        # Removed this line because it was redundant the while will catch
        # it, I left it for history
        # if len(lines_found) > lines:
        #    break

        # decrement the block counter to get the
        # next X bytes
        block_counter -= 1

    return lines_found[-lines]

def write_output(data, form="json"):
    """
    Write data in the given format
    """
    import json
    
    if form == "json":
        output = json.dumps(data, indent=4)

    print(output)

def bytes2human(n, format='%(value).1f%(symbol)s', symbols='customary'):
    """
    See https://gist.github.com/leepro/9694638
    Convert n bytes into a human readable string based on format.
    symbols can be either "customary", "customary_ext", "iec" or "iec_ext",
    see: http://goo.gl/kTQMs
      >>> bytes2human(0)
      '0.0 B'
      >>> bytes2human(0.9)
      '0.0 B'
      >>> bytes2human(1)
      '1.0 B'
      >>> bytes2human(1.9)
      '1.0 B'
      >>> bytes2human(1024)
      '1.0 K'
      >>> bytes2human(1048576)
      '1.0 M'
      >>> bytes2human(1099511627776127398123789121)
      '909.5 Y'
      >>> bytes2human(9856, symbols="customary")
      '9.6 K'
      >>> bytes2human(9856, symbols="customary_ext")
      '9.6 kilo'
      >>> bytes2human(9856, symbols="iec")
      '9.6 Ki'
      >>> bytes2human(9856, symbols="iec_ext")
      '9.6 kibi'
      >>> bytes2human(10000, "%(value).1f %(symbol)s/sec")
      '9.8 K/sec'
      >>> # precision can be adjusted by playing with %f operator
      >>> bytes2human(10000, format="%(value).5f %(symbol)s")
      '9.76562 K'
    """
    n = int(n)
    if n < 0:
        raise ValueError("n < 0")
    symbols = HSYMBOLS[symbols]
    prefix = {}
    for i, s in enumerate(symbols[1:]):
        prefix[s] = 1 << (i+1)*10
    for symbol in reversed(symbols[1:]):
        if n >= prefix[symbol]:
            value = float(n) / prefix[symbol]
            return format % locals()
    return format % dict(symbol=symbols[0], value=n)


def human2bytes(s):
    """
    See https://gist.github.com/leepro/9694638
    Attempts to guess the string format based on default symbols
    set and return the corresponding bytes as an integer.
    When unable to recognize the format ValueError is raised.
      >>> human2bytes('0 B')
      0
      >>> human2bytes('1 K')
      1024
      >>> human2bytes('1 M')
      1048576
      >>> human2bytes('1 Gi')
      1073741824
      >>> human2bytes('1 tera')
      1099511627776
      >>> human2bytes('0.5kilo')
      512
      >>> human2bytes('0.1  byte')
      0
      >>> human2bytes('1 k')  # k is an alias for K
      1024
      >>> human2bytes('12 foo')
      Traceback (most recent call last):
          ...
      ValueError: can't interpret '12 foo'
    """
    init = s
    num = ""
    while s and s[0:1].isdigit() or s[0:1] == '.':
        num += s[0]
        s = s[1:]
    num = float(num)
    letter = s.strip()
    for name, sset in HSYMBOLS.items():
        if letter in sset:
            break
    else:
        if letter == 'k':
            # treat 'k' as an alias for 'K' as per: http://goo.gl/kTQMs
            sset = HSYMBOLS['customary']
            letter = letter.upper()
        else:
            raise ValueError("can't interpret %r" % init)
    prefix = {sset[0]:1}
    for i, s in enumerate(sset[1:]):
        prefix[s] = 1 << (i+1)*10
    
    return int(num * prefix[letter])

def splitpath(path):
    """ Splits a path into its constituant elements """
    head, tail = os.path.split(path)
    components = []
    while len(tail) > 0:
        components.insert(0, tail)
        head, tail = os.path.split(head)
    
    return components

def create_tar_archive(path, filename, compress_mode="", filter=None):
    import tarfile
    assert os.path.isdir(path), "Only directory paths are supported"

    mode = "w:" + compress_mode

    try:
        with tarfile.open(filename, mode) as tf:
            if filter is not None:
                tf.add(path, arcname=os.path.basename(path), filter=filter)
            else:
                tf.add(path, arcname=os.path.basename(path))
    except tarfile.TarError as err:
        print_exception_info(err)
        raise exceptions.ArchiveFailed(err)

class LiveDataReader(threading.Thread):
    """
    A threaded reader for asynchronous reading of data.

    Pushes read lines on a queue to be consumed in another thread.
    Multiple files passed can be read sequentially if they're the same format
    Built upon https://github.com/soxofaan/asynchronousfilereader

    To be subclassed to implement code specifics
    """

    def __init__(self, fpath, 
                       maxlen=None, 
                       live=False, 
                       _queue=None,
                       autostart=True, 
                       wait=None, 
                       terminate=None,
                       compression=None,
                       openmode='r'):
        # check if we've been passed a list of files
        if isinstance(fpath, list):
            self.flist = fpath
        else:
            self.flist = [fpath]

        # check all the files exist
        for f in self.flist:
            if not os.path.isfile(f):
                raise FileNotFoundError(f": '{f}'")

        self.tlist = [0.0 for f in self.flist]
        
        # set defaults
        self.live = live
        self.maxlen = maxlen
        self.headers = []
        self.ignore_cols = []
        self.block = False
        self.findex = 0
        self.generate_row_list = False
        
        self.ignore_headers = []
        self.column_index = []
        self.ftype = None
        
        # we allow the code to pre-assign column indicies to axis
        # this gives the plotting program a means to plot whatever
        # against whatever, but still having somewhere sensible to start
        self.xaxis_assign = None
        self.yaxis_assign = []
        self.y2axis_assign = []

        # databuffers which will hold the plotable data
        self.databuffers = []

        # if we've not been passed a queue, load one here
        if _queue is None:
            self.queue = queue.Queue()

        # set the ready event
        self.ready = threading.Event()

        # set the wait event
        if wait is None:
            self.wait = live if live is not None else False

        # set the kill switch
        if terminate:
            self.terminate = terminate
        else:
            self.terminate = threading.Event()

        # check if we have compression
        fname = self.flist[self.findex]
        
        if compression is None:
            # detect
            if fname.endswith(".xz"):
                compression = "lzma"
                openmode = "rt"
        
        self.compression = compression

        if hasattr(self, 'fd'):
            if not self.fd.closed:
                self.fd.close()
            self.file_reader = None

        # open the file
        self.fd = self.open(fname, openmode)

        # set the headers 
        self.set_headers()

        # init the thread
        threading.Thread.__init__(self)

    def open(self, fname, openmode):
        """ Open a new file, accounting for different compression options """
        compression = self.compression


        if compression == "lzma":
            fd = lzma.open(fname, openmode)
        elif compression is None:
            fd = open(fname, openmode)
        else:
            raise NotImplementedError("Invalid compression option %s", compression)

        return fd 

    
    def set_headers(self):
        """ set and process headers """
        try:
            self.headers = self.get_headers()
        except IndexError:
            # raised when we read no headers (i.e. file empty)
            raise exceptions.EmptyFile
        except ValueError as err:
            # raised when we get something unexpected
            print_exception_info(err)
            exit(1)
        
        if self.headers:
            # do we have headers (y_1,y_2...y_n)
            for header in self.headers:
                self.databuffers.append(collections.deque(maxlen=self.maxlen))    
        else:
            # assume we have a text file
            self.databuffers.append(collections.deque(maxlen=self.maxlen))    
        
    def run(self):
        """
        The main body of the thread: read lines, process, and add to deque
        """
        while not self.terminate.is_set():
            for line in self.readlines():
                if isinstance(line, str):
                    # we add the entire line to the first databuffer
                    self.databuffers[0].append(line)
                elif line is not None:
                    # assign to databuffers
                    for i, col in enumerate(line):
                        self.databuffers[i].append(col)
                
            if self.wait:
                time.sleep(0.5)
                continue
            else:
                break
        
    def readlines(self):
        """ Implements a readlines type iterator """
        while True:
            line = self.fd.readline()
            if not line:
                # eof
                if len(self.flist) - 1 != self.findex:
                    self.next()
                    continue
                self.ready.set()
                break
            yield self.process(line.rstrip())
    
    def next(self):
        """ Moves to the next file in the provided list """
        # close the current fd if it's open
        self.close()

        # get the file path from the index and open
        self.findex += 1
        self.fd = self.open(self.flist[self.findex], 'r')

    def get_filename(self):
        """ returns the current filename """
        fpath = self.flist[self.findex]
        return str(os.path.basename(fpath))

    def process(self, line):
        """ process the data """
        return line

    def get_headers(self):
        """ Return headers """
        return False

    def stop(self):
        """ Sets the terminate event """
        self.terminate.set()

    def close(self):
        """ Close the currently open file descriptor """
        if hasattr(self, 'fd'):
            if not self.fd.closed:
                self.fd.close()

    def reset(self):
        """ Restart reading threads """
        # terminate the thread if active
        if self.is_alive():
            self.stop()
            self.join()

        # clear the databuffers
        self.clear()

        # reinitialize this
        self.__init__()
    
    def clear(self):
        """ Clear the databuffers """
        for b in self.databuffers:
            b.clear()

        self.ready.clear()
    
    def status(self):
        """ DEBUG: Print the status of the thread """
        return self.file_reader.is_alive()

    def guess_filetype(self, comment="#"):
        """ Attempts to guess the filetype by reading  """
        fname = self.get_filename()

        if isinstance(comment, str):
            while True:
                line = self.fd.readline()
                if line.startswith(comment):
                    continue
                else:
                    break
        
        # reset the fd
        self.fd.seek(0,0)

        if "," in line:
            return "csv"
        else:
            return "dat"

        #splitext = os.path.splitext(fname)

        #if len(splitext) > 1:
        #    # we have an extension 
        #    ext = splitext[-1]
        #    if ext == ".dat":
        #        return "dat"
        #    elif ext == ".csv":
        #        return "csv"
        #    else:
        #        return "csv"
        #else:
        #    return "csv"

class CSVLiveDataReader(LiveDataReader):
    """
    Special class of LiveDataReader for "standard" csv files
    """
    def readlines(self, process=True):
        """
        Readlines type iterator for CSV files
        """
        from csv import reader
        csv_reader = reader(self.fd, delimiter=",", quotechar='"')
        try:
            while True:
                line = next(csv_reader)
                if not process:
                    yield line
                yield self.process(line)
        except StopIteration:
            self.ready.set()
            return
    
    def process(self, data):
        """ process the data """
        # cannot handle zero values for sone reason
        try: 
            #data = [float(data[i]) if float(data[i]) != 0.0 else float("nan") for i in self.column_index]
            data = [float(data[i]) for i in self.column_index]
        except ValueError:
            data = None
        return data


def file_reader(fpath, **kwargs):
    """ Returns a reader capable of reading files """
    return LiveDataReader(fpath, **kwargs)

class FileSequenceReader(LiveDataReader):
    """
    An reader than reads files in sequence
    """
    def __init__(self, flist, flist_int):

        # skip headers
        self.skip_headers = 0
        
        if not isinstance(flist, list):
            raise ValueError("A file list must be passed")

        assert len(flist) == len(flist_int)
        self.flist_int = flist_int

        super().__init__(flist, live=False)
        # use a queue to manage the file read list
        self.fqueue = queue.Queue() 
        
        # set the event for hitting eof
        self.eof = threading.Event()

        # set the event for hitting the final file
        self.eol = threading.Event()
        self.sol = threading.Event()
        
        # store the maximum number of files
        self.ftotal = len(self.flist)

        # close the first file and add it to the queue
        self.close()
        self.fqueue.put(self.flist[self.findex])
        self.fint = self.flist_int[self.findex]

    def get_max_fnumber(self):
        """ Returns the largest file number """
        return self.flist_int[-1]

    def readlines(self, fd):
        """ Reads until the end of the file and waits """
        while True:
            line = fd.readline()
            if not line:
                # eof
                self.ready.set()
                self.eof.set()
                break
            yield self.process(line.rstrip())

    def run(self):
        """
        This uses a queue system to read files
        """
        while not self.terminate.is_set():
            fpath = self.fqueue.get(block=True)
            
            # short circuit if we get the terminate switch
            if fpath is self.terminate and self.terminate.is_set():
                break
            fd = self.open(fpath, 'r')
            for i in range(self.skip_headers):
                line = fd.readline()
            
            for line in self.readlines(fd):
                if isinstance(line, str):
                    # we add the entire line to the first databuffer
                    self.databuffers[0].append(line)
                elif line is not None:
                    # assign to databuffers
                    for i, col in enumerate(line):
                        self.databuffers[i].append(col)
            fd.close()

    def first_file(self):
        """ Load the first file """
        findex = 0
        self.load_findex(findex)
        
    def last_file(self):
        """ Load the last file """
        findex = self.ftotal - 1
        self.load_findex(findex)

    def forward(self):
        """ Load the next file """
        findex = self.findex + 1
        self.load_findex(findex)
    
    def previous(self):
        """ Load the next file """
        findex = self.findex - 1
        self.load_findex(findex)
    
    def load_findex(self, findex):
        """ Clears the current file and loads findex """
        
        # short circuit if we request the same file
        if self.findex == findex: 
            return

        # short circuit if we're at the end
        if findex > self.ftotal-1 or findex < 0:
            raise StopIteration()
        
        self.clear()

        self.fqueue.put(self.flist[findex])
        self.fint = self.flist_int[findex]
        self.findex = findex
    
    def stop(self):
        """ Sets the terminate event and joins the threads """

        self.terminate.set()
        self.fqueue.put(self.terminate)

        if self.is_alive():
            self.join()

class SignalHandler:
    """
    Implementation of a signal handler.
    https://christopherdavis.me/blog/threading-basics.html
    """

    # the stop event that's shared by this handler and threads
    event = None
    
    # the pool of worker threads
    threads = None

    def __init__(self, event, threads):
        self.event = event
        if not isinstance(threads, list):
            threads = [threads]
        
        self.threads = threads

    def __call__(self, signum, frame):
        """
        This will be called by the python signal module
        https://docs.python.org/3/library/signal.html#signal.signal
        """
        self.event.set()

        for thread in self.threads:
            thread.join()

    def set_handler(self):
        """ Set up the signal hanlder """
        signal.signal(signal.SIGINT, self)



def waiting(counter, msg, print_msg=True):
    text = msg + LOADING_SYM[counter % len(LOADING_SYM)]
    
    if print_msg:
        print(text, end='\r')
    else:
        return text

def make_list_unique(seq):
    """ 
    Takes a list ad returns a list with unique elements
    https://stackoverflow.com/questions/4459703/how-to-make-lists-contain-only-distinct-element-in-python
    """
    seen = set()
    return [x for x in seq if x not in seen and not seen.add(x)]
