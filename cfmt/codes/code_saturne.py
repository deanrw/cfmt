"""
    cft: A management tool for Computational Fluid Dynamics
    
    codes/fluent.py: Interface to the FLUENT code
"""
import os
import sys
import shutil
import logging
import lzma
import glob
import cfmt.utils as utils
import math

from cfmt.config import configtool as config
from cfmt.codes.base import Code
from cfmt import exceptions

class SaturneCode(Code):
    """ Class to implement an interface to the class code """
    name = "code_saturne"
    maintainer = "Dean Wilson"
    description = "Open source CFD solver from EDF"
    path = {}
    requires_build = False
    can_build = True
    url = "https://www.code-saturne.org"

    residual_fname = "residuals_combined.csv"

    LOG_EXT = ".log"
    SOLVER_LOG_NAME = "run_solver"
    LISTING_MASK = SOLVER_LOG_NAME + ".{job.id}" + LOG_EXT

    # folders (maybe) present in a run directory
    run_dirs = ["checkpoint", "restart", "src"]

    # log files and other files created
    LOG_FILES = ["performance", "run_solver", "setup"]
    OTHER_FILES = ["summary"]

    # files to compress upon job completion
    compress_logs = False
    compression = "lzma"
    COMPRESSION_EXT = ".xz"

    # note the git-ssh contains frequently used user subroutines 
    DEFAULT_CONFIG = {
        name: {
            'git-url': '',
            'binary': 'code_saturne',
            'input_mask': 'setup.xml',
            'mesh_mask': '*.case *.ccm *cgns *.des *.med *.msh *.neu *.unv',
            'monitor_mask': 'probes_*.dat',
            'results_mask': '*.case *.cgns *.med',
            'tout_mask': 'tout*',
            'post_utility': 'paraview',
            'precision': 'dp',
            'use_gui': True,
            'init_from_use_symlink': False,
        }
    }
    # batch systems that this code has tight integration with
    TIGHT_INTEGRATION = [
        "sge", "slurm", "none"
    ]

    # run_solver for tight integration
    SOLVER_SCRIPT = "./run_solver"
    SOLVER_BINARY = "./cs_solver"

    supported_output_formats = [
        'ensight',
        'cgns',
        'med'
    ]
    output_format_map = {
        'ensight': '*.case',
        'cgns': '*.cgns',
        'med': '*.med',
    }

    default_output_format = 'ensight'
    default_output_utility = 'paraview'
    
    capability_true = ['edit_input', 'tout']
    capability_false = []
    
    # files that can be edited 
    edit_mask = '*.xml'

    # placeholder for results file
    results_file = None

    def __init__(self, study=None, db_record=None):
        """ Constructor """
        super().__init__(study, db_record)
        
        self.binary_path = None

        # gui
        self.gui = False

        # extra solver arguments
        self.solver_arguments = None

        # xml setup file
        self.setup_xml = None

        
        # set some variables
        # -> get the site variable 
        #self.cspath = 
        
        # -> set the code version
        #self.set_version()

        # -> load some default config

        self.set_configs()


    def set_configs(self):
        """
        Loads some CS specific config to the class
        """

        DEFAULT_CONFIG = self.DEFAULT_CONFIG[self.name]
        self.RESTART_MESH_INPUT = "restart_mesh_input"
        self.RESTART_DIR = "restart"
        self.CHECKPOINT_DIR = "checkpoint"
        self.PARTITION_OUTPUT_DIR = "partition_output"
        self.PARTITION_INPUT_DIR = "partition_input"
        self.USE_GUI = config.getboolean(self.name, "use_gui", fallback=DEFAULT_CONFIG["use_gui"])
        self.INIT_FROM_USE_SYMLINK = config.getboolean(self.name, "init_from_use_symlink", fallback=DEFAULT_CONFIG["init_from_use_symlink"])

    def init_case(self, case=None):
        """
            Modify the create routine
        """ 
        import subprocess
        from cfmt.vcs.controller import VCSController
        
        if case is None:
            case = self.case
        
        # allow existing directory to be used
        case.allow_existing_dir = True

        # remove directories 
        case.dirs.pop('src', None)
        case.dirs.pop('runs', None)
        
        # check here to make sure code_saturne environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)

        # ask code_saturne to create the case
        cmd = ['create', '-c', case.path]
        proc = self.cs_run(cmd, cwd=self.study.path, stdout=subprocess.DEVNULL)
        
        proc.check_returncode()

        # use git to clone the cs_usr_src.git if it exists
        cs_src_git = os.path.join(self.study.path, 'src', self.name, 'cs-usr-src.git')
        if os.path.exists(cs_src_git):
            dst_dir = os.path.join(case.abspath, 'SRC')
            
            # we clone out the SRC repository
            vcs = VCSController.get_vcs('git')
            vcs.clone(cs_src_git, dst_dir)

        else:
            # copy over any user subroutines we find
            src_dir = os.path.join(self.study.path, 'src', self.name, 'SRC')
            dst_dir = os.path.join(case.abspath, 'SRC')
            try:
                if not os.path.isdir(src_dir):
                    msg = "Cannot find common user subroutine directory '{}'".format(src_dir)
                    self.logger.warning(msg)
                    self.console.warning(msg)
                    raise NotADirectoryError()
                if not os.path.isdir(dst_dir):
                    msg = "Cannot find case user subroutine directory '{}'".format(dst_dir)
                    self.logger.warning(msg)
                    self.console.warning(msg)
                    raise NotADirectoryError()

                # copy all files
                for f in os.listdir(src_dir):
                    src = os.path.join(src_dir, f)
                    if os.path.isfile(src):
                        dst = os.path.join(dst_dir, f)
                        shutil.copy2(src, dst)

            except NotADirectoryError:
                msg = "Common user subroutines not copied"
                self.logger.warning(msg)
                self.console.warning(msg)

    
    def set_config(self):
        """ Writes the default config to the config file """
    
    @staticmethod
    def init_check(args):
        """ Pre-initialization check """
        # -> check if fluent can be found
        if shutil.which(config.get(SaturneCode.name, 'binary')) is None:
            raise exceptions.CheckFailed("Code_saturne cannot be found in user PATH")

    def set_version(self):
        """
        sets the version number
        """
        # check here to make sure code_saturne environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)

        # call the info command
        cmd = ['info', '--version']

        proc = self.cs_run(cmd, capture_output=True, text=True)
        proc.check_returncode()

        # parse the version string
        version = proc.stdout.rstrip().split(":")[1]
        self.version = version.split("-")[0]
        self.major_version = int(version.split(".")[0])
    
    def prepost_exec_locations(self, base_dir=""):
        """ Locations to add to potential script sourcing """

        # if we're relative to the case directory, then use the SCRIPTS folder
        if base_dir == self.case.abspath:
            return os.path.join(base_dir, "SCRIPTS")
        else:
            return os.path.join(base_dir, "input", self.name)

    def cs_gui(self, file=None, new=False, cwd=None, **kwargs):
        """
        Wrapper to load the gui
        """
        # check here to make sure code_saturne environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)
        
        cmd = ["gui"]
        
        if file is not None:
            assert isinstance(file, str), "file must be a string"
            cmd = cmd + ["-p", file]
        else:
            cmd = cmd + ["-n"]

        return self.cs_run(cmd, cwd=cwd)

    def cs_run(self, cmd, cwd=None, **kwargs):
        """
        Wrapper script to run the code_saturne binary
        """
        import subprocess
        assert isinstance(cmd, list), "cmd must be a list"
        
        # check here to make sure code_saturne environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)

        cmd = [self.binary] + cmd

        return subprocess.run(cmd, cwd=cwd, **kwargs)

        #return utils.run(cmd, shell=False, cwd=cwd)
    
    def build(self, case, print_output=False, rebuild=False):
        """
        Compiles and build the binary necessary to run the code
        """
        import subprocess
        # check here to make sure code_saturne environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)
        # -> check we have a valid case directory
        if case.abspath is None:
            msg = "Case directory not provided"
            self.logger.error(msg)
            raise exceptions.BuildFailed(msg)

        # if rebuilding, 
        case_src_path = os.path.join(case.abspath, "SRC")
        # check the SRC folder is present
        if rebuild:
            assert len(case.runs) > 0, "Run not found"
            run = case.runs[0]
            run_dir = run.get_run_dir()
            src_path = os.path.join(run_dir, "src")
            dst_path = run_dir
        else:
            src_path = os.path.join(case.abspath, "SRC")
            dst_path = src_path
            
        
        if not os.path.isdir(src_path):
            msg = "src directory '{}' cannot be found".format(src_path)
            self.logger.error(msg)
            raise exceptions.BuildFailed(msg)

        self.logger.info("Building '%s' ...", self.name)

        # use the ASYNC runner
        cmd = ["compile", "-s", src_path, "-d", dst_path]
        #runner = utils.AsyncRun(cmd, cwd=case.abspath)
        
        # -> set logging
        #runner.stdout_log = True
        #runner.stdout_print = print_output
        stdout = None if print_output else subprocess.DEVNULL
        stderr = None if print_output else subprocess.DEVNULL

        #proc = runner.execute()
        proc = self.cs_run(cmd, cwd=case.abspath, stdout=stdout, stderr=stderr)

        if proc.returncode != 0:
            self.logger.info("Building '%s' FAILED", self.name)
            raise exceptions.BuildFailed("Code_saturne failed to build")
        else:
            self.is_built = True
            msg = "Build"
            self.logger.info("Building '%s' OK", self.name)



    def copy_bin(self, case, dest):
        """
        Copies the code binary from the case level build dir to the dest
        folder

        Only runs on rebuild for cs
        """
        pass


    def init(self, db=None, args=None):
        """ 
        Initializes a new study 

        Called in study.init(), after directories have been written
        """
        from cfmt.vcs.controller import VCSController

        super().init(db, args)
        
        # code_saturne only creates a directory structure,
        # we don't need to mimic this 
        
        src_dir = os.path.join(self.study.path, 'src')
        if not os.path.isdir(src_dir):
            raise NotADirectoryError()
        
        self.set_version()
        if self.major_version == 6:
            # link to existing study structure for compatibility
            for d in ['POST', 'MESH']:
                dst = os.path.join(self.study.path, d)
                os.symlink(d.lower(), dst)
            # we handle case creation here

        # --> clone our user subroutines (if the path exists)
        git_repo = config.get(self.name, 'git-url')
        vcs = VCSController.get_vcs('git')
        if git_repo:
            repo_path = os.path.join(src_dir, self.name, 'SRC')
            vcs.clone(git_repo, repo_path)
        else:
            # create a bare repository to hold SRC files
            repo_path = os.path.join(src_dir, self.name, 'cs-usr-src.git')
            vcs.init(repo_path, bare=True)

    def create_case(self, path):
        """ 
            Called after creating a new case,
            dir structure exists

            For saturne we do the following
            -> create a new temporary case with the create case
            -> move contents of directories back and symlink
        """

        # symlink our directories
        dirmap = {
            'RESU': 'runs',
        }
        for src, dst in dirmap.items():
            _src = os.path.join(self.study.path, self.case.path, src)
            _dst = os.path.join(self.study.path, self.case.path, dst)

            # create symlink if src != dst
            new_dst = os.path.join(self.study.path, self.case.path, dst)
            os.symlink(src, new_dst)

    
    def pre_run_tasks(self, run, args=None):
        """ Task to carry out pre-run """
        # check that we have a few files
        # NOTE: cs_solver only exists if we have custom SRC to compile
        files = [
            'run_solver',
            #'cs_solver', 
        ]
        for f in files:
            _f = os.path.join(run.get_run_dir(), f)
            if not os.path.isfile(_f):
                raise FileNotFoundError("File not found '{}'".format(_f))
        
        # if we've initialized from something, remove the init_from.xml
        if args.init_from is not None and args.keep_input is True:
            init_from = os.path.join(self.case.abspath, 'DATA', "init_from.xml")
            if os.path.isfile(init_from):
                os.remove(init_from)

    
    def create_restart(self, run):
        """ Handle a restart/continue calc request """
        
        run_dir = run.get_run_dir()
        
        # determine if a restart file exists already
        frestart = os.path.join(run_dir, self.RESTART_DIR)

        # remove the existing restart link or dir and recreate it
        if os.path.exists(frestart) or os.path.islink(frestart):
            # if its a real directory, remove it
            if not os.path.islink(frestart) and os.path.isdir(frestart):
                utils.rmdir(frestart)
            else:
                os.unlink(frestart)

        # check we have a checkpoint directory
        fcheck = os.path.join(run_dir, self.CHECKPOINT_DIR)
        if not os.path.isdir(fcheck):
            self.logger.error(f"Checkpoing directory not found: '{fcheck}'")
            raise FileNotFoundError("checkpoint directory is missing")

        # symlink the restart directory
        _src = os.path.relpath(fcheck, run_dir)
        self.logger.debug("Creating symbolic link '%s' -> '%s'", frestart, fcheck)
        os.symlink(_src, frestart)

        # determine if we have a partition_output
        fpartition_output = os.path.join(run_dir, self.PARTITION_OUTPUT_DIR)
        fpartition_input = os.path.join(run_dir, self.PARTITION_INPUT_DIR)

        # do nothing if we already have a symlinked partition input dir
        if not os.path.islink(fpartition_input) and os.path.isdir(fpartition_output):
            # if something else is there, remove it
            if os.path.isdir(fpartition_input):
                utils.rmdir(fpartition_input)

            src = os.path.relpath(fpartition_output, run_dir)
            self.logger.debug("Creating symbolic link '%s' -> '%s'", fpartition_output, fpartition_input)
            os.symlink(src, fpartition_input)

    def remove_restart(self, run):
        """ 
        Remove an existing restart directory if it exists 
         -> removes the restart_mesh_input file as well by
            default 
        """
        
        run_dir = run.get_run_dir()
        frestart = os.path.join(run_dir, self.RESTART_DIR)

        self._remove_dir_or_link([frestart])

    def remove_restart_mesh(self, run):
        """
        Remove the restart_mesh_input
        """
        run_dir = run.get_run_dir()
        frestart_mesh = os.path.join(run_dir, self.RESTART_MESH_INPUT)

        self._remove_dir_or_link([frestart_mesh])

    def _remove_dir_or_link(self, flist):
        """
        Removes a dir or symbolic link
        """
        if not isinstance(flist, list):
            raise TypeError("_remove_dir_or_link requires a list")

        for path in flist:
            if os.path.islink(path):
                self.logger.debug("Removing symbolic link '%s'", path)
                os.remove(path)
            elif os.path.isdir(path):
                self.logger.debug("Removing directory '%s'", path)
                shutil.rmtree(path)

    
    def create_restart_mesh(self, run):
        """
        Code_saturne requires a restart_mesh_input file when
        mapping results from a different mesh
        """
        run_dir = run.get_run_dir()

        # determine if a restart file already exists
        frestart_mesh = os.path.join(run_dir, "restart_mesh_input")

        # if its not a symlink, we remove and recreate it
        if not os.path.islink(frestart_mesh):

            # if its a real file, remove it
            if os.path.isfile(frestart_mesh):
                os.unlink(frestart_mesh)
            
            # check we have a restart directory 
            fcheck = os.path.join(run_dir, "restart")
            if not os.path.isdir(fcheck):
                self.logger.error(f"Restart directory not found: '{fcheck}'")
                raise FileNotFoundError("restart directory is missing")

            # check the mesh_input file exists
            fmesh_input = os.path.join(fcheck, "mesh_input")
            if not os.path.exists(fmesh_input):
                self.logger.error(f"'mesh_input' file not present in restart directory '{fcheck}''")
                raise FileNotFoundError("restart mesh_input file missing")

            # symlink the restart directory
            self.logger.debug("Creating symbolic link '%s' -> '%s'", frestart_mesh, fmesh_input)#
            src = os.path.relpath(fmesh_input, run_dir)
            os.symlink(src, frestart_mesh)
    
    def pre_run_check(self, run, args=None, batchname=None):
        """ Checks to carry out pre-run generation """
        
        # find the xml file
        # look in the DATA folder
        directory = os.path.join(self.case.abspath, 'DATA')
        
        mask_list = self.edit_mask.split(" ")
        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = sorted(glob.glob(os.path.join(directory, _mask)))
            for fname in files:
                src_list.append(fname)

        if len(src_list) > 1:
            # we need to select the file to run
        
            # add an option to edit nothing
            src_list.append("Exit")
            prompt="Please select a file to run: "

            selection = utils.list_select(src_list, title=prompt, options_map_func=os.path.basename)
            if selection == len(src_list):
                sys.exit(0)
            fxml = src_list[selection]
        elif len(src_list) == 1:
            fxml = src_list.pop()
        else:
            raise FileNotFoundError("Cannot find xml file")

        self.setup_xml = fxml
        
            # set the binary path
            #if args.parallel:
            #    self.binary_path = ["./cs_solver", "--mpi"]
            #else:
        self.binary_path = ["./run_solver"]

        return

    def init_run(self, run, args=None):
        """
        Called after the run dir and db is created, but before it is staged
        
        code_saturne run

        --stage : the "prepare_data" stage
        --initialize: the "initialize" stage
        --execute: the "run_solver" stage
        --finalize: the "save_results" stage 
        
        """
        if args is not None:
            if args.continue_calc:
                # we don't need to run the preprocessor, restart is handled in pre_run_check
                # create the restart directory
                self.create_restart(run)
                
                # if we don't specify this, remove the restart_mesh_input
                if not args.cs_restart_different_mesh:
                    self.remove_restart_mesh(run)
                
                return 
            
            elif args.init_from is None:    
                # don't remove if we're initialising from a different run
                self.remove_restart(run)
            elif args.init_from is not None and args.cs_restart_different_mesh:
                # we have a restart_mesh_input to creatre
                self.create_restart_mesh(run)
        
        # check here to make sure code_saturne environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)

        cmd = ["run"]
        # get the run name
        
        # set the right case
        cmd.append("--case")
        cmd.append(run.case.abspath)

        # set the id 
        cmd.append("--id")
        cmd.append(run.dir_name)

        # set how many processes we have
        if run.batch.mode == "parallel":
            cmd.append("-n")
            cmd.append(str(run.batch.np))
        
        # allow the directory to exist
        cmd.append("--force")

        # copy the files
        cmd.append("--stage")
        
        # we're initializing 
        cmd.append("--initialize")

        # append the setup file if we have one
        if self.setup_xml is not None:
            cmd.append("-p")
            cmd.append(self.setup_xml)
                
        print(" -> Initializing run '{}' ...".format(run.dir_name), end="\r")
        # append the code_saturne scripe        
        cmd = [self.binary] + cmd

        # use the ASYNC runner
        runner = utils.AsyncRun(cmd, cwd=run.case.abspath)
        
        # -> enable logging
        runner.stdout_log = True
        runner.stdout_print = False
        runner.stdout_print = True

        proc = runner.execute()

        # proc = self.cs_run(cmd, cwd=run.case.abspath)
        if proc.returncode != 0:
            print(" -> Initializing run '{}' ... FAILED".format(run.dir_name), end="\n")
            self.logger.debug("ASYNC runner failed, with cmd %s", " ".join(cmd))

            # write the stderr to the log
            raise exceptions.RunFailed("Error processing case")
        
        print(" -> Initializing run '{}' ... OK".format(run.dir_name), end="\n")

        # if we've init_from, then replace the symlink setup.xml with init_from.xml
         
        finit = os.path.join(run.abspath, "init_from.xml")
        if args.init_from is not None and os.path.isfile(finit):
            # move the init_from to setup.xml
            fsetup = os.path.join(run.abspath, "setup.xml")
            assert os.path.islink(fsetup), f"Expecting '{fsetup}' to be a symlink"
            os.rename(finit, fsetup)
        
        # the run_solver script contains a "cd" which we need to comment out
        self.modify_run_solver(run)

        return

    def modify_run_solver(self, run):
        """
        code_saturne initialization adds a cd ${EXEC_DIR} to the run_solver script.
        This messes up our staging setup, so we remove it and ensure we always run from the cwd

        """
        import fileinput
        
        # locate the file
        script_name = "run_solver"

        fpath = os.path.join(run.abspath, script_name)

        if not os.path.isfile(fpath):
            raise FileNotFoundError("File not found '{}'".format(fpath))
        
        comment = "# Commented out by cfmt\n"

        # open 
        #with open(fpath, 'r+') as f:
        with fileinput.input([fpath], inplace=True) as f:
            for line in f:
                if line.startswith("cd"):
                    line = line.replace(line, comment + "#" + line)
                print(line, end='')

        # if we have env modules, prevent CS from hardcoding the LD_LIB_PATH
        with fileinput.input([fpath], inplace=True) as f:
            for line in f:
                if line.startswith("export LD_LIBRARY_PATH"):
                    line = line.replace(line, comment + "#" + line)
                print(line, end='')
        
        # for slurm, we replace mpiexec with srun
        #if run.batch.system_name == "slurm":
        #    with fileinput.input([fpath], inplace=True) as f:
        #        for line in f:
        #            if line.startswith("mpiexec"):
        #                line = line.replace(line, comment + "#" + line)
        #            print(line, end='')


    
    def get_command(self, run, batch, cmd=None, cmdline_args=None):
        """
        code_saturne creates its own run script as part of the setup process

        It automatically places a run_solver bash script inside the run directory
        which automatically accounts for parallel submission.
        """
        # append the binary
        if isinstance(self.binary_path, list):
            command = self.binary_path
        else:
            command = [self.binary_path]
        
        # append to the passed command
        if isinstance(cmd, list):
            _cmd = cmd + command
        else:
            _cmd = command

        if batch.system_name == "slurm":
            command = ["OMP_NUM_THREADS=1", "srun"]
            if batch.mode == "parallel":
                mpi_plugin=config.get("slurm", "mpi_plugin", fallback="none")
                command.append("--mpi=" + mpi_plugin)
            command.append("./cs_solver")
            if batch.mode == "parallel":
                command.append("--mpi")
            return command

        return _cmd
    
    def set_bin_path(self, dest):
        """ Sets the binary path based on a provided run directory """
        # should already be set, since the binary is system installe
        if self.binary_path is None:
            raise exceptions.CheckFailed("Solver binary not set")

    def get_bin_path(self, wd=None, batch_system=None):
        """ 
        Returns the path to the binary 
        """
        # if we're not tight integration (where we use run_solver)
        # -> binary path depends on the run type
        assert batch_system is not None, "Cannot determine binary without batch"

        if batch_system.mode == "parallel":
            return [self.SOLVER_BINARY, "--mpi"]
        else:
            return [self.SOLVER_BINARY]
        
    def copy_mesh(self, src, dst, mask=None, args=None):
        """ 
        Code_saturne handles the copying
        """
        return 

        if mask is None:
            mask = config.get(self.name, 'mesh_mask')

            # saturne can read files gzipped, detect these as well
            mask_list = mask.split()
            mask_list = [m + ".gz" for m in mask_list]
            mask = " ".join(mask_list)

        # self.copy_mask(mask, src, dst)

    def copy_input(self, src, dest, args=None, mask=None, history=False):
        """
        Code_saturn handles the copying
        """
        # for the history, we copy the input mask over
        if history:
            # copy the input from the run to the history file
            mask = config.get(self.name, 'input_mask')
            self.copy_mask(mask, src, dest)

            # copy the src directory from the run dir
            mask = "src"
            self.copy_mask(mask, src, dest)
    
    def copy_src(self, dest):
        """
        Copy the build src to the specified directory
        """
        pass

    def get_src(self, run, case_src=False):
        """
        Return the location of the src 
        """
        if case_src:
            case_dir = run.case.abspath
            src_dir = os.path.join(case_dir,"SRC")
        else:
            # then we get the 
            run_dir = run.get_run_dir()
            src_dir = os.path.join(run_dir, "src")

        return src_dir
        
    def run(self, cwd, args=None):
        pass
        
    
    def edit_input(self, directory, mask=None, prompt=None):
        """ Edit the input files in the run directory """
        # check the destination dir exists
        if not os.path.isdir(directory):
            raise NotADirectoryError("Cannot find run directory")

        # look in the DATA folder
        data_dir = os.path.join(directory, 'DATA')
        if os.path.isdir(data_dir):
            # then we're in a case folder,
            directory = data_dir
        
        # cannot use input mask since that's specific
        if mask is None:
            mask = self.edit_mask

        mask_list = mask.split(" ")
        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = sorted(glob.glob(os.path.join(directory, _mask)))
            for fname in files:
                src_list.append(fname)

        fno = len(src_list)
        if fno == 1:
            self.edit(src_list[0], cwd=directory)
        
        elif fno > 1:

            print(" -> Found {} files to edit".format(fno))

            # add an option to edit nothing
            src_list.append("Exit")
            prompt="Please select a file to edit or select exit to edit none: "

            while True:
                selection = utils.list_select(src_list, title=prompt, options_map_func=os.path.basename)
                if selection == fno:
                    break

                self.edit(src_list[selection])
        else:
                #self.run(cwd=cwd, case=fcas) run fluent launcher
            if self.USE_GUI:
                prompt="No editable files found. Do you want to run the Saturne GUI?"
                if utils.confirm(prompt, default=True):
                    self.cs_gui(file=None, new=True, cwd=directory)
            else:
                print("No editable files found")

    def edit(self, flist, cwd = None, args = None):
        """ 
        Edits files
        """
        if cwd is None:
            # use the current directory
            cwd = os.getcwd()

        # see what we've been passed
        # -> if we have an xml file we load the gui
        if flist:
            if flist.endswith('.xml'):
                spoof_case = False
                # determine whether we're in a DATA folder
                print(" -> Editing '{}' in '{}'".format(os.path.basename(flist), cwd))

                if self.USE_GUI:
                    # wrap in a try block, so that we can be as atomic as possible
                    data_dir = os.path.join(self.case.abspath, "DATA")
                    try:
                        if os.path.basename(cwd) != "DATA":
                            assert self.case is not None, "No case loaded"


                            # get the data folder
                            # NOTE: v8 updates a run.cfg file with the temp name
                            #       so we need to move the original and keep the 
                            #       right name
                            # move the file
                            data_xml = os.path.join(data_dir, "setup.xml")
                            if os.path.exists(data_xml):
                                self.logger.debug("Moving existing setup.xml: '%s'", data_xml)
                                os.rename(data_xml, data_xml + ".cfmt")
                        
                            src = os.path.join(cwd, flist)
                            dst = os.path.join(data_dir, "setup.xml")

                            # symlink 
                            #if os.path.isfile(dst):
                            #    self.logger.debug("Removing existing file '%s'", dst)
                            #    os.remove(dst)
                        
                            self.logger.debug("Creating symlink '%s'", dst)
                            os.symlink(src, dst)
                        
                            flist = dst
                            cwd = data_dir
                            spoof_case = True
                        self.cs_gui(file=flist, cwd=cwd)
                    finally:
                        # remove the symlink
                        data_xml = os.path.join(data_dir, "setup.xml")
                        if os.path.islink(data_xml):
                            self.logger.debug("Removing symlink '%s'", data_xml)
                            os.remove(data_xml)
                        data_xml = os.path.join(data_dir, "setup.xml.cfmt")
                        # move the file back
                        if os.path.exists(data_xml):
                            new_xml = utils.remove_suffix(data_xml, ".cfmt")
                            self.logger.debug("Moving original setup.xml back: '%s'", new_xml)
                            os.rename(data_xml, new_xml)
                        
                else:
                    utils.edit_file(flist, cwd=cwd)

            else:
                # refer to parent to load in vim
                super().edit(flist, cwd)
        else:
            # we have nothing - prompt
            self.edit_input(cwd)
            # self.run(cwd=cwd)

    
    def get_tout_filter(self, run, up=False):
        """
        Given a run, check the staging directory for timeoutput
        Returns a filter list which can be used in rsync
        """
        if up:
            src_dir = run.abspath
        else:
            if not run.in_staging:
                return []
            src_dir = run.staging_dir

        # get the staging directory
     
        if not os.path.isdir(src_dir):
            return []
        
        rfilter = []
        # search in the staging directory for tout folders
        mask = config.get(self.name, "tout_mask")
        results_mask = config.get(self.name, "results_mask", fallback="")
        files = []
        for m in mask.split():
            f = glob.glob(os.path.join(src_dir, m))
            if f: files += f

        # for each result we want to include the dir, but 
        #  - exclude all data files
        #  - include the results mask file (so that output can be continued)
        for fpath in files:
            # get relative path
            path = os.path.relpath(fpath, src_dir)
            rfilter.append(['+', path])
            for m in results_mask.split():
                rfilter.append(['+', os.path.join(path, m)])
            rfilter.append(['-', os.path.join(path, "*")])

        return rfilter
    
    
    def convert_ensight(self, directory, args, run=None):
        """
        Returns arguments required to load ensight output
        """
        pass

    def select_results(self, run):
        """
        Checks the run directory for suitable cases to load
        """
        run_dir = run.get_run_dir()

        if self.results_available is None:
            self.load_results(run)

        results = self.results_available
        assert isinstance(results, list)
        
        fno = len(self.results_available)
        
        if fno > 1:
            # we ask the user to choose
            print(" -> Found {} files to view".format(fno))

            # add an option to exit (appears as if its in the run dir)
            results.append(os.path.join(run_dir,"Exit"))
            prompt="Please select a results file to view or exit"

            # map functon
            fmap = lambda d: os.path.relpath(d, start=run_dir)

            selection = utils.list_select(results, title=prompt, options_map_func=fmap)
        
            if selection == fno:
                sys.exit(0)
        
            fselect = results[selection]
        elif fno == 1:
            fselect = results[0]
        else:
            # we ask the user to choose
            print(" -> No results files found")
            sys.exit(0)

        # store the results file
        self.results_file = fselect
        
        return fselect

    def load_results(self, run, mask=None):
        """
        Load results present in the given run directory
        """
        run_dir = run.get_run_dir()

        # get the results mask if not passed
        if mask is None:
            mask = config.get(self.name, "results_mask")
        self.logger.debug("Loading results_mask '%s'", mask)

        # get list of directories in the run folder
        sub_dirs = [f.name for f in os.scandir(run_dir) if f.is_dir()]
        
        # remove directories we know are present
        sub_dirs = list(set(sub_dirs) - set(self.run_dirs))
        
        flist = []
        # for each sub directory, search for suitable results files using results_mask
        for d in sub_dirs:
            # glob each mask
            for m in mask.split():
                f = glob.glob(os.path.join(run_dir, d, m))
                if f:
                    for _f in f:
                        self.logger.debug("Found results file '%s'", str(_f))
                    flist+= f

        self.results_available = flist

    
    def get_output_format(self, run):
        """ 
        Load and get the output format of the run passed
        """
        import fnmatch
        
        if self.results_file is None:
            raise FileNotFoundError("Results not loaded")

        fres = self.results_file

        for output, m in self.output_format_map.items():
            if fnmatch.fnmatch(fres, m):
                return output

        msg = "Cannot determine output format of file '{}".format(fres)
        self.logger.debug(msg)
        raise exceptions.PostFailure(msg)

    def get_post_args_paraview(self, run, args=None):
        """
        Return argv for paraview
        """
        if self.results_file is not None:
            assert isinstance(self.results_file, str)
            argv = [self.results_file] 
            return argv
        else:
            raise exceptions.PostFailure("Results file not loaded")
    
    def copy_case_hook(self, case, new_path):
        """
        Called after the files have been copeid by case.copy
        """
        # remove the existing runs directory
        run_dir = os.path.join(new_path, "runs") 
        if os.path.isdir(run_dir):
            os.rmdir(run_dir)
        
        # create the RESU directory 
        resu_dir = os.path.join(new_path, "RESU")
        if not os.path.isdir(resu_dir):
            os.mkdir(resu_dir)

        # symlink our directories
        dirmap = {
            'RESU': 'runs',
        }
        for src, dst in dirmap.items():
            _src = os.path.join(self.study.path, new_path, src)
            _dst = os.path.join(self.study.path, new_path, dst)

            # create symlink if src != dst
            new_dst = os.path.join(self.study.path, new_path, dst)
            os.symlink(src, new_dst)

        return

    
    def copy_results(self, src, dest, 
        keep_files=None, 
        remove_files=None, 
        keep_residuals=False, 
        keep_monitors=False, 
        keep_tout=False,
        keep_input=False,
        keep_dir=None, 
        continue_tout=False,
        init_to_scratch=False):
        """
        Copies results files to dest.
        Code_saturne needs a restart file. We copy this from the src

        --keep-residuals - 
        --keep-monitors - will use the monitor mask
        """

        # check the src directory for a checkpoint folder
        checkdir = os.path.join(src, "checkpoint")
        datadir = os.path.join(self.case.abspath, "DATA")

        if not os.path.isdir(checkdir):
            msg = "Cannot find checkpoint directory '{}'".format(checkdir)
            raise exceptions.TransferFailed(msg)

        # create a restart directory in the dst folder
        _src = checkdir
        _dst = os.path.join(dest, "restart")
        if self.INIT_FROM_USE_SYMLINK:
            # get the relative path
            os.symlink(_src, _dst)
        else:
            shutil.copytree(checkdir, _dst)
        
        # search in the directory for tout folders
        if continue_tout:
            mask = config.get(self.name, "tout_mask")
            files = []
            for m in mask.split():
                f = glob.glob(os.path.join(src, m))
                if f:
                    files += f

            res_mask = config.get(self.name, "results_mask").split()
            # for each, we create the folder in the destination and copy the timeoutput.dat if
            # it exists
            for fpath in files:
                dirname = os.path.basename(fpath)
                dirpath = os.path.join(dest, dirname)
                os.mkdir(dirpath)

                # only supported for ensight, but we make the folders
                if "*.case" in res_mask:
                    mask = "*.case"
                    self.copy_mask(mask, fpath, dirpath)

        if keep_input:
            # we copy the setup.xml as init_from.xml to DATA dir (this will be picked up by init_run, then removed)
            _src = os.path.join(src, "setup.xml")
            _dst = os.path.join(datadir, "init_from.xml")
            if not os.path.isfile(_src):
                raise FileNotFoundError("Cannot find '%s'", _src)
            self.logger.debug("Copying '%s' to '%s'", _src, _dst)
            shutil.copy2(_src, _dst)
            # modify the setupxml
            self.setup_xml = _dst


        if keep_residuals:
            fres = os.path.join(src, self.residual_fname)
            if os.path.isfile(fres):
                self.logger.debug("Copying '%s' to '%s'", fres, dest)
                shutil.copy2(fres, dest)
        
        if keep_monitors:
            monitor_files = self.get_monitor_files(src)
            _files = []
            if monitor_files:
                for fname in monitor_files:
                    # get the relative path to the src run folder
                    relpath = os.path.relpath(fname, src)
                    fdst = os.path.join(dest, relpath)
                    #print(fname, "->", fdst)
                    # create the directory if required
                    dirname = os.path.dirname(fdst)
                    if dirname != dest and not os.path.isdir(dirname):
                        self.logger.debug("Creating directory '%s'", dirname)
                        os.makedirs(dirname)
                    
                    self.logger.debug("Copying '%s' to '%s'",fname, fdst)
                    shutil.copy2(fname, fdst)
                    #dirname = os.path.basename(os.path.dirname(fname))
                    #dst_dir = os.path.join(dest, dirname)
                    #os.makedirs(dst_dir, exist_ok=True)
                    #self.logger.debug("Copying '%s' to '%s'", fname, dst_dir)

        if keep_dir is not None:
            # we've got some directories to copy over
            for d in keep_dir:
                # test if it exists and warn if not
                _dir = d.pop()
                src_dir = os.path.join(src, _dir)
                dst_dir = os.path.join(dest, _dir)
                if os.path.isdir(src_dir):
                    # copy over
                    shutil.copytree(src_dir, dst_dir)
                    self.logger.debug("Copying '%s' to '%s'",src_dir, dst_dir)
                else:
                    print(utils.WARNING + f": Directory '{src_dir}' not found.")
                    self.logger.warning(f"Requested to keep directory {src_dir} but no such directory can be found.")

    def job_status_change(self, job, old_status, new_status):
        """
        Called when updating a job status 
        """
        # if the job is being changing from "running", run the post cleanup
        finished = [job.COMPLETED, job.ABORTED, job.ERROR]
        if (old_status is not new_status) and (new_status in finished):
            self.job_finished(job)

 
    def job_finished(self, job):
        """ 
        Called when a job status changes from running to finished
        Note for batch systems this may be way after the job has finished 

        If the job is aborted early, some of these may not exixt
        """
        self.logger.info("Running post finish tasks for JOB: %s", job.id)
        run_dir = job.working_dir
        
        # save some files frum the run
        log_ext = self.LOG_EXT
        log_files = self.LOG_FILES
        other_files = self.OTHER_FILES
        
        log_files = [l + log_ext for l in log_files]
        log_files += other_files

        for f in log_files:
            src = os.path.join(run_dir, f)
            fname, fext = os.path.splitext(f)
            dst = os.path.join(run_dir, fname + "." + str(job.id) + fext)
            
            if not os.path.isfile(src):
                self.logger.warning("Expected file '%s' not found", src)
                continue
            
            # skip if the log file has already been created
            if not os.path.isfile(dst):
                shutil.copy2(src, dst)
        
        # code_saturne doesn't produce stderr, but might produce an "error" file
        ferr = os.path.join(run_dir, "error")
        if os.path.isfile(ferr):
            stderr = job.get_stderr_fname(job.run)
            dst = os.path.join(run_dir, stderr)
            src = ferr
            self.logger.warning(f"{self.name} error file found")
            utils.concatenate_files(src, dst)

        # merge on-going statistics, but only for jobs that were marked as completed.
        if job.status == job.COMPLETED:
            files_to_merge = {
                'residuals.csv': self.get_residual_file(),
                'timer_stats.csv': "timer_stats_combined.csv"
            }
            for src, dst in files_to_merge.items():
                _dst = os.path.join(run_dir, dst)
                _src = os.path.join(run_dir, src)

                if not os.path.isfile(_src):
                    self.logger.warning("Expected file '%s' not found", _src)
                    continue
                
                utils.concatenate_files(_src, _dst, remove_header=1)

            # handle monitor probes, if they exist
            mondir = os.path.join(run_dir, "monitoring")
        
            if os.path.isdir(mondir):
                # we loop over the files present, and cat to .dat files
                flist = glob.glob(os.path.join(mondir, "probes_*.csv"))

                # remove all the files ending with "_combined.csv"
                flist = [f for f in flist if not f.endswith("_combined.csv")]

                # remove the coords file from the list
                fcoords = os.path.join(mondir, "probes_coords.csv")
                if fcoords in flist: flist.remove(fcoords)

                # merge the remainder
                for f in flist:
                    src = f
                    dst = os.path.splitext(f)[0] + "_combined.csv"

                    if not os.path.isfile(src):
                        self.logger.warning("Expected file '%s' not found", _src)
                        continue

                    utils.concatenate_files(src, dst, remove_header=1)
        else:
            self.logger.info(f"Monitors not merged for job {job.id} as it is marked as {job.status}")
        
    def get_monitor_files(self, path, fmask=None):
        """ Return a list of paths of files to monitor """
        # get the mask
        if fmask is None:
            fmask = config.get(self.name, 'monitor_mask')

        mask = fmask.split()
        
        files = []
        
        # glob the files
        for m in mask:
            f = glob.glob(os.path.join(path, m))
            if f:
                files += f
        
        if files:
            return files
        else:
            raise FileNotFoundError("No monitor files found")
    
    def residual_reader(self, path, **kwargs):
        """ Returns a residual reader """
        fpath = os.path.join(path, self.get_residual_file())
        return ResidualReader(fpath, **kwargs)
    
    def get_massflow_file(self, path):
        """
        DEPRECATED
        Returns the pass to the massflow rate file
        """
        flist = self.get_monitor_files(path, fmask="mon-massflow.dat")
        return flist.pop()
    
    def monitor_reader(self, path, **kwargs):
        """ Returns a monitor reader """
        return MonitorReader(path, **kwargs)
    
    def plot_reader(self, path, **kwargs):
        """ Returns a monitor reader """
        return PlotReader(path, **kwargs)

    def file_sequence_reader(self, fpath, regex=None):
        """ 
        loads a regex to define the sequence list 
        
        here we preload all of the files since we cannot be sure of the
        sequence 
        """
        import re
        
        if isinstance(fpath, list):
            fpath = fpath[0]
        
        # get the basename and split
        bname = os.path.basename(fpath)
        fname, ex = os.path.splitext(bname)
        
        
        # apply the regex to the remaining filename 
        if regex is None:
            regex_str = config.get(self.name, "sequence_regex", fallback=r"\d+")
            regex = re.compile(regex_str)
        
        mask = regex.findall(fname)

        if len(mask) < 1:
            raise ValueError(f"No sequence found using path '{bname}'")
        
        
        def _sort_nt_filename(fname):
            bname = os.path.basename(fname)
            matches = regex.findall(bname)
            return int(matches[-1])
        
        # substitute the found integer string with * for glob
        glob_mask = bname.replace(mask[-1], '*')
        abs_glob_mask = os.path.join(os.path.dirname(fpath), glob_mask)
        
        # glob the files and sort
        flist = sorted(glob.iglob(abs_glob_mask), key=_sort_nt_filename)

        # get a corresponding list of ints
        fint = [_sort_nt_filename(f) for f in flist]

        return SequenceReader(flist, fint)

    def get_case_ignore_list(self, case):
        """
        Returns a list of directories to ignore when copying a case
        """
        return ["RESU"]
    
    def get_stdout_fname(self, job, wd=None):
        """
        Code_saturne writes its stdout to a run_solver file.
        -> for running jobs, this exists as run_solver.log
        -> for finished jobs, the self.job_finished method moves this
           to run_solver.{job.id}.log

        This may also be compressed, so we have to check
        """
        assert wd is not None, "Working directory required for get_stdout_fname"

        LOG_EXT = self.LOG_EXT
        MASK = self.LISTING_MASK
        FNAME = self.SOLVER_LOG_NAME
        job_finished = [job.COMPLETED, job.ABORTED, job.SUSPENDED, job.ERROR]
        
        if job.status in job_finished:
            fn = MASK.format(job=job)
        else:
            fn = FNAME + LOG_EXT

        # compression first
        fn += self.COMPRESSION_EXT
        if not os.path.exists(os.path.join(wd,fn)):
            # if that doesn't exist, remove it
            fn = fn.rstrip(self.COMPRESSION_EXT)
        
        return fn

    def terminate(self, proc):
        """ We pass a SIGINT to fluent """
        import signal
        print("Terminating {}".format(self.name))
        os.kill(proc.pid, signal.SIGINT)

class ResidualReader(utils.LiveDataReader):
    """ Read residuals """
    column_index = []
    def get_headers(self):
        """ Sets the headers """

        # headers are csv
        header = self.fd.readline().split(",")

        # remove extra whitespace
        header = [x.strip() for x in header]
        
        headers = [] 
        # x_axis assignment
        x_axis = ["iteration", "t"]
        i = 0
        if header and len(header) > 1:
            for h in header:
                headers.append(h)
                self.column_index.append(i)

                if h.lower() in x_axis:
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                else:
                    self.yaxis_assign.append(i)

                i += 1
        else:
            # discard and rewind
            self.fd.seek(0)
            headers = None

        return headers

    def process(self, line):
        """ process the data """
        data = line.split(",")

        # cannot handle zero values for sone reason
        try: 
            data = [float(data[i]) if float(data[i]) != 0.0 else float("nan") for i in self.column_index]
        except ValueError:
            data = None
        return data
    
class MonitorReader(utils.LiveDataReader):
    """ Read fluent monitor files """
    column_index = []
    ftype = "csv"
    ftype = None
    
    # whether to add a column for the line count

    line_count = 0

    def get_headers(self):
        """ Monitors are of the format:
        First line is the name of the report file
        Second line is a comment
        Third line contains headers enclosed in parenthesis
        """
        header = None
        last_pos = 0
        
        self.counter_index = None

        header_line = self.fd.readline()

        if self.ftype is None:
            # attempt to determine the delimeter
            if "," in header_line:
                self.ftype = "csv"
                header = header_line.split(",")
            else:
                self.ftype = "dat"
                header = header_line.split()
        
        # remove the extra gaps
        header = [x.strip() for x in header if x.strip()]
        headers = []
        
        # valid x_axis headers
        x_axis = ["t", "iteration", "nt", "time"]
        # check we have a valid header line
        i = 0
        
        # test for headers 
        # test if we have some headers
        try:
            [float(h) for h in header]
        except ValueError:
            has_headers = True
        else:
            has_headers = False

        if has_headers:
            # add a counter if needed
            # assign headers
            for h in header:
                if h.lower() in x_axis:
                    headers.append(h)
                    self.column_index.append(i)
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                    i += 1
                    continue
                else:
                    # assign to y_axis
                    headers.append(h)
                    self.column_index.append(i)
                    self.yaxis_assign.append(i)
                    i += 1
                    continue
            # if we dont have an xaxis, prepend one
            if self.xaxis_assign is None:
                headers.append("_I_")
                self.xaxis_assign = i
                self.column_index.append(i)
                self.counter_index = i
                i += 1
        else:

            # create a new column for IT 
            headers.append("_I_")
            self.xaxis_assign = i
            self.column_index.append(i)
            self.counter_index = i
            i += 1
            
            # rewind to start of non-header line
            self.fd.seek(last_pos)

            # assign default headers based on columns
            for h in header:
                h_str = "COL{:d}".format(i+1)
                headers.append(h_str)
                self.column_index.append(i)
                self.yaxis_assign.append(i)
                i += 1

        return headers

    def process(self, line):
        """ process the data """
        self.line_count += 1

        if self.ftype == "csv":
            data = line.split(",")
        else:
            data = line.split()
        if self.counter_index is not None:
            # find the counter 
            i = self.counter_index
            data.insert(i, float(self.line_count))
            #data = [float(self.line_count)] + data

        _data = []
        for i in self.column_index:
            try:
                _data.append(float(data[i]))
            except ValueError:
                _data.append(math.nan)
            except IndexError as err:
                print(f"WARNING: {err}")
                _data.append(math.nan)

        return _data

class PlotReader(utils.LiveDataReader):
    """ Read plottable data """
    
    def get_headers(self):
        """ Sets the headers """
        header = None
        last_pos = 0

        # code saturne offers two file-types, dat or csv
        if self.ftype is None:
            self.ftype = self.guess_filetype()

        # assume its a dat file
        # -> skip all commented lines, last one is headers
        # -> if we have a time-value, read it 
        time_found = False
        prevline = None
        ih = 0
        while (True):
            line = self.fd.readline()
            if not line.startswith('#'):
                break
            
            if line.lower().find("time") != -1 and not time_found :
                time_str = line.split()[-1]
                try:
                    time = float(time_str)
                except ValueError:
                    time = None
                else:
                    self.tlist[self.findex] = time
                    time_found = True

            ih += 1
            prevline = line

        if prevline is not None:
            if prevline.startswith('#COLUMN_TITLES:'):
                header = prevline.lstrip('#COLUMN_TITLES:')
                header = header.split('|')
            else:
                header = prevline.lstrip('#')
                if self.ftype == "csv":
                    header = header.split(",")
                else:
                    header = header.split()
        else:
            if self.ftype == "csv":
                header = line.split(",")
            else:
                header = line.split()
            # strip any extra whitespace
            #header = [x.strip() for x in header]
        # no comments, first line should be the header
        # - check again for csv
        header = [x.strip() for x in header if x.strip()]
        if header:
            self.skip_headers = ih
            self.xaxis_assign = 0
            self.column_index.append(0)
            i = 1
            # only assign one curve
            self.yaxis_assign.append(i)
            for h in header[1:]:
                self.column_index.append(i)
                i += 1
        else:
            # rewind to start of non-header line
            self.fd.seek(last_pos)
            header = None
        
        return header 
    
    def process(self, line):
        """ process the data """

        if self.ftype == "csv":
            data = line.split(",")
        else:
            data = line.split()

        _data = []
        for i in self.column_index:
            try:
                _data.append(float(data[i]))
            except ValueError:
                _data.append(math.nan)
            except IndexError as err:
                print(f"WARNING: {err}")
                _data.append(math.nan)
        return _data

class SequenceReader(utils.FileSequenceReader, PlotReader):
    """
    Sequence plot
    """
    pass
