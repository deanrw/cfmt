"""
    cfmt: A management tool for Computational Fluid Dynamics

    codes/starccm.py: Interface to the starccm+ code
"""

import os
import sys
import shutil
import glob

from cfmt.config import configtool as config
from cfmt.codes import base
from cfmt import exceptions, utils

"""
To run in batch mode StarCCM+ requires either:
 - A java macro file which provides the commands to execute
 - Appending "mesh", "run" or "step" to the "-batch" option to execute that step.
"""

class StarCCMCode(base.Code):
    name = "starccm"
    maintainer = "Siemens"
    description = "Commericial CFD software developed by Siemens"
    path = {}
    requires_build = False
    url = "https://www.plm.automation.siemens.com/global/en/products/simcenter/STAR-CCM.html"

    # batch systems the code has tight integration with
    TIGHT_INTEGRATION = ["sge", "none", "slurm"]
    SIM_FILE_EXT = ".sim" 
    MACRO_FILE_EXT = ".java"
    DEFAULT_CONFIG = {
        name: {
            'binary': 'starccm+',
            'input_mask': '*.sim *.java',
            'mesh_mask': '*.cgns *.ccmg *.msh *.grd *.p2d *.xyz',
            'monitor_mask': 'mon-*.dat',
            'results_mask': '*' + SIM_FILE_EXT,
            'tout_mask': 'tout*',
            'post_utility': 'starccm',
            'precision': 'dp',
            'default_mode': 'batch',
            '# default_mpi': '',
            #'env_modules': '',
        }
    }
    residual_fname = "residuals.csv"

    default_output_format = "starccm"
    default_output_utility = "starccm"

    capability_true = ["edit_input"]
    capability_false = []

    edit_mask = '*' + SIM_FILE_EXT + " " + '*'+ MACRO_FILE_EXT

    def __init__(self, study=None, db_record=None):
        """ Constructor """
        # call parent
        super().__init__(study, db_record)

        # solver specific options to set
        self.solver_mode = None 
        self.batch_mode = None
        self.fsim = None
        self.fmacro = None
        self.mpi = None
    
    @staticmethod
    def init_check(args):
        """ Pre-initialization check """
        # -> check if starccm can be found
        if shutil.which(config.get(StarCCMCode.name, 'binary')) is None:
            raise exceptions.CheckFailed("StarCCM+ cannot be found in user PATH")
    
    def init(self, db=None, args=None):
        """ Initializes the code for a study """

        print(": -> Initializing " + self.name)

        # create the input folder
        path = os.path.join(self.study.path, 'input', self.name)
        if not os.path.isdir(path):
            os.makedirs(path)

        # initialize db
        if db is not None:
            self.init_db(db)

        # add config to config file

        self.logger.info("Code '%s' added to study '%s'", self.name, self.study.name)
    
    def init_run(self, run, args=None):
        """
        Code specific initialization
        Previous options is set when we're continuing a run
        """
        if args and args.init_from is not None:
            # we copied results over and need to select the sim to run
            if args.init_to_scratch: 
                src_dir = run.staging_dir
            else:
                src_dir = run.abspath

            prompt="Please select the sim file to use:"
            m = "*" + self.SIM_FILE_EXT
            self.fsim = self.select_from_multiple(src_dir, m, prompt)

    def init_db(self, db):
        """ Adds an entry to the codes db """
        self.db_record = db.new_code(
            name=self.name,
            identifier=self.name,
            url=self.url,
            description=self.description,
            maintainer=self.maintainer,
            path=self.path
        )

    def create_case(self, path):
        """ Runs when creating a new case """
        # get the study directory
        from distutils.dir_util import copy_tree
        
        # get the sudy directory
        input_path = os.path.join(self.study.path, 'input', self.name)
        copy_tree(input_path, path)
    
    def pre_run_tasks(self, run, args=None):
        """ Task to carry out pre-run """
        # check the module is loaded
        
        # check we have required options selected
        keys = ["sim file", "solver_mode"]
        values = [self.fsim, self.solver_mode]
        
        for k,v in zip(keys, values):
            if not isinstance(v, str):
                msg = f"{k} set incorrectly: {v}"
                raise exceptions.RunFailed(msg)

        # macro might not be there
        if self.fmacro:
            # check its a string
            if not isinstance(self.fmacro, str):
                msg = f"macro file set incorrectly: {self.fmacro}"
                raise exceptions.RunFailed(msg)
        
            if args.continue_calc and args.starccm_macro:
                rundir = run.get_run_dir()
                fpath = os.path.join(rundir, self.fmacro)
                if not os.path.exists(fpath):
                    self.logger.info("Requested macro file not present in run directory")
                    # check if it exists in the case directory
                    casedir = run.case.abspath
                    fpath2 = os.path.join(casedir, self.fmacro)
                    if not os.path.exists(fpath2):
                        msg = "Cannot find requested macro file "
                        raise FileNotFoundError(msg)
                    else:
                        self.logger.info("Copying macro file from case directory")
                        self.copy_mask(self.fmacro,casedir, rundir)

        return 

    def pre_run_check(self, run, args, batchname=None):
        """
        Pre-run checks

        Here we determine if we're  
        
        """
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)

        # determine if we have a previous job
        if args.continue_calc and hasattr(run, "job"):
            options = run.job.options["code"]
        else:
            options = {}
            
        self.set_code_options(options, args=args, run=run, batchname=batchname)

    
    def set_bin_path(self, dest):
        """ Sets the binary path based on a provided run directory """
        # should already be set, since the binary is system installe
        try:
            # -> do not use which here as the module loaded might be different 
            #    to those requested
            self.binary_path = config.get(self.name, 'binary')
        except:
            raise exceptions.CheckFailed("Solver binary not set")
        
        if self.binary_path is None:
            raise exceptions.CheckFailed("Solver binary not set")


    def get_bin_path(self, wd=None, batch_system=None):
        """ 
        Returns the path to the binary 
        """
        if self.binary_path is not None:
            return self.binary_path
    
    def copy_mesh(self, src, dst, mask=None, args=None):
        """ 
        Copies the mesh 

        Called from 
        """
        if mask is None:
            mask = config.get(self.name, 'mesh_mask')

        self.copy_mask(mask, src, dst)
    
    def copy_input(self, src, dst, args=None, mask=None, history=False):
        """
        Copies input files to dest

        pre_run_tasks has already determined which fsim and fmacro to use
        """
        import re
        if mask is None:
            mask = config.get(self.name, 'input_mask')
        mask = mask.split(" ")
        
        # macro file
        if self.fmacro:
            mask = self.mask_replace(mask, "*" + self.MACRO_FILE_EXT, os.path.basename(self.fmacro))

        # don't copy sim files to history, and don't copy if init-from
        if not history and not args.init_from:
            mask = self.mask_replace(mask, "*" + self.SIM_FILE_EXT, self.fsim)
        else:
            # remove everything apart from java files
            r = re.compile(r'.*\.java')
            mask = [x for x in mask if r.match(x)]
        # if we're init-from, then we'll have already copied the sim file
        mask = " ".join(filter(None, mask))

        self.logger.info(f"Mask is {mask}")
        
        # do not copy the backup simulation.
        self.copy_mask(mask, src, dst)


    def run(self, cwd, args=None, sim=None, host=None, port=None, macro=None, server=False):
        """
        Runs the starccm client
        """
        # check here to make sure fluent environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)
        
        cmd = [self.binary]

        if args.starccm_server:
            # running in server mode
            cmd += ["-server"]
            if args.starccm_np:
                cmd += ["-np", str(args.starccm_np)]
            if sim:
                cmd += ["-load", str(sim)]
        else:
            # running in client mode
            # connect to host if provided
            if args.starccm_np:
                cmd += ["-np", str(args.starccm_np)]
            
            if host:
                cmd += ["-host", str(host)]
        
            if port:
                cmd += ["-port", str(port)]

            if macro:
                # running our own macro
                cmd += ["-m", str(macro)]
            elif sim:
                cmd += [str(sim)]


        utils.run(cmd, cwd=cwd)
    
    def edit_input(self, directory, mask=None, prompt=None, args=None):
        """ Edit the input files """

        if not os.path.isdir(directory):
            raise NotADirectoryError("Cannot find run directory")
        
        if mask is None:
            mask = config.get(self.name, "edit_mask", fallback=self.edit_mask)

        mask_list = mask.split(" ")
        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = sorted(glob.glob(os.path.join(directory, _mask)))
            for fname in files:
                src_list.append(fname)

        fno = len(src_list)
        print("")
        if fno > 0:
            selection = 0
            if fno > 1:
                print(" -> Found {} files to edit".format(fno))

                # add an option to edit nothing
                src_list.append("Exit")
                prompt="Please select a file to edit or select exit to edit none: "

                selection = utils.list_select(src_list, title=prompt, options_map_func=os.path.basename)
                if selection == fno:
                    sys.exit(0)
            self.edit(src_list[selection], args=args)
        else:
            # no files, prompt to run fluent launcher
            prompt="No editable files found. Do you want to run StarCCM?"
            if utils.confirm(prompt, default=True):
                self.run(directory, args=args, sim=None)
        
    def results(self, run):
        """ 
        Given a run, load the results
        
        This defaults to a local serial client
        """
        # prompt for cas file
        cwd = run.get_run_dir()

        # find results files
        mask = config.get(self.name, 'results_mask')

        flist = []
        for _mask in mask:
            files = glob.glob(os.path.join(cwd, _mask))
            for fname in files:
                flist.append(fname)

        # sort alphabetically
        flist=sorted(flist)

        if len(flist) == 0:
            self.console.error("No results files found")
            sys.exit(1)
        elif len(flist) == 1:
            fpath = flist[0]
            print("  --> Found results file: '{}'".format(os.path.basename(fpath)))
        else:
            prompt = "Please select a results file: "
            ifile = utils.list_select(flist, title=prompt, options_map_func=os.path.basename)
            fpath = flist[ifile]
            print("  --> Selected results file: '{}'".format(os.path.basename(fpath)))

        # load in client
        self.run(cwd, sim=fpath)

    
    def set_code_options(self, options, args=None, **kwargs):
        """
        Sets code options

        Cmdline arguments take precedence if set
        """
        if "run" in kwargs:
            run = kwargs["run"]
        else:
            run = None
        
        if "batchname" in kwargs:
            batchname = kwargs["batchname"]
        else:
            batchname = None

        ## Set the mode 
        if "mode" in options:
            mode = options["mode"]
        else:
            mode = config.get(self.name, "default_mode")
        
        modes = [args.starccm_server, args.starccm_batch, args.with_gui]
        # convert the modes to boolean to check if we have something
        modes_b = [bool(x) for x in modes]
        modes_n = ["server", "batch", "client"]
        if modes_b.count(True) > 1:
            msg = "Only one solver mode can be selected"
            self.logger.error(msg)
            raise ValueError(msg)
        elif modes_b.count(True) == 1:
            i = next(i for i,v in enumerate(modes_b) if v)
            mode = modes_n[i]
        
        # set the batch mode if we have one
        batch_mode = None
        if mode == "batch":
            if isinstance(args.starccm_batch, str):
                batch_mode = args.starccm_batch
            elif "batch_mode" in options:
                batch_mode = options["batch_mode"]

        self.solver_mode = mode
        self.logger.debug("Setting solver_mode: %s", mode)
        self.batch_mode = batch_mode
        self.logger.debug("Setting batch_mode: %s", batch_mode)
            
        ## Set the sim and macro files
        # which directory are we searching for files
        if args.continue_calc and run is not None:
            src_dir = run.get_run_dir()
        else:
            src_dir = self.case.abspath
        
        # check we have at least 1 sim file
        if args and args.starccm_sim is not None:
            fs = args.starccm_sim[0]
            if not os.path.exists(fs):
                raise FileNotFoundError(f"Sim file not found: {fs}")
        elif args and args.init_from is not None:
            # we're starting from a previous case, so we won't know until we copy
            fs = None
        else:
            # check how many sim files we have
            fsim_d = options["fsim"] if "fsim" in options else None
            if fsim_d:
                fsim_d = os.path.join(src_dir, fsim_d)
            prompt="Please select the sim file to use:"
            m = "*" + self.SIM_FILE_EXT
            fsim = self.select_from_multiple(src_dir, m, prompt, default_option=fsim_d)
            
            if fsim is None: 
                msg = f"No {self.SIM_FILE_EXT} files found."
                self.logger.error(msg)
                raise FileNotFoundError(msg)
            else:
                fs = fsim
                self.logger.debug(f"setting fsim: {fsim}")
        self.fsim = fs

        # get the list of files to copy from config
        if args is not None and args.starccm_macro is not None:
            # command line takes precedence
            fm = args.starccm_macro[0]

            # test for existence if we're not continuing.
            # if we're continuing, we check in pre_run_tasks as the rules are more complicated
            if not args.continue_calc and not os.path.exists(fm):
                msg = f"Macro file '{fm}' does not exist"
                self.logger.error(msg)
                raise FileNotFoundError(msg)
        elif "fmacro" in options:
            fm = options["fmacro"]
        elif not args.continue_calc and not batch_mode:
            # check how many macro files we have
            m = "*" + self.MACRO_FILE_EXT
            prompt="Please select the macro file to use:"
            fm = self.select_from_multiple(src_dir, m, prompt, include_none=True) 
        else:
            fm = None

        
        # allow a zero length macro file
        if fm == "":
            fm = None
        self.fmacro = fm
        self.logger.debug(f"setting fmacro: {fm}")
        
        ## Set the mpi driver
        if args is not None and args.starccm_mpi is not None:
            mpi = args.starccm_mpi[0]
        elif "mpi" in options:
            mpi = options["mpi"]
        else:
            mpi = config.get(self.name, "default_mpi", fallback=None)
        
        self.mpi = mpi
        self.logger.debug(f"setting mpi driver: {mpi}")

        # any additional options
        additional_options = []
        if args is not None and args.starccm_options:
            additional_options = args.starccm_options
        elif "extra" in options:
            additional_options = options["extra"]
        else:
            # options for all cases
            additional_options = []
            _opt = config.get(self.name, "additional_options", fallback=None)
            if _opt:
                additional_options += _opt.split(" ")
            # specific options for batch systems
            if batchname is not None:
                _opt = config.get(self.name, "additional_options_"+batchname, fallback=None)
                if _opt:
                    additional_options += _opt.split(" ")

        self.additional_options = additional_options
        self.logger.debug(f"setting additional options: {additional_options}")

    
    def get_code_options(self, run):
        """
        Returns a dict of options to save with a job
        """
        options = {
            "mode": self.solver_mode,
            "fsim": os.path.basename(self.fsim),
        }
        
        if self.fmacro is not None:
            options["fmacro"] = os.path.basename(self.fmacro)

        if self.mpi:
            options["mpi"] = str(self.mpi)

        if self.batch_mode:
            options["batch_mode"] = str(self.batch_mode)
        
        if self.additional_options:
            options["extra"] = self.additional_options

        return options

    def edit(self, flist, cwd = None, args = None):
        """
        Edits files, loading starccm+ client if necessary
        """
        if cwd is None:
            # use the current directory
            cwd = os.getcwd()

        # see what we've been passed
        if flist:
            if flist.endswith('.sim'):
                fsim = os.path.join(cwd,flist)
                # we have a case file - > load fluent
                # attempt to detect the dimensions
                # parse the case file
                self.run(cwd=cwd, sim=fsim, args=args)
            else:
                # refer to parent to load in vim
                super().edit(flist, cwd)
        #elif args.starccm_server:
        #    print("Running a starccm server ... ")
            #self.run(cwd, args=args, sim=None, server=True)

        else:
            # we have nothing - prompt
            self.edit_input(cwd, args=args)
            # self.run(cwd=cwd)

    def get_command(self, run, batch, cmd=None, cmdline_args=None):
        """
        Since StarCCM has tight integration we provide the command here
        returns a command list

        We have to tailor this to the batch system as well
        """
        
        args = cmdline_args
        # initialize
        if cmd is not None:
            command = cmd
        else:
            command = []
        
        # append the bindary name
        command.append(self.binary_path)

        # determine the mode
        mode = self.solver_mode

        if mode == "server":
            command.append("-server")
        elif mode == "batch":
            command.append("-batch")
            if self.fmacro:
                command.append(str(self.fmacro))
            elif self.batch_mode:
                command.append(self.batch_mode)

        if batch.system_name == "sge":
            command.append("-batchsystem")
            command.append("sge")

        if self.mpi:
            command.append("-mpi")
            command.append(str(self.mpi))
        
        if batch.mode == "parallel":
            command.append("-np")
            command.append(str(batch.np))

        if self.additional_options:
            [command.append(x) for x in self.additional_options] 
        
        command.append(os.path.basename(self.fsim))

        return command

    def get_tout_filter(self, run, up=False):

        if up:
            src_dir = run.abspath
        else:
            if not isinstance(run.staging_dir,str):
                return []
            src_dir = run.staging_dir

        # get the staging directory
     
        if not os.path.isdir(src_dir):
            return []
        
        rfilter = []
        # search in the staging directory for tout folders
        mask = config.get(self.name, "tout_mask")
        files = glob.glob(os.path.join(src_dir, mask))

        # for each result we want to include the dir, but exclude everything else
        # but exclude everything else
        for fpath in files:
            # get relative path
            path = os.path.relpath(fpath, src_dir)
            rfilter.append(['+', path])
            rfilter.append(['-', os.path.join(path, "*")])

        return rfilter

    def copy_results(self, src, dest, 
        keep_files=None, 
        remove_files=None, 
        keep_residuals=False, 
        keep_monitors=False, 
        keep_tout=False,
        keep_input=False,
        keep_dir=None, 
        continue_tout=False,
        init_to_scratch=False):
        """
        Copies results files to dest

        --keep-residuals - does nothing
        --keep-monitors - will use the monitor mask
        """
        # prompt the user to copy the required files
        mask = config.get(self.name, 'results_mask')
        mask_list = mask.split(" ")

        if keep_input:
            mask = config.get(self.name, 'input_mask')
            mask_list += mask.split(" ")

        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = glob.glob(os.path.join(src, _mask))
            for fname in files:
                src_list.append(fname)
        
        # sort alphabetically
        src_list = sorted(src_list)
        
        fno = len(src_list)
        print("")
        if fno > 0:
            print(" -> Found {} files to copy".format(fno))

            # add an option to copy nothing
            prompt="Please select the files to copy (press SPACE to mark, ENTER to continue):"

            selection = utils.list_select(src_list, title=prompt, options_map_func=os.path.basename, multi_select=True, min_selection_count=0)

            for f,i in selection:
                shutil.copy2(f, dest)
            
        # search in the directory for tout folders
        if continue_tout:
            mask = config.get(self.name, "tout_mask")
            files = glob.glob(os.path.join(src, mask))

            # for each, we create the folder in the destination and copy the timeoutput.dat if
            # it exists
            for fpath in files:
                dirname = os.path.basename(fpath)
                dirpath = os.path.join(dest, dirname)
                os.mkdir(dirpath)

        if keep_monitors:
            monitor_files = self.get_monitor_files(src)
            _files = []
            if monitor_files:
                for fname in monitor_files:
                    shutil.copy2(fname, dest)
    
    def residual_reader(self, path, **kwargs):
        """ Returns a residual reader """
        # if the processed residual file exists, we choose that
        fpath = os.path.join(path, self.get_residual_file())
        if os.path.isfile(fpath):
            return ResidualReader(fpath, openmode='r', **kwargs)
        else:
            msg = "No plottable residual files found. Load in StarCCM+ client to view."
            raise FileNotFoundError(msg)
    
    def plot_reader(self, path, **kwargs):
        """ Returns a monitor reader """
        return PlotReader(path, **kwargs)
    
    def monitor_reader(self, path, **kwargs):
        """ Returns a monitor reader """
        return MonitorReader(path, **kwargs)
    

class ResidualReader(utils.CSVLiveDataReader):
    """ Read residuals """
    column_index = []
    
    def get_headers(self):
        """ Sets the headers """
        # headers are csv
        #header = self.fd.readline().split(",")
        header = next(self.readlines(process=False))
        
        headers = [] 
        # x_axis assignment
        x_axis = ["iteration: iteration", "iteration", "it"]
        i = 0
        if header and len(header) > 1:
            for h in header:
                headers.append(h)
                self.column_index.append(i)

                if h.lower() in x_axis:
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                else:
                    self.yaxis_assign.append(i)

                i += 1
        else:
            # discard and rewind
            self.fd.seek(0)
            headers = None

        return headers

class MonitorReader(utils.CSVLiveDataReader):
    """ Read residuals """
    column_index = []
    
    def get_headers(self):
        """ Sets the headers """
        # headers are csv
        #header = self.fd.readline().split(",")
        header = next(self.readlines(process=False))
        
        headers = [] 
        # x_axis assignment
        i = 0
        if header and len(header) > 1:
            for h in header:
                headers.append(h)
                self.column_index.append(i)
                if i != 0:
                    self.yaxis_assign.append(i)
                i += 1
            # assign first and second columns only
            self.xaxis_assign = 0
        else:
            # discard and rewind
            self.fd.seek(0)
            headers = None

        return headers

class PlotReader(utils.CSVLiveDataReader):
    """ Read residuals """
    column_index = []
    
    def get_headers(self):
        """ Sets the headers """
        # headers are csv
        #header = self.fd.readline().split(",")
        header = next(self.readlines(process=False))
        
        headers = [] 
        # x_axis assignment
        i = 0
        if header and len(header) > 1:
            for h in header:
                headers.append(h)
                self.column_index.append(i)


                i += 1
            # assign first and second columns only
            self.xaxis_assign = 0
            self.yaxis_assign.append(1)
        else:
            # discard and rewind
            self.fd.seek(0)
            headers = None

        return headers