"""
    cft: A management tool for Computational Fluid Dynamics
    
    codes: Interface with cfd codes
"""

import os
import logging
from cfmt.config import configtool as config
from cfmt.config import add_config
from cfmt import exceptions, utils

class Code(object):
    """ A base class to implement interfaces to different codes """
    name = "code_name"
    
    # the name to the residual file
    residual_fname = "residual.dat"

    # whether the code required building before a run
    requires_build = False

    # whether the code can be manually built (without running)
    can_build = False

    # list of batch systems that the code has tight integration with
    TIGHT_INTEGRATION = []

    # required config for all codes
    REQUIRED_CONFIG = {
        'input_mask': '',
        'mesh_mask': '',
        'monitor_mask': '',
        'results_mask': '',
        'tout_mask': '',
        'post_utility': '',
        'env_modules': ''
    }

    # whether we should copy binaries to run directories
    requires_bin_copy = False

    # default output format for all codes
    default_output_format = "ensight"

    # available results
    results_available = None

    # database fields
    db_fields = ['name',
                'identifier',
                'url',
                'description',
                'maintainer',
                'path'
               ]
    capability_true = []
    capability_false = ['edit_input','tavg']

    def __init__(self, study, db_record=None):
        """
        Required initialization for each instance
        Sets config
        """
        self.study = study
        self.case = None
        # set db_record
        
        self.load_config()

        self.binary = config.get(self.name, 'binary')
        
        if db_record is not None:
            """ set attributes from db record """
            self.db_record = db_record
            for db_field in self.db_fields:
                record = getattr(db_record, db_field)
                setattr(self, db_field, record)
            
            self.abspath = os.path.join(study.path, self.path)
        
        # store loggers
        self.console = logging.getLogger('console')
        self.logger = logging.getLogger('file')
        
        m = config.get(self.name, "env_modules", fallback=self.REQUIRED_CONFIG["env_modules"])
        self.env_modules = m.split() if m else []
    
    def __eq__(self, other):
        """ Determine whether two cases are equal """
        return self.db_record.id == other.db_record.id

    def set_case(self, case):
        """ Sets the case """
        self.case = case
    
    def init_case(self, case):
        pass

    def init_run(self, run, args=None):
        """
        Called after the run has been created
        """
        pass
    
    def init_db(self, db):
        """ 
        Initializes the db
        """
        self.db_record = db.new_code(
            name=self.name,
            identifier=self.name,
            url=self.url,
            description=self.description,
            maintainer=self.maintainer,
            path=self.path
        )

    def init(self, db=None, args=None):
        """ 
        Called when adding a case to a study
        """
        print(": -> Initializing " + self.name)

        # create the input folder
        path = os.path.join(self.study.path, 'input', self.name)
        if not os.path.isdir(path):
            os.makedirs(path)

        # initialize db
        if db is not None:
            self.init_db(db)

        self.logger.info("Code '%s' added to study '%s'", self.name, self.study.name)

    
    def get_residual_file(self):
        """ Return the residual file relative to the run directory """
        res = config.get(self.name, "residual_fname", fallback=self.residual_fname)
        return res
    
    def get_timeres_file(self, path):
        raise NotImplementedError
    
    def copy_mask(self, mask, src, dest):
        """ Takes a space seperated list of file masks, globs in src and copies to dest """
        import glob
        # checks
        if not os.path.isdir(src):
            raise NotADirectoryError("No such file or directory '{}'".format(src))
        
        if not os.path.isdir(dest):
            raise NotADirectoryError("No such file or directory '{}'".format(src))
        
        # expand mask
        mask_list = mask.split()
        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = glob.glob(os.path.join(src, _mask))
            for fname in files:
                src_list.append(fname)
        if src_list:
            import shutil
            from distutils.dir_util import copy_tree
            # copy the files
            for src_file in src_list:
                self.logger.debug("Copying '{}' to '{}' for mask '{}'".format(src_file, dest, mask))
                if os.path.isdir(src_file):
                    _dest = os.path.join(dest, os.path.basename(src_file))
                    copy_tree(src_file, _dest)
                else:
                    shutil.copy2(src_file, dest)
        #else:
        #    raise FileNotFoundError

    def mask_replace(self, mask, find, replace):
        """ Searches a mask for find and, if found, replaces with replace """
        import re

        # we need to escape any asterisks
        r = re.compile(re.escape(find))

        # get get indices of matches
        idx = [i for i, item in enumerate(mask) if re.search(r, item)]
        if idx:
            # delete all matches
            for i in idx:
                del mask[i]
        
        # add back what we want
        mask+= [replace]
        return mask

    @classmethod
    def has_tight_integration(cls, batch_system):
        return True if batch_system in cls.TIGHT_INTEGRATION else False

    def prepost_exec_locations(self, base_dir=""):
        """ Locations to add to potential script sourcing """
        return os.path.join(base_dir, "input", self.name)

    def commit(self, msg=None):
        """ Commits checked out src with provided msg """
        pass

    def take_snapshot(self):
        """ Takes a vcs snapshot of the source """
        raise NotImplementedError
    
    def compile(self):
        """ Compiles the code ready to run """
        raise NotImplementedError
    
    def run(self):
        """ Runs the code """
        raise NotImplementedError

    @classmethod
    def get_default_config(cls):
        """ Returns the default config """
        c = {
            cls.name: cls.REQUIRED_CONFIG
        }
        
        if cls.DEFAULT_CONFIG:
            c.update(cls.DEFAULT_CONFIG) 
        
        return c

    @classmethod
    def load_config(cls):
        """ 
        loads the config 
        NOTE: The study level configuration file has already been read here,
              so we ensure that we don't overwrite any options already set
        """
        c = cls.get_default_config()
        add_config(c, overwrite=False)

    def write_config(self):
        """ WRites the configuation for the code """
        raise NotImplementedError

    def pre_run_check(self, run, args=None, batchname=None):
        """ Runs through required pre-run checkes """
        raise NotImplementedError

    def get_command(self, job, cmd=None):
        """ For codes which are tightly integrated with batch systems """
        pass

    def get_tout_info(self, run):
        """ Returns information about time averaged statistics """
        raise NotImplementedError("get_tout_info not implemented in code")

    def get_tavg_info(self):
        """ Returns information about time averaged statistics """
        raise NotImplementedError("get_tavg_info not implemented in code")

    def provides_post_args(self, post_name):
        """
        Does the code provide arguments to post_programs?
        """
        assert isinstance(post_name, str), "post_name must be a string"

        function = "get_post_args_" + post_name

        if hasattr(self, function):
            return True
        else:
            return False
    
    def get_post_args(self, post_name, run, args=None):
        """
        Does the code provide arguments to post_programs?
        """
        assert isinstance(post_name, str), "post_name must be a string"

        function = "get_post_args_" + post_name

        if hasattr(self, function):
            func = getattr(self, function)
            return func(run, args=args)
        else:
            raise AttributeError("Function missing")

        
    def load_output(self, directory, args, fmt=None, run=None):
        """ Load the given output """
        

    def convert_output(self, directory, args, fmt=None, run=None):
        """ 
        Convert data output from stream to the specified format if supported 
        Functions must return a list of arguments to pass to the post processing program
        """
        
        if fmt is None:
            fmt = self.default_output_format

        if fmt not in self.supported_output_formats:
            raise NotImplementedError("Format '{}' not supported by {}".format(fmt, self.name))

        # test the directory
        if not os.path.isdir(directory):
            raise FileNotFoundError("Run directory '{}' does not exist".format(directory))

        function = "convert_" + fmt
        if hasattr(self, function):
            convert = getattr(self, function)
            return convert(directory, args, run=run)
        else:
            raise NotImplementedError("Conversion to format '{}' not implemented for {}".format(fmt, self.name))

    def edit(self, flist, cwd=None, args=None):
        """ Edit the given list of files """
        # get the editor
        return utils.edit_file(flist, cwd=cwd)
    
    def edit_input(self, directory):
        """ Edit the input files in the given directory """
        raise NotImplementedError
    def copy_input(self, src, dest, mask=None, history=False):
        """ Copy the input files from the source to the dest """
        raise NotImplementedError
    
    def pre_run_tasks(self, run, args=None):
        """ Any tasks to run once the run dir has been created """
        pass
    def terminate(self, proc):
        """ Given a subprocess proc, terminate the code """
        # we send the terminate command by default
        proc.terminate()

    @staticmethod
    def init_check(args):
        """ Pre-initialization check. Study will not initiazlie if this fails """
        pass

    def supports(self, capability):
        """ Checks whether the code supports the passed capability """
        if capability in self.capability_true:
            return True
        
        if capability in self.capability_false:
            return False

        # currently a blanket yes for backwards compatibility
        return True

    def set_code_options(self, options, args=None):
        """
        Sets the code options to use 
        """
        pass
    
    def get_code_options(self, run):
        """
        Returns a dict of options to save with a job
        """
        return {}
    
    def get_output_format(self, run):
        """
        Given a run directory, return the output format present
        """
        return self.default_output_format
    
    def load_results(self, run, mask=None):
        """ 
        If the code is responsible for handling results, do it here
        """ 
        pass

    def select_results(self, run):
        """ Select runs from a run directory """
        pass

    def job_completed(self, job):
        """ Called when a job has been completed, for post clearup """
        pass

    def job_status_change(self, job, old_status, new_status):
        """
        Called when updating the job status
        """
        pass
    
    def get_case_ignore_list(self, case):
        """
        Returns a list of directories to ignore when copying a case
        """
        return []

    def copy_case_hook(self, case, new_path):
        """
        Called after the files have been copeid by case.copy
        """
        return
    
    def move_case_hook(self, case):
        """
        Called after the files have been copeid by case.copy
        """
        return
    
    def move_run_hook(self, run):
        """
        Called after the run files have been moved by run.move
        """
        return
    
    def select_from_multiple(self, path, mask, prompt=None, include_none=False, default_option=None):
        """
        Given a mask, present a list of options to choose from

        include_none adds an option to select zero
        Returns the basename of the file selected (NOT FULL PATH)
        Returns None if there is no options
        """
        import glob

        if prompt is None:
            prompt = "Please select the file to use:"
        else:
            prompt = str(prompt)
        # check how many files we have
        src_list = []

        # glob the files in src
        files = glob.glob(os.path.join(path, mask))
        for fname in files:
            src_list.append(fname)
        
        # sort alphabetically
        src_list = sorted(src_list)
        if len(src_list) > 0 and include_none:
            # only append None as a choice if there is a choice to make
            src_list.append("None")
        
        fno = len(src_list)
        if fno > 1:
            print("")
            print(" -> Found {} files for input".format(fno))
            kwargs = {
                "title": prompt,
                "options_map_func": os.path.basename,
                "multi_select": False,
                "min_selection_count": 0
            }
            if default_option is not None:
                kwargs["default_option"] = default_option

            # add an option to copy nothing
            selection = utils.list_select(src_list, **kwargs)
            if include_none and src_list[selection] == "None":
                f = None
            else:
                f = os.path.basename(src_list[selection])
        elif fno == 1:
            f = src_list[0]
        elif fno == 0:
            f = None

        return f
    
    def get_plot_files(self, path, fmask=None):
        
        import glob
        
        if fmask is None:
            fmask = config.get(self.name, 'plot_mask', fallback="*.csv *.dat")

        mask = fmask.split()
        files = []

        # glob the files
        for m in mask:
            f = glob.glob(os.path.join(path, m))
            if f:
                files += f

        if files:
            return files
        else:
            raise FileNotFoundError("No plot files found")

    def get_log_files(self, job):
        """
        Returns a list of log files. If job is not NOne, then return for the 
        provided job (possibly old), else return the current ones
        """
        if not hasattr(self, "LOG_FILES"):
            return []
        
        log_files = self.LOG_FILES
        
        if hasattr(self, "LOG_EXT"):
            log_ext = self.LOG_EXT
        else:
            log_ext = ""

        if job is not None:
            _files = []
            for l in log_files:
                fname = os.path.join(l + "." + str(job.id) + log_ext)
                _files.append(fname)
            log_files = _files
        else:
            log_files = [l + log_ext for l in log_files]

        return log_files
    
    def get_monitor_files(self, path, fmask=None):
        """ Return a list of paths of files to monitor """
        import glob
        # get the mask
        if fmask is None:
            fmask = config.get(self.name, 'monitor_mask')

        # glob the files (handle multiple masks)
        files = []
        for m in fmask.split():
            files += glob.glob(os.path.join(path, m))
        
        if files:
            return sorted(files)
        else:
            raise FileNotFoundError("No monitor files found")
        
    def file_sequence_reader(self, flist):
        """ Returns a file sequence reader """
        return utils.FileSequenceReader(flist)
    
    def get_plot_reader(self, path, **kwargs):
        """ Returns a plot reader """
        raise NotImplementedError
    
    def get_src(self, run, **kwargs):
        """ Returns a plot reader """
        raise NotImplementedError

    def copy_src(self, dest):
        """
        Copies the src files necessary to compile the code
        """
        pass