"""
    cft: A management tool for Computational Fluid Dynamics
    
    codes/fluent.py: Interface to the FLUENT code
"""
import os
import signal
import sys
import shutil
import logging
import re
import glob
import cfmt.utils as utils
from cfmt.config import configtool as config
from cfmt.codes import base
from cfmt import exceptions

class FluentCode(base.Code):
    """ Class to implement an interface to the class code """
    name = "fluent"
    maintainer = "Dean Wilson"
    description = "Commercial general purpose computational fluid dynamics (CFD) software from ANSYS"
    path = {}
    requires_build = False
    url = "https://www.ansys.com/en-gb/products/fluids/ansys-fluent"

    # batch systems the code has tight integration with
    TIGHT_INTEGRATION = ["sge", "slurm"]

    DEFAULT_CONFIG = {
        name: {
            'binary': 'fluent',
            'input_mask': 'init.cas init.dat input.jou case.cas.gz init.dat.gz',
            'mesh_mask': '*.msh',
            'monitor_mask': 'mon-*.dat',
            'results_mask': '*.dat *.dat.gz',
            'tout_mask': 'tout*',
            'post_utility': 'fluent',
            'precision': 'dp'
        }
    }

    supported_output_formats = [
        'ensight',
        'fluent',
    ]

    default_output_format = 'fluent'
    default_output_utility = 'fluent'
    
    capability_true = ['edit_input']
    capability_false = []
    
    # files that can be edited 
    edit_mask = '*.cas *.cas.gz *.jou'
        

    def __init__(self, study=None, db_record=None):
        """ Constructor """
        super().__init__(study, db_record)

        # 2 or 3 dimensions
        self.solver_dim = None

        # dp
        self.solver_precision = None

        # the version string (2d,2ddp etc.)
        self.solver_version = None

        self.needs_mesh = False

        # case/data/journal files
        self.fcas = None
        self.fdat = None
        self.fjou = None
        self.fmsh = None

        # gui
        self.gui = False

        # extra solver arguments
        self.solver_arguments = None

        # default parsed case information
        self.pcase = {
            'dimensions': 0
        }
        self.case_parsed = None

    def set_config(self):
        """ Writes the default config to the config file """
    
    @staticmethod
    def init_check(args):
        """ Pre-initialization check """
        # -> check if fluent can be found
        if shutil.which(config.get(FluentCode.name, 'binary')) is None:
            raise exceptions.CheckFailed("FLUENT cannot be found in user PATH")


    def create_case(self, path):
        """ Runs when creating a new case """
        from distutils.dir_util import copy_tree
        # get the sudy directory
        input_path = os.path.join(self.study.path, 'input', self.name)
        copy_tree(input_path, path)
    
    def pre_run_tasks(self, run, args=None):
        """ Task to carry out pre-run """
        import fnmatch
        # if we're running parallel, and are using sge, set the pe to fluent.pe
        if run.batch.system_name == "sge" and run.batch.mode == "parallel":
            run.batch.pe = "fluent-smp.pe"
        
        # default is without a gui
        if args and args.with_gui:
            self.gui = True
        
        ## determine what fluent specific arguments we've been passed
        # get the relevant masks
        input_mask = config.get(self.name, 'input_mask')
        input_mask = input_mask.split(" ")

        mesh_mask = config.get(self.name, 'mesh_mask')
        mesh_mask = mesh_mask.split(" ")

        # determine if we've been passed a case file
        if args and args.fluent_case is not None:
            cas = args.fluent_case.pop()
            # note this should already exist, and be parsed, since it's been checked by copy_input
        else:
            # if we're keeping input (from another run) then don't use the input mask
            if not args.keep_input:
                _cas = fnmatch.filter(input_mask, "*.cas*")
            else:
                _cas = glob.glob(os.path.join(run.get_run_dir(), "*.cas*"))
                _cas = [os.path.basename(x) for x in _cas]
            cas = None
            # we could possibly have more than one here.. check
            for c in _cas:
                fcas = os.path.join(run.get_run_dir(),c)
                if os.path.isfile(fcas):
                    cas = fcas
                    break
        self.fcas = cas

        # determine if we've been passed a data file
        if args and args.fluent_data is not None:
            dat = args.fluid_data.pop()
            # note this should already exist since it's been checked by copy_input
        else:
            _dat = fnmatch.filter(input_mask, "*.dat*")
            # we could possibly have more than one here.. check
            dat = None
            for c in _dat:
                fdat = os.path.join(run.get_run_dir(),c)
                if os.path.isfile(fdat):
                    dat = fdat
                    break
        self.fdat = dat
            
        # determine if we've been passed a journal file
        if args and args.fluent_input is not None:
            jou = args.fluent_input.pop()
            # note this should already exist since it's been checked by copy_input
        else:
            _jou = fnmatch.filter(input_mask, "*.jou*")
            jou = None
            # we could possibly have more than one here.. check
            for c in _jou:
                fjou = os.path.join(run.get_run_dir(),c)
                if os.path.isfile(fjou):
                    jou = fjou
                    break
        self.fjou = jou

        # determine if we've been passed a journal file
        if args and args.fluent_mesh is not None:
            self.fmsh = args.fluent_mesh.pop()
        
        # get the version, check arguments first, else we try and guess
        if args and args.fluent_version is not None:
            self.solver_version = args.fluent_version.pop()
        else:
            if self.fcas is not None:
                self.parse_case(self.fcas)
                self.solver_version = str(self.solver_dim) + 'd'
                if self.solver_precision == 'dp':
                    self.solver_version = self.solver_version + 'dp'
            else:
                self.console.error("FLUENT Version required")
                self.logger.error("FLUENT Version required")
                sys.exit(1)
        
        # check for additional options
        if args and args.fluent_options:
            self.solver_arguments = args.fluent_options

    def pre_run_check(self, run, args=None, batchname=None):
        """ Checks to carry out pre-run generation """
        return


    def set_bin_path(self, dest):
        """ Sets the binary path based on a provided run directory """
        # should already be set, since the binary is system installe
        try:
            self.binary_path = shutil.which(config.get(self.name, 'binary'))
        except:
            raise exceptions.CheckFailed("Solver binary not set")

    def get_bin_path(self, wd=None, batch_system=None):
        """ 
        Returns the path to the binary 
        """
        if self.binary_path is not None:
            return self.binary_path

    def copy_mesh(self, src, dst, mask=None, args=None):
        """ 
        Copies the mesh 
        """
        # this is handled as part of copy_input.. since
        # whether we need a mesh depends on the existance
        # of a case file
        if mask is None:
            mask = config.get(self.name, 'mesh_mask')

        if args and args.fluent_mesh is not None:
        # the path will be relative to the cwd
            msh = args.fluent_mesh[0]
            fmsh = os.path.join(os.getcwd(), msh)
            if not os.path.isfile(fmsh):
                raise FileNotFoundError("Mesh file not found '{}".format(fmsh))
            else:
                fdst = os.path.join(dst, os.path.basename(fmsh))
                shutil.copyfile(fmsh, fdst)
                return
        elif self.needs_mesh:
            self.copy_mask(mask, src, dst)


    def copy_input(self, src, dest, args=None, mask=None, history=False):
        """
        Copies input files to dest
        """
        # get the list of files to copy from config
        if mask is None:
            mask = config.get(self.name, 'input_mask')

        mask = mask.split(" ")
        # check for mask changes
        # -> journal file
        if args and args.fluent_input:
            # check it's a file 
            jou = args.fluent_input[0]
            fjou = os.path.join(src, jou)
            if not os.path.isfile(fjou):
                raise FileNotFoundError(fjou)
            
            mask = self.mask_replace(mask, ".*jou", jou)

        if not history: 
            # -> case file
            if args and args.fluent_case:
                # check it's a file
                cas = args.fluent_case[0]
                fcas = os.path.join(src, cas)
                if not os.path.isfile(fcas):
                    raise FileNotFoundError(fcas)

                mask = self.mask_replace(mask, ".*cas", cas)
        
            # -> data file
            if args and args.fluent_data:
                dat = args.fluent_data[0]
                fdat = os.path.join(src, dat)
                if not os.path.isfile(fdat):
                    raise FileNotFoundError(fdat)

                mask = self.mask_replace(mask, ".*dat", dat)
        else:
            # remove everything apart from jou files
            r = re.compile('.*jou')
            mask = [x for x in mask if r.match(x)]

        mask = " ".join(mask)
        
        self.copy_mask(mask, src, dest)

        # if we only have a .jou file present (no case)
        # then we copy the mesh as well.
        fcas = glob.glob(os.path.join(src,"*.cas"))
        fcas += glob.glob(os.path.join(src,"*.cas.gz"))
        if not fcas:
            self.needs_mesh = True

        
    def run(self, cwd, args=None, case=None, data=None):
        """
        Runs the fluent launcher with the arguments
        """
        # check here to make sure fluent environment is loaded
        try:
            self.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)
        
        cmd = [self.binary]
        version = None
        
        if case is not None and self.parse_case is not True:
            self.parse_case(case)

        if self.solver_dim:
            version = str(self.solver_dim) + 'd'
            if self.solver_precision == 'dp':
                version = version + 'dp'
            
        if version:
            cmd = cmd + [version]

        # for reading case/data files we pass a dummy journal
        import tempfile
        tf = None
        
        if case and data:
            with tempfile.NamedTemporaryFile(mode="r+", suffix=".jou", delete=False) as tf:
                tf.write("rc " + str(case))
                tf.write("\n")
                tf.write("rd " + str(data))
                tf.flush()
            # 
        elif case:
            with tempfile.NamedTemporaryFile(mode="r+", suffix=".jou", delete=False) as tf:
                tf.write("rc " + str(case))
                tf.flush()

        if tf:
            cmd = cmd + ['-i', tf.name]

        utils.run(cmd, shell=True, cwd=cwd)

        if tf:
            os.remove(tf.name)
    
    def edit_input(self, directory, mask=None, prompt=None):
        """ Edit the input files in the run directory """
        # check the destination dir exists
        if not os.path.isdir(directory):
            raise NotADirectoryError("Cannot find run directory")
        
        # cannot use input mask since that's specific
        if mask is None:
            mask = self.edit_mask

        mask_list = self.edit_mask.split(" ")
        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = sorted(glob.glob(os.path.join(directory, _mask)))
            for fname in files:
                src_list.append(fname)

        fno = len(src_list)
        print("")
        if fno > 0:
            print(" -> Found {} files to edit".format(fno))

            # add an option to edit nothing
            src_list.append("Exit")
            prompt="Please select a file to edit or select exit to edit none: "

            while True:
                selection = utils.list_select(src_list, title=prompt, options_map_func=os.path.basename)
                if selection == fno:
                    break

                self.edit(src_list[selection])
        else:
            # no files, prompt to run fluent launcher
            prompt="No editable files found. Do you want to run the Fluent launcher?"
            if utils.confirm(prompt, default=True):
                self.run(cwd=directory)


    
    def results(self, run):
        """
        Given a run, load fluent to view results
        """
        # prompt for cas file
        cwd = run.get_run_dir()

        # prompt for cas file
        mask = ["*.cas", "*.cas.gz"]
        fcasl = []
        for _mask in mask:
            files = glob.glob(os.path.join(cwd, _mask))
            for fname in files:
                fcasl.append(fname)

        # sort alphabetically
        fcasl=sorted(fcasl)

        if len(fcasl) == 0:
            self.console.error("No FLUENT case files found")
            sys.exit(1)
        elif len(fcasl) == 1:
            fcas = fcasl[0]
            print("  --> Found case file: '{}'".format(os.path.basename(fcas)))
        else:
            prompt = "Please select a case file: "
            icas = utils.list_select(fcasl, title=prompt, options_map_func=os.path.basename)
            fcas = fcasl[icas]
            print("  --> Selected case file: '{}'".format(os.path.basename(fcas)))

        # search/prompt for dat file
        mask = ["*.dat", ".dat.gz"]
        fdatl = []
        for _mask in mask:
            files = glob.glob(os.path.join(cwd, _mask))
            for fname in files:
                fdatl.append(fname)

        if len(fdatl) == 0:
            prompt = "  --> No data files found, continue?"
            cont = utils.confirm(prompt, default=True)
            if not cont:
                sys.exit(1)
            fdat = None
        elif len(fdatl) == 1:
            fdat = fdatl[0]
            print("  --> Found data file: '{}'".format(os.path.basename(fdat)))
        else:
            prompt = "Please select a data file: "
            idat = utils.list_select(fdatl, title=prompt, options_map_func=os.path.basename)
            fdat = fdatl[idat]
            print("  --> Selected data file: '{}'".format(os.path.basename(fdat)))

        # load
        self.run(cwd, case=fcas, data=fdat)
    
    def edit(self, flist, cwd = None, args = None):
        """ 
        Edits files
        Since we need to load fluent
        """
        if cwd is None:
            # use the current directory
            cwd = os.getcwd()

        # see what we've been passed
        # -> we need both a case and a data file
        if flist:
            if flist.endswith('.dat') or flist.endswith('.dat.gz'):
                utils.print_exception_info("Please load the .cas file first")
                sys.exit(1)
            elif flist.endswith('.cas') or flist.endswith('.cas.gz'):
                fcas = os.path.join(cwd,flist)
                # we have a case file - > load fluent
                # attempt to detect the dimensions
                # parse the case file
                self.parse_case(fcas)
                self.run(cwd=cwd, case=fcas)
            else:
                # refer to parent to load in vim
                super().edit(flist, cwd)
        else:
            # we have nothing - prompt
            self.edit_input(cwd)
            # self.run(cwd=cwd)

    def get_command(self, run, batch, cmd=None, cmdline_args=None):
        """
        Since FLUENT has tight integration we provide the command here
        returns a command list

        We have to tailor this to the batch system as well
        """
        
        if cmd is not None:
            command = cmd
        else:
            command = []

        # due to a bug in sge handling functions within env variables,
        # we need to remove the "-V" if it exists
        if "-V" in command:
            command.remove("-V")
            # pass the fluent
        
        # append the bindary name
        command.append(self.binary_path)
        # version
        assert self.solver_version is not None, "Solver version not set"
        command.append(self.solver_version)

        # gui
        if not self.gui:
            command.append("-g")
        
        # handle parallel options
        if batch.mode == "parallel":
            # we run fluent in parallel mode
            command.append("-t" + str(batch.np))

        # do not display mesh after reading
        command.append("-nm")

        
        # we always append the jou if it's present
        if self.fjou is not None:
            command.append("-i")
            command.append(str(os.path.basename(self.fjou)))
        else:
            # we need to have a journal file for a batch system,
            # since we cannot run interactive jobs (qrsh anyone?)
            if batch.system_name != "none":
                self.console.error("Journal file required for batch jobs")
                self.logger.error("Journal file required for batch jobs")
            else:
                # we can attach the pre-load the case/data file if we have one
                import tempfile
                tf = None
                if self.fcas and self.fdat:
                    with tempfile.NamedTemporaryFile(mode="r+", suffix=".jou", delete=False) as tf:
                        tf.write("rc " + str(self.fcas))
                        tf.write("\n")
                        tf.write("rd " + str(self.fdat))
                        tf.flush()
                    # 
                elif self.fcas:
                    with tempfile.NamedTemporaryFile(mode="r+", suffix=".jou", delete=False) as tf:
                        tf.write("rc " + str(self.fcas))
                        tf.flush()

                if tf:
                    command.append('-i')
                    command.append(tf.name)

        # tight integration for batch system
        #if batch.system_name == "sge":
        #    command.append('-sge')
        #    if batch.mode == "parallel":
        #        command.append('-sgepe')
        #        command.append("fluent-smp.pe")
        #        command.append(str(batch.slots))
        #    
        #    command.append("-sgeq")
        #    if batch.queue is not None:
        #        command.append(batch.queue)
        #    elif batch.mode == "parallel":
        #        command.append(batch.parallel_queue)
        #    else:
        #        command.append(batch.serial_queue)

        if self.solver_arguments is not None:
            assert isinstance(self.solver_arguments, list)
            command += self.solver_arguments

        return command
    
    def get_tout_filter(self, run, up=False):
        """
        Given a run, check the staging directory for timeoutput
        Returns a filter list which can be used in rsync
        """
        if up:
            src_dir = run.abspath
        else:
            if not isinstance(run.staging_dir,str):
                return []
            src_dir = run.staging_dir

        # get the staging directory
     
        if not os.path.isdir(src_dir):
            return []
        
        rfilter = []
        # search in the staging directory for tout folders
        mask = config.get(self.name, "tout_mask")
        files = glob.glob(os.path.join(src_dir, mask))

        # for each result we want to include the dir, but exclude everything else
        # but exclude everything else
        for fpath in files:
            # get relative path
            path = os.path.relpath(fpath, src_dir)
            rfilter.append(['+', path])
            rfilter.append(['-', os.path.join(path, "*")])

        return rfilter
    
    def copy_results(self, src, dest, 
        keep_files=None, 
        remove_files=None, 
        keep_residuals=False, 
        keep_monitors=False, 
        keep_tout=False,
        keep_input=False,
        keep_dir=None, 
        continue_tout=False,
        init_to_scratch=False):
        """
        Copies results files to dest

        --keep-residuals - does nothing
        --keep-monitors - will use the monitor mask
        """
        # prompt the user to copy the required files
        mask = config.get(self.name, 'results_mask')
        mask_list = mask.split(" ")
        mask_list.extend(["*.cas", "*.cas.gz", "*.jou"])

        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = glob.glob(os.path.join(src, _mask))
            for fname in files:
                src_list.append(fname)
        
        # sort alphabetically
        src_list = sorted(src_list)
        
        fno = len(src_list)
        print("")
        if fno > 0:
            print(" -> Found {} files to copy".format(fno))

            # add an option to copy nothing
            prompt="Please select the files to copy (press SPACE to mark, ENTER to continue):"

            selection = utils.list_select(src_list, title=prompt, options_map_func=os.path.basename, multi_select=True, min_selection_count=0)

            for f,i in selection:
                shutil.copy2(f, dest)
            
        # search in the directory for tout folders
        if continue_tout:
            mask = config.get(self.name, "tout_mask")
            files = glob.glob(os.path.join(src, mask))

            # for each, we create the folder in the destination and copy the timeoutput.dat if
            # it exists
            for fpath in files:
                dirname = os.path.basename(fpath)
                dirpath = os.path.join(dest, dirname)
                os.mkdir(dirpath)

        if keep_monitors:
            monitor_files = self.get_monitor_files(src)
            _files = []
            if monitor_files:
                for fname in monitor_files:
                    shutil.copy2(fname, dest)
                    
    def parse_case(self, fcas):
        """
        Takes a fluent case file and parses some common information
        Makes use of regular expressions
        """
        self.case_parsed = False
        # check the file exists and is a case file
        if not os.path.isfile(fcas):
            raise FileNotFoundError("File not Found: '%s'",fcas)

        if not fcas.endswith('.cas') or fcas.endswith('.cas.gz'):
            raise exceptions.CaseDoesNotExist("Not a case file")



        # compile the re we require
        # -> dimensions
        re_dimline = re.compile(b"^\(2\s(\d)\)")
        # -> machine config (still not sure on this line)
        re_mconfig = re.compile(b"^\(4\s\(((\d+\s?)+)\)\)")

        # open the file
        with open(fcas, 'rb') as f:
            mconfig = None
            while True:
                line = f.readline()
                if len(line) == 0:
                    # finished reading file
                    break

                # read machine config
                if not mconfig:
                    test = re.search(re_mconfig, line)
                    if test:
                        # convert to a list of ints
                        mconfig = test.group(1).split(b" ")
                        mconfig = [int(x) for x in mconfig]
                        if mconfig[8] == 4:
                            self.pcase['precision'] = 'sp'
                            self.solver_precision = self.pcase['precision']
                        elif mconfig[8] == 8:
                            self.pcase['precision'] = 'dp'
                            self.solver_precision = self.pcase['precision']
                        else:
                            self.console.error("Cannot determine precision of case")
                        # precision i think is number 
                # read dimensions
                if self.pcase['dimensions'] == 0:
                    test = re.search(re_dimline, line)
                    if test:
                        self.pcase['dimensions'] = int(test.group(1))
                        self.solver_dim = self.pcase['dimensions']
                        continue
        
        self.case_parsed = True
    
    
    def residual_reader(self, path, **kwargs):
        """ Returns a residual reader """
        # if the processed residual file exists, we choose that
        fpath = os.path.join(path, self.get_residual_file())
        if os.path.isfile(fpath):
            return ResidualReader(fpath, openmode='r', **kwargs)
        
        # search/prompt for dat file
        mask = ["*.dat", ".dat.gz"]
        fdatl = []
        for _mask in mask:
            files = glob.glob(os.path.join(path, _mask))
            for fname in files:
                fdatl.append(fname)
        if len(fdatl) == 0:
            self.console.info("No data files found")
        else:
            prompt = "Please select a data file: "
            idat = utils.list_select(fdatl, title=prompt, options_map_func=os.path.basename)
            fdat = fdatl[idat]
            fpath = os.path.join(path, fdat)
        return ResidualReaderDat(fpath, openmode='rb', **kwargs)
    
    def get_massflow_file(self, path):
        """
        DEPRECATED
        Returns the pass to the massflow rate file
        """
        flist = self.get_monitor_files(path, fmask="mon-massflow.dat")
        return flist.pop()
    
    def monitor_reader(self, path, **kwargs):
        """ Returns a monitor reader """
        return MonitorReader(path, **kwargs)
    
    def terminate(self, proc):
        """ We pass a SIGINT to fluent """
        print("Terminating fluent")
        os.kill(proc.pid, signal.SIGINT)

class ResidualReaderDat(utils.LiveDataReader):
    """ Reads residuals from a Dat file """

    # mapping for equations see xfile.h
    equation_id = {
        1: 'continuity',
        2: 'momentum',
        3: 'temperature',
        4: 'enthalpy',
        5: 'tke',
        6: 'ted',
        14: 'momentum-x',
        15: 'momentum-y',
        16: 'momentum-z',
        50: 'vof'
    }
    
    # we need to locate the section that contains this section
    re_residuals = re.compile(b"^\(3302\s\((\d+)\s(\d+)\s(\d+)\s(\d+)\)")
    column_index = []

    def get_headers(self):
        """
        Sets the headers
        (302 (n residual-section-id size)
        (
            interation_number unscaled_residual scaling_factor
        .
        rn
        ))

        where
        n -- the number of residuals
        size - the length of the variable vector
        residual-section-id -- an integer (decimal indicating the equation)
        """
        import struct

        headers = ['IT']
        self.xaxis_assign = 0
        self.column_index.append(0)
        j=1

        while True:
            line = self.fd.readline()
            if len(line) == 0:
                # finished reading the file
                break
            test = re.search(self.re_residuals, line)
            if test:
                equation_id = int(test.group(2))
                if equation_id in self.equation_id:
                    headers.append(self.equation_id[equation_id])
                    self.yaxis_assign.append(j)
                else:
                    headers.append(str(equation_id))
                self.column_index.append(j)
                j += 1
                size = int(test.group(1))
                struct_fmt = 'ddd'
                struct_len = struct.calcsize(struct_fmt)
                # read the first parenthasis
                first = self.fd.read(struct.calcsize('c'))

                for i in range(0,size):
                    data = self.fd.read(struct_len)
                    s = struct.unpack_from(struct_fmt, data)

                # read the closing parenthesis
                last = self.fd.read(struct.calcsize('c'))
        
        # rewind to the start of the fd
        self.fd.seek(0)

        return headers

    def run(self):
        """
        The main body of the thread:
        """
        import struct
        j = 1
        while not self.terminate.is_set():
            line = self.fd.readline()
            if len(line) == 0:
                # finished reading the file
                break
            test = re.search(self.re_residuals, line)
            if test:
                equation_id = int(test.group(2))
                size = int(test.group(1))
                struct_fmt = 'ddd'
                struct_len = struct.calcsize(struct_fmt)
                # read the first parenthasis
                first = self.fd.read(struct.calcsize('c'))

                for i in range(0,size):
                    data = self.fd.read(struct_len)
                    s = struct.unpack_from(struct_fmt, data)
                    if j == 1:
                        self.databuffers[0].append(float(s[0]))
                    try:
                        self.databuffers[j].append(float(s[1])/float(s[2]))
                    except ZeroDivisionError:
                        self.databuffers[j].append(float(s[1]))


                # read the closing parenthesis
                last = self.fd.read(struct.calcsize('c'))
                j += 1
        self.ready.set()
        self.terminate.set()

class ResidualReader(utils.LiveDataReader):
    """ Read residuals """
    column_index = []
    def get_headers(self):
        """ Sets the headers """
        
        header = self.fd.readline().split()
        headers = [] 
        # x_axis assigment
        x_axis = ["it"]
        i = 0
        if header[0].lower() == "it":
            for h in header:
                headers.append(h)
                self.column_index.append(i)
                
                if h.lower() in x_axis:
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                else:
                    self.yaxis_assign.append(i)

                i += 1
        else:
            # discard and rewind
            self.fd.seek(0)
            headers = None
        
        return headers

    def process(self, line):
        """ process the data """
        data = line.split()
        # remove the Max column

        # canot handle zero values for sone reason
        try: 
            data = [float(data[i]) if float(data[i]) != 0.0 else -1.0 for i in self.column_index]
        except ValueError:
            data = None
        return data
    
class MonitorReader(utils.LiveDataReader):
    """ Read fluent monitor files """
    column_index = []
    def get_headers(self):
        """ Monitors are of the format:
        First line is the name of the report file
        Second line is a comment
        Third line contains headers enclosed in parenthesis
        """
        header = None
        last_pos = 0


        # title
        line = self.fd.readline()
        
        # comment
        line = self.fd.readline()

        # headers 
        line = self.fd.readline()
        line = line.rstrip()

        if line.startswith("(") and line.endswith(")"):
            # remove the parenthesis
            line = line[1:-1]
            # split according to matching quotes
            line = line.split("\"")

            # remove the extra gaps
            header = [x.strip() for x in line if x.strip()]
        
        headers = []
        
        # valid x_axis headers
        x_axis = ["time step", "flow-time", "it"]
        # check we have a valid header line
        i = 0
        if header[0].lower() in x_axis:
            # assign headers
            for h in header:
                if h.lower() in x_axis:
                    headers.append(h)
                    self.column_index.append(i)
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                    i += 1
                    continue
                elif h.lower() == "(s)":
                    # not a header, skip
                    continue
                else:
                    # assign to y_axis
                    headers.append(h)
                    self.column_index.append(i)
                    self.yaxis_assign.append(i)
                    i += 1
                    continue
        else:
            # rewind to start of non-header line
            self.fd.seek(last_pos)
            headers = None

        return headers

    def process(self, line):
        """ process the data """
        data = line.split()
        try:
            data = [float(data[i]) for i in self.column_index]
        except ValueError:
            data = [None for i in self.column_index]
        
        return data

