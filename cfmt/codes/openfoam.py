"""
    cft: A management tool for Computational Fluid Dynamics
    
    codes/openfoam.py: Interface to the OpenFOAM code
"""
import os
import sys
import glob
import logging
import time
from cfmt.config import configtool as config
from cfmt.codes import base
from cfmt import exceptions
from cfmt import utils



class OpenFOAMCode(base.Code):
    """ Class to implement an interface to the class code """
    name = "openfoam"
    maintainer = "Dean Wilson"
    description = "OpenFOAM is a free, open source computational fluid dynamics (CFD) software package released by the OpenFOAM Foundation. It has a large user base across most areas of engineering and science, from both commercial and academic organisations. OpenFOAM has an extensive range of features to solve anything from complex fluid flows involving chemical reactions, turbulence and heat transfer, to solid dynamics and electromagnetics."
    path = {}
    requires_build = False
    url = "https://github.com/OpenFOAM"

    DEFAULT_CONFIG = {
        name: {
            'binary': 'foamExec',
            'input_mask': 'constant system 0',
            'mesh_mask': 'polyMesh',
            'solver': 'icoFoam',
            'post_utility': 'paraview',
            #'env_modules': None
        }
    }
    residual_fname = "residual.dat"

    REQUIRED_ENV = [
        'FOAM_INST_DIR',
        'WM_PROJECT',
        'WM_PROJECT_DIR',
        'WM_PROJECT_VERSION',
        'FOAM_TUTORIALS',
        'FOAM_SOLVERS',
        'FOAM_APPBIN',
        'FOAM_ETC'
    ]

    supported_output_formats = [
        'ensight',
        'vtk',
        'openfoam'
    ]
    utilities = [
        'foamToEnsight',
        'foamToVTK',
        'paraFoam', # via paraview
        'decomposePar'
    ]

    capability_true = []
    capability_false = ['edit_input']

    default_output_format = 'openfoam'
    default_output_utility = 'paraview'

    def __init__(self, study=None, db_record=None):
        """ Constructor """
        super().__init__(study, db_record)
        
        self.has_binary = False
        self.binary_path = None
        self.env = {}
        self.solver = None
        self.fileHandler = None
        
        # whether the binary is local
        self.local_bin = False

        # whether the code has been built
        self.is_built = False
        self.bin_dir = None

        # update environmental variables
        # check here to make sure openfoam environment is loaded
        try:
            OpenFOAMCode.init_check(None)
        except exceptions.CheckFailed as err:
            self.console.error(err)
            sys.exit(1)

        self.set_environment()
        
        # set utility paths for convinience
        self.set_utilities()

        # set an empty controlDict
        self.controlDict = {}
        self.build_src = None
        self.makefile = None

    
    @staticmethod
    def init_check(args):
        """ Pre-initialization check """
        # -> check if the openfoam environment has been set
        for env in OpenFOAMCode.REQUIRED_ENV:
            try:
                os.environ[env]
            except KeyError:
                raise exceptions.CheckFailed("Missing OpenFOAM environmental variable '${}'".format(env))
        if args:
            if args.list_examples:
                print(":: -> Available Openfoam examples can be found in '{}'".format(os.environ['FOAM_TUTORIALS']))
                sys.exit(0)

    

    def set_environment(self):
        """ Sets the OpenFOAM environmental variables """
        for env in OpenFOAMCode.REQUIRED_ENV:
            self.env[env] = os.environ[env]

        # these should exist 
        self._name = self.env['WM_PROJECT']
        self.version = self.env['WM_PROJECT_VERSION']
    
    def set_utilities(self):
        """ Sets the paths to OpenFOAM utilities """ 
        self.utilities = {
            'foamToEnsight': self.env['FOAM_APPBIN'] + '/foamToEnsight',
            'foamToVTK': self.env['FOAM_APPBIN'] + '/foamToVTK',
            'paraFoam': self.env['WM_PROJECT_DIR'] + '/bin/paraFoam',
            'decomposePar': self.env['FOAM_APPBIN'] + '/decomposePar',
            'foamDictionary': self.env['FOAM_APPBIN'] + '/foamDictionary'
        } 
    
    def set_config(self):
        """ Writes the default config to the config file """
        pass

    def init(self, db=None, args=None):
        """ Initilizes a new study """
        import shutil

        # call parent
        super().init(db, args)
        
        # copy existing examples to src directory if provided
        for i, example in enumerate(args.openfoam_example):
            # check it exists
            path = os.path.join(self.env['FOAM_TUTORIALS'], example)
            print("  -> Copying example '{}' ... ".format(example), end='\r')
            if os.path.isdir(path):
                try:
                    shutil.copytree(path, os.path.join(self.study.path, 'src', example))
                except shutil.Error as err:
                    self.logger.error(err)
                    print("  -> Copying example '{}' ... FAILED".format(example), end='\n')
                else:
                    print("  -> Copying example '{}' ... OK".format(example), end='\n')
            else:
                print("  -> Copying example '{}' ... NOT FOUND".format(example), end='\n')

    def init_db(self,db):
        """ Adds an entry to the codes db """
        self.db_record = db.new_code(
            name=self.name,
            identifier=self.name,
            url=self.url,
            description=self.description,
            maintainer=self.maintainer,
            path=self.path
        )
        
    def create_case(self, path):
        """ Runs when creating a new case """
        # get the study directory
        from distutils.dir_util import copy_tree
        
        input_path = os.path.join(self.study.path, 'input', self.name)
        copy_tree(input_path, path)
    
    def pre_run_check(self, run, args=None, batchname=None):
        """ 
        Pre run checks 
        """
        if args is None:
            return

        # parse controlDict
        self.parse_controlDict()

        if self.solver is not None:
            solver = self.solver
        elif args.openfoam_solver is not None:
            solver = args.openfoam_solver.pop()
        else:
            # attempt to get from controlDict
            solver = self.controlDict["application"]
            if solver is None:
                # get from config, though it's highly likely to be wrong
                solver = config.get(self.name, 'solver')

        # check the solver exists
        spath = os.path.join(self.env['FOAM_APPBIN'], solver)
        if os.path.isfile(spath):
            if args.parallel:
                self.binary_path = [spath, '-parallel']
            else:
                self.binary_path = spath
            self.binary = solver
            self.has_binary = True
        else:
            self.logger.error("Cannot find solver binary '%s' in path '%s'", solver, self.env['FOAM_APPBIN'])
            raise exceptions.CheckFailed("Cannot find solver binary '{}'".format(solver))
        
        # if we've requested an edit, warn that we have to do this before
        if args.edit_input:
            self.console.warning("Cannot edit "  + self._name + " runs before submitting. Please change to the run directory to do this.")
            sys.exit(1)

        # if we're running parallel, check the decomposeParDict exists
        if args.parallel:
            path = os.path.join(self.case.abspath, 'system', 'decomposeParDict')
            if not os.path.isfile(path):
                self.console.error("Decomposition file '%s' required for parallel runs", path)
                sys.exit(1)
            
            # assign the correct number of slots
            slots = int(self.parseDict("numberOfSubdomains", path))
            _slots = self.case.run_args["slots"]
            if _slots != slots:
                self.console.warning("decomposeParDict specifies '%i' slots but '%i' were requested. Changing to '%i' slots...", int(slots), int(_slots), int(slots))
                self.case.run_args["slots"] = slots
            
            # determine whether we're running collated or uncollated
            masterControlDict = self.env['FOAM_ETC'] + "/controlDict"
            self.fileHandler = self.parseDict("OptimisationSwitches.fileHandler", masterControlDict)
            if self.fileHandler is None:
                self.console.error("Cannot determine fileHandler settings")
                sys.exit(1)
            
            input_mask = config.get(self.name, "input_mask")
            input_mask = input_mask.split(" ")
            if self.fileHandler == "collated":
                # input mask needs "processors$N" where $N is the number of domain decompositions
                input_mask.append("processors" + str(slots))
            elif self.fileHandler == "uncollated":
                # input mask needs $N processor$N folders
                input_mask += ["processor" + str(n) for n in range(slots)]
            input_mask
            input_mask = " ".join(input_mask)
            config.set(self.name, "input_mask", input_mask)

    def copy_bin(self, dest):
        """
        Copies the run time binaries to dest
        Return the full path
        """
        pass
    def set_bin_path(self, dest):
        """ Sets the binary path based on a provided run directory """
        # should already be set, since the binary is system installed
        if self.binary_path is None:
            raise exceptions.CheckFailed("Solver binary not set")
    
    def get_bin_path(self, wd=None, batch_system=None):
        """ Returns the path to the binary """
        if self.binary_path is not None:
            return self.binary_path
    
    def copy_input(self, src, dest, mask=None, history=False):
        """
        Copies input files to dest
        Initial field files depend on the values in controlDict
        """
        import re
        # get the list of files to copy from config
        if mask is None:
            mask = config.get(self.name, 'input_mask')

        if not history and not self.case.run_args["is_parallel"]:
            # for serial runs we need to determine which folder to copy for initial conditions

            if self.controlDict['startFrom'] == "startTime":
                # time dir should be given
                time = self.controlDict['startTime']
            elif self.controlDict['startFrom'] == "latestTime":
                # get the largest timedir
                # top level list
                dir_list = os.listdir(src)
                r = re.compile(r'[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?')
                # filter out non numeric
                dir_list = list(filter(r.match, dir_list))
                # sort by float value
                dir_list = sorted(dir_list, key=float)
                # get biggest on 
                time = dir_list.pop()
                # convert to floats
            else:
                time = None
            
            
            if time:
                time_dir = os.path.join(src, time)
                if os.path.isdir(time_dir):
                    mask_list = mask.split(" ")
                    # remove 0 and add the time value to mask
                    if "0" in mask_list:
                        del mask_list[mask_list.index("0")]
                    mask_list.append(time)

            mask = " ".join(mask_list)


        self.copy_mask(mask, src, dest)
    
    def edit_input(self, directory):
        """ Edit the input files in the run directory """
        # check the destination dir exists
        if not os.path.isdir(directory):
            raise NotADirectoryError("Cannot find run directory")
        
        # get the list of files to edit from config
        mask = config.get(self.name, 'input_mask')
        mask_list = mask.split(" ")

        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = glob.glob(os.path.join(directory, _mask))
            for fname in files:
                src_list.append(fname)

        utils.edit_file(src_list)

    def get_tout_filter(self, run, up=False):
        """
        Given a run, check the staging directory for timeoutput
        Returns a filter list which can be used in rsync
        """
        if up:
            src_dir = run.abspath
        else:
            if not run.in_staging:
                return []
            src_dir = run.staging_dir

        # get the staging directory
        
        if not os.path.isdir(src_dir):
            return []
        
        rfilter = []
        # search in the staging directory for tout folders
        mask = config.get(self.name, "tout_mask")
        files = glob.glob(os.path.join(src_dir, mask))

        #print(src_dir)
        #print(files)
        # for each result we want to include the dir and the timeoutput.dat file
        # but exclude everything else
        for fpath in files:
            # get relative path
            path = os.path.relpath(fpath, src_dir)
            rfilter.append(['+', path])
            rfilter.append(['+', os.path.join(path, "timeoutput.dat")])
            rfilter.append(['-', os.path.join(path, "*.dat")])
            rfilter.append(['-', os.path.join(path, "*.ens")])
            rfilter.append(['-', os.path.join(path, "*.case")])

        return rfilter
    
    def copy_mesh(self, src, dest):
        """
        Copies mesh files to dest
        """
        # this is done as part of the copy_input
        # get the list of files to copy from config
        pass
    
    def copy_results(self, src, dest, 
        keep_files=None, 
        remove_files=None, 
        keep_residuals=False, 
        keep_monitors=False,
        keep_tout=False,
        keep_input=False,
        keep_dir=None, 
        continue_tout=False,
        init_to_scratch=False):
        """
        Copies results files to dest
        """
        # get the list of files to copy from config
        mask = config.get(self.name, 'results_mask')
        self.copy_mask(mask, src, dest)

        # default remove residuals
        files_to_remove = []
        if not keep_residuals:
            files_to_remove += [self.residual_fname]
        
        if not keep_monitors:
            monitor_files = self.get_monitor_files(src)
            _files = []
            if monitor_files:
                for fname in monitor_files:
                    _files.append(os.path.basename(fname))
                    
                files_to_remove += _files

        if isinstance(remove_files, list):
            files_to_remove += remove_files
        
        # remove duplicates
        files_to_remove = list(set(files_to_remove))
        
        if isinstance(keep_files,list):
            for fname in keep_files:
                if fname in files_to_remove:
                    files_to_remove.remove(fname)

        if files_to_remove:
            # remove files from the destination
            for fname in files_to_remove:
                path = os.path.join(dest, fname)
                if os.path.isfile(path):
                    os.unlink(path)

    def commit(self, msg=None):
        """ 
        Commits any checked out srcs with msg
        Returns the sha1 of the commit
        """
        from cfmt.vcs.controller import VCSController
        # get the path to the checked out src directory
        try:
            repo_path = os.path.join(self.study.path, self.path)
        except AssertionError:
            self.logger.error("Cannot find path to checked out code source")
            raise
        except:
            raise
        
        # fetch the controller and load the repo
        vcs = VCSController.get_vcs('git')
        return vcs.commit(repo_path, msg, name=self.name)

    def residual_reader(self, path, **kwargs):
        """ 
        Returns a residual reader 
        path: The path of the current run
        """
        residuals_path = os.path.join(path, "postProcessing/residuals")
        fpath = glob.glob(residuals_path + "/*/residuals.dat", recursive=True)

        # glob is unsorted
        fpath.sort(key=lambda x: self.time_dir_no(x))
        return ResidualReader(fpath, **kwargs)
    
    def time_dir_no(self, path):
        """ 
        Given a path, returns the time dir 
        
        e.g. postProcessing/residuals/i/residuals.dat would return i
        """
        # traverse up and get the first purely numeric entry
        dirname = path
        while dirname != "/":
            dirname = os.path.dirname(dirname)
            try:
                i = int(os.path.basename(dirname))
                return i
            except ValueError:
                continue

        return 0


    def monitor_reader(self, path):
        """ Returns a monitor reader """
        return MonitorReader(path)

    def timeres_reader(self,path):
        """ Returns a timeres reader """
        return TimeResReader(path)

    def get_monitor_files(self, path, fmask=None):
        """ Return a list of paths of files to monitor """
        # get the mask
        if fmask is None:
            fmask = config.get(self.name, 'monitor_mask')

        # glob the files
        files = glob.glob(os.path.join(path, fmask))
        if files:
            return files
        else:
            raise FileNotFoundError("No monitor files found")
    
    def get_massflow_file(self, path):
        """
        DEPRECATED
        Returns the pass to the massflow rate file
        """
        flist = self.get_monitor_files(path, fmask="mon-massflow.dat")
        return flist.pop()

    def get_timeres_file(self, path):
        return self.get_monitor_files(path, fmask="timeres.dat")


    def supports(self, capability):
        """ Supports passed capability """
        assert isinstance(capability, str)
        
        # openfoam has too many files, thus we only support editing
        # them by directly passing names
        if capability == "edit_input":
            return False

        return True 

    def parseDict(self, keyword, dictionary):
        """ Returns the value for the given keyword in the given dict """
        # we expect dict to be a full path to the file in question
        import subprocess
        
        fpath = dictionary
        if not os.path.isfile(fpath):
            msg = "Dictionary '{}' not found".format(fpath)
            self.logger.error(msg)
            raise FileNotFoundError(msg)

        assert isinstance(keyword, str), "keyword needs to be a string"
        
        # use the foamDictionary app
        exe = self.utilities['foamDictionary']
        args = [exe, "-entry", keyword, "-value", fpath]

        proc = subprocess.run(args, capture_output=True, text=True)

        if proc.returncode != 0:
            # we didn't get anything
            self.logger.warning("Entry '%s' not found in dict '%s'", keyword, fpath)
            return None
        else:
            return str(proc.stdout).rstrip()

    
    def parse_controlDict(self, case_dir=None):
        """
        Parses some information from controlDict
        """
        if case_dir is None:
            case_dir = self.case.abspath

        fpath = os.path.join(case_dir, 'system', 'controlDict')
        values = ['application', 'startFrom', 'startTime']

        for value in values:
            self.controlDict[value] = self.parseDict(value, fpath)

    def convert_openfoam(self, directory, args, run=None):
        """
        Enables openfoam case to be loaded in paraview.
        Note that no conversion is done, this simply touches a .foam file
        which the built in paraview reader will handle.
        """
        if run is not None:
            name = run.name + '.foam'
        else:
            name = self.name + '#' + os.path.basename(directory) + '.foam'
        # create foam file 
        fname = os.path.join(directory, name)

        with open(fname, 'a'):
            os.utime(fname, None)

        return {'path':fname}
    

class ResidualReader(utils.LiveDataReader):
    """ Read stream residuals """
    column_index = []
    def get_headers(self):
        """ Sets the headers """
        # first line should be "# Residuals" 
        header = self.fd.readline().split()
        if header and len(header) > 1:
            if header[1] != "Residuals":
                raise ValueError("Expected '# Residuals' as first line of file")
        else:
            raise ValueError("Unexpected header in file")

        # next line should be headers
        header = self.fd.readline().split()
        headers = []
        x_axis = ["time"]
        i = 0
        if header and len(header) > 1:
            for h in header:
                if h == '#':
                    continue
                headers.append(h)
                self.column_index.append(i)
                if h.lower() not in ["time"]:
                    self.yaxis_assign.append(i)
                
                if h.lower() in x_axis:
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                i += 1
        else:
            raise ValueError("Unexpected header in file")
        
        return headers

    def process(self, line):
        """ process the data """
        data = line.split()
        # remove any non-numeric values
        _data = []
        for i in self.column_index:
            try:
                _data.append(float(data[i]))
            except ValueError:
                _data.append(1.0)

        return _data

class TimeResReader(utils.LiveDataReader):
    """ Read stream residuals """
    column_index = [1]
    def get_headers(self):
        """ Sets the headers """
        # timeres does not have headers, we replace with
        header = self.fd.readline().split()
        if len(header) > 2:
            # we have something sensible
            self.column_index = [x for x in range(len(header))]
            del header[1]
            del self.column_index[1]
            columns = len(header) - 1 
            headers = ["Time (s)"] + ["EQN " + str(i+1) for i in range(columns)]
        else:
            headers = None
        return headers

    def process(self, line):
        """ process the data """
        data = line.split()

        # remove any non-numeric values
        try: 
            data = [float(data[i]) for i in self.column_index]
        except ValueError:
            data = [None for i in self.column_index]

        return data

class MonitorReader(utils.LiveDataReader):
    """ Read stream monitor files """
    column_index = [1]
    def get_headers(self):
        """ Monitors are of the format:
        Header lines at the top, prefixed by '#'
        First col is IT or Time
        Rest are variables
        """
        header = None
        last_pos = 0

        while True:
            line = self.fd.readline()
            line = line.lstrip()
            # ignore lines starting with '#'
            if line.startswith('#'):
                last_pos = self.fd.tell()
                continue
            else:
                if line:
                    header = line.split()
                break

        # all the columns
        self.column_index = [x for x in range(len(header))]

        if header[0].lower() == "it": 
            headers = header
        elif header[0].lower() == "time":
            # ignore the (s)
            # ignore the NT
            del header[1]
            del header[1]
            del self.column_index[1]
            del self.column_index[-1]
            headers = header
        elif header[0].lower() == "nt":
            # ignore the time
            # ignore the (s)
            del header[1]
            del header[1]
            del self.column_index[1]
            del self.column_index[-1]
            headers = header
        else:
            # rewind to start of non-header line
            self.fd.seek(last_pos)
            headers = None
            # discard

        #header = self.fd.readline().split()
        return headers

    def process(self, line):
        """ process the data """
        data = line.split()
        try:
            data = [float(data[i]) for i in self.column_index]
        except ValueError:
            data = [None for i in self.column_index]
        
        return data
