"""
    cft: A management tool for Computational Fluid Dynamics
    
    codes: Interface with cfd codes
"""
import importlib

# set the available codes
AVAILABLE_CODES = {
    'stream': 'StreamCode',
    'fluent': 'FluentCode',
    'openfoam': 'OpenFOAMCode',
    'code_saturne': 'SaturneCode',
    'starccm': 'StarCCMCode'
}

def get(code_name):
    """ Return the correct instance of the code """
    if code_name in AVAILABLE_CODES:
        module = importlib.import_module('cfmt.codes.' + str(code_name))
        return getattr(module, str(AVAILABLE_CODES[code_name]))


def add_arguments(code_name, mode, argparser):
    """ Adds code specific arguments """
    from argparse import Action
    # custome openfoam action
    class OpenFOAMAction(Action):
        """ set --code openfoam if we select one of it's options """
        def __call__(self, parser, args, values, option_string=None):
            store_true = ['list_examples']
            if self.dest in store_true:
                setattr(args, self.dest, True)

            # set the code argument if it's not already set
            if args.code is None:
                args.code = []

            if 'openfoam' not in args.code:
                args.code.append('openfoam')

    # code_saturne
    if code_name == "code_saturne":
        arg = argparser.add_argument_group('code_saturne options')
        if mode == "run":
            arg.add_argument(
                '--cs-restart-different-mesh',
                action="store_true",
                default=False,
                help="Tell Code_saturne that our old mesh is different."
            )
    elif code_name == "openfoam":
        arg = argparser.add_argument_group('openfoam options')
        if mode == 'init':
            arg.add_argument(
                '--openfoam-example',
                nargs='?',
                type=str,
                action='append',
                help="A list of OpenFOAM tutorials to copy",
                default=[]
            )
            arg.add_argument(
                '--openfoam-list-examples',
                help="List currently available open foam tutorials",
                nargs=0,
                type=bool,
                default=False,
                dest="list_examples",
                action=OpenFOAMAction
            )
        if mode == 'run':
            arg.add_argument(
                '--openfoam-solver',
                nargs=1,
                type=str,
                help="The OpenFOAM solver to use. See https://www.openfoam.com/documentation/user-guide/standard-solvers.php for a list.",
                default=None,
            )
    elif code_name == "fluent":
        from argparse import REMAINDER
        arg = argparser.add_argument_group('fluent options')
        if mode == 'run':
            arg.add_argument(
                '-f:c','--fluent-case',
                nargs=1,
                type=str,
                help="The FLUENT case file",
                default=None,
            )
            arg.add_argument(
                '-f:d','--fluent-data',
                nargs=1,
                type=str,
                help="The FLUENT data file",
                default=None,
            )
            arg.add_argument(
                '-f:v','--fluent-version',
                nargs=1,
                type=str,
                help="The FLUENT version to run",
                choices=['2d', '2ddp', '3d', '3ddp'],
            )
            arg.add_argument(
                '-f:m', '--fluent-mesh',
                nargs=1,
                type=str,
                help="The FLUENT mesh file to run",
                default=None,
            )
            arg.add_argument(
                '-f:i', '--fluent-input',
                nargs=1,
                type=str,
                help="The FLUENT journal file to run",
                default=None,
            )
            arg.add_argument(
                '-f:o', '--fluent-options',
                nargs=REMAINDER,
                help="Additional options to pass to fluent",
                default=None,
            )
    elif code_name == "starccm":
        from argparse import REMAINDER
        arg = argparser.add_argument_group('starccm options')
        if mode == "run":
            arg.add_argument(
                '-s:f' , '--starccm-sim',
                nargs=1,
                type=str,
                help="The StarCCM+ sim file",
                default=None
            )
            arg.add_argument(
                '-s:h' , '--starccm-host',
                nargs=1,
                type=str,
                help="The host for the StarCCM+ client to connect",
                default=None,
                metavar="HOST[:PORT]"
            )
            arg.add_argument(
                '-s:p' , '--starccm-port',
                nargs=1,
                type=str,
                help="The host for the StarCCM+ client to connect",
                default=None,
                metavar="PORT"
            )
            arg.add_argument(
                '-s:m' , '--starccm-macro',
                nargs=1,
                type=str,
                help="The macro file to run",
                default=None,
                metavar="FILE"
            )
            arg.add_argument(
                '-s:s', '--starccm-server',
                action="store_true",
                help="Run a starccm server instead of batch",
                default=None,
            )
            arg.add_argument(
                '-s:b', '--starccm-batch',
                nargs='?',
                const=True, 
                help="Run starccm in batch mode",
                default=None,
                choices=["run", "step", "mesh"]
            )
            arg.add_argument(
                '-s:mpi','--starccm-mpi',
                nargs=1,
                type=str,
                help="The mpi driver to use",
                default=None
            )
            arg.add_argument(
                '-s:o', '--starccm-options',
                nargs=REMAINDER,
                help="Additional options to pass to starccm",
                default=None,
            )
        if mode == "edit":
            arg.add_argument(
                '-s:s', '--starccm-server',
                action="store_true",
                help="Run a starccm server to edit",
                default=None,
            )
            arg.add_argument(
                '-s:np', '--starccm-np',
                type=int,
                help="Number of processes to run starccm  with",
                default=0
            )

    return
    