"""
    cft: A management tool for Computational Fluid Dynamics
    
    codes/stream.py: Interface to the STREAM code
"""
import os
import sys
import shutil
import glob
import logging
import time
from cfmt.config import configtool as config
from cfmt.codes import base
from cfmt import exceptions
from cfmt import utils


class StreamCode(base.Code):
    """ Class to implement an interface to the class code """
    name = "stream"
    maintainer = "Dean Wilson"
    description = "STREAM is a fully elliptic 3D finite volume solver capable of handling multi-block structured non-orthogonal meshes."
    path = {}
    requires_build = True
    can_build = True
    
    TIGHT_INTEGRATION = []

    DEFAULT_CONFIG = {
        name: {
            'git-url': 'git@gitlab.com:deanrw/stream.git',
            'binary': 'tubb',
            'input_mask': 'init.dat case.dat',
            'mesh_mask': 'blocks.dat x.dat y.dat z.dat',
            'monitor_mask': 'mon-*.dat',
            'results_mask': '*.dat',
            'tout_mask': 'tout*',
            'tavg_mask': 'tavg*',
            'post_utility': 'paraview',
            'residual_fname': 'residual.dat'
        }
    }
    residual_fname = "residual.dat"

    build_dirs = {
        'src': 'src',
        'build': 'build',
        'bin': 'bin',
    }
    supported_output_formats = [
        'ensight',
        'stream'
    ]
    utilities = {
        'strmtoensb' : 'bin/strmtoensb',
        'strmtoensbt': 'bin/strmtoensbt'
    }
    
    capability_true = ['tavg', 'edit_input', 'tout']
    capability_false = []

    requires_bin_copy = True
    default_output_format = 'stream'

    def __init__(self, study=None, db_record=None):
        """ Constructor """
        super().__init__(study, db_record)

        self.has_binary = False
        self.binary_path = "./" + self.binary
        
        # whether the code has been built
        self.is_built = False
        self.bin_dir = None

        self.build_src = None
        self.makefile = None
        
        self.url = config.get(self.name, 'git-url', fallback="")
    
    def set_config(self):
        """ Writes the default config to the config file """
        pass

    def init(self, db=None, args=None):
        """ Initilizes a new study """
        from cfmt.vcs.controller import VCSController
        
        src_dir = os.path.join(self.study.path, 'src')
        
        # --> check src folder exists
        assert os.path.isdir(src_dir), f"Path {src_dir} expected to exist"
        
        repo_path = os.path.join(src_dir, self.name)
        self.path = os.path.relpath(repo_path, self.study.path)

        # call parent to set db
        super().init(db, args)


        # --> clone using git
        git_repo = config.get(self.name, 'git-url')
        vcs = VCSController.get_vcs('git')
        vcs.clone(git_repo, repo_path)

        # create the input/stream folder
        path = os.path.join(self.study.path, 'input', self.name)
        assert os.path.isdir(path), f"Path {path} expected to exist"

        # copy init.dat
        init_dat = os.path.join(repo_path, 'examples', 'init.dat')
        if os.path.isfile(init_dat):
            shutil.copy2(init_dat, path)

    def create_case(self, path):
        """ Runs when creating a new case """
        # get the study directory
        study_path = self.study.path
        inputs = glob.glob(os.path.join(study_path, 'input', self.name) + '/*')
        for src in inputs:
            shutil.copy2(src,path)
    
    def get_case_ignore_list(self, case):
        """
        Returns a list of directories to ignore when copying a case
        """
        return ["build"]
    
    def pre_run_check(self, run, args=None, batchname=None):
        """ 
        Pre run checks 
        """
        # check for compressed files - these will not be read by stream
        if isinstance(run, bool):
            # then we're running from scratch
            return

        return
        #run_dir = run.get_run_dir()
        #mfiles = self.get_monitor_files(run_dir)

    
    def copy_bin(self, case, dest):
        """
        Copies the run time binaries to dest
        Return the full path
        """
        path = None

        # do we have a bin dir stored from compilation
        if self.bin_dir is not None:
            path = self.bin_dir
        else:
            # we search for them in the case build directory (if it exists)
            path = os.path.join(case.abspath, 'build', self.build_dirs['bin'])
            self.bin_dir = path

        files = []
        if os.path.isdir(path):
            files = os.listdir(path)
        
        self.has_binary = False
        if files:
            for fname in files:
                src = os.path.join(path, fname)
                self.logger.debug("Copied binary '%s' to '%s'",fname, dest)
                
                # store the known binary if we find it
                if fname == self.binary:
                    self.has_binary = True
                    #self.binary_path = os.path.join(dest, fname)

                shutil.copy2(src, dest)

        else:
            self.logger.error("Cannot find runtime binary")
            self.console.error("Cannot find runtime binary")
            raise FileNotFoundError
        
        if not self.has_binary:
            msg = f"Expecting binary '{self.binary}' for code {self.name} but it wasn't copied"
            self.logger.warning(msg)
            self.console.warning(msg)
    def set_bin_path(self, dest):
        """ Sets the binary path based on a provided run directory """
        if self.binary_path is None:
            raise RuntimeError(f"Binary path not set for code {self.name}")
        #self.binary_path = os.path.join(dest, self.binary)
    
    def get_bin_path(self, wd=None, batch_system=None):
        """ Returns the path to the binary """
        if self.binary_path is not None:
            return self.binary_path
        else:
            raise FileNotFoundError
    
    def get_command(self, run, batch, cmd=None, cmdline_args=None):
        """
        SLURM provides tight integration, so we can just use "srun"
        """
        if batch.system_name == "slurm":
            command = []
            command.append("srun")
            mpi_plugin=config.get("slurm", "mpi_plugin", fallback="none")
            command.append("--mpi=" + mpi_plugin)
            command.append(self.binary_path)

            return command

    
    def get_tout_info(self, run):
        """
        Returns structured infomration on tout data sets
        sn - start index
        en - end index
        st - start time
        et - end time
        """
        import glob
        run_dir = run.get_run_dir()
        # get the tavg mask
        mask = config.get(self.name, 'tout_mask')

        # glob in the run dir for files
        # list directories in the run_dir, filtering for the mask
        pattern = os.path.join(run_dir, mask)
        dirs = [d for d in glob.glob(pattern) if os.path.isdir(d)]

        info = {
            'num_sets': len(dirs),
            'sets': {}
        }
        for d in dirs:
            # read timeoutput.dat
            fpath = os.path.join(run_dir, d, 'timeoutput.dat')
            if os.path.isfile(fpath):
                with open(fpath, 'r') as f:

                    # get the first line
                    line = f.readline()
                    if line:
                        t = line.split()
                        sn = t[0]
                        st = t[1]
                    else:
                        self.console.error("Error reading timeoutput.dat")

                    # get the last line
                    line = utils.get_last_line(f)
                    if line:
                        t = line.split()
                        en = t[0]
                        et = t[1]
                    else:
                        self.console.error("Error reading timeoutput.dat")

            else:
                self.console.warning("Cannot load timeoutput.dat")
                tout_nt = 0

            size = utils.get_dir_size(d)
            info['sets'][os.path.basename(d)] = {
                    'nt': int(en)-int(sn),
                    'start_time' : float(st),
                    'end_time': float(et),
                    'size': float(size)
            }

        return info

    
    def get_tavg_info(self, run):
        """
        Returns structured information on time-averaged sets associated with the attached run
        """
        import fnmatch
        
        run_dir = run.get_run_dir()
        # get the tavg mask
        tavg_mask = config.get(self.name, 'tavg_mask')

        # glob in the run dir for files
        # list directories in the run_dir, filtering for the mask
        tavg_dirs = [d for d in os.listdir(run_dir) if os.path.isdir(os.path.join(run_dir,d)) and fnmatch.fnmatch(d,tavg_mask)]

        tavg_info = {
            'num_sets': len(tavg_dirs),
            'sets': {}
        }
        for d in tavg_dirs:
            # read tavg-nt, tavg-en, tavg-st files
            fpath = os.path.join(run_dir, d, 'tavg-nt.dat')
            if os.path.isfile(fpath):
                with open(fpath, 'r') as f:
                    tavg_nt = f.readline()
            else:
                self.console.warning("Cannot read tavg-nt.dat")
                tavg_nt = 0
            
            fpath = os.path.join(run_dir, d, 'tavg-st.dat')
            if os.path.isfile(fpath):
                with open(fpath, 'r') as f:
                    tavg_st = f.readline()
            else:
                self.console.warning("Cannot read tavg-st.dat")
                tavg_st = 0
            
            fpath = os.path.join(run_dir, d, 'tavg-en.dat')
            if os.path.isfile(fpath):
                with open(fpath, 'r') as f:
                    tavg_en = f.readline()
            else:
                self.console.warning("Cannot read tavg-en.dat")
                tavg_en = 0

            tavg_info['sets'][d] = {
                    'nt': int(tavg_nt),
                    'start_time' : float(tavg_st),
                    'end_time': float(tavg_en),
                    'snapshots': {}
            
            }
            # snapshots are saved every n folders, we read all folders with integer names

            with os.scandir(os.path.join(run_dir, d)) as it:
                for s in it:
                    if not s.is_dir():
                        continue

                    # check if the name is an integer
                    try:
                        int(s.name)
                    except ValueError:
                        continue
                    else:
                        # read tavg-nt, tavg-en files inside
                        fpath = os.path.join(run_dir, d, s.name, 'tavg-en.dat')
                        if os.path.isfile(fpath):
                            with open(fpath, 'r') as f:
                                tavg_en = f.readline()
                        else:
                            self.console.warning("Cannot read tavg-en.dat")
                            tavg_en = 0
            
                        fpath = os.path.join(run_dir, d, 'tavg-nt.dat')
                        if os.path.isfile(fpath):
                            with open(fpath, 'r') as f:
                                tavg_nt = f.readline()
                        else:
                            self.console.warning("Cannot read tavg-nt.dat")
                            tavg_nt = 0
                        
                        tavg_info['sets'][d]['snapshots'][s.name] = {
                            'nt': int(tavg_nt),
                            'end_time': float(tavg_en)
                        }

            #snap_dirs = [d for d in os.listdir(os.path.join(run_dir, d)) if os.path.isdir(os.path.join(run_dir,d)) and fnmatch.fnmatch(d,tavg_mask)]

        return tavg_info

    
    def copy_input(self, src, dest, args=None, mask=None, history=False):
        """
        Copies input files to dest
        """
        # get the list of files to copy from config
        if mask is None:
            mask = config.get(self.name, 'input_mask')
        self.copy_mask(mask, src, dest)
    
    def edit_input(self, directory):
        """ Edit the input files in the run directory """
        # check the destination dir exists
        if not os.path.isdir(directory):
            raise NotADirectoryError("Cannot find run directory")
        
        # get the list of files to edit from config
        mask = config.get(self.name, 'input_mask')
        mask_list = mask.split(" ")

        src_list = []
        for _mask in mask_list:
            # glob the files in src
            files = glob.glob(os.path.join(directory, _mask))
            for fname in files:
                src_list.append(fname)

        utils.edit_file(src_list, cwd=directory)

    def get_tout_filter(self, run, up=False):
        """
        Given a run, check the staging directory for timeoutput
        Returns a filter list which can be used in rsync
        """
        if up:
            src_dir = run.abspath
        else:
            if not run.in_staging:
                return []
            src_dir = run.staging_dir

        # get the staging directory
        
        if not os.path.isdir(src_dir):
            return []
        
        rfilter = []
        # search in the staging directory for tout folders
        mask = config.get(self.name, "tout_mask")
        files = glob.glob(os.path.join(src_dir, mask))

        #print(src_dir)
        #print(files)
        # for each result we want to include the dir and the timeoutput.dat file
        # but exclude everything else
        for fpath in files:
            # get relative path
            path = os.path.relpath(fpath, src_dir)
            rfilter.append(['+', path])
            rfilter.append(['+', os.path.join(path, "timeoutput.dat")])
            rfilter.append(['-', os.path.join(path, "*.dat")])
            rfilter.append(['-', os.path.join(path, "*.ens")])
            rfilter.append(['-', os.path.join(path, "*.case")])

        return rfilter
    
    def copy_mesh(self, src, dest, args=None):
        """
        Copies mesh files to dest
        """
        # get the list of files to copy from config
        mask = config.get(self.name, 'mesh_mask')
        self.copy_mask(mask, src, dest)
    
    def copy_results(self, src, dest, 
        keep_files=None, 
        remove_files=None, 
        keep_residuals=False, 
        keep_monitors=False, 
        keep_tout=False, 
        keep_input=False,
        keep_dir=None, 
        continue_tout=False,
        init_to_scratch=False):
        """
        Copies results files to dest
        """
        # get the list of files to copy from config
        mask = config.get(self.name, 'results_mask')
        self.copy_mask(mask, src, dest)

        # copy input if required
        if keep_input:
            mask = config.get(self.name, 'input_mask')
            self.copy_mask(mask, src, dest)
        
        # copy residuals if required
        if keep_residuals:
            mask = config.get(self.name, 'residual_fname')
            self.copy_mask(mask, src, dest)

        if keep_monitors:
            mask = config.get(self.name, 'monitor_mask')
            self.copy_mask(mask, src, dest)
        
        # search in the directory for tout folders
        if continue_tout:
            mask = config.get(self.name, "tout_mask")
            files = glob.glob(os.path.join(src, mask))

            # for each, we create the folder in the destination and copy the timeoutput.dat if
            # it exists
            for fpath in files:
                dirname = os.path.basename(fpath)
                dirpath = os.path.join(dest, dirname)
                os.mkdir(dirpath)
                try:
                    self.copy_mask("timeoutput.dat", fpath, dirpath)
                except FileNotFoundError:
                    self.logger.debug("Could not find timeoutput.dat in '%s'", fpath)

        return

        # default remove residuals
        files_to_remove = []
        if not keep_residuals:
            files_to_remove += [self.residual_fname]
        
        if not keep_monitors:
            monitor_files = self.get_monitor_files(src)
            _files = []
            if monitor_files:
                for fname in monitor_files:
                    _files.append(os.path.basename(fname))
                    
                files_to_remove += _files

        if isinstance(remove_files, list):
            files_to_remove += remove_files
        
        # remove duplicates
        files_to_remove = list(set(files_to_remove))
        
        if isinstance(keep_files,list):
            for fname in keep_files:
                if fname in files_to_remove:
                    files_to_remove.remove(fname)

        if files_to_remove:
            # remove files from the destination
            for fname in files_to_remove:
                path = os.path.join(dest, fname)
                if os.path.isfile(path):
                    os.unlink(path)

    def build(self, case, print_output=False, rebuild=False):
        """
        Compiles and builds the binary necessary to run the code
        """
        import subprocess
        
        # -> check case directory has been provided
        if case.abspath is None:
            msg = "Case directory not provided"
            self.logger.error(msg)
            raise exceptions.BuildFailed(msg)
        
        # check the src folder is present
        src_path = os.path.join(self.study.path, self.path)
        if not os.path.isdir(src_path):
            msg = "Code directory '{}' cannot be found".format(src_path)
            self.logger.error(msg)
            raise exceptions.BuildFailed(msg)
        
        # -> check makefile exists
        makefile = os.path.join(src_path, 'makefile')
        if not os.path.isfile(makefile):
            msg = "Cannot find makefile for code '{}'".format(self.name)
            self.logger.error(msg)
            raise exceptions.BuildFailed(msg)

        # -> check 'make' is available
        make = shutil.which("make")
        if make is None:
            msg = "'make' not available"
            self.logger.error(msg)
            self.console.error(msg)
            sys.exit(0)

        # -> create build directory if it doesn't exist
        build_dir = os.path.join(case.abspath, self.build_dirs['build'])
        if not os.path.isdir(build_dir):
            os.makedirs(build_dir)

        # --> create mock build environment
        for name, directory in self.build_dirs.items():
            path = os.path.join(build_dir, directory)
            if not os.path.isdir(path):
                os.makedirs(path)

        # --> symlink required files to build directory.
        #     _currently we just grab all .f and .h files in order
        src_dir_order = [
            os.path.join(src_path, self.build_dirs['src']),
            os.path.join(case.abspath, self.build_dirs['src'])
        ]

        # file mask to apply
        file_mask = ['*.f', '*.h']
        msg = ", ".join("'{0}'".format(w) for w in src_dir_order)
        self.logger.debug("Looking for files with mask '%s' in directories %s", file_mask, msg)
        to_link = {}
        
        # create the links to the build src directory
        build_src = os.path.join(build_dir, self.build_dirs['src'])

        # search src_dir_order for those files, buidling a dictionary of links
        for directory in src_dir_order:
            for fmask in file_mask:
                paths = glob.glob(os.path.join(directory, fmask))
                for path in paths:
                    # convert the path to relative
                    relpath = os.path.relpath(path, start=build_src)
                    fname = os.path.basename(path)
                    to_link[fname] = relpath


        if os.path.isdir(build_src):
            for fname, link in to_link.items():
                target = os.path.join(build_src, fname)

                # if the target exists, check it's desination
                if os.path.islink(target):
                    link_target = os.path.realpath(target)
                    if link_target == link and os.path.isfile(link_target):
                        # we already have the correct target, skip
                        self.logger.debug("Link '{}' to '{}' already exists, skipping".format(fname, link))
                        continue
                    else:
                        # the desintation is incorrect, we remove
                        self.logger.debug("Destination of existing link '%s' has changed, removing..", target)
                        os.unlink(target)
                elif os.path.isfile(target):
                    # the target is a real file, which should be removed
                    os.unlink(target)
                    self.logger.debug("Found existing file '%s' in build directory, removing..", target)
                
                # create the new links
                self.logger.debug("Linking '{}' to '{}'".format(fname, link))
                os.symlink(link, target)

        # check if we need to overwrite the makefile
        case_makefile = os.path.join(case.abspath, 'src', 'makefile')
        if os.path.isfile(case_makefile):
            # we have a custom makefile
            self.logger.debug("Using custom makefile '%s'", case_makefile)
            makefile_to_use = case_makefile
        else:
            # we use the makefile provided with the code
            self.logger.debug("Using code makefile '%s'", makefile)
            makefile_to_use = makefile

        target_makefile = os.path.join(build_dir, 'makefile')
        if os.path.islink(target_makefile) and os.path.realpath(target_makefile) == makefile_to_use:
            # we're good
            pass
        else:
            if os.path.islink(target_makefile) or os.path.isfile(target_makefile):
                # remove whatever is in the way
                os.unlink(target_makefile)
            self.logger.debug("Linking makefile '%s' to '%s'", makefile_to_use, target_makefile)
            os.symlink(makefile_to_use, target_makefile)

        # we're ready to build this shit
        # --> double check we have a valid makefile
        if not os.path.isfile(target_makefile):
            msg = "Makefile '{0}' not found".format(target_makefile)
            self.logger.error(msg)
            self.console.error(msg)
            sys.exit(1)

        # --> check if make says we need to build
        result = subprocess.run([make, '-q', '-C', build_dir])
        if result.returncode != 0:
            self.logger.info("Building '%s'...", self.name)
            print(" -> Building code: '{}' ...".format(self.name), end='\r')

            # use the AsyncRunner
            runner = utils.AsyncRun(
                [make, '-C', build_dir],

            )
            # enable logging
            runner.stdout_log = True
            runner.stdout_print = False
            p = runner.execute()

            # get the return codes
            result = p.returncode
            
            if result != 0:
                print(" -> Building code: '{}' ... FAILED".format(self.name), end='\n')
                raise exceptions.BuildFailed("Make returned an error")
            else:
                self.is_built = True
                print(" -> Building code: '{}' ... OK".format(self.name), end='\n')

        else:
            print(" -> Code '{}' up to date".format(self.name))
            self.logger.info("Code '%s' up to date", self.name)
            # make reports that executable is up to date
        
        # store the location of the bin_dir
        self.bin_dir = os.path.join(os.path.join(build_dir, self.build_dirs['bin']))
        
        # store the location of the build_src
        self.build_src = build_src

        # store the location of the target_makefile
        self.makefile = makefile_to_use

    
    def copy_src(self, dest):
        """
        Copy the build src to the specified directory
        """
        if self.build_src is None:
            self.logger.debug("No src code to commit")
            return
        
        # create the directory if it doesn't exist
        if not os.path.isdir(dest):
            self.logger.debug("Creating 'src' folder within case history directory")
            os.makedirs(dest)

        assert isinstance(self.build_src, str)
        # copy src files
        files = glob.glob(os.path.join(self.build_src, '*'))
        if files:
            for fname in files:
                self.logger.debug("Copying '%s' to '%s'", fname, dest)
                shutil.copy(fname, dest, follow_symlinks=True)
        
        # copy make file
        if self.makefile is not None:
            if os.path.isfile(self.makefile):
                shutil.copy(self.makefile, dest, follow_symlinks=True)

    
    def commit(self, msg=None):
        """ 
        Commits any checked out srcs with msg
        Returns the sha1 of the commit
        """
        from cfmt.vcs.controller import VCSController
        # get the path to the checked out src directory
        try:
            repo_path = os.path.join(self.study.path, self.path)
        except AssertionError:
            self.logger.error("Cannot find path to checked out code source")
            raise
        except:
            raise
        
        # fetch the controller and load the repo
        vcs = VCSController.get_vcs('git')
        return vcs.commit(repo_path, msg, name=self.name)

    def residual_reader(self, path, **kwargs):
        """ Returns a residual reader """
        fpath = os.path.join(path, self.get_residual_file())
        return ResidualReader(fpath, **kwargs)

    def monitor_reader(self, path, **kwargs):
        """ Returns a monitor reader """
        return MonitorReader(path, **kwargs)

    def timeres_reader(self,path, **kwargs):
        """ Returns a timeres reader """
        return TimeResReader(path, **kwargs)

    def plot_reader(self, path, **kwargs):
        """ Returns a plot reader """
        return PlotReader(path, **kwargs)
        

    def get_massflow_file(self, path):
        """
        DEPRECATED
        Returns the pass to the massflow rate file
        """
        flist = self.get_monitor_files(path, fmask="mon-massflow.dat")
        return flist.pop()

    def get_timeres_file(self, path):
        return self.get_monitor_files(path, fmask="timeres.dat")
    
    def get_post_args_paraview(self, run, args=None):
        """
        Return argv for paraview
        """
        f = os.path.join(run.get_run_dir(), 'blocks.dat')
        return [f]

    def convert_stream(self, directory, args, run=None):
        """
        If the STREAM plugin is in paraview then we can load it directly
        """
        cwd = directory
        return {'path': os.path.join(cwd, 'blocks.dat')}

    def delete_ensight(self, wd):
        """
        Deletes ensight files *.ens and *.case in the directory
        """
        fmask = "*.ens *.case"
        paths = []
        for mask in fmask.split():
            paths += glob.glob(os.path.join(wd, mask))

        for path in paths:
            self.logger.debug("Deleting %s",path)
            os.remove(path)

        return paths


    def convert_ensight(self, directory, args, run=None):
        """
        Converts stream run results to ensight format
        Uses the strmtoensb tool found in utils
        """
        import subprocess
        
        # handle defaults
        unsteady = getattr(args, 'unsteady', False)
        tout_dir = getattr(args, 'tout_dir', None)
        force = getattr(args, 'force', False)
        
        cmd = []
        do_convert = True

        # check we have the tool
        tool = 'strmtoensbt' if unsteady else 'strmtoensb'
        
        fbin = os.path.join(self.abspath, self.utilities[tool])
        if not os.path.isfile(fbin):
            raise NotImplementedError("Cannot find '{}'".format(tool))
        else:
            cmd.append(fbin)

        # for strmtoensbt we run the bin in the tout dir, passing the run
        # directory as --mesh-dir
        cwd = directory
        paths = []
        
        if unsteady:
            # we're running in batch
            cmd.append("--batch")
            if tout_dir is not None:
                # check it exists
                fmask = tout_dir.pop()
                if not os.path.isdir(fmask):
                    self.console.error("tout directory '%s' does not exist", fmask)
                    sys.exit(1)
                else:
                    paths = [fmask]
            else:
                # get the tout dir from the mask
                fmask = config.get(self.name, 'tout_mask')
            
                # search the run directory
                paths = glob.glob(os.path.join(directory, fmask))
            
            if paths:
                cwd = paths.pop()
                # we need the mesh dir relative to the cwd
                path = os.path.relpath(directory, cwd)
                cmd.append("--mesh-dir={}".format(path))
            else:
                self.console.error("Cannot find tout directory using file mask '%s'", fmask)
                sys.exit(1)

            # for large jobs we don't want to convert if it's already been done
            # check the times
            fpath1 = os.path.join(cwd, 'ensightbt.case')
            fpath2 = os.path.join(cwd, 'timeoutput.dat')
            if os.path.isfile(fpath1) and os.path.isfile(fpath2):
                last1 = os.path.getmtime(fpath1)
                last2 = os.path.getmtime(fpath2)
                if last1 >= last2:
                    self.logger.info("Output files up to date")
                    do_convert = False
            
        if force:
            # we convert regardless
            do_convert = True
        
        # whether or not to print output
        print_output = True
        
        # lauch the strmtoensb process
        if do_convert:
            proc = subprocess.run(cmd, cwd=cwd)
            #proc = subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            #                        universal_newlines=True)
            
            # launch the asynchronous readers of the process' stdout stderr
            #stdout_queue = utils.get_queue()
            #stdout_reader = utils.AsynchronousFileReader(proc.stdout, stdout_queue)
            
            #stderr_queue = utils.get_queue()
            #stderr_reader = utils.AsynchronousFileReader(proc.stderr, stderr_queue)
            
            ## check the queues for some output
            #while not stdout_reader.eof() or not stderr_reader.eof():
            #    # show what we received from stdout
            #    while not stdout_queue.empty():
            #        line = stdout_queue.get()
            #        self.logger.info(line.rstrip())
            #        if print_output:
            #            sys.stdout.write(line)
            #    
            #    # show what we received from stderr
            #    while not stderr_queue.empty():
            #        line = stderr_queue.get()
            #        self.logger.warning(line.rstrip())
            #        self.console.warning(line.rstrip())
            #        if print_output:
            #            print(line, end='')
            #    
            #    # sleep a bit
            #    time.sleep(.1)

            # join the threads
            #stdout_reader.join()
            #stderr_reader.join()

            # close the file descriptors
            #proc.stderr.close()
            #proc.stdout.close()


            # get the return codes

            if proc.returncode == 0:
                #self.logger.info(proc.stdout.decode())
                if not unsteady:
                    return {'path': os.path.join(directory, 'ensightb.case')}
            else:
                #self.logger.error(proc.stdout.decode())
                raise exceptions.PostFailure("Strmtoensb failed")
        
        if unsteady:
            return {'path': os.path.join(cwd, 'ensightbt.case')}

class ResidualReader(utils.LiveDataReader):
    """ Read stream residuals """
    column_index = []
    def get_headers(self):
        """ Sets the headers """
        
        header = self.fd.readline().split()
        headers = [] 
        # x_axis assigment
        x_axis = ["it"]
        i = 0
        if header[0].lower() == "it":
            for h in header:
                headers.append(h)
                self.column_index.append(i)
                
                if h.lower() in x_axis:
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                elif h.lower() not in ["maximum"]:
                    self.yaxis_assign.append(i)

                i += 1
        else:
            # discard and rewind
            self.fd.seek(0)
            headers = None
        
        return headers

    def process(self, line):
        """ process the data """
        data = line.split()
        # remove the Max column

        # remove any non-numeric values
        try: 
            data = [float(data[i]) for i in self.column_index]
        except ValueError:
            data = None
        return data

class TimeResReader(utils.LiveDataReader):
    """ Read stream residuals """
    lines = 0
    column_index = []
    def get_headers(self):
        """ Sets the headers """
        # timeres does not have headers, we replace with
        header = self.fd.readline().split()

        # note also timeres does not include NT, we add this on in self.process
        if len(header) > 2:
            # we have something sensible
            self.column_index = [x for x in range(len(header))]

            # assign NT to the xaxis
            self.xaxis_assign = 0

            # assign IT/DT to the yaxis 
            columns = len(self.column_index) - 1
            self.yaxis_assign = [2]
            headers = ["NT", "Time (s)", "IT/DT"] + ["EQN " + str(i+1) for i in range(columns)]
        else:
            headers = None
        return headers

    def process(self, line):
        """ process the data """
        # line counter
        data = line.split()

        # remove any non-numeric values
        try: 
            data = [float(data[i]) for i in self.column_index]
            self.lines += 1
            data = [self.lines] + data
        except ValueError:
            data = [None for i in self.column_index]
        return data

class MonitorReader(utils.LiveDataReader):
    """ Read stream monitor files """
    lines=0
    column_index = []
    def get_headers(self):
        """ Monitors are of the format:
        Header lines at the top, prefixed by '#'
        First col is IT or Time
        Rest are variables
        """
        header = None
        last_pos = 0

        while True:
            line = self.fd.readline()
            line = line.lstrip()
            # ignore lines starting with '#'
            if line.startswith('#'):
                last_pos = self.fd.tell()
                continue
            else:
                if line:
                    header = line.split()
                break
        
        if header is None:
            # nothing read in
            raise IndexError
        
        headers = []
        
        # valid x_axis headers
        x_axis = ["it", "time", "nt"]
        # check we have a valid header line
        i = 0
        if header[0].lower() in x_axis:
            # assign headers
            for h in header:
                if h.lower() in x_axis:
                    headers.append(h)
                    self.column_index.append(i)
                    if self.xaxis_assign is None:
                        self.xaxis_assign = i
                    i += 1
                    continue
                elif h.lower() == "(s)":
                    # not a header, skip
                    continue
                else:
                    # assign to y_axis
                    headers.append(h)
                    self.column_index.append(i)
                    self.yaxis_assign.append(i)
                    i += 1
                    continue
        else:
            # rewind to start of non-header line
            # assign NT to the xaxis
            #self.xaxis_assign = 0

            # assign IT/DT to the yaxis 
            #columns = len(self.column_index) - 1
            #self.yaxis_assign = [2]
            #headers = ["NT", "Time (s)", "IT/DT"] + ["EQN " + str(i+1) for i in range(columns)]
            # No headers
            i=0
            headers.append("ROW")
            self.xaxis_assign = 0
            self.generate_row_list = True
            self.column_index.append(i)
            i += 1
            for h in header:
                headers.append(f"COL{i:03d}")
                self.column_index.append(i)
                self.yaxis_assign.append(i)
                i += 1
            self.fd.seek(last_pos)
            #headers = None
        # look for x-axis headers
        #if header[0].lower() == "it": 
        #    # probably steady state
        #    headers = header
        #    self.xaxis_assign.append(0)
        #elif header[0].lower() == "time":
        #    # ignore the (s)
        #    # ignore the NT
        #    del header[1]
        #    del header[1]
        #    del self.column_index[1]
        #    del self.column_index[-1]
        #    headers = header
        #elif header[0].lower() == "nt":
        #    # ignore the time
        #    # ignore the (s)
        #    del header[1]
        #    del header[1]
        #    del self.column_index[1]
        #    del self.column_index[-1]
        #    headers = header
        #else:
        #    # rewind to start of non-header line
        #    self.fd.seek(last_pos)
        #    headers = None
        #    # discard
        #header = self.fd.readline().split()
        return headers

    def process(self, line):
        """ process the data """
        data = line.split()
        try:
            if self.generate_row_list:
                self.lines += 1
                data = [self.lines] + data

            data = [float(data[i]) for i in self.column_index]
        except ValueError:
            data = [None for i in self.column_index]
        
        return data

class PlotReader(utils.LiveDataReader):
    """ Read plottable date """
    
    def get_headers(self):
        """ Sets the headers """
        header = None
        last_pos = 0

        # assume its a dat file
        # -> skip all commented lines, last one is headers
        # -> if we have a time-value, read it 
        time_found = False
        prevline = None
        ih = 0
        while (True):
            line = self.fd.readline()
            if not line.startswith('#'):
                break
            
            if line.lower().find("time") != -1 and not time_found :
                time_str = line.split()[-1]
                try:
                    time = float(time_str)
                except ValueError:
                    time = None
                else:
                    self.tlist[self.findex] = time
                    time_found = True

            ih += 1
            prevline = None
        if prevline is not None:
            header = prevline.split()
            # strip any extra whitespace
            header = [x.strip() for x in header]
        else:
            # no comments, first line should be the header
            # - check again for csv
            header = line.split()
            header = [x.strip() for x in header if x.strip()]

        if header:
            self.skip_headers = ih
            self.xaxis_assign = 0
            self.column_index.append(0)
            i = 1
            # only assign one curve
            self.yaxis_assign.append(i)
            for h in header[1:]:
                self.column_index.append(i)
                i += 1
        else:
            # rewind to start of non-header line
            self.fd.seek(last_pos)
            header = None

        return header 
    
    def process(self, line):
        """ process the data """

        data = line.split()

        _data = []
        for i in self.column_index:
            try:
                _data.append(float(data[i]))
            except ValueError:
                _data.append(float("nan"))
            except IndexError as err:
                print(f"WARNING: {err}")
                _data.append(float("nan"))
        return _data