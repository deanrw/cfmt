"""
    cft: A management tool for Computational Fluid Dynamics

    archives.py: For run archives
"""

import os,sys
import logging

import cfmt.config as config
from cfmt.entities.base import BaseEntity
from cfmt import exceptions, utils

# hard coded defaults
DEFAULT_README_NAME="README.cfmt.md"
DEFAULT_HASH_FUNCTION="md5sum"
DEFAULT_COMPRESS_EXT="gz"
DEFAULT_COMPRESS=True
DEFAULT_KEEP_SOURCE=True
DEFAULT_KEEP_ARCHIVE=True
DEFAULT_FORCE=False
DEFAULT_COMPRESS_FUNCTION="pigz"
DEFAULT_DELETE=False
DEFAULT_REMOTE_PV=False
DEFAULT_RESTORE_TO_EMPTY=True
DEFAULT_ARCHIVE_TYPE="borg"
DEFAULT_NAME_FORMAT="archive_{name}_{created}{ext}"


class Archive(BaseEntity):
    """
    The Archive class
    """
    db_fields = [
        'id',
        'path',
        'name',
        'hostname',
        'hash',
        'size',
        'created',
        'last_modified',
        'description',
        'type'
    ]
    # options
    _opt = {}

    default_status_columns = [
        'id',
        'size',
        'type',
        'created',
        'last_modified',
        'description'
    ]
    FILENAME_TIME_FMT="%Y%m%d-%H%M%S"
    
    ## logging
    logger = logging.getLogger('file')
    console = logging.getLogger('console')
        
    # class constants
    COMPRESS_FUNCTION = DEFAULT_COMPRESS_FUNCTION
    COMPRESS = DEFAULT_COMPRESS
    
    def __init__(self, run, db=None, db_record=None, detached=False):
        """ Class constructor """
        
        # store the loggers
        #self.logger = logging.getLogger('file')
        #self.console = logging.getLogger('console')
            
        # get the db from the run
        assert hasattr(run, "db"), "Run is missing database"

        
        
        self.db = run.db
        self.run = run
        
        self.archive_created = False
        
        # whether to compute and store hashes
        self.set_option(
            checksum=config.configtool.getboolean('archive', 'checksum', fallback=False),
            hash_function=config.configtool.get('archive', 'hash_function', fallback=DEFAULT_HASH_FUNCTION),
            compress=config.configtool.getboolean('archive', 'compress', fallback=self.COMPRESS),
            compress_function=config.configtool.get('archive', 'compress_function', fallback=self.COMPRESS_FUNCTION),
            readme_name=config.configtool.get('archive', 'readme_name', fallback=DEFAULT_README_NAME),
            keep_source=config.configtool.getboolean('archive', 'keep_source', fallback=DEFAULT_KEEP_SOURCE),
            keep_archive_after_withdraw=config.configtool.getboolean('archive', 'keep_archive_after_withdraw', fallback=DEFAULT_KEEP_ARCHIVE),
            restore_to_empty=config.configtool.getboolean('archive', 'restore_to_empty', fallback=DEFAULT_RESTORE_TO_EMPTY),
            force=config.configtool.getboolean('archive', 'force', fallback=DEFAULT_FORCE),
            remote_pv=config.configtool.getboolean('archive', 'remote_pv', fallback=DEFAULT_REMOTE_PV),
            name_format=config.configtool.get('archive', 'name_format', fallback=DEFAULT_NAME_FORMAT),
            delete=DEFAULT_DELETE
        )
        
        self.hash = None

        # the local path to the tar file
        self.tarpath = None

        # the description
        self.description = None
        
        self.hostname = None
        self.path = None
        self.name = None
        self.size = None
        self.created = None
        self.id = None

        if db_record is not None:
            self.db_record = db_record
            for db_field in self.db_fields:
                record = getattr(db_record, db_field)
                setattr(self, db_field, record)
            
            # handle hostname
            self.hostname = None if self.hostname == "" else self.hostname
            
            # handle path
            self.path = self.get_archive_path(self.path, self.hostname, reverse=True)
            self.archive_created = True 

    def set_option(self, **kwargs):
        """ Sets archive options """
        for opt, value in kwargs.items():
            # None values leave the default
            if value is None:
                continue
            
            self._opt[opt] = value

    def get_option(self, opt, fallback=None, missing_ok=False):
        """ Gets an archive option """
        if opt in self._opt:
            return self._opt[opt]
        else:
            if missing_ok:
                raise ValueError("Option '%s' missing", opt)
            else:
                return fallback

    @classmethod 
    def get_proc(cls, cmd, stdin=None, stdout=None, stderr=None, **kwargs):
        """ Returns a subprocess """
        assert isinstance(cmd, list)

        import subprocess
       
        cls.logger.debug("Executing subprocess with command '%s'", " ".join(cmd))
        
        return subprocess.Popen(
            cmd, 
            stdin=stdin, 
            stderr=stderr, 
            stdout=stdout,
            **kwargs
        )

    def get_pv_proc(self, **kwargs):
        """ Returns a pipe viewer process """
        cmd = []
        cmd.append("pv")
        cmd.append("--bytes")
        cmd.append("-n")        # print out bytes line by line

        return self.get_proc(cmd, **kwargs)

    def get_ssh_proc(self, host, remote_cmd, **kwargs):
        """ Returns an ssh process """
        assert isinstance(remote_cmd, list)

        remote_cmd_str = ' '.join(remote_cmd)

        cmd = []
        cmd.append("ssh")
        cmd.append(host)
        cmd.append(remote_cmd_str)

        return self.get_proc(cmd, **kwargs)

    def get_tee_proc(self, fd, hash_func, **kwargs):
        """ 
        Returns a tee process 
        fd - an open named pipe  
        """
        cmd = ["tee", fd.name]
        cmd.append()


    def generate_hash(self, fpath, size=None):
        """ Given the file fpath, generate a hash """
        from tqdm import tqdm 
        desc = " ---> Generating md5sum: {}".format(os.path.basename(fpath))
        with tqdm(unit='B', total=size, unit_divisor=1024, unit_scale=True, leave=False) as pbar:
            pbar.set_description(desc=desc)
            def add_delta(delta):
                pbar.update(delta)
        
            _hash = utils.get_hash(fpath, callback=add_delta)
        
        return _hash

    def restore_archive_path(self, repo_path, host):
        """
        Checks for a None host, and returns an absolute path if 
        the host is None and the repo_path is not absolute
        """
        if host is None and not os.path.isabs(repo_path):
            study_path = self.run

    
    def get_archive_path(self, repo_path, host, reverse=False):
        """
        Checks for a None host, and returns a repo path relative to
        the study path
        """
        if host is None and not os.path.isabs(repo_path):
            # store relative path to study
            study_path = self.run.study.path
            if not reverse:
                apath = os.path.relpath(repo_path, study_path)
            else:
                apath = os.path.join(study_path, repo_path)
        else:
            apath = repo_path 

        return apath

    def check(self):
        """ Check a remote archive was uploaded successfully created """
        pass
    
    def create_db(self):
        """ Creates the database record """
        required = ['name', 'TYPE', 'path', 'run', 'hostname', 'size', 'hash', 'created', 'description']
        for a in required:
            assert hasattr(self, a), "Attribute missing: '{}'".format(a)

        self.db_record = self.db.new_archive(
            name=self.name,
            path=self.path,
            run=self.run.db_record,
            hostname=self.hostname,
            hash=self.hash,
            size=self.size,
            created=self.created,
            last_modified=self.created,
            type=self.TYPE,
            description=self.description
        )
    
    def save_db(self):
        """ Saves the database record """
        assert hasattr(self, "db_record")

        self.db_record.save()

    def remove_source(self, src):
        """ Remove the source files for an archive """
        from tqdm import tqdm 
        desc = " ---> Removing source tree ..."
        with tqdm(unit=' files', desc=desc, leave=False) as pbar:
            
            def callback(item):
                pbar.update()
                desc = " ---> Removing {}".format(os.path.basename(item))
                pbar.set_description(desc=desc)
            
            utils.delete_dir_contents(src, callback=callback)
    
    def delete_readme(self, ignore_error=True):
        """ Remove the readme """
        fname = self.get_option("readme_name")
        fpath = os.path.join(self.run.abspath, fname)

        if os.path.isfile(fpath):
            os.remove(fpath)
            self.logger.debug("Deleting '%s'", fpath)
        else:
            if not ignore_error:
                raise exceptions.ArchiveFailed("Cannot find local readme")
    
    def add_readme(self, src):
        """ Adds a readme """
        fname = self.get_option("readme_name")
        fpath = os.path.join(src, fname)

        if self.get_option('checksum'):
            checksum = str(self.hash)
        else:
            checksum = "n/a"

        msg = "*This CFMT run has been archived*"
        msg += "\n created: {}".format(utils.print_datetime(self.created))
        msg += "\n run: {}".format(self.run.name)
        msg += "\n study: {}".format(self.run.study.name)
        msg += "\n archive-type: {}".format(self.TYPE)
        msg += "\n archive-host: {}".format(self.hostname)
        msg += "\n archive-path: {}".format(self.path)
        msg += "\n archive-name: {}".format(self.name)
        msg += "\n size: {}".format(utils.bytes2human(self.size))
        msg += "\n type: {}".format(str(self.size))
        msg += "\n {}: {}".format(self.get_option("hash_function"), checksum)

        msg += "\n\nRun `cfmt archive --withdraw {}` to restore it".format(self.run.name)
        msg += "\n\n# vim: ft=markdown"
        
        with open(fpath, 'w') as f:
            f.write(msg)

    def generate_name(self, fmt=None, **kwargs):
        """ Generates an archive name according to fmt """
        if fmt is None:
            fmt = DEFAULT_NAME_FORMAT

        return fmt.format(**kwargs)
    
    
    @staticmethod 
    def get_status_headers(columns=None):
        """ Returns the header output for the status table """
        
        if columns is None:
            columns = Archive.default_status_columns

        header_mapping = {
            'id': utils.cstr('ID', fg=248),
            'size': utils.cstr('Size', fg=248),
            'type': utils.cstr('Type', fg=248),
            'created': utils.cstr('Created', fg=248),
            'last_modified': utils.cstr('Modified', fg=248),
            'description': utils.cstr('Description', fg=248),
        }
        
        status_headers = []
        columns = utils.make_list_unique(columns)

        for key in columns:
            if key in header_mapping:
                header = header_mapping[key]
            else:
                header = 'N/A'
            
            status_headers.append(header)

        return status_headers

    def get_status_row(self, columns=None, format_row_with=None):
        """ Returns a list of items to print in a table """

        from datetime import datetime
        
        row = []

        if columns is None:
            columns = Archive.default_status_columns

        columns = utils.make_list_unique(columns)
        
        if isinstance(self.created, datetime):
            created = utils.print_datetime(self.created)
        else:
            created = utils.cstr("N/A", fg=238)
        
        if isinstance(self.last_modified, datetime):
            modified = utils.print_datetime(self.last_modified, timeago=True)
        else:
            modified = utils.cstr("N/A", fg=238)
        
        row_mapping = {
            'id': str(self.id) if self.id is not None else "-",
            'size': utils.bytes2human(self.size) if self.size is not None else "-",
            'type': str(self.type),
            'created': created,
            'last_modified': modified,
            'description': str(self.description) if self.description is not None else "-",
        }

        for key in columns:
            if key in row_mapping:
                cell = row_mapping[key]
            else:
                cell = "N/A"

            row.append(cell)
        # apply to the entire row
        if format_row_with is not None:
            # remove existing formatting
            for item in row:
                base_string = utils.str_remove_ansi(str(item))
                row[row.index(item)] = format_row_with(base_string)
        
        # indent first row
        row[0] = utils.cstr("      ", attr=['hidden']) + row[0]

        return row

    def create(self, remote_path, host, description=None):
        """ Creates an archive """
        raise NotImplementedError
    
    def withdraw(self):
        """ Withdraws an archive """
        raise NotImplementedError
    
    def restore(self):
        """ Restores the archive to the associate run directory """
        raise NotImplementedError
    
    def delete(self, delete_data=True, force=None):
        """ Deletes the database record, optionally data as well """
        raise NotImplementedError
    
    def verify(self):
        """ Verifies the remote archive has not been tampered with """
        raise NotImplementedError
    
    def exists(self):
        """ Checks if an archive exists """
        raise NotImplementedError
    
    def list_contents(self):
        """ Lists the contents of an archive """
        raise NotImplementedError
    
    def download(self):
        """ Lists the contents of an archive """
        raise NotImplementedError

    @classmethod
    def init_repo(cls, host, path):
        """ Initialize a repository for the archive type """
        raise NotImplementedError
    
    @classmethod
    def get_repo_path(cls, base_path, case=None):
        """ Initialize a repository for the archive type """
        return os.path.join(base_path, str(cls.TYPE).lower())
