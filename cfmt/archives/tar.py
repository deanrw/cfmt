"""
    cft: A management tool for Computational Fluid Dynamics

    tar.py: For tar archives
"""
import os

from datetime import datetime

from cfmt.archives import base
from cfmt import exceptions
import cfmt.utils as utils 

class TarArchive(base.Archive):
    """
    Methods for creating/retrieving a tar archive
    """
    TYPE="tar"
    
    # default extraction tools/arguments
    _extract = {
        ".gz": "pigz",
    }

    def create(self, repo_path, host, description=None):
        """ Create an archive """
        assert hasattr(self, "run")

        if description is not None:
            self.description = str(description)
        
        self.logger.info("Creating %s archive for run %s ...", self.TYPE, self.run.name)

        # source is the run directory
        src = self.run.abspath

        # set the created
        self.created = datetime.now()

        # determine compression options
        ext = ".tar"
        if self.get_option("compress"):
            cfunc = self.get_option("compress_function")
            for _ext, func in self._extract.items():
                if func == cfunc:
                    ext += _ext

        
        # determine the filename
        fname = self.generate_name(
            name = os.path.basename(src),
            created = self.created.strftime(self.FILENAME_TIME_FMT),
            ext = ext 
        )
        rpath = os.path.join(repo_path, fname)

        self._tarpipe_up(src,rpath, host)

        self.hostname = host 
        self.path = repo_path
        self.name = fname
        # save the database
        try:
            self.create_db()
            self.save_db()
        except Exception as err:
            msg = "Cannot save database: " + str(err)
            raise exceptions.ArchiveFailed(msg)
        else:
            # set the id
            self.id = self.db_record.id
            
        if not self.get_option("keep_source"):
            # remove the source files
            self.remove_source(src)
            
            # add a marker
            self.add_readme(src)

        
        self.archive_created = True
        self.logger.info("Creating %s archive for run %s ... DONE", self.TYPE, self.run.name)
    
    def withdraw(self):
        """ Retrive an archive """
        assert hasattr(self, "run")
        assert self.hostname is not None
        assert self.path is not None
        assert self.name is not None
        
        src = self._get_full_path()
        dst = self.run.abspath
        host = self.hostname

        # check the archive still exists
        if not self.exists():
            msg = "Archive '{}' missing on host '{}'".format(src, host)
            raise exceptions.ArchiveFailed(msg)
        

        # check if the local directory has any extra junk in it
        if not self.get_option("force"):
            dst_files = os.listdir(dst)
            if len(dst_files) > 1:
                raise exceptions.ArchiveFailed("Unknown junk in the local run folder. Use --force to override")
    
        # get the data, extract
        self._tarpipe_down(src, dst, host)

        if not self.get_option("keep_archive_after_withdraw"):
            # we delete the source archive and the record
            self.delete(force=True)

        # remove the stub file 
        self.delete_readme()
    
    def restore(self, restore_to_empty=None):
        """ Restores the archive to the associate run directory """
        assert hasattr(self, "run")
        
        self.logger.info("Restoring %s archive %s for run %s", self.TYPE, str(self.id), self.run.name)

        if restore_to_empty is None:
            restore_to_empty = self.get_option("restore_to_empty")
        
        src = self._get_full_path()
        dst = self.run.abspath
        host = self.hostname
        
        # check the archive still exists 
        if not self.exists():
            msg = "Archive '{}' missing on host '{}'".format(src, host)
            raise exceptions.ArchiveFailed(msg)
        

        if restore_to_empty:
            # delete the directory
            try:
                utils.delete_dir_contents(dst)
            except IOError as err:
                utils.print_exception_info(err, no_print=True)
                raise exceptions.ArchiveFailed(err)
        
        # get the the data, extract
        self._tarpipe_down(src, dst, host)
        
        self.logger.info("Archive %s restored", str(self.id))


    def delete(self, delete_data=True, force=None):
        """ Deletes the database record, optionally data as well """
        import subprocess
        assert hasattr(self, "run")

        if force is None:
            force = self.get_option("force")

        if not self.archive_created:
            raise ValueError("Archive does not exist")

        if self.id == self.run.is_archived and not force:
            msg = "Archive {} was deposited for run {} ... ".format(self.id, self.run.name) + utils.SKIPPING + ". Use --force to delete anyway."
            raise exceptions.ArchiveFailed(msg)

        if delete_data:
            path = self._get_full_path()
            host = self.hostname
            fname = os.path.basename(path)
            remote_cmd = ["\'rm\'", "-r" , "\'" + path + "\'"]
            ssh = self.get_ssh_proc(host, remote_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            self.logger.info("Deleting archive '%s' ... ", os.path.basename(fname))
            returncode = ssh.wait()
            
            if returncode != 0:
                # test to see if we want to remove the database any way
                self.console.warning(" ---> Deleting archive '{}' ... ".format(os.path.basename(fname)) + utils.FAILED)
                query = utils.confirm("Do you want to remove the archive from the database anyway?")
                
                if not query:
                    self.logger.error("Deleting archive '{}' ... ".format(os.path.basename(fname)) + utils.FAILED)
                    raise exceptions.ArchiveFailed("Remote ssh command failed")
        
        self.logger.info("Deleting archive '%s' ... OK", os.path.basename(fname))

        # delete the database
        self.db.delete(db_record=self.db_record, cascade=False)
        self.logger.info("Deleted archive id: '%d'", self.id)

    def verify(self):
        """ Verifies the remote archive has not been tampered with """
        import subprocess
        from tqdm import tqdm 
        
        assert self.archive_created == True
        
        path = self._get_full_path()
        filehash = self.hash
        host = self.hostname
        filesize = self.size
                
        # check the archive still exists
        if not self.exists():
            msg = "Archive '{}' missing on host '{}'".format(path, host)
            raise exceptions.ArchiveFailed(msg)
        
        if filehash is None:
            msg = "Archive '{}' was not checksumed.".format(self.id)
            raise exceptions.ArchiveFailed(msg)

        
        # in both cases we return the filehash to stdout;
        # - pv output to stderr, md5sum to stdin
        hash_func = self.get_option("hash_function")

        passed=None

        if self.get_option("remote_pv"):
            remote_cmd = ["\'pv\'", "--bytes", "-n", "\'" + path + "\'", "|", "\'" + hash_func + "\'"]
            total = filesize
        else:
            remote_cmd = ["\'" + hash_func + "\'", path]
            total = None
        
        ssh = self.get_ssh_proc(host, remote_cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        base_desc = " ---> Processing "
        bar_fmt = "{l_bar}{bar}{r_bar}"
        with tqdm(desc=base_desc, position = 1, total=total, leave=True, ascii=True, unit_scale=True, unit_divisor=1024, unit='B', bar_format=bar_fmt) as pbar:
        # check the queues for output 
            while ssh.poll() is None:
                total = 0
                for line in ssh.stderr:
                    try:
                        new_total = int(line.decode('utf-8'))
                    except ValueError as err:
                        pbar.write("ERROR: " + str(err))
                    else:
                        pbar.update(new_total - total)
                        total = new_total

            returncode = ssh.wait()
            if returncode == 0:
                _hash = ssh.stdout.readline().decode('utf-8').rstrip().split()[0]
                passed = str(_hash) == str(filehash)
                msg = " ---> Archive {:3}: {}".format(self.id, utils.OK if passed else utils.FAILED)
                pbar.set_description(desc=msg)    

        if returncode != 0:
            raise exceptions.ArchiveFailed("Error running verify remote command")

        return passed
    
    def exists(self):
        """ Checks if an archive exists """
        import subprocess
        
        if not self.archive_created:
            return False

        filepath = self._get_full_path()

        # it should exist remotely, use ssh to check
        cmd = ["ssh", str(self.hostname), "test -f " + filepath]

        proc = self.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        stdout, stderr = proc.communicate()
        
        stdout_str = stdout.decode()
        if stdout_str:
            self.logger.info(stdout.decode())

        stderr_str = stderr.decode()
        if stderr_str:
            self.logger.error(stderr.decode())

        
        if proc.returncode == 0:
            return True
        else:
            return False

    def list_contents(self):
        """ Lists the contents of an archive """
        import subprocess
        
        assert self.archive_created == True
        
        path = self._get_full_path()
        host = self.hostname

        remote_cmd = ["\'tar\'", "-tvf", "\'" + path + "\'"]

        ssh = self.get_ssh_proc(host, remote_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        queue = utils.get_queue()
        reader = utils.AsynchronousFileReader(ssh.stdout, queue)

        while not reader.eof():
            while not queue.empty():
                line = queue.get()
                if line:
                    print(line.decode(), end="")
        
        reader.join()
        returncode = ssh.wait()

        # check the return code 
        if returncode != 0:
            print(utils.ERROR + ": " + ssh.stderr.read().decode(), end="")
            raise exceptions.ArchiveFailed("Failed to list archive. Command returned non-zero exit code")
    
    
    def download(self):
        """
        Download the archive
        """
        # -> use rsync transfer 
        assert self.archive_created == True
        assert self.hostname is not None
        assert self.name is not None
        assert self.path is not None

        fpath = self._get_full_path()
        host = self.hostname
        src = host + ":" + fpath
        dst = os.path.dirname(self.run.abspath)

        # check the archive still exists 
        if not self.exists():
            msg = "Archive '{}' missing on host '{}'".format(fpath, host)
            raise exceptions.ArchiveFailed(msg)
        
        # prompt if a download already exists
        lpath = os.path.join(dst, self.name)
        if os.path.exists(lpath):
            prompt = "Archive '{}' has already been downloaded, overwrite?".format(self.name)
            confirm = utils.confirm(prompt)

            if not confirm:
                raise exceptions.ArchiveFailed("Archive '{}' SKIPPING".format(self.id))

        self.logger.info("Downloading archive '%d' from '%s'", self.id, src)

        # rsync transfer 
        # TODO: Move this to utils
        assert hasattr(self.run, "rsync_transfer")

        desc = " ---> Archive {}".format(self.id)

        try:
            self.run.rsync_transfer(src, dst, bar_desc=desc, src_is_file=True)
        except exceptions.TransferFailed as err:
            utils.print_exception_info("Failed to downloaded archive '{}'".format(self.id), no_print=True)
            raise exceptions.ArchiveFailed(err)
        else:
            self.logger.info("Downloaded archive '%d'", self.id)
        
    def _get_full_path(self):
        """
        Return a full path to the archive file
        """
        return os.path.join(self.path, self.name)
    
    @classmethod 
    def check_repo(cls, case, host, path):
        """
        Check the repository exists

        For tar, we only need a case folder so we just create one if needed
        """
        assert isinstance(host, str), "Host should be a string"
        assert isinstance(path, str), "Path should be a string"

        # -> get the remote path
        repo_path = cls.get_repo_path(path, case)

        # -> create/check it exists
        cmd = ["ssh", host, "mkdir -p " + repo_path]
        proc = utils.run(cmd, capture_output=True)
        if proc.returncode != 0:
            raise exceptions.ArchiveFailed(proc.stderr.decode('utf-8'))
        else:
            return repo_path
    
    @classmethod 
    def init_repo(cls, host, path):
        """
        Initialize a repository for the archvie type
        """
        print("Archive type {} does not need initializing".format(cls.TYPE))
    
    @classmethod
    def get_repo_path(cls, base_path, case=None):
        """ Initialize a repository for the archive type """
        return os.path.join(base_path, str(cls.TYPE).lower(), case.path)

    def get_tar_extract_proc(self, wd, compress_function=None, **kwargs):
        """ 
        Returns a tar extraction subprocess 
        
        wd is the working directory from where the extraction will occur
        """
        cmd = []
        cmd.append("tar")

        if compress_function is not None:
            cmd.append("-I")
            cmd.append(compress_function)
        
        cmd.append("-xvC")
        cmd.append(wd)
        cmd.append("-f")
        cmd.append("-")

        self.logger.debug("Running command {}".format(" ".join(cmd)))
        return self.get_proc(cmd, **kwargs)

    def get_tar_create_proc(self, src, **kwargs):
        """ Returns a tar subprocess """

        cmd = []
        cmd.append("tar")
        if self.get_option("compress"):
            compress_func = self.get_option("compress_function")
            cmd.append("-I")
            cmd.append(compress_func)
        
        cmd.append("-cvf")
        cmd.append("-")
        cmd.append("-C")
        cmd.append(os.path.dirname(src))
        cmd.append(os.path.basename(src))

        self.logger.debug("Running command {}".format(" ".join(cmd)))
        return self.get_proc(cmd, **kwargs)
    
    def _tarpipe_up(self, src, dst, host, compress=None, checksum=None):
        """ Runs a tar pipe command """
        import subprocess
        import math
        import time
        
        from tqdm import tqdm 
        
        if compress is not None:
            self.set_option(compress=compress)
        
        if checksum is not None:
            self.set_option(checksum=checksum)

        proc = []
        
        # tar
        tar = self.get_tar_create_proc(src, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        proc.append(tar)

        # pipe viewer
        pv = self.get_pv_proc(stdin=tar.stdout, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        proc.append(pv)

        # ssh remote command
        if self.get_option("checksum"):
            hash_func = self.get_option("hash_function")
            # we tee the input to both a file and the chosen hash function
            remote_cmd = ["\'tee\'", dst, "|", "\'" + hash_func + "\'"] 
        else:
            # we cat the input to a file
            remote_cmd = ["\'cat\'", ">", "\'" + dst + "\'"]
        
        # get the size (done by PV)
        #remote_cmd += ["&&", "\'stat\'", "-c", "%s", dst] 
        
        ssh = self.get_ssh_proc(host, remote_cmd, stdin=pv.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.append(ssh)

        # launch asynchronous readers 
        pv_queue = utils.get_queue()
        pv_reader = utils.AsynchronousFileReader(pv.stderr, pv_queue)
        tar_queue = utils.get_queue()
        tar_reader = utils.AsynchronousFileReader(tar.stderr, tar_queue)
        
        readers = [pv_reader, tar_reader]

        try:
            base_desc = " ---> Processing "
            bar_fmt = "{l_bar}{bar}{r_bar}"
            with tqdm(desc=base_desc, position = 1, total=10, leave=True, ascii=True, unit_scale=True, unit_divisor=1024, unit='B', bar_format=bar_fmt) as pbar:
            # check the queus for output 
                total = 0
                while not pv_reader.eof() or not tar_reader.eof():
                    # what have we got from pv
                    while not pv_queue.empty():
                        line = pv_queue.get()
                        try:
                            new_total = int(line.decode('utf-8'))
                        except ValueError as err:
                            pbar.write(utils.ERROR + ":" + str(err))
                        else:
                            pbar.update(new_total - total)
                            total = new_total
                            pbar.total = 2.5**(math.ceil(math.log(total, 2.5)))

                    # what have we got from tar
                    while not tar_queue.empty():
                        line = tar_queue.get().decode('utf-8').rstrip()
                        fname = os.path.basename(line)
                        if len(fname) > 20:
                            fname = '{}~{}'.format(fname[:3], fname[-16:])
                        desc = base_desc + "{:<20}".format(fname)
                        pbar.set_description(desc=desc)
                        self.logger.debug(line)
                    
                    time.sleep(0.1)
                
                # set the final total and pbar
                pbar.total = new_total
                desc = " ---> Archive " + "{:<20}".format(" ... " + utils.OK)
                pbar.set_description(desc=desc)
                filesize = new_total
        except KeyboardInterrupt:
            pbar.write("Caught CTRL-C ... terminating")
            # kill the subprocesses
            [p.kill() for p in proc]

            # join the threads and exit
            [t.join() for t in readers]
            sys.exit(1)

        # join the threads
        [t.join() for t in readers]
        
        # make sure all processes are finished
        [p.wait() for p in proc]

        # check for non-zero return codes 
        error_msgs = []
        for p in proc:
            error = False
            if p.returncode != 0:
                # grab any remaining stderr
                stderr = p.stderr.read().decode('utf-8')
                msg = "ERROR running subprocess "
                if stderr:
                    msg += stderr
                
                self.console.error(msg)
                self.logger.error(msg)
                error = True
                
        if error:
            raise exceptions.ArchiveFailed("Subprocess error")
        

        if self.get_option("checksum"):
            line = ssh.stdout.readline().decode('utf-8').rstrip().split()
            filehash = line[0]
            filename = line[1]
        
            # TODO: filehash length should depend on the hash function used
            if len(filehash) != 32:
                raise exceptions.ArchiveFailed("Checksum is invalid '%s'", str(filehash))

            # set the filehash
            self.hash = filehash
        
        # set the size
        self.size = filesize 
    
    def _tarpipe_down(self, src, dst, host):
        """ 
        Construct a tarpipe down to retrive an archive 
        src - the source tarball 
        dst - the local folder to extract into
        """
        import pipes
        import subprocess
        from tqdm import tqdm 
        
        # attempt to determine the compression type from the suffix
        ext = os.path.splitext(src)[1]
        if ext in self._extract:
            c_func = self._extract[ext]
        else:
            c_func = None

        proc = []
        named_pipes = []
        fds = []

        # ssh remote command
        remote_cmd = ["\'cat\'","\'" + src + "\'"]
        ssh = self.get_ssh_proc(host, remote_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.append(ssh)

        # pipe viewer 
        pv = self.get_pv_proc(stdin=ssh.stdout,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.append(pv)
        
        do_checksum = self.hash is not None and self.get_option("checksum")
        if do_checksum:
            # open a named pipe
            tpipe = utils.get_named_pipe()
            named_pipes.append(tpipe)
            self.logger.debug("Created named pipe '%s", tpipe)
            # set-up tee
            tee = self.get_proc(["tee", tpipe], stdin=pv.stdout, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
            proc.append(tee) 

            # set-up md5sum
            hash_func = self.get_option("hash_function")
            fhash = open(tpipe, 'rb',0)
            fds.append(fhash)
            hashp = self.get_proc([hash_func], stdin=fhash, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            proc.append(hashp)
            stdin = tee.stdout
        else:
            stdin = pv.stdout
        
        # tar extract
        tar = self.get_tar_extract_proc(
            os.path.dirname(dst), 
            compress_function=c_func,
            stdin=stdin,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        proc.append(tar)

        #stdout, stderr = tar.communicate()

        # launch asynchronous readers 
        pv_queue = utils.get_queue()
        pv_reader = utils.AsynchronousFileReader(pv.stderr, pv_queue)
        tar_queue = utils.get_queue()
        tar_reader = utils.AsynchronousFileReader(tar.stdout, tar_queue)
        
        readers = [pv_reader, tar_reader]

        base_desc = " ---> Processing "
        bar_fmt = "{l_bar}{bar}{r_bar}"
        with tqdm(desc=base_desc, position = 1, total=self.size, leave=True, ascii=True, unit_scale=True, unit_divisor=1024, unit='B', bar_format=bar_fmt) as pbar:
        # check the queus for output 
            total = 0
            while not pv_reader.eof() or not tar_reader.eof():
                # what have we got from pv
                while not pv_queue.empty():
                    line = pv_queue.get()
                    try:
                        new_total = int(line.decode('utf-8'))
                    except ValueError as err:
                        pbar.write("ERROR: " + str(err))
                    else:
                        pbar.update(new_total - total)
                        total = new_total

                # what have we got from tar
                while not tar_queue.empty():
                    line = tar_queue.get().decode('utf-8').rstrip()
                    fname = os.path.basename(line)
                    if len(fname) > 20:
                        fname = '{}~{}'.format(fname[:3], fname[-16:])
                    desc = base_desc + "{:<20}".format(fname)
                    pbar.set_description(desc=desc)
                    self.logger.debug(line)
                    

            # join the threads
            [t.join() for t in readers]
        
            # make sure all processes are finished
            [p.wait() for p in proc]

            # make sure all files are closed
            [f.close() for f in fds]

            # remove all pipes
            [utils.rmdir(os.path.dirname(p)) for p in named_pipes]

            # check for non-zero return codes 
            error_msgs = []
            for p in proc:
                error = False
                if p.returncode != 0:
                    # grab any remaining output
                    stderr, stdout = p.communicate()
                    msg = utils.ERROR + ": subprocess: " + stderr.decode().rstrip()
                    error_msgs.append(msg)
                    error = True
                    pbar.write(msg)
            if error:
                [self.logger.error(m) for m in error_msgs]
                raise exceptions.ArchiveFailed("Subprocess error")

            base_desc = " ---> Archive {}".format(self.id)
            desc = base_desc + "{:20}".format(" ... " + utils.OK)
            if do_checksum:
                # read from the hashfile
                line = hashp.stdout.readline().decode('utf-8').rstrip().split()
                filehash = line[0]

                if filehash != self.hash:
                    pbar.write(utils.WARNING + ": Checksum failed for archive {}".format(self.id))
                    desc = base_desc + "{:20}".format(" ... " + utils.ERROR)
            
            pbar.set_description(desc=desc)

                
        # set the created
        #self.created = datetime.now()

        #if self.get_option("checksum"):
        #    line = ssh.stdout.readline().decode('utf-8').rstrip().split()
        #    filehash = line[0]
        #    filename = line[1]
        
        #    # TODO: filehash length should depend on the hash function used
        #    if len(filehash) != 32:
        #        raise exceptions.ArchiveFailed("Checksum is invalid '%s'", str(filehash))

        #    # set the filehash
        #    self.filehash = filehash
        
        # set the size
        #self.size = filesize 