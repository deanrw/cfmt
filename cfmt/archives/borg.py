"""
    cft: A management tool for Computational Fluid Dynamics

    borg.py: For borg based archives
"""
import os

from cfmt.archives.base import Archive
from cfmt import exceptions, utils

class BorgArchive(Archive):
    """
    Methods for creating/retriving a 
    """
    TYPE="borg"
    ENCRYPTION="none"
    COMPRESS_FUNCTION = "zlib"
    EXPORT_EXT = ".tar.gz"

    VALID_COMPRESSION = ["zlib", "lzma"]

    def create(self, repo_path, host, description=None):
        """ Create an archive """
        from datetime import datetime
        
        assert hasattr(self, "run")

        if description is not None:
            self.description = str(description)

        self.logger.info("Creating %s archive for run %s ...", self.TYPE, self.run.name)
        # set the source source
        src = self.run.abspath

        # set created date
        self.created = datetime.now()

        # determine the archive name
        aname = self.generate_name(
            name = self.run.name,
            created = self.created.strftime(self.FILENAME_TIME_FMT),
            ext = ''
        )

        # check the compression
        if self.get_option("compress"):
            compress = self.get_option("compress_function")
            if compress not in self.VALID_COMPRESSION:
                raise exceptions.ArchiveFailed("Compression type '{}' not supported".format(compress))
                
        # add the archive
        filehash, size = self._borg_create(
            src,
            aname,
            repo_path,
            host,
            wd=os.path.dirname(src),
            compress=compress
        )

        # store the attributes
        self.hostname = host if host is not None else ""
        self.path = self.get_archive_path(repo_path, host)
        self.name = aname
        self.size = size
        self.hash = filehash

        # save the database
        try:
            self.create_db()
            self.save_db()
        except Exception as err:
            msg = "Cannot save database: " + str(err)
            raise exceptions.ArchiveFailed(msg)
        else:
            # set the id
            self.id = self.db_record.id
        
        if not self.get_option("keep_source"):
            # remove the source files
            self.remove_source(src)
            
            # add a marker
            self.add_readme(src)
    
        self.archive_created = True
        self.logger.info("Creating %s archive for run %s ... DONE", self.TYPE, self.run.name)
    
    def list_contents(self):
        """ List the contents of an archive """

        assert self.archive_created == True

        path = self.path
        host = self.hostname
        name = self.name

        self._borg_list(host, path, name)
    
    def withdraw(self):
        """ Retrieve an archive """
        self.restore(restore_to_empty=True)

        if not self.get_option("keep_archive_after_widthdraw"):
            # we delete the source archive and the record
            self.delete(force=True)

    
    def restore(self, restore_to_empty=None):
        """ Restores the archive to the associate run directory """
        assert hasattr(self, "run")
        assert self.path is not None
        assert self.name is not None
        
        if self.hostname is not None:
            assert isinstance(self.hostname, str)

        if restore_to_empty is None:
            restore_to_empty = self.get_option("restore_to_empty")
        
        self.logger.info("Restoring %s archive %s for run %s", self.TYPE, str(self.id), self.run.name)
        
        # check the archive still exists 
        if not self.exists():
            msg = "Archive '{}' missing on host '{}'".format(self.path, self.hostname)
            raise exceptions.ArchiveFailed(msg)
            
        dst = self.run.abspath
        host = self.hostname
        repo_path = self.path
        aname = self.name
        
        src = self.get_archive_uri(host, repo_path, aname)

        if restore_to_empty:
            # delete the directory
            try:
                utils.delete_dir_contents(dst)
            except IOError as err:
                utils.print_exception_info(err, no_print=True)
                raise exceptions.ArchiveFailed(err)
        
        # extract the data
        self._borg_extract(src, dst, wd=os.path.dirname(dst), id=self.id)
        
        self.logger.info("Archive %s restored", str(self.id))

    def delete(self, delete_data=True, force=None):
        """ Deletes the database record, optionally data as well """
        assert hasattr(self, "run")

        if force is None:
            force = self.get_option("force")

        if not self.archive_created:
            raise exceptions.ArchiveFailed("Archive {} does not exist".format(self.id))

        # prevent deletion of an archive is it is marked as deposited
        if self.id == self.run.is_archived and not force:
            msg = "Archive {} was deposited for run {} ... ".format(self.id, self.run.name) + utils.SKIPPING + ". Use --force to delete anyway."
            raise exceptions.ArchiveFailed(msg)

        if delete_data:
            if self.hostname is not None:
                assert isinstance(self.hostname,str)
            assert self.path is not None
            assert self.name is not None

            host = self.hostname
            repo = self.path
            aname = self.name
            
            archive_uri = self.get_archive_uri(host, repo, aname)
            
            self.logger.info("Deleting archive '%s' ... ", os.path.basename(aname))

            # try and delete
            try:
                self._borg_delete(archive_uri)
            except exceptions.ArchiveFailed as err:
                self.console.warning(" ---> Deleting archive '{}' ... ".format(os.path.basename(aname)) + utils.FAILED)
                utils.print_exception_info(err)
                # prompt to see if we want to delete database anyway
                query = utils.confirm("Do you want to remove the archive from the database anyway?")
                if not query:
                    self.logger.error("Deleting archive '%s' ... FAILED", os.path.basename(aname))
                    raise exceptions.ArchiveFailed()

            else:
                self.logger.info("Deleting archive '%s' ... OK", os.path.basename(aname))

        # delete the database
        self.db.delete(db_record=self.db_record, cascade=False)
        self.logger.info("Deleted archive '%s'", self.id)
    
    def exists(self):
        """
        Determines whether the archive still exists
        """
        if not self.archive_created:
            return False
        
        host = self.hostname
        repo = self.path
        aname = self.name
        
        archive_uri = self.get_archive_uri(host, repo, aname)

        # try and get the repository info
        try:
            self._borg_info(archive_uri)
        except exceptions.ArchiveMissing:
            return False

        return True

    def verify(self):
        """
        Verifies the integrity of the archive
        """
        assert self.archive_created == True
        assert self.path is not None
        assert self.name is not None

        host = self.hostname
        aname = self.name
        repo_path = self.path

        archive_uri = self.get_archive_uri(host, repo_path, aname)

        # run borg check
        self.logger.info("Verifying archive '%s' ... ", str(self.id))

        return self._borg_check(archive_uri, id=self.id)

    
    def download(self):
        """
        Download the archive

        makes use of borg export-tar
        """
        assert self.archive_created == True
        assert self.path is not None
        assert self.name is not None

        host = self.hostname
        aname = self.name
        repo_path = self.path

        src = self.get_archive_uri(host, repo_path, aname)
        dstdir = os.path.dirname(self.run.abspath)
        lpath = os.path.join(dstdir, self.name + self.EXPORT_EXT)

        # prompt is the download already exists
        # prompt if a download already exists
        if os.path.exists(lpath):
            prompt = "Archive '{}' has already been downloaded, overwrite?".format(self.name)
            confirm = utils.confirm(prompt)
            
            if not confirm:
                raise exceptions.ArchiveFailed("Archive '{}' SKIPPING".format(self.id))

        self.logger.info("Downloading archive '%d' from '%s'", self.id, src)

        # borg export
        self._borg_export_tar(src, lpath, id=self.id)




    @classmethod 
    def check_repo(cls, case, host, path):
        """
        Check the repository exists

        """
        repo_path = cls.get_repo_path(path, case)

        cls.logger.info("Checking for existing borg repo at '%s'", repo_path)

        # check borg list
        try:
            cls._borg_list_repo(host, repo_path)
        except exceptions.ArchiveRepoMissing as err:
            raise err
        else:
            return repo_path

    
    @classmethod
    def init_repo(cls, host, path):
        """
        Initialize a borg repository
        """ 
        # -> get the remote path
        repo_path = cls.get_repo_path(path)

        print(" --> Initializing Borg repository ...", end="\r")
        # -> initialize the repo (creates parent directories)
        try:
            cls._borg_init(host, repo_path)
        except exceptions.ArchiveRepoExists as err:
            print(" --> Initializing Borg repository ... " + utils.FAILED, end="\n")
            print("Borg repository already exists")
        except exceptions.ArchiveFailed as err:
            print(" --> Initializing Borg repository ... " + utils.ERROR, end="\n")
            utils.print_exception_info(err)
        else:
            print(" --> Initializing Borg repository ... " + utils.OK, end="\n")
    
    @classmethod
    def _borg_info(cls, uri):
        """
        Wrapper for borg info host:URI
        """
        import subprocess
        borg = cls.get_borg_exec()
        
        cmd = borg + ["info", uri]

        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # wait and grab the remaining output
        stdout, stderr = proc.communicate()

        if proc.returncode != 0:
            msg = utils.ERROR + ": Borg: " + stderr.decode().rstrip()
            cls.logger.error(msg)
            if msg.find("does not exist") != -1:
                raise exceptions.ArchiveMissing(msg)
            else:
                raise exceptions.ArchiveFailed(msg)
        
        return stdout


    @classmethod
    def _borg_list(cls, host, repo_path, name):
        """
        Wrapper for borg list REPO::ARCHIVE
        """
        import subprocess
        borg = cls.get_borg_exec()

        cmd = borg + ["list", cls.get_archive_uri(host, repo_path, name)]

        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        queue = utils.get_queue()
        reader = utils.AsynchronousFileReader(proc.stdout, queue)

        while not reader.eof():
            while not queue.empty():
                line = queue.get()
                if line:
                    print(line.decode(), end="")
        
        reader.join()
        returncode = proc.wait()

        # check the return code 
        if returncode != 0:
            print(utils.ERROR + ": " + proc.stderr.read().decode(), end="")
            raise exceptions.ArchiveFailed("Failed to list archive. Command returned non-zero exit code")

    @classmethod
    def _borg_check(cls, archive_uri, archives_only=True, id=None):
        """
        Wrapper for borg check
        """
        import subprocess
        import json
        from tqdm import tqdm
        _id = "" if id is None else str(id)

        borg = cls.get_borg_exec()

        cmd = borg + ["check", "-v", archive_uri]

        # add progress and log-json
        cmd.append("--log-json")
        cmd.append("--progress")

        if archives_only:
            cmd.append("--archives-only")

        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # launch asynchronous readers 
        stderr_queue = utils.get_queue()
        stderr_reader = utils.AsynchronousFileReader(proc.stderr, stderr_queue)

        base_desc = " ---> Processing "
        bar_fmt = "{l_bar}{bar}{r_bar}"

        passed = None
        # pbar only if we're doing a repo check


        with tqdm(desc=base_desc, position = 1, total=1, leave=True, ascii=True, bar_format=bar_fmt) as pbar:
            # check the queues for output
            total = 0
            while not stderr_reader.eof():
                # what have we got from tar
                while not stderr_queue.empty():
                    line = stderr_queue.get().decode('utf-8').rstrip()
                    data = json.loads(line)

                    if data["type"] == "progress_percent":
                        if data["finished"] is not True:
                            pbar.total = int(data["total"])
                            new_total = int(data["current"])
                            if not archives_only:
                                pbar.update(new_total - total)
                            total = new_total
                    elif data["type"] == "log_message":
                        # write any mesages to the log
                        if "message" in data and "levelname" in data:
                            msg = data["message"]
                            _logger = getattr(cls.logger, data["levelname"].lower())
                            _logger(msg)
            # check the exit codes
            proc.wait()
            if proc.returncode == 0:
                if archives_only:
                    pbar.total = 1
                    pbar.update(1)
                passed = True
                out = " ---> Archive {:3}: {}".format(_id, utils.OK)
                pbar.set_description(desc=out)
            else:
                passed = False
                out = " ---> Archive {:3}: {}".format(_id, utils.FAILED)
                pbar.set_description(desc=out)

        return passed


    @classmethod
    def _borg_list_repo(cls, host, repo_path):
        """
        Wrapper for borg list
        """
        import subprocess
        borg = cls.get_borg_exec()

        cmd = borg + ["list", cls.get_remote_repo_path(host, repo_path)]
        
        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        # wait and grab any remaining output
        stdout, stderr = proc.communicate() 

        if proc.returncode != 0:
            # check for the string 'does not exist'
            msg = stderr.decode()
            _msg = utils.ERROR + ": Borg: " + msg.rstrip()
            cls.logger.debug("Borg error: %s", _msg)
            if msg.find('does not exist') != -1:
                raise exceptions.ArchiveRepoMissing(repo_path)
            else:
                raise exceptions.ArchiveFailed(msg)

        return stdout.decode()

    @classmethod
    def _borg_init(cls, host, repo_path):
        """
        Wrapper for borg init
        """
        import subprocess
        cls.logger.debug("Initializing borg repository at '%s'", repo_path)
        borg = cls.get_borg_exec()
        
        cmd = borg + ["init"] 
        
        # encryption
        cmd.append("--encryption")
        cmd.append(cls.ENCRYPTION)

        cmd.append("--make-parent-dirs")

        # create the repo path
        _cmd = cls.get_remote_repo_path(host, repo_path)

        # add the host
        cmd.append(_cmd)

        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        # wait and grab any remaining output
        stdout, stderr = proc.communicate()
        
        if proc.returncode != 0:
            msg = utils.ERROR + ": Borg: " + stderr.decode().rstrip()
            if msg.find("A repository already exists") != -1:
                raise exceptions.ArchiveRepoExists(msg)
            else:
                raise exceptions.ArchiveFailed(msg)

    @classmethod
    def get_remote_repo_path(cls, host, repo_path):
        """
        Returns a repo cmd suitable for borg cmd line
        """
        assert isinstance(repo_path, str)
        if host is None:
            return repo_path
        else:
            assert isinstance(host, str)
            return host + ":" + repo_path
    
    @classmethod
    def get_archive_uri(cls, host, repo_path, archive_name):
        """
        Return a URI of the form
        host:{repo_path}::{archive_name}
        """
        assert isinstance(repo_path, str)
        assert isinstance(archive_name, str)
        
        if host is not None:
            assert isinstance(host, str)
        
        return cls.get_remote_repo_path(host, repo_path) + "::" + archive_name

        
    @classmethod
    def get_borg_exec(cls):
        """
        Returns the path to the borg executable
        Raises an exception if it doesnt exist
        """
        import shutil
        import cfmt.config as config
        
         
        borg = shutil.which("borg")

        if borg is None:
            raise exceptions.ArchiveFailed("Cannot find borg executable")
        
        exc = [borg]        
        
        borg_remote_path = config.configtool.get('archive', 'borg_remote_path', fallback=None)
        if borg_remote_path:
            exc += ["--remote-path", borg_remote_path]

        
        return exc

    @classmethod
    def _borg_create(cls, src, name, repo, host, wd=None, compress=None):
        """ Create a borg archive """
        import subprocess
        import json
        import time
        from tqdm import tqdm
        # check borg exists
        borg = cls.get_borg_exec()
        
        # assemble the command
        cmd = borg + ["create"]
        
        # comrpession
        if compress is not None:
            assert compress in cls.VALID_COMPRESSION
            cmd.append("--compression")
            cmd.append(compress)
        
        # -> switch on json
        cmd.append("--log-json")
        
        # -> show progress
        cmd.append("--progress")

        # -> list files
        cmd.append("--list")
        cmd.append("--stats")
                
        # get the archive URL
        cmd.append(cls.get_archive_uri(host, repo, name))

        # add the source basename
        cmd.append(os.path.basename(src))

        # get the proc
        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=os.path.dirname(src))
        
        # launch asynchronous readers 
        stderr_queue = utils.get_queue()
        stderr_reader = utils.AsynchronousFileReader(proc.stderr, stderr_queue)
        log_queue = utils.get_queue()

        base_desc = " ---> Processing "
        bar_fmt = "{l_bar}{bar}{r_bar}"
        size = 0
        with tqdm(desc=base_desc, position = 1, total=10, leave=True, ascii=True, bar_format=bar_fmt) as pbar:
        # check the queus for output 
            total = 0
            while not stderr_reader.eof():
                # what have we got from tar
                while not stderr_queue.empty():
                    line = stderr_queue.get().decode('utf-8').rstrip()
                    data = json.loads(line)
                    cls.logger.debug(str(data))

                    if data["type"] == "archive_progress":
                        _size = data.get("deduplicated_size", None)
                        if _size is not None:
                            size = _size
                    elif data["type"] == "progress_percent":
                        if data["finished"] != True:
                            pbar.total = int(data["total"])
                            new_total = int(data["current"])
                            pbar.update(new_total - total)
                            total = new_total
                    elif data["type"] == "file_status":
                        _path = data.get("path", None)
                        if _path:
                            fname = os.path.basename(_path)
                            if len(fname) > 20:
                                fname = '{}~{}'.format(fname[:3], fname[-16:])
                            desc = base_desc + "{:<20}".format(fname)
                            pbar.set_description(desc=desc)
                    elif data["type"] == "log_message":
                        # stash them for later
                        log_queue.put(data)
                        
                        # write any mesages to the log
                        if "message" in data and "levelname" in data:
                            _logger = getattr(cls.logger, data["levelname"].lower())
                            _logger(data["message"])

                time.sleep(0.1)
            
            # set the final total and pbar
            desc = " ---> Archive " + "{:<20}".format(" ... " + utils.OK)
            pbar.set_description(desc=desc)
            filesize = size
        
        # make sure all processes are finished 
        stderr_reader.join()
        proc.wait()

        # get any remaining log messages
        error_msg = []
        filehash = None
        while not log_queue.empty():
            line = log_queue.get()
            if line["name"] == "borg.output.stats":
                msg = line.get("message", "")
                if msg.find("Archive fingerprint:") != -1:
                    filehash = msg.split()[-1]
            if "levelname" in line and line["levelname"] in ["WARNING", "ERROR", "CRITICAL"]:
                error_msg.append(line.get("message", ""))
        
        # check the return code
        if proc.returncode != 0:
            msg = utils.ERROR + ": Borg: " + "\n".join(error_msg)
            raise exceptions.ArchiveFailed(msg)
        
        # return the size and has
        return (filehash, filesize)
    
    @classmethod
    def _borg_delete(cls, archive_uri):
        """ Deletes a borg archive uri """
        import subprocess
        borg = cls.get_borg_exec()

        cmd = borg + ["delete", archive_uri]

        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # wait and grab output 
        stdout, stderr = proc.communicate()

        cls.logger.debug(stdout.decode())

        if proc.returncode != 0:
            cls.logger.error(stderr.decode())
            raise exceptions.ArchiveFailed(stderr.decode())


    @classmethod
    def _borg_extract(cls, archive_uri, dst, wd=None, id=None):
        """ Create a borg archive """
        import subprocess
        import json
        from tqdm import tqdm
        # check borg exist
        borg = cls.get_borg_exec()
        
        # assemble the command
        cmd = borg + ["extract"]
        
        # -> switch on json
        cmd.append("--log-json") 
        
        # -> list files
        cmd.append("--list")

        # -> show progress
        cmd.append("--progress")
                
        # get the archive URL
        cmd.append(archive_uri)

        # get the proc
        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=wd)
        
        # launch asynchronous readers 
        queue = utils.get_queue()
        reader = utils.AsynchronousFileReader(proc.stderr, queue)
        stderr_log = utils.get_queue()

        base_desc = " ---> Processing "
        bar_fmt = "{l_bar}{bar}{r_bar}"
        with tqdm(desc=base_desc, position = 1, total=10, leave=True, unit_scale=True, 
                  unit='B', unit_divisor=1024, ascii=True, bar_format=bar_fmt) as pbar:
        # check the queus for output 
            total = 0
            while not reader.eof():
                while not queue.empty():
                    line = queue.get().decode('utf-8').rstrip()
                    data = json.loads(line)
                    #cls.logger.debug(line)

                    if data["type"] == "progress_percent":
                        if data["finished"] != True:
                            pbar.total = int(data["total"])
                            new_total = int(data["current"])
                            pbar.update(new_total - total)
                            total = new_total
                    elif data["type"] == "log_message":
                        cls.logger.debug(data["message"])
                        if data["name"] == "borg.output.list":
                            fname = os.path.basename(data["message"])
                            if len(fname) > 20:
                                fname = '{}~{}'.format(fname[:3], fname[-16:])
                
                            desc = base_desc + "{:<20}".format(fname)
                            pbar.set_description(desc=desc)
                        else:
                            stderr_log.put(data)
            
            # join the threads
            reader.join()

            # grab stderr
            stdout, stderr = proc.communicate()
            
            error_msg = []
            while not stderr_log.empty():
                line = stderr_log.get()
                msg = line.get("message", "")
                if "levelname" in line:
                    logger = getattr(cls.logger, line["levelname"].lower())
                    logger(msg)
                    if line["levelname"] in ["WARNING", "ERROR", "CRITICAL"]:
                        error_msg.append(msg)

            # check for non-zero return code
            if proc.returncode != 0:
                # dump stderr 
                msg = utils.ERROR + ": Borg: " + "\n".join(error_msg)
                pbar.write(msg)
                raise exceptions.ArchiveFailed("\n".join(error_msg))
            
            base_desc = " ---> Archive {}".format(id if id is None else "")
            desc = base_desc + "{:20}".format(" ... " + utils.OK)

            pbar.set_description(desc=desc)

    @classmethod
    def _borg_export_tar(cls, archive_uri, dst, id=None):
        """ Create a borg archive """
        import subprocess
        import json
        from tqdm import tqdm
        # check borg exists
        borg = cls.get_borg_exec()
        
        # assemble the command
        cmd = borg + ["export-tar", "-v"]
        
        # -> switch on json
        cmd.append("--log-json") 
        
        # -> show progress
        cmd.append("--progress")
                
        # get the archive URL
        cmd.append(archive_uri)

        # append the destination
        cmd.append(dst)

        # get the proc
        proc = cls.get_proc(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        # launch asynchronous readers 
        queue = utils.get_queue()
        reader = utils.AsynchronousFileReader(proc.stderr, queue)
        error_msg = []

        base_desc = " ---> Downloading "
        bar_fmt = "{l_bar}{bar}{r_bar}"
        with tqdm(desc=base_desc, position = 1, total=10, leave=True, unit_scale=True, 
                  unit='B', unit_divisor=1024, ascii=True, bar_format=bar_fmt) as pbar:
        # check the queus for output 
            total = 0
            while not reader.eof():
                while not queue.empty():
                    line = queue.get().decode('utf-8').rstrip()
                    data = json.loads(line)
                    cls.logger.debug(line)

                    if data["type"] == "progress_percent":
                        if data["finished"] != True:
                            pbar.total = int(data["total"])
                            new_total = int(data["current"])
                            pbar.update(new_total - total)
                            total = new_total
                        if data["msgid"] == "extract":
                            info = data.get("info", None)
                            if isinstance(info, list):
                                fname = os.path.basename(data["info"][0])
                                if len(fname) > 20:
                                    fname = '{}~{}'.format(fname[:3], fname[-16:])
                
                                desc = base_desc + "{:<20}".format(fname)
                                pbar.set_description(desc=desc)
                    elif data["type"] == "log_message":
                        if "levelname" in data:
                            msg = data["message"]
                            _logger = getattr(cls.logger, data["levelname"].lower())
                            _logger(msg)
                            if data["levelname"] in ["WARNING", "ERROR", "CRITICAL"]:
                                error_msg.append(msg)
            
            # join the threads
            reader.join()

            # grab remaining output
            stdout, stderr = proc.communicate()

            # check for non-zero return code
            if proc.returncode != 0:
                # dump stderr 
                msg = utils.ERROR + ": Borg: " + "\n".join(error_msg)
                pbar.write(msg)
                raise exceptions.ArchiveFailed("\n".join(error_msg))
            
            base_desc = " ---> Archive {}".format(id if id is not None else "")
            desc = base_desc + " " + utils.OK

            pbar.set_description(desc=desc)