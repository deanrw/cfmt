"""
    cft: A management tool for Computational Fluid Dynamics

    notes.py: For note taking
"""
from datetime import datetime

from cfmt.entities.base import BaseEntity
from cfmt import exceptions

class Note(BaseEntity):
    """
    The Note Class
    """
    db_fields = [
        'id',
        'case',
        'last_modified',
        'created',
        'text'
    ]

    def __init__(self, case=None, text=None, db_record=None, db=None):
        """ Class constructor """

        self.text = ""

        if db_record is not None:
            self.db_record = db_record
            for db_field in self.db_fields:
                record = getattr(db_record, db_field)
                setattr(self, db_field, record)

            if db is None:
                raise exceptions.NoDB("Notes requires access to the database controller")

            self.db = db

            # fetch the case if we're not passed it
            if self.case is None:
                raise NotImplementedError

        else:
            if case is None:
                raise exceptions.NotACase()

            self.case = case

            if db is None:
                self.db = self.case.db
            else:
                self.db = db

            if text:
                # generate the db record now
                self.text = text
                self.create()


    def set_text(self, text):
        """ Updates the text in the note """
        self.text = text
        self.db_record.text = text
    
    def save(self):
        """ Saves the note to the db """
        if self.db_record is not None:
            self.db_record.save()

    def create(self):
        """ Create the note """
        text = self.text

        if text is None:
            raise ValueError("Cannot add note with no text")

        self.db_record = self.db.new_note(
            case=self.case.db_record,
            last_modified=datetime.today(),
            created=datetime.today(),
            text=text,
        )

        # add the entry to the db
        self.db.save(self.db_record)
    
    def __str__(self):
        """ Return the string representation """
        return self.text
