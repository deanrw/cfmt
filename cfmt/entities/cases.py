"""
    cft: A management tool for Computational Fluid Dynamics
"""
import os
import sys
import shutil
import logging
from datetime import datetime
import cfmt.config as config
import cfmt.utils as utils
from cfmt.utils import cprint,cstr
from cfmt.entities.base import BaseEntity
from cfmt import exceptions

def load_case(path, study=None):
    """ Load a case from the given path, return the case object if it exists """
    if path is None:
        path = os.getcwd()
    else:
        path = os.path.normpath(path)
        
    if study is not None:
        rel_path = study.get_rel_path(os.path.abspath(path))
    else:
        raise NotImplementedError

    # load the database
    from cfmt.db import controller as dbc
    
    if study.db is not None:
        db = study.db
    else:
        db = dbc.load_database(study.path)

    
    # fetch the case from the database
    case = db.get_case(path=rel_path)

    return Case(db_record=case, study=study)

class Case(BaseEntity):
    """
    The Case class
    """
    info_fields = [
        'name',
        'path',
        'study',
        'created',
        'last_modified',
        'creator',
        'study_name',
        'study_path',
        'run_number',
        'code_db'
    ]
    db_fields = [
        'id',
        'name',
        'path',
        'study',
        'code_db',
        'created',
        'last_modified',
        'creator',
    ]

    status_fields = [
        'name',
        'last_modified',
        'code',
    ]

    template = """
        === CFMT Case ===
        Case name           : {name}
        Part of Study       : {study_name}
        Number of runs      : {run_number}
        Code                : {code_db}
        Study path          : '{study_path}'
        Created             : {created}
        Last modified       : {last_modified}
        Created by          : {creator}
        """
    
    # the initial message to add to the editor
    initial_note_message = ("\n# Enter your note. Lines starting with '#' will be ignored"
                            "\n# and an empty message aborts the note"
                            "\n#\n# case: {case_name}"
                            "\n# study: {study_name}"
                            "\n# vim: ft=markdown"
                           )

    def __init__(self, db_record=None, db=None, study=None, 
                 path=None, name=None, empty=False, 
                 code=None):
        """
        Constructor: We either create a new case from the given information,
        or generate a case from a db record

        code str Default code to associate with the case
        """
        # store the loggers
        self.logger = logging.getLogger('file')
        self.console = logging.getLogger('console')

        # Defaults
        # -> no configuration directory
        self.create_config = None       # the config instance to create
        self.config_dir = None              # the name of the config dir, if existing
        self.allow_existing_dir = False     # Do not allow creation of case in existing dir

        
        if db_record is not None:
            # set from database record
            self.db_record = db_record
            for db_field in self.db_fields:
                record = getattr(db_record, db_field)
                setattr(self, db_field, record)
            if study is not None:
                self.study = study
                # set study name
                self.study_name = study.name
                self.study_path = study.path
                self.db = study.db
            else:
                # get the study
                raise exceptions.NoStudy
                #self.study_path = "N/A"
                #self.study_name = "N/A"

            if self.db is not None:
                # get the number of runs
                self.run_number = self.get_number_of_runs()
            else:
                self.run_number = "N/A"

            # repalce the code record with the code identifier
            code = self.code_db.identifier

            # set no attached runs
            self.runs = None

            # set no attached notest
            self.notes = None


        else:
            # we're creating a new case from provided arguments

            # --> set the name 
            if name is None:
                name = os.path.basename(path)
            
            self.name = str(name)

            # --> set the path
            self.path = path

            # --> store a reference to the study 
            self.study = study
            if self.study is None:
                raise exceptions.NoStudy

            # --> store the path relative to the study
            self.rel_path_to_study = self.study.get_rel_path(os.path.abspath(path))

            # --> are we creating an empty case folder            
            self.empty = empty

            # --> store a reference to the db controller
            self.db = study.db

            if self.db is None:
                # --> we need a db to continue
                raise exceptions.NoDB

            # --> set the list of directories which we require for each case
            self.dirs = {} if empty else config.CASE_DIRS
        
        # set the abspath for convinience
        self.abspath = os.path.join(self.study.path, self.path)

        # --> each case can only be associated with one code and it must be
        #     associated with the study
        # --> first check that the provided code is present in the study
        if code not in self.study.codes:
            msg = "Code '{}' is not associated with study '{}', please run 'cfmt ini --code {}'"
            raise exceptions.UnknownCode(msg.format(code, self.study.name,code))
        else:
            # --> store a reference to the code
            self.code = self.study.codes[code]
            self.code.set_case(self)
        
        # -> set absolute path to directories for convienience
        for directory in config.CASE_DIRS:
            attr_name = directory + "_dir"
            dir_path = os.path.join(self.study.path, self.path, directory)
            if hasattr(self, attr_name):
                # warn if the attribute already exists
                msg = "Existing attribute '{}' overwritten".format(attr_name)
                self.console.warning(msg)
                self.logger.warning(msg)

            # set the attribute 
            setattr(self, attr_name, dir_path)
            
    def __eq__(self, other):
        """ Determine whether two cases are equal """
        return self.db_record.id == other.db_record.id

    def create(self):
        """ Creates a case """
        path = self.path
        
        # does the code need to modify anything
        self.code.init_case(self)

        # generate db_record
        self.db_record = self.db.new_case(
            name=self.name,
            path=self.rel_path_to_study,
            study_id=self.study.db_record,
            created=datetime.today(),
            last_modified=datetime.today(),
            creator=utils.get_username(),
            code_db=self.code.db_record,
        )


        # case directory cannot be one of the existing names
        if path in self.study.dirs:
            msg = "Directory name '{0}' is reserved"
            msg = msg.format(os.path.basename(path))
            raise exceptions.ReservedName(msg)

        # case directory cannot contain a slash
        if "/" in path:
            # not sure of the implications of this yet
            raise ValueError("Cannot create a directory in a subfolder")
        
        # using existing directory?
        if self.allow_existing_dir is False:
            # create case directory
            if os.path.isdir(path):
                raise exceptions.CaseExists("Case directory '{0}' already exists".format(path))
        
            try:
                os.mkdir(path)
            except IOError:
                raise IOError("Cannot create case directory '{0}'".format(path))

        # create sub-directories
        if not self.empty:
            from cfmt.vcs.controller import VCSController
            
            for folder, options in self.dirs.items():
                new_dir = os.path.join(self.path, folder)
                os.makedirs(new_dir)

                # initialize vcs repositories
                if 'vcs' in options:
                    vcs = VCSController.get_vcs(options['vcs'])
                    vcs.init(new_dir)

        # add case-config if required
        if self.create_config:
            self.write_config()

        # run code specific stuff
        self.code.create_case(path)

        # add the db entry
        self.db.save(self.db_record)
    
    def run(self, args=None):
        """ Runs the case 
            args Object The arguments passed from the cmd
        """
        from cfmt.batch_systems.controller import Batch
        from cfmt.entities.runs import Run
        from cfmt.entities.jobs import Job
        
        # store reference to arguments
        self.args = args
        # store final run arguments
        self.run_args = {}

        # check arguments for support from code
        if args.edit_input:
            if not self.code.supports('edit_input'):
                msg = "Code '{}' does not support pre-run editing, please use 'cfmt edit'".format(self.code.name)
                utils.print_exception_info(msg)
                sys.exit(1)
        
        if args.continue_calc:
            # we set the args based on the previous job
            # distinguish between run names/job ids
            arg_list = args.select
            job_ids = []
            run_names = []
            _runs = []
            if arg_list:
                for item in arg_list:
                    try:
                        job_ids.append(int(item))
                    except ValueError:
                        # is not an integer
                        run_names.append(str(item))
                if run_names:
                    # we were passed runs to operate on, convert the runs
                    # to objects
                    __runs = self.get_runs(records=self.db.get_runs(names=run_names), last_job=True, job_type=Job.SOLVER)
                    
                    # --> warn if a run we requested is not present
                    for run in __runs:
                        if run.name in run_names:
                            run_names.remove(run.name)

                    if run_names:
                        self.console.warning("Runs '%s' selected but not found", ", ".join(run_names))
                        self.logger.warning("Runs '%s' selected but not found", ", ".join(run_names)) 

                    _runs += __runs
            
                if job_ids is not None:
                    # we're passed job_ids to fetch, these should be assigned jobs ready to run
                    __runs = self.get_runs(with_job_ids=job_ids, last_job=False, job_type=Job.SOLVER)

                    # --> warn if a job we requested is not present,
                    #     or is not job.CREATED
                    invalid_jobs = {}
                    for run in __runs:
                        for job in run.jobs:
                            if job.id in job_ids:
                                job_ids.remove(job.id)
                            
                            if job.status is not job.CREATED:
                                invalid_jobs.setdefault(run, []).append(job)
                                self.console.warning("Job '%s' not marked as CREATED, ignoring", str(job.id))
                                self.logger.warning("Job '%s' not marked as CREATED, ignoring", str(job.id))
                    
                    if job_ids:
                        # we have some we didn't find
                        self.console.warning("Job(s) '%s' selected but not found", ", ".join(str(x) for x in job_ids))
                        self.logger.warning("Job(s) '%s' selected but not found", ", ".join(str(x) for x in job_ids))
                    
                    if invalid_jobs:
                        # we have some we need to remove
                        for run, _jobs in invalid_jobs.items():
                            for job in _jobs:
                                if job in run.jobs:
                                    run.jobs.remove(job)
                            if not run.jobs:
                                __runs.remove(run)
                    _runs += __runs
            else:
                # we assume the last known run in the case
                is_dev = True if args.run_dev else None
                _runs = self.get_last_run(last_job=True, is_dev=is_dev)
        else:
            # set a dummy run to loop through
            _runs = [True]

        # store the runs (overwrites what was set in commands)
        self.runs = _runs

        for run in _runs:
            # if the run has a code associated, set the case level code to match
            if hasattr(run, "code"):
                self.code = run.code

            if args.continue_calc:
                # we prejudice the argument logic with what we know
                
                # -> first check we have a previous created job to submit
                _jobs = [j for j in run.jobs if j.status is j.CREATED]
                if len(_jobs) > 0:
                    # warn that we only support submitting one job
                    self.console.warning("Each run only supports one concurrent job, submitting first one only")

                    # we submit those that are there
                    # build a list of jobs that are created
                    for job in _jobs:
                        # load the job ready to submit
                        # set the code options
                        #run.code.set_code_options(job.options["code"], args=args)
                        # grab the batch system
                        batch = Batch(job.batch_system, batch_options=job.options["batch"])

                        if args.print_command:
                            print(job.sys_command)
                        else:
                            batch.submit(job)
                            job.save(cascade=True)
                        break
                    
                    sys.exit(0)

                # if we don't have a previous job, because perhaps an error, then bail
                if run.job is None:
                    msg = f"No previous job to continue from with run '{run.name}'"
                    self.logger.error(msg)
                    print(msg)
                    sys.exit(1)

                # reset the run status to None
                run.status = None

                # -> local/remote remains unchanged
                # -> dev mode should be false/ignored
                args.run_dev = False

                # -> init_from should be false/ignored
                args.init_from = None

                # warn if some options are different
                # -> staging
                # this is as per the run
                config.configtool.set('staging', 'mode', run.staging_mode)
                #config.configtool.set('staging', 'location', run.staging_dir)

                ## -> serial/parallel
                #if run.job.slots is not None:
                #    config.configtool.set('run', 'slots', str(run.job.slots))
                #    if run.job.slots > 1:
                #        config.configtool.set('run', 'mode', 'parallel')
                #    else:
                #        config.configtool.set('run', 'mode', 'serial')

                ## -> batch system
                config.configtool.set('run', 'batch_system', str(run.job.batch_system))
                # -> specific batch system options, are loaded by the batch system

            # Argument handling logic
            # --> remote/local
            if args.remote is not None or args.local is not None:
                is_remote = True if args.remote else False
                is_local = True if args.local else False
                machine = 'remote' if is_remote else 'local'
                config.configtool.set('run', 'machine',machine)
            else:
                machine = config.configtool.get('run','machine')
                is_remote = True if machine == 'remote' else False
                is_local = True if machine == 'local' else False
        
            # --> dev mode
            if args.run_dev:
                is_local = True if not args.remote else False

            # if we're initializing from another run, check we can read it
            if args.init_from is not None:
                # note the run need not be from this case
                __runs = self.study.get_runs(names=args.init_from, from_case=self)
                if not __runs:
                    utils.print_exception_info("Run '{}' not found".format(args.init_from.pop()))
                    sys.exit(1)
                else:
                    init_from = __runs.pop()
            else:
                init_from = None
        
            assert isinstance(is_local, bool)
            assert isinstance(is_remote, bool)
            assert isinstance(machine, str)

            if args.run_dev:
                args.no_staging = True 

            # staging
            staging_root = args.staging_root[-1] if args.staging_root is not None else config.configtool.get('staging', 'location')
            
            if args.staging is not None or args.staging_mode is not None:
                staging = True
                staging_mode = args.staging_mode[-1] if args.staging_mode is not None else config.configtool.get('staging', 'mode')
            else:
                if args.continue_calc:
                    staging_mode = run.staging_mode
                else:
                    staging_mode = config.configtool.get('staging', 'mode')
                
                staging = False if staging_mode == 'none' or args.no_staging else True
                staging = False if args.run_dev else staging
            
            if args.init_to_scratch and not staging:
                msg = "--init_to_scratch requested but run not due to be staged"
                utils.print_exception_info(msg)
                sys.exit(1)

            transfer_mode = args.transfer_mode[-1] if args.transfer_mode is not None else config.configtool.get('staging', 'transfer_mode')
        
            # no-submit
            do_submit = False if args.no_submit else True
            do_submit = False if args.print_command else do_submit
            
            # --> batch system
            if args.batch_system is not None:
                assert len(args.batch_system) == 1
                batch_system = args.batch_system[-1]
            else:
                batch_system = config.configtool.get('run', 'batch_system')
            is_batch = False if batch_system == 'none' else True
            if args.run_dev:
                is_batch = False

            
            if not is_batch and not args.continue_calc:
                # if we're not running a batch calculation, set the default to local
                # can be overridden by passing the --staging explicitely
                staging = True if args.staging else False

            if not staging:
                transfer_mode = None
                staging_root = None
                staging_mode = 'none'

            # --> build logic
            if args.continue_calc:
                do_build = True if args.rebuild else False
            else:
                do_build = False if args.no_build else True

            self.run_args["is_remote"] = is_remote
            self.run_args["is_local"] = is_local
            self.run_args["machine"] = machine
            self.run_args["dev"] = args.run_dev
            self.run_args["staging"] = staging
            self.run_args["staging_mode"] = staging_mode
            self.run_args["staging_root"] = staging_root
            self.run_args["transfer_mode"] = transfer_mode
            self.run_args["is_batch"] = is_batch
            self.run_args["batch_system"] = batch_system
            self.run_args["batch_options"] = args.batch_options
            self.run_args["do_build"] = do_build
            
            # print run debug info
            self.logger.debug("Run info...")
            self.logger.debug("is remote: %s", is_remote)
            self.logger.debug("is local: %s", is_local)
            self.logger.debug("machine: %s", machine)
            self.logger.debug("dev mode: %s", args.run_dev)
            self.logger.debug("staging?: %s", staging)
            self.logger.debug("staging mode: %s", staging_mode)
            self.logger.debug("staging root: %s", staging_root)
            self.logger.debug("transfer mode: %s", transfer_mode)
            self.logger.debug("is batch?: %s", is_batch)
            self.logger.debug("batch system: %s", batch_system)
            self.logger.debug("do build: %s", do_build)
            
            # -----------
            # We should now know all we need to know
            # Start case/run set up
        
            # -> get absolute case path directory
            self.abspath = os.path.join(self.study.path, self.path)

            # tell the code what case we're running
            self.code.set_case(self)
            
            # -> run some checks
            try:
                # --> general checks
                self.pre_run_check(run)
                # --> code specific checks
                self.code.pre_run_check(run, args=args, batchname=batch_system)
            except exceptions.CheckFailed as err:
                raise exceptions.RunFailed("Check Failed: " + str(err))

            # build (if required - or if we can build and we requested a rebuild)
            if (self.code.requires_build and do_build) or (self.code.can_build and args.rebuild):
                try:
                    self.code.build(self, rebuild=args.rebuild)
                except exceptions.BuildFailed as err:
                    self.logger.error("Build failed", exc_info=err)
                    self.console.error("Build failed: '%s'", str(err))
                    sys.exit(1)
            
            
            # if we're not continuing, create a new run directory
            if not args.continue_calc and run is True:
                # The batch system,
                batch = Batch(batch_system, parallel=args.parallel, np=args.np, parallel_exec=args.parallel_exec, cmdline_args=args)
                # get a new run object if not dev
                if self.run_args["dev"]:
                    # get the existing dev run if we have one
                    dev_name = [self.name + '#' + 'dev']
                    run = self.get_runs(names=dev_name, last_job=False)
                    if run:
                        # we get a list back, but we pop
                        run = run.pop()
                        # check the dev folder hasn't gone missing
                        if not os.path.isdir(run.abspath):
                            os.makedirs(run.abspath)
                        
                        # replace the batch with our own
                        run.batch = batch
                        # repalce the code with our own
                        run.code = self.code
                        # reset the status
                        run.set_status(run.CREATED)
                    else:
                        # nothing we grab something new
                        run = Run(study=self.study, case=self, code=self.code, batch=batch, dev=True)
                else:
                    run = Run(study=self.study, case=self, code=self.code, batch=batch)
            else:
                # we're continuing a calculation
                # --> prevent continuing if the run is marked as running
                # --> use the last job to generate the batch object
                if run.status in [run.RUNNING, run.SUBMITTED, run.QUEUING]:
                    self.logger.error("Run to continue has status '%s', not proceeding", run.status_name)
                    print(utils.ERROR + f": Run is marked as '{run.status_name}', not proceeding")
                    sys.exit(1)
                
                # if we're enabling staging, create the required staging directory
                if staging and run.staging_dir is None:
                    path = run.create_staging_dir(exist_ok=True)
                    run.set_db(staging_dir = path)

                # if we've been asked to change staging directories, error if we're currently marked as in
                # staging
                if args.staging_root is not None:
                    if run.in_staging:
                        msg = f"Run {run.name}: Cannot change staging root when a run is marked as in staging"
                        self.logger.error(msg)
                        print(utils.ERROR + ": " + msg)
                        sys.exit(1)
                    else:
                        run.staging_root = staging_root
                        # staging root already updated
                        path = run.create_staging_dir() 
                        run.set_db(staging_dir = path)


                
                batch_options = run.job.options["batch"]
                
                batch = Batch(batch_system, 
                    batch_options=batch_options, 
                    parallel=args.parallel, 
                    np=args.np, 
                    parallel_exec=args.parallel_exec, 
                    cmdline_args=args
                )
                # we already have a run, set the batch
                run.batch = batch

            # --> tell it what we know
            run.staging = self.run_args["staging"]
            run.staging_mode = self.run_args["staging_mode"]
            run.transfer_mode = self.run_args["transfer_mode"]
            run.staging_root = self.run_args["staging_root"]
            
            # check the 'runs' directory exists
            try:
                runs_dir = self.runs_dir
            except AttributeError:
                msg = "Run directory does not exist"
                self.logger.error(msg)
                raise NotADirectoryError(msg)
            else:
                # create a new run directory
                if run.created is None:
                    run.create(runs_dir)
                else:
                    if run.is_dev and not os.path.isdir(run.abspath):
                        # recreate the dev directory if it's been deleted
                        os.makedirs(run.abspath)
                        
                    # set the existing working directory for dev
                    if run.staging:
                        run.working_dir = run.staging_dir
                    else:
                        run.working_dir = run.abspath
            
            # store the reason if we have one
            if args.reason is not None:
                reason = args.reason.pop() if isinstance(args.reason, list) else args.reason
                run.set_db(reason=str(reason))
            
            # store the tags if we have them
            if args.tag is not None:
                # should have a list of lists
                # i.e. [['tag1'], ['tag2']]
                for t in args.tag:
                    tag = t.pop()
                    run.add_tag(tag)
                
                run.set_db(tags=run.tags)

            # if we're initializing from another run try to do so here
            if init_from is not None:
                # get the run directory
                print(" -> Copying results from '{}' ...".format(init_from.name), end="\r")
                self.logger.info("Initializing run with results from run '%s'", init_from.name)
                if args.init_to_scratch:
                    dest = run.staging_dir
                else:
                    dest = run.abspath
                try:
                    self.code.copy_results(
                        init_from.get_run_dir(local_only=args.init_from_home), 
                        dest, 
                        keep_residuals=args.keep_residuals, 
                        keep_monitors=args.keep_monitors,
                        continue_tout=args.continue_tout,
                        keep_input=args.keep_input,
                        keep_dir=args.keep_dir,
                        init_to_scratch=args.init_to_scratch
                    )
                except (exceptions.TransferFailed, FileNotFoundError, NotADirectoryError) as err:
                    print(" -> Copying results from '{}' ... FAILED".format(init_from.name), end="\n")
                    utils.print_exception_info(str(err))
                    sys.exit(1)
                else:
                    if args.init_to_scratch:
                        run.set_db(in_staging=True)
                        run.in_staging = True
                    print(" -> Copying results from '{}' ... OK".format(init_from.name), end="\n")

            # if the code requires building, we copy required code binaries to run_dir
            if (self.code.requires_build and do_build) or (self.code.can_build and args.rebuild):
                self.code.copy_bin(self, run.get_run_dir())
            else:
                self.code.set_bin_path(run.get_run_dir())
            
            # create the staging directory
            if run.staging:
                if not hasattr(run, "staging_dir") and run.staging_dir is None:
                    self.console.error("Staging directory does not exist")
                    exit()

                # check the staging directory exists
                if os.path.isdir(run.staging_dir):
                    run.staging_dir_created = True
                else:
                    # the staging directory has gone missing
                    run.create_staging_dir(recreate=True)
            
            # copy the required mesh and input files
            if not args.continue_calc:
                if not args.keep_input:
                    self.copy_input(run, args=args, silent=False)
                self.copy_mesh(run, args=args, silent=False)
            


            ## pass on any remaining options if we have them
            #if args.batch_options is not None:
            #    batch.additional_options = self.run_args["batch_options"]
            # tell the code we're initializing the rtun
            try:
                self.code.init_run(run, args=args)
            except exceptions.RunFailed as err:
                # set the run as failed, and prevent running
                run.set_status(run.FAILED)
                run.clean()
                #raise exceptions.RunFailed("Code failed")
            
            if args.edit_input:
                self.edit_input(run)
                # prompt for continue?
                prompt = "Continue running case?"
                cont = utils.confirm(prompt=prompt, default=True)
                if cont is False:
                    if not args.continue_calc and not args.run_dev:
                        # the run is new, prompt for deletion
                        _dir = os.path.relpath(run.get_run_dir())
                        prompt = f"Delete created run directory '{_dir}'"
                        cont = utils.confirm(prompt=prompt, default=True)
                        if cont:
                            utils.rmdir(run.get_run_dir())
                    self.console.info("Exiting. The run has not been saved")
                    sys.exit(0)

            
            # stage the files
            if run.staging:
                if not run.in_staging:
                    try:
                        run.transfer_up()
                    except exceptions.TransferFailed as err:
                        utils.print_exception_info(str(err), debug=True)
                        sys.exit(1)
                else:
                    pass
                    # make sure the binary path is correct
                    #run.set_staging_bin()

            # --> Code specific task once run directory has been created
            try:
                self.code.pre_run_tasks(run, args=args)
            except (exceptions.RunFailed, FileNotFoundError) as err:
                utils.print_exception_info(err)
                run.set_status(run.FAILED)
                run.clean()
            
            # check we havn't aleady set the status
            if run.status is None:
                run.set_status(run.CREATED)
            
            # --> save the run
            run.save()

            # quit
            if run.status is run.FAILED:
                sys.exit(1)

            # ask the batch system to generate a job
            job = batch.create_job(run)

            
            if args.edit_command:
                self.edit_run_command(job)
                # edit the script as well
                if run.job.fsubmission_script is not None:
                    self.edit_run_script(job)
            
            if args.print_command:
                print(job.sys_command)
            
            # --> save code history if required (after save as we need the id)
            save_history = config.configtool.getboolean('defaults', 'save_history')

            # --> submit
            if do_submit:
                job.save()
                if save_history:
                    self.commit_history(run, job)
                batch.submit(job)
            else:
                run.set_status(run.ASSIGNED)
            
            job.save()
            
            
            # --> update the run to the database
            run.save()
        
    def commit_history(self, run, job):
        """ Commits the source history for the case """
        print(" -> Saving run input state ... ", end='\r')
        self.logger.info("Saving code state")
        
        history_path = os.path.join(self.abspath, '.history')
        if os.path.isdir(history_path):
            # copy the code as it was built
            try:
                self.code.copy_src(os.path.join(history_path, 'src'))
            except shutil.Error:
                self.console.warning("Error copying code history")
            
            # copy the input
            self.copy_input(run, history_path, history=True)

            from cfmt.vcs.controller import VCSController

            # make the commit if we need to 
            vcs = VCSController.get_vcs('git')
            msg = f"Commit for job {job.id} - '{job.name}'"
            try:
                code_sha1 = vcs.commit(history_path, msg, name=".history", add_untracked=True)
                job.case_src_ref = code_sha1
            except Exception as err:
                self.logger.error("Failed to save run input state", exc_info=True)
                print(" -> Saving run input state ... FAILED", end='\n')
                raise
            else:
                print(" -> Saving run input state ... OK", end='\n')
        else:
            print(" -> Saving run input state ... FAILED", end='\n')
            msg = ".history folder does not exist"
            self.console.warning(msg)
            self.logger.warning(msg)

    def set_last_run(self):
        """ Get the last modified run """
        self.runs = self.get_runs(records=self.db.get_last_run(case=self.db_record))
    
    def get_last_run(self, last_job=False, is_dev=None):
        """ Get the last modified run """
        return self.get_runs(records=self.db.get_last_run(case=self.db_record, is_dev=is_dev), last_job=last_job)
    
    def archive(self, args, remote_path, host):
        """
        Archive the runs attached to the case to the remote path
        """
        from cfmt.entities.archives import check_archive_repo
        
        repo_path = None
        _runs = self.runs
        if _runs:
            # make sure the remote case folder exists
            #remote_path = os.path.join(remote_path, self.path)
            create_archive = args.create or args.deposit
            # check the repository
            if create_archive:
                try:
                    repo_path = check_archive_repo(self, host, remote_path, archive_type=args.type)
                except exceptions.ArchiveRepoMissing as err:
                    print("Cannot find repository at '{}'".format(err))
                    print("Has it been initialized?")
                    sys.exit(1)
                
                if repo_path is None:
                    raise exceptions.ArchiveFailed("Error finding repository at '{}'".format(remote_path))

            from tqdm import tqdm

            with tqdm(total=len(_runs), unit='runs', ascii=True) as pbar:
                for run in _runs:
                    pbar.set_description(" -> Run '{}'".format(run.name))
                    # skip if the run is already archived
                    if run.is_archived is None:
                        # unknown state, skip
                        pbar.write("WARNING: Run '{}' has unknown archive state ... ".format(run.name) + utils.SKIPPING)
                        pbar.update(1)
                        continue
                    elif run.is_archived > 0:
                        if args.create or args.deposit:
                            pbar.write("Run '{}' is already archived ... ".format(run.name) + utils.SKIPPING)
                            pbar.update(1)
                            continue
                    else:
                        if args.withdraw:
                            pbar.write("Run '{}' is not marked as archived ... ".format(run.name) + utils.SKIPPING)
                            pbar.update(1)
                            continue

                    if args.withdraw:
                        try:
                            run.withdraw_archive(args)
                        except exceptions.ArchiveFailed as err:
                            pbar.write(utils.WARNING + ": {}".format(str(err)))
                            utils.print_exception_info(err, no_print=True)
                        except exceptions.ArchiveFailed as err:
                            utils.print_exception_info(err)
                            
                        pbar.update(1)
                        continue

                    elif create_archive:
                        # skip if the run is in staging or currently marked as instaging 
                        if not args.force:
                            if run.status in [run.RUNNING, run.SUBMITTED, run.QUEUING]:
                                pbar.write("Run '{}' has status {} ... ".format(run.name, run._level_to_name[run.status]) + utils.SKIPPING + " Use --force")
                                pbar.update(1)
                                continue

                            if run.in_staging:
                                pbar.write("Run '{}' is marked in staging ....".format(run.name) + utils.SKIPPING + " Use --force")
                                pbar.update(1)
                                continue
                    
                        try:
                            run.create_archive(
                                repo_path,
                                host,
                                args,
                                pbar=pbar
                            )
                        except exceptions.ArchiveFailed as err:
                            utils.print_exception_info(err, no_print=False)
                            pbar.write("WARNING: " + str(err))
                        else:
                            pbar.update(1)
            print('')

        else:
            print("No runs to archive from case '{}'".format(self.name))
    
    def fetch(self, args,preserve_source=False, keep_staged=False, with_tout=False, exclude=None):
        """ 
        For runs in staging, download the results to the local directory 
        
        preserve_source -- whether to keep the source after transferring (i.e. sync)
        keep_staged -- whether to keep the run marked as staged
        with_tout -- includes tout folders as defined in tout_mask
        exclude -- a further exclude filter
        force -- ignore run status

        Batch options are supported, in which case the sync task will be submitted as a job.
        """
        
        if self.runs:
            from tqdm import tqdm

            # determine if we're submitting batch jobs
            if args.batch_system is not None:
                from cfmt.batch_systems.controller import Batch, get_batch_cls
                
                batch_system = args.batch_system[-1]

                batch_class = get_batch_cls(batch_system) 
                # get options from config
                system_name = batch_class.system_name
                default_queue = config.configtool.get(system_name, "sync_queue", fallback=None)
                default_extra = config.configtool.get(system_name, "sync_extra", fallback=None)

                batch_options = {
                    "script_name": config.configtool.get(system_name, "sync_script_name", fallback="sync.sh"),
                }
                if default_queue is not None:
                    batch_options["queue"] = default_queue
                
                if default_extra is not None:
                    batch_options["extra"] = default_extra
                
                batch = batch_class(batch_options=batch_options, parallel=False, np=1, cmdline_args=args)
            else:
                batch = None

            _runs = self.runs
            with tqdm(total=len(_runs), unit='runs', ascii=True) as pbar:
                for run in _runs:
                    pbar.set_description(" -> Run '{}'".format(run.name))

                    # skip if the run has no staging method
                    if run.staging_mode == 'none' :
                        pbar.write(" -> Run '{}' ".format(run.name) + utils.FAILED + ": no staging mode")
                        pbar.update(1)
                        continue
                    
                    # if the run is archived, skip
                    if run.is_archived:
                        pbar.write(" -> Run '{}' ".format(run.name) + utils.SKIPPING + ": is archived.")
                        pbar.update(1)
                        continue
                    
                    # skip if the run is not in staging
                    if not run.in_staging and not args.force:
                        pbar.write(" -> Run '{}' ".format(run.name) + utils.SKIPPING + ": not in staging")
                        pbar.update(1)
                        continue
                    
                    if args.mode == 'fetch':
                        if run.status in [run.RUNNING, run.SUBMITTED, run.QUEUING] and not args.force:
                            pbar.write("SKIPPED run '{}' as status {}. Use --force".format(run.name, run._level_to_name[run.status]))
                            pbar.update(1)
                            continue
                    
                    # -> for both fetch and sync if we specify args.with_tout and do not specify a batch system, 
                    #    then we fetch the tout first since then the full rsync will only be responsible for deleting (if required)
                    # 
                    # if we want tout, get it now, so long as we haven't requested a batch system.
                    if (args.mode == 'sync' and args.get_tout) and batch is None:
                        
                        try:
                            run.fetch_tout(args)
                        except exceptions.TransferFailed as err:
                            utils.print_exception_info(err, no_print=True)
                            pbar.write(" -> Run '{}' ".format(run.name) + utils.FAILED + ": " + str(err))
                            pbar.update(1)
                            continue
                        else:
                            if args.mode =='sync' and args.get_tout:
                                pbar.write(" -> Run '{}' ".format(run.name) + utils.OK)
                            pbar.update(1)
                    
                    # if we didn't specify --get-tout, for sync only, we fetch the rest
                    if not (args.mode == 'sync' and args.get_tout and batch is None):
                        try:
                            job = run.fetch(
                                args,
                                batch=batch,
                                preserve_source=preserve_source, 
                                keep_staged=keep_staged, 
                                with_tout=with_tout,
                                exclude=exclude
                            )
                        except exceptions.TransferFailed as err:
                            pbar.write("WARNING: " + str(err))
                        else:
                            pbar.write(" -> Run '{}' ".format(run.name) + utils.OK)
                            pbar.update(1)
            print('')
    
    
    def edit_run_script(self, job):
        """ Edits the submission script prior to submission """
        print(" -> Editing submission script ...", end='\r')
        
        fpath = job.fsubmission_script
        md51 = utils.get_hash(fpath)

        utils.edit_file(fpath, job.working_dir)

        md52 = utils.get_hash(fpath)

        if md51 != md52:
            print(" -> Editing submission script ... OK", end='\n')
        else:
            print(" -> Editing submission script ... NO CHANGE", end='\n')


    def edit_run_command(self, job):
        """ Edits the run command prior to submission """
        print(" -> Editing run command ...", end='\r')
        initial_message = job.sys_command
        initial_message += "\n# Edit the run command. Lines starting with '#' will be igored" 
        initial_message += "\n#\n# job: {}".format(job.name)
        initial_message += "\n# case: {}".format(self.name)
        initial_message += "\n# study: {}".format(self.study.name)

        edited_command = utils.editor_prompt(initial_msg=initial_message, strip_comments=True, remove_initial_msg=True)

        if edited_command:
            # change the db 
            job.sys_command = edited_command
            command_list = edited_command.split()
            job.set_command(command_list)

            print(" -> Editing run command ... OK", end='\n')
        else:
            print(" -> Editing run command ... NO CHANGE", end='\n')
        
    def edit_input(self, run):
        """ Edits the input files in the run directory before submission """

        print(" -> Editing input files ...", end='\r')

        # determine where the actualy run directory is 

        # we ask the code, since it's code depended
        try:
            self.code.edit_input(run.get_run_dir())
        except FileNotFoundError:
            self.logger.warning("No input files to edit")
            print(" -> Editing input files ... FAILED")
        except NotADirectoryError as err:
            print(" -> Editing input files ... FAILED")
            utils.print_exception_info(str(err))
            sys.exit(1)
        except:
            raise
        else:
            print(" -> Editing input files ... OK")

    
    def copy_input(self, run, dest=None, history=False, silent=True, args=None):
        """ Copies input files to the run directory """
        
        if not silent:
            print(" -> Copying input files ... ", end='\r')
        
        # tell the code where we can find the input files
        try:
            if dest is None:
                if args.init_to_scratch and run.staging_dir_created:
                    dst=run.staging_dir
                else:
                    dst=run.abspath
                self.code.copy_input(self.abspath, dst, args=args, history=history)
            else:
                src = self.abspath if not history else run.abspath
                self.code.copy_input(src, dest, args=args, history=history)
        except FileNotFoundError as err:
            if not silent:
                print(" -> Copying input files ... WARNING", end='\n')
            msg = "Input file missing: " + str(err)
            self.logger.warning(msg)
            self.console.warning(msg)
        except NotADirectoryError as err:
            if not silent:
                print(" -> Copying input files ... FAILED", end='\n')
            self.console.error(str(err))
            self.console.debug(str(err), exc_info=err)
            self.logger.error("Directory not found", exc_info=err)
            sys.exit(1)
        except:
            if not silent:
                print(" -> Copying input files ... FAILED", end='\n')
            raise
        else:
            if not silent:
                print(" -> Copying input files ... OK", end='\n')


    def copy_mesh(self, run, args=None, silent=False):
        """ Copies mesh files to the run directory """
        if not silent:
            print(" -> Copying mesh files ...", end='\r')
        # try the case level mesh directory
        try:
            mesh_dir = os.path.join(self.abspath, 'mesh')
            self.code.copy_mesh(mesh_dir, run.abspath, args=args)
        except FileNotFoundError:
            self.logger.debug("No mesh files present in '{}'".format(mesh_dir))
            # try study mesh directory
            try:
                mesh_dir = os.path.join(self.study.path, 'mesh')
                self.code.copy_mesh(mesh_dir, run.abspath, args=args)
            except FileNotFoundError:
                if not silent:
                    print(" -> Copying mesh files ... WARNING", end='\n')
                msg = "No mesh files found"
                self.logger.warning(msg)
                self.console.warning(msg)
            except NotADirectoryError as err:
                if not silent:
                    print(" -> Copying mesh files ... FAILED", end='\n')
                self.console.error(str(err))
                self.console.debug(str(err), exc_info=err)
                self.logger.error("Directory not found", exc_info=err)
                sys.exit(1)
            except:
                if not silent:
                    print(" -> Copying mesh files ... FAILED", end='\n')
                raise
            else:
                if not silent:
                    print(" -> Copying mesh files ... OK", end='\n')
        else:
            if not silent:
                print(" -> Copying mesh files ... OK", end='\n')

    def commit_src(self, msg):
        """
        Commits and changed src files with msg
        Returns the sha1 of the commit
        """
        # get the path to the checked out src directory
        try:
            repo_path = os.path.join(self.study.path, self.src_dir)
        except AttributeError:
            self.logger.error("Cannot find path to case src")
            raise
        except:
            raise

        from cfmt.vcs.controller import VCSController
        
        # name
        name = "{} src".format(self.name)

        # fetch the controller and load the repo
        vcs = VCSController.get_vcs('git')
        return vcs.commit(repo_path, msg, name=name)

    
    def add_note(self, text=None, editor=None):
        """ Adds a note """
        from cfmt.entities.notes import Note
        
        if not isinstance(text, str):
            # we're note provided with any text, prompt
            if editor is None:
                editor = config.get_editor()

            msg = self.initial_note_message.format(
                case_name = self.name,
                study_name = self.study.name
            )
            # we request an editor
            text = utils.editor_prompt(initial_msg=msg,
                                       strip_comments=True, 
                                       remove_initial_msg=True,
                                       editor=editor)

        if not text:
            print("Empty message. Aborting")
            sys.exit(0)
        
        note = Note(case=self, text=text)
        _id = note.db_record.id
        self.logger.info(f"Added note {_id} to case '{self.name}' ")
        print(f" -> Note {_id} added to case '{self.name}'")

    def get_note(self, id):
        """ Gets the note with id """
        ids = [id]
        
        self.get_notes(ids=ids)
        
    def get_notes(self, last_n=None, ids=None):
        """ Retrieves the last n notes """
        from cfmt.entities.notes import Note

        _notes = self.db.get_notes(case=self.db_record, limit=last_n, ids=ids, order_by='last_modified')

        self.notes = []
        for note in _notes:
            self.notes.append(Note(case=self, db=self.db, db_record=note))

    def format_notes(self):
        """ Formats notes for display """
        # get notes if we have none
        if self.notes is None:
            self.get_notes()

        # sort the notes by most recent first
        notes = sorted(self.notes, key=lambda x: x.created, reverse=True)
        text = ""
        if notes:
            text += cstr("Case: {}\n\n".format(self.name), fg=3)
            for note in notes:
                # add date
                date_string = "Date:   " + utils.print_datetime(note.created)
                text += cstr(date_string, fg=248)
                text += "\n"
                
                # id
                text += cstr("ID  : {:3}\n".format(note.id), fg=250)

                # new line
                text += "\n\n"

                # indent the rest
                text += utils.indent_string(str(note))

                # new line
                text += "\n\n"

        if not text:
            # default
            text = cstr("No notes for case: '{}'".format(self.name), fg=248)
        return text
    
    def update(self, run_kwargs=None, case_kwargs=None):
        """ Updates the database """
        from tqdm import tqdm
        
        # loop over and update
        if self.runs:
            _runs = self.runs
            with tqdm(total=len(_runs), unit='runs', ascii=True) as pbar:
                msg = utils.OK
                
                # save each run
                for run in _runs:
                    self.logger.info("Updating run '%s'", run.name)
                    run_name = " -> Case '{}' ... ".format(self.name) + run.dir_name
                    pbar.set_description(desc=run_name)
                    try:
                        run.update(**run_kwargs)
                    except exceptions.UpdateFailed as err:
                        pbar.write(str(err))
                        msg = utils.PARTIAL
                    finally:
                        pbar.update(1)

                run_name = " -> Case '{}' ... ".format(self.name) + msg
                pbar.set_description(desc=run_name)

        elif case_kwargs is not None:
            # we have some case stuff to update
            print(" -> Case '{}' ... OK (nothing to update)".format(self.name))

        else:
            # non to update
            print(" -> Case '{}' ... OK (nothing to update)".format(self.name))
        
    def get_runs(self, records=None, status=None, names=None, last_job=True,
                       with_job_ids=None, tags=None, job_type=None):
        """ Queries the database for runs matching passed parameters """
        from cfmt.entities.runs import Run

        # sanitize input
        if status is not None:
            # we query by status
            if isinstance(status, int):
                status = [status]
            assert isinstance(status, list)
        if records is None:
            records = self.db.get_runs(case=self.db_record, names=names, has_job_ids=with_job_ids,
                                       status=status, tags=tags) 
        _runs = []
        if records:
            for record in records:
                # make sure the right case is sent
                if record.case.id != self.id:
                    case = Case(study=self.study, db_record=record.case)
                else:
                    case = self
                run = Run(study=self.study, case=case, db=self.db, db_record=record)
                
                if last_job:
                    # fetch the latest job if it exists
                    run.set_last_job()

                if with_job_ids:
                    # fetch the correct jobs
                    run.set_jobs(ids=with_job_ids)

                _runs.append(run)

        return _runs

    def get_jobs(self, status=None, records=None, ids=None):
        """ 
        Queries the database for jobs matching the passed parameters 
        Jobs are returned with the correct job.run set
        """
        raise NotImplementedError()
        # sanitize the input
        if status is not None:
            # we query by status
            if isinstance(status, int):
                status = [status]
            
            assert isinstance(status, list)
            records = self.db.get_jobs(status=status)
        elif records is not None:
            pass
        else:
            records = self.db.get_jobs()

        records = self.db.get_jobs(status=status, has_case=self.db_record, ids=ids)

        # we can ask the study, and pass the case along as well
        
        return "HI"


    def pre_run_check(self, run):
        """ General checks to carry out before the run """
        # --> check the case directory actually exists
        if not os.path.isdir(self.abspath):
            msg = "Case directory '{}' does not exist".format(self.abspath)
            raise NotADirectoryError(msg)
        
    
    def get_number_of_runs(self):
        """ Returns the number of runs associated with the case """
        if self.db_record is None:
            # should raise an error here
            return 0

        return self.db.get_number_of_runs(self.db_record)

    def create_config_dir(self):
        """ Create's a config dir if one doesn't already exist """
        # have we already created it
        if self.config_dir is not None:
            return

        # check if it exists in the filesystem
        config_dir = os.path.join(self.path, config.CFMT_DIR_NAME)
        if os.path.isdir(config_dir):
            return

        # attempt to make
        try:
            os.mkdir(config_dir)
        except FileExistsError:
            self.config_dir = False
        else:
            self.config_dir = config_dir

    def write_config(self):
        """ Writes the current configuration file out """
        # make sure we have a config directory
        if self.config_dir is None:
            self.create_config_dir()

        # write out the file
        if self.config_dir is not None:
            fname = os.path.join(self.config_dir, config.CFMT_CONFIG_NAME)
            with open(fname, 'w') as f:
                self.create_config.write(f)

    def set_runs(self, **kwargs):
        """ Asssings the runs """
        self.runs = self.get_runs(**kwargs)

    def print_status(self, dates=False, notes=False, details=False, src_ref=False, 
                           job_list=False, stats=False, sizes=False, archive_list=False,
                           with_dev=False, job_type=None, with_deleted=False):
        """ Prints the status of the case """
        from cfmt.entities.runs import Run

        if details:
            case_data = {
                'created': utils.print_datetime(self.created),
                'last_modified': utils.print_datetime(self.last_modified),
                'run_number': self.run_number,
                'code': self.code.name,
                'path': self.abspath,
            }
            cprint("     Runs  : {run_number:<20} Created       : {created}".format(**case_data), fg=248)
            cprint("     Code  : {code:<20} Last modified : {last_modified}".format(**case_data), fg=248)
            cprint("     Path  : {path}".format(**case_data), fg=248)
            print("")

        # print list of runs
        headers = Run.get_status_headers(dates=dates, src_ref=src_ref, sizes=sizes, stats=stats)
        data = []
        if sizes:
            # total up run sizes
            local_size_total = 0
            remote_size_total = 0
            archive_size_total = 0

        # fetch the data
        if self.runs:
            from cfmt import tables
            
            for run in self.runs:
                if run.status == run.DELETED and not with_deleted:
                    continue

                if job_list:
                    data.append(run.get_status_row(job_id=False))
                    
                    # print run info and clear data
                    tables.print_table(data, tablefmt="plain")
                    data = []
                    
                    # get all jobs for run if there are not
                    if not run.jobs:
                        run.set_jobs(type_=job_type)
                    
                    if run.is_dev and not with_dev:
                        dev_jobs = len(run.jobs)
                        utils.cprint("       --- {} dev jobs hidden (use --with-dev) ---".format(dev_jobs), fg=248)
                        print("")
                        continue

                    # loop and print jobs info
                    job_data = run.get_job_table(dates=True, src_ref=src_ref)
                    # get headers from first row
                    job_headers = job_data.pop(0)
                    
                    if len(job_data) > 0:
                        tables.print_table(job_data, job_headers, tablefmt="plain")
                    else:
                        utils.cprint("       --- No jobs ---", fg=248)
                    print("")
                elif archive_list:
                    data.append(run.get_status_row())

                    # print run and clear data
                    tables.print_table(data, tablefmt="plain")
                    data = []

                    # load the archives
                    run.load_archives()

                    # loop and print archive ifno
                    archive_data = run.get_archive_table()

                    # get headers from first row
                    headers = archive_data.pop(0)

                    if len(archive_data) > 0:
                        tables.print_table(archive_data, headers, tablefmt="plain")
                    else:
                        utils.cprint("       --- No archives ---", fg=248)

                    print("")

                else:
                    data.append(run.get_status_row(dates=dates, src_ref=src_ref, sizes=sizes, stats=stats))
                    if sizes:
                        # total up run sizes
                        local_size_total += run.local_size
                        remote_size_total += run.remote_size
                        archive_size_total += run.archive_size
            
            if sizes:
                # add dummy row for total size
                pre_col = 7 if dates else 4
                post_col = len(data[-1]) - pre_col - 1
                size_string = utils.bytes2human(local_size_total) + " (" + utils.bytes2human(remote_size_total) + ")" + " (" + utils.bytes2human(archive_size_total) + ")"
                data.append([" "]*pre_col + [cstr(size_string, attr=['bold'])] + [" "]*post_col)
            
            no_print = (job_list or archive_list)
            if not no_print:
                tables.print_table(data, headers, tablefmt="simple")


            # formats = plain, simple, grid, pipe, orgtbl

        # if we have notes print the last n here

        if notes is None:
            notes = config.configtool.getboolean('notes', 'print_with_status', fallback=False)
        
        if notes:
            last_n = config.configtool.getint('notes', 'number_with_status', fallback=2)
            self.get_notes(last_n=2)
        
            if self.notes:
                print("")
                note_str = cstr(f"      Last {last_n} notes", fg=3)
                print(note_str)
                for note in self.notes:
                    note_data = {
                        'created': utils.print_datetime(note.created),
                        'last_modifield': utils.print_datetime(note.last_modified),
                    }
                    cprint("          Date: {created}".format(**note_data), fg=248)
                    print(utils.indent_string(note.text, indent=10))
                    print("")
        
        print("")

    def get_status_row(self):
        """ Returns a list containing status information """
        row = []
        for field in self.status_fields:
            row.append(getattr(self, field))

        return row

    
    def move(self, destination, args=None):
        """ 
        Moves a case to a folder named destination 
       
        """
        parent = self.study.path
        abs_destination = os.path.abspath(destination)
        
        # do not allow moving outside the study directory
        if os.path.commonpath([parent]) != os.path.commonpath([parent, abs_destination]):
            utils.print_exception_info("Cannot move a case outside the study directory")
            sys.exit(1)

        # cannot move if the destination exists subdirectory of an existing case
        path = abs_destination
        while path != self.study.path:
            try:
                case = load_case(path, self.study)
            except exceptions.CaseDoesNotExist:
                pass
            else:
                utils.print_exception_info("An existing case exists at '{}'".format(path))
                sys.exit(1)
            finally:
                oldpath, path = path, os.path.dirname(path)
        
        # make a backup of the database
        try:
            print(" -> Backing up database ... ", end='\r')
            self.db.backup_db()
        except IOError as err:
            print(" -> Backing up database ... FAILED", end='\n')
            print(str(err))
            print("Copy halted")
            exit(1)
        else:
            print(" -> Backing up database ... OK", end='\n')
        
        ## CATCH KEBOARD HERE
        try:
            # modifiy the database
            print(" -> Modifying database record ... ", end="\r")
            
            # for cases we need to change
            # -> name
            # -> path
            # get a new database record
            db_record = self.db_record
            new_case_path = destination
            new_case_name = args.name.pop() if args.name is not None else new_case_path
            db_record.path = new_case_path
            db_record.name = new_case_name
            print(" -> Modifying database record ... OK", end="\n")

            # update the current case object
            old_case_name = self.name
            old_case_path = self.path
            self.name = new_case_name
            self.path = new_case_path

            
            print(" -> Modifying runs ... ", end="\r")
            # get all runs
            runs = self.get_runs(last_job=False)
            
            # modify the runs, keeping track of failed
            failed_runs = []
            old_staging_dirs = []
            new_staging_dirs = []
            case_staging_dirs = {}
            
            # we don't move staging unless there's at least 1 run staged.
            mv_staging = False 
            
            for run in runs:
                # fetch the jobs
                _jobs = run.get_jobs()
                
                run_record = run.db_record

                # change path#
                split_path = utils.splitpath(run_record.path)
                split_path[0] = new_case_path
                run_record.path = os.path.join(*split_path)
                run.path = run_record.path
                
                # change the name
                split_name = run_record.name.split('#')
                split_name[0] = new_case_name
                run_record.name = '#'.join(split_name)
                run.name = run_record.name

                if run.in_staging:
                    mv_staging = True
                    # update staging directory
                    old_staging_path = run_record.staging_dir
                    old_staging_dirs.append(old_staging_path)
                    split_path = utils.splitpath(old_staging_path)
                    try:
                        # we reverse the list, so that index returns the right most instance
                        ix = len(split_path) - 1 - split_path[::-1].index(old_case_path)
                    except ValueError:
                        failed_runs.append("Failed to update staging directory for run '{}'. Old case name not found in directory.".format(run.name))
                        continue

                    c = "/" + os.path.join(*split_path[:ix+1])
                    split_path[ix] = new_case_path
                    new_staging_path = "/" + os.path.join(*split_path)
                    new_staging_dirs.append(new_staging_path)
                    if os.path.isdir(new_staging_path):
                        failed_runs.append("Failed to copy run '{}', staging directory already exists".format(run.name))
                        self.logger.warning("Failed to copy run '%s', staging directory already exists", run.name)
                        continue
                    else:
                        case_staging_dirs[c] = "/" + os.path.join(*split_path[:ix+1])
                        run_record.staging_dir = new_staging_path
                        run.staging_dir = run_record.staging_dir


                # copy jobs
                for job in _jobs:
                    job_record = job.db_record

                    # change name
                    job_record.name = run.name
                    
                    # change working directory
                    old_wd = job_record.working_dir
                    split_path = utils.splitpath(old_wd)
                    try:
                        # find the right most index 
                        ix = len(split_path) - 1 - split_path[::-1].index(old_case_name)
                    except ValueError:
                        failed_runs.append("Failed to change wd for job '{}".format(job.id))
                    else:
                        split_path[ix] = new_case_path
                        new_wd = "/" + os.path.join(*split_path)
                        job.working_dir = new_wd
                        job_record.working_dir = job.working_dir
                        
                        # save
                        job_record.save()

                # save the record
                run_record.save()

            if mv_staging:
                # each run should be within 
                # group the staging paths according to the first 
                for src, dst in case_staging_dirs.items():
                    # check the dirname of the dst exsists
                    dirname = os.path.dirname(dst)
                    os.makedirs(dirname, exist_ok=True)
                    os.rename(src, dst)

                    self.logger.debug("Moving '%s' to '%s", src, dst)
                
            if failed_runs:
                for msg in failed_runs:
                    self.console.warning(msg)
                print(" -> Modifying runs ... PARTIAL", end="\n")
            else:
                print(" -> Modifying runs ... OK", end="\n")

            # hooks for any code actions
            self.code.move_case_hook(self)

            print(" -> Moving files ...", end="\r")
            os.rename(old_case_path, new_case_path)
            print(" -> Moving files ... OK", end="\n")

        except (KeyboardInterrupt, exceptions.DatabaseException,shutil.Error) as err:
            if isinstance(err, KeyboardInterrupt):
                print("Caught SIGINT ...")
            utils.print_exception_info(str(err), no_log=True)
            # restore backup
            print(" -> Restoring backup database ... ", end="\r")
            self.db.restore_db_backup()
            print(" -> Restoring backup database ... OK", end="\n")
            print("Copy halted")
            exit(1)
        else:
            print(" -> Saving new database record ...", end="\r")
            self.db.save(self.db_record)
            print(" -> Saving new database record ... OK", end="\n")
            print("Move complete")
            exit(0)


    
    def copy(self, destination, args=None):
        """ Copies a case to a folder named destination """
        parent = self.study.path
        abs_destination = os.path.abspath(destination)

        # do not allow moving outside the study directory
        if os.path.commonpath([parent]) != os.path.commonpath([parent, abs_destination]):
            utils.print_exception_info("Cannot copy a case outside the study directory")
            sys.exit(1)

        # cannot copy to an existing case or subdirectory of an existing case
        path = abs_destination
        while path != self.study.path:
            try:
                case = load_case(path, self.study)
            except exceptions.CaseDoesNotExist:
                pass
            else:
                utils.print_exception_info("An existing case exists at '{}'".format(path))
                sys.exit(1)
            finally:
                oldpath, path = path, os.path.dirname(path)
        
        # make a backup of the database
        try:
            print(" -> Backing up database ... ", end='\r')
            self.db.backup_db()
        except IOError as err:
            print(" -> Backing up database ... FAILED", end='\n')
            print(str(err))
            print("Copy halted")
            exit(1)
        else:
            print(" -> Backing up database ... OK", end='\n')

        ## CATCH KEBOARD HERE
        try:
            # modifiy the database
            print(" -> Creating new database record ... ", end="\r")
            # get a new database record
            db_record = self.db.new_case(
                name=os.path.basename(abs_destination),
                path=self.study.get_rel_path(abs_destination),
                study_id=self.study.db_record,
                created=datetime.today(),
                last_modified=datetime.today(),
                creator=utils.get_username(),
                code_db=self.code.db_record,
            )
            self.db.save(db_record)
            print(" -> Creating new database record ... OK", end="\n")

            # copy the files, construct an ignore list
            ignore_list = ['runs']
            # ask the code for any extras
            ignore_list+= self.code.get_case_ignore_list(self)
            
            if not args.with_history:
                ignore_list.append('.history') 
            
            if not args.with_post:
                ignore_list.append('post')
            
            if args.no_mesh:
                ignore_list.append('mesh')

            ignore = shutil.ignore_patterns(*ignore_list)

            print(" -> Copying files ... ", end="\r")
            shutil.copytree(self.path, abs_destination, ignore=ignore, symlinks=True, ignore_dangling_symlinks=True)
            #shutil.copytree(self.path, abs_destination)

            # create the post directory if required
            if not args.with_post:
                os.mkdir(os.path.join(abs_destination, 'post'))
            
            # create the mesh directory if required
            if args.no_mesh:
                os.mkdir(os.path.join(abs_destination, 'mesh'))

            if not args.with_history:
                from cfmt.vcs.controller import VCSController
                new_dir = os.path.join(abs_destination, ".history")
                os.mkdir(new_dir)
                vcs = VCSController.get_vcs('git')
                vcs.init(new_dir)
            
            # create the runs directory
            os.mkdir(os.path.join(abs_destination, 'runs'))
            new_run_dir = os.path.join(db_record.path, 'runs')

            # hooks for any code actions
            self.code.copy_case_hook(self, destination)

            print(" -> Copying files ... OK", end="\n")
            
            print(" -> Copying runs ... ", end="\r")
            # handle the runs
            if args.all:
                runs = self.get_runs(last_job=False)
            elif (args.run_names or args.run_status):
                runs = self.get_runs(names=args.run_names, status=args.run_status)
            else:
                runs = []
            
            if args.last_run:
                runs.append(self.get_last_run().pop())
            
            # remove duplicates
            runs = list(set(runs))

            # modify the runs, keeping track of failed
            failed_runs = []
            for run in runs:
                # fetch the jobs
                _jobs = run.get_jobs()
                
                run_record = run.db_record
                # set the id to zero, peewee will add a new record on save
                run_record.id = None

                # change path
                dir_name = os.path.basename(run.path)
                run_record.path = os.path.join(new_run_dir, dir_name)

                # change the name
                run_record.name = db_record.name + '#' + dir_name 

                # change case_id
                run_record.case=db_record

                # copy the run data
                # --> local directory, slow
                run_dst = os.path.join(self.study.path, run_record.path)
                shutil.copytree(run.abspath, run_dst, symlinks=True)
                
                if run.in_staging:
                    # update staging directory
                    study_dir = os.path.basename(self.study.path)
                    staging_dir = config.configtool.get('staging', 'location')
                    new_staging = os.path.join(staging_dir, study_dir, db_record.name, dir_name)
                    if os.path.isdir(new_staging):
                        failed_runs.append("Failed to copy run '{}', staging directory already exists".format(run.name))
                        self.logger.warning("Failed to copy run '%s', staging directory already exists", run.name)
                        continue
                    else:
                        # copytree creates required parent directories
                        try:
                            shutil.copytree(run.staging_dir, new_staging, symlinks=True)
                        except FileNotFoundError:
                            failed_runs.append("Failed to copy staging directory for run '{}'. No such file or folder.".format(run.name))
                            run_record.in_staging = 0
                            self.logger.warning("Staging directory '%s' for run '%s' not found.")
                        else:
                            # update the staging directory
                            run_record.staging_dir = new_staging

                # save the record
                run_record.save()

                # copy jobs
                for job in _jobs:
                    job_record = job.db_record
                    # set the id to zero, peewee will add a new record on save
                    job_record.id = None

                    # change run
                    job_record.run = run_record

                    # save
                    job_record.save()

                

            if failed_runs:
                for msg in failed_runs:
                    self.console.warning(msg)
                print(" -> Copying runs ... PARTIAL", end="\n")
            else:
                print(" -> Copying runs ... OK", end="\n")

        except (KeyboardInterrupt, exceptions.DatabaseException,shutil.Error) as err:
            if isinstance(err, KeyboardInterrupt):
                print("Caught SIGINT ...")
            utils.print_exception_info(str(err), no_log=True)
            # restore backup
            print(" -> Restoring backup database ... ", end="\r")
            self.db.restore_db_backup()
            print(" -> Restoring backup database ... OK", end="\n")
            print("Copy halted")
            exit(1)
        else:
            print("Copy complete")
            exit(0)

    
    def delete(self, 
               delete_runs=False,
               delete_case=False,
               keep_files=False,
               prompt=True,
               keep_database=True,
               staging_only=False):
        """
        Deletes the runs within a case, or if delete_case, all the runs.
        If keep_files is True, we only remove the entry from the database
        If keep_database is True, we only mark the runs as deleted.
        """
        if delete_case:
            # check for staging
            if self.runs:
                has_staging = True if True in [run.in_staging for run in self.runs] else False
            else:
                has_staging = False
            # --> files
            if not keep_files:
                self.console.warning("Removing directory '%s'", self.abspath)
                if has_staging:
                    staging_dir = config.configtool.get('staging', 'location')
                    staging_dir = os.path.join(staging_dir, os.path.basename(self.study.path), self.path)
                    self.console.warning("Removing staging directory '%s'", staging_dir)
            
            # we're deleting the entire case
            if prompt:
                _prompt = utils.confirm(" -> Delete case '{}'".format(self.name))
            else:
                print(" -> Deleting case '{}'".format(self.name))
                _prompt = True

            if _prompt:
                # --> database
                self.db.delete(db_record=self.db_record, cascade=True)
                self.logger.info("Deleted caue '%s'", self.name)

                # --> files
                if not keep_files:
                    shutil.rmtree(self.abspath, ignore_errors=True)
                    self.logger.info("Removed directory '%s'", self.abspath)
                    if has_staging:
                        shutil.rmtree(staging_dir, ignore_errors=True)
                        self.logger.info("Removed directory '%s'", staging_dir)

        elif delete_runs:
            if self.runs:
                for run in self.runs:
                    run.delete(keep_files=keep_files,
                               prompt=prompt,
                               keep_database=keep_database,
                               staging_only=staging_only)
