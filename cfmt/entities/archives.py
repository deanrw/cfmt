"""
    cft: A management tool for Computational Fluid Dynamics

    archives.py: For run archives
"""
import importlib

import cfmt.config as config
from cfmt import exceptions, utils
from cfmt.archives import base

AVAILABLE_ARCHIVE_TYPES = {
    "tar": "TarArchive",
    "borg": "BorgArchive"
}
    
def get_module(archive_type):
    """ Returns the archive module """
    if archive_type in AVAILABLE_ARCHIVE_TYPES:
        # import the module
        module = importlib.import_module("cfmt.archives." + str(archive_type))

        # get the type
        return getattr(module, AVAILABLE_ARCHIVE_TYPES[archive_type])
    else:
        raise ImportError(f"Module '{archive_type}' not found")


def new_archive(run, archive_type=None):
    """ Returns a new archive """
    
    # determine the type
    if archive_type is None:
        archive_type = config.configtool.get('archive', 'type', fallback=base.DEFAULT_ARCHIVE_TYPE)

    # return the correct class
    if archive_type in AVAILABLE_ARCHIVE_TYPES:
        archive = get_module(archive_type)
    else:
        msg = "Invalid archive type '{}'".format(str(archive_type))
        utils.print_exception_info(msg)
        raise exceptions.ArchiveFailed(msg)

    return archive(run)

def load_archive(run, db_record):
    """ Loads an existing archive """

    assert db_record is not None

    # determine the type
    archive_type = db_record.type
    # return the correct class
    if archive_type in AVAILABLE_ARCHIVE_TYPES:
        archive = get_module(archive_type)
    else:
        msg = "Invalid archive type '{}'".format(str(archive_type))
        utils.print_exception_info(msg)
        raise exceptions.ArchiveFailed(msg)
    
    return archive(run, db_record=db_record)

def check_archive_repo(case, host, path, archive_type=None):
    """ Check an archive repository """
    # determine the type
    if archive_type is None:
        archive_type = config.configtool.get('archive', 'type', fallback=base.DEFAULT_ARCHIVE_TYPE)
    
    # return the correct class
    if archive_type in AVAILABLE_ARCHIVE_TYPES:
        archive = get_module(archive_type)
    else:
        msg = "Invalid archive type '{}'".format(str(archive_type))
        utils.print_exception_info(msg)
        raise exceptions.ArchiveFailed(msg)

    return archive.check_repo(case, host, path)


def init_archive_repo(host, path, archive_type=None):
    """ Initializes an archive """
    # determine the type
    if archive_type is None:
        archive_type = config.configtool.get('archive', 'type', fallback=base.DEFAULT_ARCHIVE_TYPE)
    
    if path is None:
        path = config.configtool.get('archive', 'path')
    
    # return the correct class
    if archive_type in AVAILABLE_ARCHIVE_TYPES:
        archive = get_module(archive_type)
    else:
        msg = "Invalid archive type '{}'".format(str(archive_type))
        utils.print_exception_info(msg)
        raise exceptions.ArchiveFailed(msg)

    return archive.init_repo(host, path)



