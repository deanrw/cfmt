"""
    cfmt: A Computational Fluids Management Tool
"""
import json
from datetime import datetime
import cfmt.utils as utils

class DBProperty(object):
    """
    Base class for a database property
    """
    def __init__(self):
        self.label = None

    def __get__(self, instance, owner):
        #print("getting", self.label)
        if "db_record" in instance.__dict__:
            db_record = instance.__dict__.get("db_record")
            return getattr(db_record, self.label)
        else:
            raise AttributeError("Database record not present")
        #if hasattr(instance, "db_record"):
        #    db_record = 
        #    return 
        #return instance.__dict__.get(self.label, None)
    
    def __set__(self, instance, value):
        #print("setting", self.label)
        instance.__dict__[self.label] = value
        if hasattr(instance, "db_record"):
            record = getattr(instance, "db_record")
            setattr(record, self.label, value) 

class DBJsonProperty(DBProperty):
    """
    Database property which translates between JSON and a dict
    """
    def __get__(self, instance, owner):
        if "db_record" in instance.__dict__:
            db_record = instance.__dict__.get("db_record")
            _json = getattr(db_record, self.label)
            return json.loads(str(_json))
        
    def __set__(self, instance, value):
        instance.__dict__[self.label] = value
        if hasattr(instance, "db_record"):
            record = getattr(instance, "db_record")
            _json = json.dumps(value)
            setattr(record, self.label, _json) 


class DBPropertyMeta(type):
    """
    Meta class for properties.
    This ensures that all properties have labels set automatically
    See: https://nbviewer.jupyter.org/urls/gist.github.com/ChrisBeaumont/5758381/raw/descriptor_writeup.ipynb
    """

    def __new__(cls, name, bases, attrs):
        # upon class creation, find all objects and auto-set their lables

        # add attributes from the database
        #if "_dbmodel" in attrs:
        #    attrs["_dbfields"] = []
        #    for field in attrs["_dbmodel"]._meta.fields.keys():
        #        attrs["_dbfields"].append(field)
        #        if field not in attrs:
        #            attrs[field] = DBProperty()
        
        for n, v in attrs.items():
            if isinstance(v, DBProperty):
                v.label = n
        return super().__new__(cls, name, bases, attrs)

class BaseEntity(object, metaclass=DBPropertyMeta):
    """ Base class for all entities to inherit """

    # blank template
    template = """
        No template specified
    """
    info_fields = []
    db_fields = []


    def info(self):
        """ Outputs information about the study """
        data = {}
        for field in self.info_fields:
            _field = getattr(self, field)
            if isinstance(_field, datetime):
                _field = utils.print_datetime(_field)
            if field == "code_db":
                _field = self.db.get_code_names(self.db_record)
            
            data[field] = _field

        template = self.template
        
        return utils.remove_left_margin(template.format(**data))

    def _load_db_record(self, db_record, ignore_fields=[]):
        """
        Given a db record, load in to the object
        """
        for item in db_record._meta.fields.keys():
            if hasattr(self, item) and item not in ignore_fields:
                setattr(self, item, getattr(db_record, item))

        setattr(self, "db_record", db_record)

    def _to_json(self, _dict):
        """ Converts a dict to json """
        assert isinstance(_dict, dict)
        return json.dumps(_dict)

    def _from_json(self, _json):
        """ Converts a json string to a dict """
        assert isinstance(_json, str)
        return json.loads(_json)

class DBStr(object):
    """
    Class to attach database fields to 
    """
    #def __init__()

class DBDate(object):
    pass
