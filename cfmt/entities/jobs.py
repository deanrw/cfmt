"""
    cfmt: A management tool for Computational Fluid Dynamics
    
    entities/job.py: New job
"""
import os
import sys
import logging
import time

import colored
from datetime import datetime, timedelta

import cfmt.utils as utils
from cfmt.config import configtool as config
from cfmt import exceptions
from cfmt.entities.base import BaseEntity, DBProperty, DBJsonProperty

class Job(BaseEntity):
    """
    The Job Class
    """
    #db_fields = [
    #    'id',
    #    'name',
    #    'batch_system',
    #    'batch_queue',
    #    'run',
    #    'sys_command',
    #    'job_id',
    #    'submit_host',
    #    'execution_host',
    #    'status',
    #    'user',
    #    'submitted',
    #    'started',
    #    'finished',
    #    'pe',
    #    'slots',
    #    'exit_status',
    #    'cpu_time',
    #    'vmem',
    #    'mem',
    #    'rusage',
    #    'created',
    #    'last_modified',
    #    'working_dir',
    #    'code_src_ref',
    #    'batch_args'
    #]

    """
    The following DBProperties are set so that, once the db_record is loaded, 
    any setting of the relevant attribute will also update the db_record
    """
    id = DBProperty()
    name = DBProperty()
    batch_system = DBProperty()
    batch_queue = DBProperty()
    #run = DBProperty()
    sys_command = DBProperty()
    working_dir = DBProperty()
    job_id = DBProperty()
    submit_host = DBProperty()
    execution_host = DBProperty()
    status = DBProperty()
    user = DBProperty()
    submitted = DBProperty()
    started = DBProperty()
    finished = DBProperty()
    pe = DBProperty()
    slots = DBProperty()
    exit_status = DBProperty()
    cpu_time = DBProperty()
    vmem = DBProperty()
    mem = DBProperty()
    rusage = DBProperty()
    created = DBProperty()
    last_modified = DBProperty()
    code_src_ref = DBProperty()
    case_src_ref = DBProperty()
    allocation_units = DBProperty()
    options = DBJsonProperty()
    type = DBProperty()
    stdout = DBProperty()
    stderr = DBProperty()

    # STATUS levels
    # these are a subset of the error definitions in cfmt.entities.runs
    CREATED = 10    # a job has been created
    SUBMITTED = 30  # a job has been submitted
    QUEUING = 40    # the job is queuing
    HOLDING = 45    # the job is on hold
    RUNNING = 50    # the job is running
    COMPLETED = 55  # the job has successfully completed
    ABORTED = 60    # the batch system has aborted the job
    SUSPENDED = 65  # the job has been suspended
    ERROR = 90      # the job has exited in an error

    _level_to_name = {
        CREATED: 'created',
        SUBMITTED: 'submitted',
        QUEUING: 'queuing',
        HOLDING: 'holding',
        RUNNING: 'running',
        COMPLETED: 'completed',
        ABORTED: 'aborted',
        SUSPENDED: 'suspended',
        ERROR: 'ERROR',
    }
    _name_to_level = {
        'created': CREATED,
        'submitted': SUBMITTED,
        'queuing': QUEUING,
        'holding': HOLDING,
        'running': RUNNING,
        'completed': COMPLETED,
        'aborted': ABORTED,
        'suspended': SUSPENDED,
        'error': ERROR,
    }
    _level_to_color = {
        CREATED: colored.fg(51),
        SUBMITTED: colored.fg(38),
        QUEUING: colored.fg('gold_3b'),
        HOLDING: colored.fg(100),
        RUNNING: colored.fg(46) + colored.attr('bold'),
        COMPLETED: colored.fg(34),
        ABORTED: colored.fg('white') + colored.bg('red'),
        SUSPENDED: colored.fg(166),
        ERROR: colored.fg('red') + colored.attr('bold'),
    }
    # set valid names
    valid_status_names = []
    for name in _name_to_level:
        valid_status_names.append(name)

    # job types
    SOLVER = 10  # the job is running a code
    POST = 20    # the job is running a post processor
    SYNC = 30    # the job is running a sync script.
    FETCH = 32   # the job is running a fetch script.
    MOVE_STAGING = 34 # the job is running a script to move staging

    _type_to_name = {
        SOLVER: "solver",
        POST: "post",
        SYNC: "sync",
        FETCH: "fetch",
        MOVE_STAGING: "move_staging"
    }
    _type_to_symbol = {
        SOLVER: "S",
        POST: "P",
        SYNC: "T",
        FETCH: "F",
        MOVE_STAGING: "M"
    }
    _name_to_type = {
        "solver": SOLVER,
        "post": POST,
        "sync": SYNC,
        "fetch": FETCH,
        "move_staging": MOVE_STAGING
    }

    # store valid types
    valid_type_names = []
    for name in _name_to_type:
        valid_type_names.append(name)

    JOB_FINISHED = [COMPLETED, ABORTED, ERROR]
    
    def __init__(self, db_record=None, db=None, run=None, batch=None, binary_path=None,
                       working_dir=None, job_name=None, batch_system=None, options=None,
                       command=None, slots=None, type_=None):
        """ 
        Constructor 

        We either create a new job based on the passed information or 
        retrieve the job from a provided database record
        
        """
        # store the loggers
        self.logger = logging.getLogger('file')
        self.console = logging.getLogger('console')

        # store the defaults
        self.db = None
        self.created = None
        self.batch_queue = None
        self.fsubmission_script = None   # submission script

        # do we have a db_record
        if db_record is not None:
            # set the database record
            self._load_db_record(db_record, ignore_fields=["run"])
            if run is not None:
                self.run = run
            else:
                # fetch from db
                raise NotImplementedError("Fix me")
                #self.run = db.get_run(run_id=self.run_id)

            if db is not None:
                # store
                self.db = db
            elif self.run is not None:
                if hasattr(self.run, 'db'):
                    self.db = self.run.db
                elif hasattr(self.run, 'case'):
                    if hasattr(self.run.case, 'db'):
                        self.db = self.run.case.db
            
            if self.db is None:
                raise exceptions.NoDB

            # make sure the command list is set         
            sys_command = self.sys_command
            if sys_command is not None:
                self.command_list = sys_command.split()
            else:
                self.command_list = None
        
        else:
            # we create from the passed information
            # --> check we've been passed a run
            if run is not None:
                self.run = run
            else:
                msg = "Run not provided to job instance"
                self.logger.error(msg)
                raise exceptions.NotARun(msg)
            # --> store a reference to the db controller
            if db is None:
                # attempt to get from the run
                try:
                    self.db = self.run.db
                except AttributeError:
                    self.logger.debug("Run has no database object")
                    raise exceptions.NoDB("No Database provided")
            else:
                # we're provided it directly
                self.db = db

            # --> get the batch system from the run
            if batch_system is None:
                msg = "Batch system not provided"
                self.logger.error(msg)
                raise ValueError(msg)

            if command is None:
                msg = "System command not provded"
                self.logger.error(msg)
                raise ValueError(msg)
            else:
                assert isinstance(command, list)

                self.command_list = command
                sys_command = self.process_command(command)

            if slots is None:
                msg = "Number of ranks not provided"
                self.logger.error(msg)
                raise ValueError(msg)
            
            if options is None:
                options = {}

            # --> store the job_name 
            if job_name is None:
                job_name = run.name if run.name is not None else "CFMT Job" 

            # --> store the working directory and binary requested
            if working_dir is None:
                if isinstance(run.working_dir, str):
                    working_dir = run.working_dir
                else:
                    msg = "Working directory not provided"
                    self.logger.error(msg)
                    raise ValueError(msg)
            else:
                assert isinstance(working_dir, str)

            if type_ is None:
                msg = "Job type not provided"
                self.logger.error(msg)
                raise ValueError(msg)

            #if binary_path is None:
            #    if isinstance(run.binary_path, (str, list)):
            #        binary_path = run.binary_path
            #    else:
            #        msg = "Binary path not provided"
            #        self.logger.error(msg)
            #        raise ValueError(msg)
            #else:
            #    assert isinstance(binary_path, (str, list))
        
            # -> user
            user = utils.get_username()

            # -> submit host
            import socket
            submit_host = socket.getfqdn()
            
            created = datetime.today()
            db_record = self.db.new_job(
                name=job_name,
                status=self.CREATED,
                run=run.db_record,
                batch_system=batch_system,
                batch_queue=None,
                last_modified=created,
                created=created,
                user=user,
                sys_command = sys_command,
                submit_host = submit_host,
                working_dir = working_dir,
                options = self._to_json(options),
                slots = slots,
                type = type_
            )
            self._load_db_record(db_record, ignore_fields=["run"])
        
        # set some useful properties
        # -> wallclock
        self.wallclock = None
        if isinstance(self.finished, datetime) and isinstance(self.started, datetime):
            # compute the difference and display in hrs
            self.wallclock = self.finished - self.started

    def create(self):
        """ Creates the database record for the job """

        #self.created = datetime.today()
        #self.status = self.CREATED
        # note we can't link the run here, since the new run has not been written to the db
    
    def process_command(self, command):
        """ given a command list sets the command properties """
        assert isinstance(command, list), "Command must be a list"

        # convert the command to a string
        command_str = " ".join("{0}".format(w) for w in command)
        self.logger.debug("Set run command '%s'", command_str)
        return command_str
        #self.set_db(sys_command=self.sys_command)
        #self.command_list = command

    
    def generate_command(self, args=None, cmd=None):
        """ Generates a command to run the job """

        # pass the job object to the batch system
        self.batch.set_job(self)
       
        # set the command, overwrite if cmd is set
        if cmd is None:
            cmd = self.batch.get_command()

        # if we generated a submission script, store it
        if self.batch.submission_script is not None:
            self.fsubmission_script = self.batch.submission_script
        
        self.set_command(cmd)

        # set the queue
        self.batch_queue = self.batch.queue

    
    def parse_sys_command(self):
        """ Parses the provided string command and sets the command list """
        assert isinstance(self.sys_command, str)

        self.command_list = self.sys_command.split()

    def load(self):
        """
        Loads an existing job
        """
        from cfmt.batch_systems.controller import Batch
        # check for what we need
        assert hasattr(self, 'batch_system')
        self.batch = Batch(self.batch_system)
    
        self.parse_sys_command()

    
    def submit(self):
        """ 
        Submits the job
        """

        # check the sys_command still has at least something in it
        assert len(self.command_list) > 0, "Sys command incorrect"

        # save the job record
        self.save()

        # start the subprocess
        self.batch.submit(self)

    def delete(self, prompt=True):
        """
        Deletes a job
        """

        if prompt:
            _prompt = utils.confirm(" -> Delete job '{}'".format(self.id))
        else:
            print(" -> Deleting job '{}' ...".format(self.id))
            _prompt = True

        if _prompt:
            # -> database
            self.db.delete(db_record=self.db_record)
            self.logger.info("Deleted job '%s'", str(self.id))

    def set_db(self, **kwargs):
        """ 
        Sets values to add to the database 
        **kwargs can be any of the db_fields
        DEPRECATED: This is kept to ensure compatibility with older code
        """
        for k, v in kwargs.items():
            if hasattr(self, k):
                attr = getattr(self, k)
                if isinstance(attr, DBProperty):
                    attr = v

        #for db_field in self.db_fields:
        #    if db_field in kwargs:
        #        # we update the db_record
        #        setattr(self.db_record, db_field, kwargs[db_field])
    
    def save(self, cascade=False, update=False):
        """
        Saves the db_record to the databsse
        """
        if update:
            self.update()
        
        # update last modified
        self.last_modified = datetime.today()
        #self.set_db(last_modified=datetime.today())

        # save the database record 
        #update_id = True if self.db_record.id is None else False
        self.db_record.save()
        
        #if update_id:
        #    self.id = self.db_record.id
        
        if cascade:
            # save the run if we have it
            if hasattr(self, 'run'):
                if hasattr(self.run, 'save'):
                    self.run.save(cascade=True)

    def set_status(self, status):
        """ Checks for a valid status """
        assert isinstance(status, int)

        self.status = status

    def update_status(self, status, update_run=False, update_code=True):
        """ 
        Acts on a change in job status
        Optionally updates the parent run
        Optionally tells the code
        """
        assert isinstance(status, int)

        current_status = self.status
        self.logger.info(
            "Updated status for JOB '%s': %s -> %s",
            self.id,
            self._level_to_name[current_status],
            self._level_to_name[status]
        )
        
        self.set_status(status)

        if current_status != status:
            self.on_status_change(status, current_status)
        
        if update_run:
            # if we've submitted a sync job, we set the run type to transferring,
            #  unless completed 
            if self.type in [self.SYNC, self.FETCH, self.MOVE_STAGING]:
                if status not in self.JOB_FINISHED:
                    self.run.set_status(self.run.TRANSFERRING)
                else:
                    self.run.set_status(status)
            else:
                self.run.set_status(status)


        if update_code and self.type == self.SOLVER:
            self.run.code.job_status_change(self, current_status, status)

    def update(self, **kwargs):
        """
        Updates the job, set's database values as required
        """
        # if we have kwargs, we set these 
        for arg, value in kwargs.items():
            if arg == 'status':
                if value in self.valid_status_names:
                    value = self._name_to_level[value]

            # try to set the value
            try:
                new = value
                old = getattr(self, arg)
                if new != old:
                    self.logger.debug("Updating job '%d', %s: '%s' -> '%s'", self.id, arg, old, new)
                    setattr(self, arg, value)
            except AttributeError:
                raise exceptions.UpdateFailed("Failed to set attribute '%s' on run '%s", arg, self.name)
        
        # sync the db_fields with the job attributes
        #for field in self.db_fields:
        #    try:
        #        new = getattr(self, field)
        #        old = getattr(self.db_record, field)
        #        #print(field, new, old)
        #        # do not overwrite the id
        #        if field == 'run':
        #            continue
        #        if new != old:
        #            self.logger.debug("Updating job '%s', %s: '%s' -> '%s'", self.name, field, old, new)
        #            setattr(self.db_record, field, new)
        #    except AttributeError:
        #        raise
    
    def on_status_change(self, new_status, old_status):
        """ Called when the job status changes """
        if new_status in self.JOB_FINISHED:
            # compute the job allocation if 
            compute_alloc = config.getboolean("defaults", "compute_allocation", fallback=False)
            if compute_alloc:
                allocation_units = self.compute_allocation()
                if allocation_units:
                    self.allocation_units = allocation_units

            # for sync jobs, we need to fetch the stdout/stdin
            if self.type in [self.SYNC, self.FETCH] and old_status != self.CREATED:
                rfilter = []
                rfilter.append(['+', self.get_stderr_fname(bypass_code=True)])
                rfilter.append(['+', self.get_stdout_fname(bypass_code=True)])
                rfilter.append(['-', "*"])
                
                ps = True if self.type == self.SYNC else False
                ks = True if self.type == self.SYNC else False
                # the run will update itself here 
                self.run.transfer_down(preserve_source=ps, keep_staged=ks,
                                       filter=rfilter, silent=True)

            # for moving staging jobs, we need to update the staging directory 
            if self.type == self.MOVE_STAGING and old_status != self.CREATED:
                # we update the staging directory if the exit code is OK
                # the staging directory is the same as the wd of the job
                if self.exit_status == str(0):
                    self.run.update(staging_dir=self.working_dir)
                else:
                    msg = f"WARNING: Moving staging job {self.id} had exit code {self.exit_status}"
                    print(msg)
                    self.logger.error(msg)

                
    def compute_allocation(self):
        """ 
        Compute the allocation quota for the job 
        
        We take the script name specified in the study config, located in the input directory
        """
        fname = config.get("defaults", "compute_allocation_script_name", fallback=None)
        
        if fname is None:
            self.logger.warning("Compute allocation requested but invalid script name")
            return
        
        study_dir = self.run.study.path
        fpath = os.path.join(study_dir, "input", fname)

        if not os.path.exists(fpath):
            self.logger.warning("Compute allocation requested but script not found: '%s'", fpath)
            return
        
        if not os.access(fpath, os.X_OK):
            self.logger.warning("Compute allocation requested but script not executable: '%s'", fpath)
            return

        import subprocess
        # we send some variables to help
        cpu_time = self.cpu_time
        if not cpu_time:
            self.logger.warning("Compute allocation requested but script not executable: '%s'", fpath)
            cpu_time = -1.0

        slots = self.slots
        wallclock = (self.finished - self.started).total_seconds()
        
        cmd = [fpath, str(cpu_time), str(slots), str(wallclock)]

        proc = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if proc.returncode != 0:
            msg = f"WARNING: Script '{fname}' returned non-zero exit code"
            print(msg)
            self.logger.warning(msg + proc.stderr)
            return
        else:
            try:
                allocation_units = float(proc.stdout.strip())
            except ValueError as err:
                utils.print_exception_info(err)
                self.logger.warning("Failed to convert output of script '%s' to float", fname, exc_info=err)
            else:
                return allocation_units

        return 

    #def has_completed(self, quiet=True):
    #    """
    #    Called when a batch system determines the job has completed successfully
    #    """
    #    self.update_status(self.COMPLETED, update_run=True)

    #    # ask the code
    #    try:
    #        if not quiet:
    #            print(" -> Finalising job '{}' ... ".format(self.id), end="\r")
    #        self.run.code.job_completed(self)
    #    except exceptions.PostFailure as err:
    #        if not quiet:
    #            utils.print_exception_info(err)
    #            print(" -> Finalising job '{}' ... FAILED".format(self.id), end="\n")
    #        else:
    #            self.logger.error("Finalising job '%s' failed", str(self.id))
    #    else:
    #        if not quiet:
    #            print(" -> Finalising job '{}' ... OK".format(self.id), end="\n")

    
    def set_submitted(self,submitted):
        """ 
        Sets required attributes once a job has been succesfully submitted
        """
        pass

    # TODO: REmove as unused 
    #def link_run(self, run=None):
    #    """ Links a run with the current job """
    #    if run is not None and hasattr(run.db_record, 'id'):
    #        self.set_db(run=run.db_record)
    #        self.save()
    #    elif self.run is not None and hasattr(self.run.db_record, 'id'):
    #        self.set_db(run=run.db_record)
    #        self.save()

    def set_rusage(self, rusage_obj, cpu_time=None, mem=None, vmem=None):
        """ 
        Takes a resource.rusage_struct object. Extracts CPU and VMEM and stores
        all as a JSON in  the rusage column
        """
        import json

        # cpu_time is utime + stime
        if cpu_time is None:
            if hasattr(rusage_obj, 'ru_utime') and hasattr(rusage_obj, 'ru_stime'):
                cpu_time = float(rusage_obj.ru_utime + rusage_obj.ru_stime)

        # for linux systems ru_maxrss is reported in kilobytes
        if mem is None:
            if hasattr(rusage_obj, 'ru_maxrss'):
                mem = float(rusage_obj.ru_maxrss)

        rusage_json = str(json.dumps(rusage_obj))
        
        self.cpu_time = cpu_time
        self.mem = mem
        self.rusage = rusage_json
        self.vmem = vmem

    
    @classmethod
    def get_total_wallclock(cls, jobs):
        """ 
        Computes the total wallclock time for the passed jobs
        """
        seconds = sum(j.wallclock.total_seconds() for j in jobs if hasattr(j, "wallclock") and j.wallclock is not None)
        return timedelta(seconds=seconds)
    
    @classmethod
    def get_total_cputime(cls, jobs):
        """ 
        Computes the total wallclock time for the passed jobs
        """
        return sum(j.cpu_time for j in jobs if hasattr(j, "cpu_time") and j.cpu_time is not None)
        
    @staticmethod 
    def get_status_headers(dates=False, src_ref=False, columns=None):
        """ Returns the header output for the status table """
        
        if columns is None:
            columns = [
                'id',
                'batch',
                'job_number',
                'status'
            ]

        if dates:
            columns.append('submitted')
            columns.append('finished')
        if src_ref:
            columns.append('src_ref')
        
        columns.append('slots')
        columns.append('wallclock')
        
        if config.getboolean("defaults", "compute_allocation", fallback=False):
            columns.append('au')

        header_mapping = {
            'id': utils.cstr('ID', fg=248),
            'batch': utils.cstr('Batch', fg=248),
            'job_number': utils.cstr('Job #', fg=248),
            'submitted': utils.cstr('Submit/Start', fg=248),
            'finished': utils.cstr('Finished', fg=248),
            'finished_h': utils.cstr('Finished', fg=248),
            'slots': utils.cstr('Slots', fg=248),
            'wallclock': utils.cstr('Wallclock', fg=248),
            'vmem': utils.cstr('vmem', fg=248),
            'src_ref': utils.cstr('SHA1', fg=248),
            'status': utils.cstr('Status', fg=248),
            'run_status': utils.cstr('Run Status', fg=248),
            'name': utils.cstr('Name', fg=248),
            'tags': utils.cstr('Run tags', fg=248),
            'au': utils.cstr('AU', fg=248)
        }
        
        status_headers = []
        columns = utils.make_list_unique(columns)

        for key in columns:
            if key in header_mapping:
                header = header_mapping[key]
            else:
                header = 'N/A'
            
            status_headers.append(header)

        return status_headers
    
    def color_status(self, status):
        """ Colors the status """
        from colored import stylize
        
        name = stylize(self._level_to_name[status], self._level_to_color[status])
        return name

    def get_status_row(self, dates=False, src_ref=False, columns=None, color_row=True):
        """
        Returns a list of items to print in a table
        """
        row = []

        if columns is None:
            columns = [
                'id',
                'batch',
                'job_number',
                'status'
            ]

        if dates:
            columns.append('submitted')
            columns.append('finished')
        if src_ref:
            columns.append('src_ref')
        
        columns.append('slots')
        columns.append('wallclock')
        
        if config.getboolean("defaults", "compute_allocation", fallback=False):
            columns.append('au')

        columns = utils.make_list_unique(columns)

        if isinstance(self.started, datetime):
            submitted = utils.print_datetime(self.started)
        elif isinstance(self.submitted, datetime):
            submitted = utils.print_datetime(self.submitted)
        else:
            submitted = utils.cstr("N/A", fg=238)
        
        if isinstance(self.finished, datetime):
            finished = utils.print_datetime(self.finished)
            finished_h = utils.print_datetime(self.finished, timeago=True)
        else:
            finished = utils.cstr("N/A", fg=238)
            finished_h = utils.cstr("N/A", fg=238)
            
        if isinstance(self.finished, datetime) and isinstance(self.started, datetime):
            # compute the difference and display in hrs
            wallclock = utils.diff_datetime_str(self.finished - self.started)
        elif isinstance(self.started, datetime) and self.status == self.RUNNING:
            wallclock = utils.diff_datetime_str(datetime.now() - self.started)
        else:
            wallclock = "-"

        type_symbol = self._type_to_symbol[self.type]
        
        
        # for sync and fetch jobs, we displace the run name to make identification easier
        id_ = str(type_symbol) + " " + str(self.id) if self.id is not None else "-"
        if self.type != self.SOLVER:
            name = utils.cstr(self.run.name, fg=183)
            id_ = utils.cstr(id_, fg=183)
        else:
            name = self.name if self.name is not None else "-"

        row_mapping = {
            'id': id_,
            'batch': self.batch_system if self.batch_system is not None else "-",
            'job_number': str(self.job_id) if self.job_id is not None else "-",
            'submitted': submitted,
            'finished': finished,
            'finished_h': finished_h,
            'slots': self.slots if self.slots is not None else "-",
            'wallclock': wallclock, 
            'vmem': self.vmem if self.vmem is not None else "-",
            'src_ref': self.case_src_ref if self.case_src_ref is not None else "-",
            'status': self.color_status(self.status),
            'run_status': self.run.color_status(self.run.status),
            'name': name,
            'tags': self.run.get_tag_str(),
            'au': "{:.2f}".format(self.allocation_units) if self.allocation_units else "-"
        }
        
        for key in columns:
            if key in row_mapping:
                cell = row_mapping[key]
            else:
                cell = 'N/A'
            
            row.append(cell)

        if color_row:
            from colored import stylize
            # apply to the entire row
            for item in row:
                base_string = utils.str_remove_ansi(str(item))
                row[row.index(item)] = stylize(base_string, self._level_to_color[self.status])

        # indent first row
        row[0] = utils.cstr("      ", attr=['hidden']) + row[0]

        return row

    def info(self):
        """
        Prints out information about the job
        """

        template = """
            === CFMT Job ===
            --------------------
            ID: {id}  |  {status_name}
            --------------------
            Part of Run         : {run_name}
            Part of Case        : {case_name}
            Part of Study       : {study_name} 
            Working directory   : '{working_dir}'

            Job Name            : {name}
            Batch System        : {batch_system}
            Batch Job ID        : {job_id}
            Batch Options       : {batch_options}
            
            Code                : {code_name}
            Code Options        : {code_options}
            
            Submit Host         : {submit_host}
            Command Executed    : {sys_command}
            Execution Host      : {execution_host}
            Submitted           : {submitted}
            Started             : {started}
            Finished            : {finished}

            Case src sha1       : {case_src_ref}
            
            Exit Status         : {exit_status}
            Wallclock           : {wallclock}
            CPU Time            : {cpu}
            VMEM                : {vmem}
            AU                  : {au}
        """
        # account for instances where options do not exist
        options = {}
        for opt in ["batch", "code"]:
            if opt in self.options:
                options[opt] = ", ".join([f"{i}={v}" for i, v in self.options[opt].items()])
            else:
                options[opt] = "N/A"
        
        data = {
            'id': self.id,
            'run_name': self.run.name,
            'case_name': self.run.case.name,
            'study_name': self.run.case.study.name,
            'status_name': self.color_status(self.status),
            'working_dir': self.working_dir,
            'batch_system': self.batch_system,
            'batch_options': options["batch"],
            'code_name': self.run.code.name,
            'code_options': options["code"],
            'submitted': utils.print_datetime(self.submitted) if self.submitted is not None else "-",
            'started': utils.print_datetime(self.started) if self.started is not None else "-",
            'finished': utils.print_datetime(self.finished) if self.finished is not None else "-",
            'wallclock': utils.diff_datetime_str(self.wallclock)  if self.wallclock is not None else "-",
            'cpu': self.cpu_time if self.cpu_time else '-',
            'vmem': utils.bytes2human(self.vmem) if self.vmem else '-',
            'name': self.name,
            'submit_host': self.submit_host if self.submit_host else "-",
            'execution_host': self.execution_host if self.execution_host else "-",
            'sys_command': self.sys_command if self.sys_command else "-",
            "job_id": self.job_id if self.job_id else "-",
            'case_src_ref': self.case_src_ref if self.case_src_ref else "-",
            'au': "{:.6f}".format(self.allocation_units) if self.allocation_units else "-",
            'exit_status': self.exit_status if self.exit_status else "-"
        }

        return utils.remove_left_margin(template.format(**data))

    def get_stdout_fname(self, bypass_code=False, bypass_db=False, wd=None):
        """ Return the stdout file for the job """
        
        
        if self.type != self.SOLVER:
            bypass_code = True
        
        # allow the code to bypass
        if hasattr(self, "run") and not bypass_code:
            run = self.run
            if run and hasattr(run, "code"):
                code = run.code
                if code and hasattr(code, "get_stdout_fname"):
                    return code.get_stdout_fname(self, wd=wd)

        # if we've got something stored in the db return it
        elif self.stdout is not None:
            return self.stdout

        # otherwise, we ask the batch system
        from cfmt.batch_systems.controller import get_batch_cls
        
        batch_system = self.batch_system
        batch_class = get_batch_cls(batch_system)
        
        # return the filename
        return batch_class.get_stdout_fname(self)

    def get_stderr_fname(self, bypass_code=False, wd=None):
        """ Return the stderr file for the job """
        
        # if we've got something stored in the db return it
        
        if self.type != self.SOLVER:
            bypass_code = True
        
        # allow the code to bypass
        if hasattr(self, "run") and not bypass_code:
            run = self.run
            if run and hasattr(run, "code"):
                code = run.code
                if code and hasattr(code, "get_stderr_fname"):
                    return code.get_stderr_fname(self, wd=wd)
        
        elif self.stderr is not None:
            return self.stderr
        
        # we reqeuest a new fname from the batch system
        from cfmt.batch_systems.controller import get_batch_cls
        
        batch_system = self.batch_system
        batch_class = get_batch_cls(batch_system)

        # return the filename
        return batch_class.get_stderr_fname(self)