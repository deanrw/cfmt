"""
    cfmt: A management tool for Computational Fluid Dynamics
    
    entities/runs.py: New run
"""
import os
import sys
import shutil
import logging
import time
from datetime import datetime
import colored

import cfmt.utils as utils
import cfmt.config as config
from cfmt import exceptions
from cfmt.entities.base import BaseEntity

def load_run(path, study=None):
    """ 
    Load a run at the given path, using either the provided case or study
    """
    if path is None:
        path = os.getcwd()
    else:
        path = os.path.normpath(path)

    if study is not None:
        rel_path = study.get_rel_path(os.path.abspath(path))
    else:
        raise NotImplementedError

    # load the database
    from cfmt.db import controller as dbc
    if study.db is not None:
        db = study.db
    else:
        db = dbc.load_database(study.path)

    # fetch the run from the database
    run = db.get_run(path=rel_path)

    return Run(db_record=run, study=study, db=db)


class Run(BaseEntity):
    """
    The Run class
    """
    db_fields = [
        'id',
        'path',
        'name',
        'status',
        'case_id',
        'reason',
#        'submitted',
#        'started',
#        'finished',
        'code_db',
#        'code_src_ref',
#        'case_src_ref',
#        'batch_system',
#        'batch_queue',
        'last_modified',
#        'run_command',
        'submit_host',
#        'job_number',
        'created',
        'last_modified',
        'staging_mode',
        'staging_dir',
        'transfer_mode',
        'in_staging',
        'is_dev',
        'is_archived',
        'tags'
    ]
    CREATED = 10        # a run has been created
    ASSIGNED = 20       # a run has been created but not submitted
    SUBMITTED = 30      # a run has been submitted
    QUEUING = 40        # a run is queuing
    HOLDING = 45        # a run is holding
    RUNNING = 50        # a run is running
    COMPLETED = 55      # a run has completed
    ABORTED = 60        # the run has been aborted
    SUSPENDED = 65      # the run has been suspended
    CLOSED = 70         # a run has been closed 
    ATTENTION = 75      # the run requires attention
    INVALID = 80        # a run is invalid
    OBSOLETE = 82        # a run is obsolete
    FAILED = 85         # a run failed
    ERROR = 90          # a run has errored
    RESOLVED = 100      # the results of the run are resolved
    TRANSFERRING = 110  # the run is transferring
    DELETED = 120

    _level_to_name = {
        CREATED: 'created',
        ASSIGNED: 'assigned',
        SUBMITTED: 'submitted',
        QUEUING: 'queuing',
        HOLDING: 'holding',
        RUNNING: 'running',
        COMPLETED: 'completed',
        ABORTED: 'aborted',
        SUSPENDED: 'suspended',
        CLOSED: 'closed',
        ATTENTION: 'attention',
        INVALID: 'invalid',
        OBSOLETE: 'obsolete',
        FAILED: 'FAILED',
        ERROR: 'ERROR',
        RESOLVED: 'resolved',
        TRANSFERRING: 'transferring',
        DELETED: 'deleted',
    }
    _name_to_level = {
        'created': CREATED,
        'assigned': ASSIGNED,
        'submitted': SUBMITTED,
        'queuing': QUEUING,
        'holding': HOLDING,
        'running': RUNNING,
        'completed': COMPLETED,
        'aborted': ABORTED,
        'suspended': SUSPENDED,
        'closed': CLOSED,
        'attention': ATTENTION,
        'invalid': INVALID,
        'obsolete': OBSOLETE,
        'failed': FAILED,
        'error': ERROR,
        'resolved': RESOLVED,
        'transferring': TRANSFERRING,
        'deleted': DELETED,
    }
    _level_to_color = {
        CREATED: colored.fg(51),
        ASSIGNED: colored.fg(45),
        SUBMITTED: colored.fg(38),
        QUEUING: colored.fg('gold_3b'),
        HOLDING: colored.fg(100),
        RUNNING: colored.fg(46) + colored.attr('bold'),
        COMPLETED: colored.fg(34),
        ABORTED: colored.fg('white') + colored.bg('red'),
        SUSPENDED: colored.fg(166),
        CLOSED: colored.fg(240),
        OBSOLETE: colored.fg(105),
        ATTENTION: colored.fg(200),
        INVALID: colored.fg(160),
        FAILED: colored.fg('red') + colored.attr('bold'),
        ERROR: colored.fg('red') + colored.attr('bold'),
        RESOLVED: colored.fg('dark_green'),
        TRANSFERRING: colored.fg(183),
        DELETED: colored.fg(235),
    }
    # set valid names
    valid_status_names = []
    for name in _name_to_level:
        valid_status_names.append(name)

    # valid staging modes
    valid_staging_modes = ["none", "share"]

    # name of the status file
    STATUS_FNAME = ".cfmt.status" 
    # fields to ignore for db update
    db_update_ignore = ['code']

    def __init__(self, db_record=None, db=None, study=None, case=None, code=None, 
                 batch=None, dev=False):
        """ 
        Constructor

        We either create a new run based on provided information or generate the run
        from a provided database record
        """
        
        # store the loggers
        self.logger = logging.getLogger('file')
        self.console = logging.getLogger('console')

        #
        self.created = None
        self.dir_name = None
        self.name = None
        self.path = None
        self.abspath = None
        self.working_dir = None

        self.staging = None
        self.in_staging = False
        self.staging_mode = config.configtool.get('staging', 'mode')
        self.transfer_mode = config.configtool.get('staging', 'transfer_mode')
        self.staging_root = config.configtool.get('staging', 'location')
        self.staging_dir = None
        self.staging_dir_created = False
        self.binary_path = None

        self.is_archived = None
        self.archives = []

        self.local_size = 0
        self.remote_size = 0
        self.archive_size = 0

        self.run_command = None
        self.status = None
        self.is_dev = False
        self.job = None
        self.jobs = []
        self.tag_list = []

        # post processing --
        # -> file send to postprocessing programs
        self.output_post_file = None
        # do we have a db_record
        if db_record is not None:
            # set from database record
            self.db_record = db_record
            for db_field in self.db_fields:
                record = getattr(db_record, db_field)
                setattr(self, db_field, record)

            # set status name
            self.status_name = self._status_name(self.status)

            if study is not None:
                self.study = study
            else:
                raise exceptions.NoStudy
            
            if db is not None:
                # store
                self.db = db 
            elif self.study is not None:
                # attempt to get it from the study
                self.db = self.study.db
            else:
                raise exceptions.NoDB

            if case is not None:
                self.case = case
            else:
                # fetch from db
                self.case = db.get_case(case_id = self.case_id)

            # set the tag list
            # -> the format of the taglist is | delimeted, with a | at the start
            #    and end 
            # -> we also make sure that  "|| translates into an empty list
            _tag_list = self.tags.strip("|")
            self.tag_list = _tag_list.split("|") if len(_tag_list) > 0 else []

            # set the dirname from the path
            self.dir_name = os.path.basename(self.path)
            
            # set the name from the path
            self.name = self.case.name + "#" + self.dir_name

            # set the abspath for convinience
            self.abspath = os.path.join(self.study.path, self.path)

            # grab outselves a batch system
            # -> this is being delegated to the job object
            #self.batch = Batch(self.batch_system)

            # set the code
            if self.case is not None:
                # we should already have a code attached, check its correct
                if self.code_db.identifier == self.case.code.identifier:
                    self.code = self.case.code
            else:
                # we fetch a new code
                from cfmt.codes import codes

                code = codes.get(self.code_db.identifier)
                self.code = code(study=self.study, db_record=self.code_db)

            #if self.db is not None:
                # get the case name
        else:
            # we're creating a run from the provided information
            #if run_dir is not None:
            #    self.path = run_dir
            #else:
            #    msg = 'Run directory not provided to run instance'
            #    self.logger.error(msg)
            #    raise NotADirectoryError(msg)


            # --> set the study
            if study is not None:
                self.study = study
            else:
                msg = 'Study not provided to run instance'
                self.logger.error(msg)
                raise exceptions.NotAStudy(msg)

            # --> set the case
            if case is not None:
                self.case = case
            else:
                msg = 'Case not provided to run instance'
                self.logger.error(msg)
                raise exceptions.NotACase(msg)

            # --> set the batch system
            if batch is not None:
                self.batch = batch
                self.batch_system = self.batch.system_name
            else:
                msg = "Batch system not provided"
                self.logger.error(msg)
                self.console.error(msg)
                raise ValueError(msg)

            # --> store a reference to the db controller
            if db is None:
                # attempt to get from study
                try:
                    self.db = self.study.db
                except AttributeError:
                    self.logger.debug("Study has no database object")
                    try:
                        self.db = self.case.db
                    except AttributeError:
                        self.logger.debug("Case has no database object")
                        self.logger.error("No database provided")
                        raise exceptions.NoDB("No database provided")
            else:
                # we're provided it directly
                self.db = db

            # --> set the code
            if code is None:
                # get from case
                try:
                    self.code = self.case.code
                except AttributeError:
                    self.logger.debug("Case has no code attached")
                    self.logger.error("No code provided")
                    raise exceptions.NoCode("No code provided")
            else:
                # we're provided it directly
                self.code = code

            # --> dev mode
            self.is_dev = dev
    
    def __eq__(self, other):
        """ Determine whether two runs are equal """
        return self.db_record.id == other.db_record.id

    def __hash__(self):
        """ The objects hash """
        return hash(('name', self.name, 'id', self.db_record.id))
    
    def _status_name(self, status, color=False):
        """ Returns the status name """
        return self._level_to_name[status]

    def color_status(self, status):
        """ Colors the status """
        from colored import stylize
        
        name = stylize(self._level_to_name[status], self._level_to_color[status])
        return name

    def add_tag(self, tag):
        """ Adds a tag to the tag_list """
        assert isinstance(tag, str)

        if tag not in self.tag_list:
            self.tag_list.append(tag)
            self._update_tag_str()

    def remove_tag(self, tag):
        """ Removes a tag from the tag_list """
        assert isinstance(tag, str)

        if tag in self.tag_list:
            self.tag_list.remove(tag)
            self._update_tag_str()

    def _update_tag_str(self):
        """ updates the tag string """
        self.tags = self._get_tag_str()
    
    def _get_tag_str(self, tag_list=None):
        """ Returns the tag list as a db suitable string """
        if tag_list is not None:
            tags = tag_list
        else:
            tags = self.tag_list
        return "|{}|".format("|".join(tags))

    def get_tag_str(self, tag_list=None):
        """ Returns the tag list as a printable string """
        if tag_list is not None:
            tags = tag_list
        else:
            tags = self.tag_list
        return ",".join(tags)


    def create(self, runs_dir):
        """ Create the folders and structure necessary for a run """
        # store the created time
        self.created = datetime.now()
        
        # create a run directory name based on the current time

        # set the run name
        if self.is_dev:
            self.dir_name = "dev"
            self.name = self.case.name + "#" + "dev"
        else:
            self.dir_name = self.created.strftime(config.RUN['dir_fmt'])
            self.name = self.case.name + "#" + self.dir_name

        # store path relative to study
        self.abspath = os.path.join(runs_dir, self.dir_name)
        self.path = os.path.relpath(self.abspath, self.study.path)
        
        run_dir = self.abspath

        if not os.path.isdir(run_dir):
            self.logger.debug("Created run directory '%s'", run_dir)
            os.makedirs(run_dir)
        else:
            msg = "Run directory '{}' already exists".format(run_dir)
            if not self.is_dev:
                self.logger.error(msg)
                self.console.error(msg)
                raise FileExistsError(msg)

        if self.staging:
            self.create_staging_dir()
        else:
            self.working_dir = self.abspath

        self.is_archived = False

        # define the database recor
        self.db_record = self.db.new_run(
            path=self.path,
            name=self.name,
            case=self.case.db_record,
            code_db=self.code.db_record,
            created=self.created,
            last_modified=self.created,
            batch_system=self.batch_system,
            staging_mode=self.staging_mode,
            staging_dir=self.staging_dir,
            transfer_mode=self.transfer_mode,
            in_staging=self.in_staging,
            is_dev=self.is_dev,
            is_archived=self.is_archived
        )

    def create_staging_dir(self, recreate=False, path=None, exist_ok=False):
        """
        Creates the staging directory if required
        """
        if self.staging_dir_created:
            self.logger.warning("Staging dir creation requested by already marked as created.")
            return 
            #raise FileExistsError("Staging directory '{}' already exists".format(self.staging_dir))
        
        # allow override and recrate
        if path is not None:
            staging_path = path
        elif self.staging_dir is not None and recreate:
            staging_path = self.staging_dir
        else:
            staging_path = self.get_staging_path()
        
        try:
            # check the staging root exists
            if not os.path.isdir(self.staging_root):
                raise NotADirectoryError("Staging root directory '{}' does not exist".format(self.staging_dir))
            
            # create the directory
            os.makedirs(staging_path, exist_ok=exist_ok)
            self.logger.debug("Creating staging directory '{}'".format(staging_path))
            self.staging_dir_created = True
            self.staging_dir = staging_path
            self.working_dir = self.staging_dir
        except (NotADirectoryError, FileExistsError) as err:
            utils.print_exception_info(err)
            sys.exit(1)
        except:
            raise

        return staging_path

    def get_staging_path(self):
        """ Returns the staging path """
        study_dir = os.path.basename(self.study.path)
        return os.path.join(self.staging_root, study_dir, self.case.path, self.dir_name)

    
    def set_db(self, **kwargs):
        """ 
        Sets values to add to the database 
        **kwargs can be any of the db_fields
        """
        for db_field in self.db_fields:
            if db_field in kwargs:
                # we update the db_record
                setattr(self.db_record, db_field, kwargs[db_field])

    def set_bin(self, binary):
        """
        Sets the absolute path to the code binary to run
        """
        pass

    def set_status(self, status, bypass_status_change=False):
        """ Checks for a valid status and submits binary """
        assert isinstance(status, int)

        old_status = self.db_record.status 

        self.set_db(status=status)
        self.status = status

        if old_status != status and not bypass_status_change:
            self.on_status_change(status, old_status)
        

    def save(self, cascade=False):
        """
        Saves the db_record to the databsse
        """
        if self.db_record is not None:
            self.set_db(last_modified=datetime.now())
            self.db_record.save()

        if cascade:
            # save the case if we have it
            if hasattr(self, 'case'):
                if hasattr(self.case, 'save'):
                    self.case.save()

    def clean(self):
        """ Clean up """
        # remove the folder
        print("Cleaning run ... ")
        print("Cleaning run ... OK")
        
    def run(self):
        """ Creates a run """
        self.logger.info("Running with command '{}'".format(self.run_command))

    def transfer_up(self):
        """ Executes a staging transfer """

        # check that staging is actually required
        if not self.staging:
            return

        # check we've sucessfully created the staging directory
        if not self.staging_dir_created:
            raise exceptions.TransferFailed("Staging directory not found")

        # execute the transfer
        # --> get src/dst dirs
        src_dir = self.abspath
        dst_dir = self.staging_dir
        transfer_success = False

        # if we have unsteady files, only transfer what we actually need
        rfilter = self.code.get_tout_filter(self, up=True)
        
        #print(" -> Staging files ...")

        if self.transfer_mode == "rsync":
            # we're doing some rsync, currently with Popen rather than a module
            # --> check we have rsync available
            options = []
            # we're doing some rsync
            
            if rfilter:
                for fil in rfilter:
                    line = " ".join(fil)
                    options.append('--filter='+line)
            
            transfer_success = self.rsync_transfer(src_dir, dst_dir, options)

        elif self.transfer_mode == "os":
            # we're doing the transfer via whatever is native to the os. Here we use
            # copy_tree from distutils.dir_utils
            from distutils.dir_util import copy_tree
            
            copy_tree(src_dir, dst_dir)
        elif self.transfer_mode == "scp":
            # we're doing some scp, currently with Popen rather than a module
            # --> check we have scp available
            raise NotImplementedError
            scp = shutil.which('scp')
            if scp is None:
                msg = "scp not found or installed"
                self.logger.error(msg)
                self.console.error(msg)
                raise FileNotFoundError(msg)
        else:
            raise exceptions.TransferFailed("'{}' is not a valid transfer mode".format(self.transfer_mode))

        if transfer_success:
            # post transfer tidying up
            # --> change the location of the binary
            #self.set_staging_bin()

            # tell the db we're now in staging
            self.set_db(in_staging=True)
            self.in_staging = True
        else:
            raise exceptions.TransferFailed("Transfer failed")
    
    def set_staging_bin(self):
        """
        Changes the binary path if the run is staged
        """
        assert self.binary_path is not None, "Binary path has gone missing"
        
        if self.code.requires_bin_copy:
            basename = os.path.basename(self.binary_path)
            new_path = os.path.join(self.staging_dir, basename)
            if os.path.isfile(new_path):
                self.binary_path = new_path
            else:
                msg = "Binary went missing during staging"
                self.logger.error(msg)
                raise FileNotFoundError(msg)

    def transfer_down(self, preserve_source=False, keep_staged=False, exclude=None, filter=None,
                      force=False, return_cmd=False, override_src=None, silent=False):
        """ 
        Executes a staging transfer down
        """
        exclude = exclude if exclude is not None else []
        assert isinstance(exclude, list)
        rfilter = filter if filter is not None else []
        assert isinstance(rfilter, list)

        # check we actually have something to stage
        if not keep_staged and not self.in_staging:
            raise exceptions.TransferFailed("Run '{}' not in staging".format(self.name))
        
        # check the staging directory exists and we can find it
        if not os.path.isdir(self.staging_dir):
            raise exceptions.TransferFailed("Staging directory '{}' not found".format(self.staging_dir))
        
        # build the command
        if override_src is None:
            src_dir = [self.staging_dir]
        elif not isinstance(override_src, list):
            raise exceptions.TransferFailed("Override src should be a list")
        else:
            src_dir = override_src

        
        dst_dir = self.abspath

        if self.transfer_mode == "rsync":
            options = []
            # we're doing some rsync
            if not preserve_source:
                options.append('--remove-source-files')
            
            if exclude:
                for exc in exclude:
                    options.append('--exclude')
                    options.append(exc)
            
            if rfilter:
                for fil in rfilter:
                    line = " ".join(fil)
                    if return_cmd:
                        options.append('--filter="' + line + '"')
                    else:
                        options.append("--filter="+line)


            host = config.configtool.get('staging', 'host', fallback=None)
            # if we have a host, try ssh
            if host:
                if override_src is not None:
                    raise exceptions.TransferFailed("Host not supported with override src")
                src_dir = host + ":" + src_dir

                
            desc = " ---> Transferring files:"
            transfer_success = self.rsync_transfer(src_dir, dst_dir, options=options, bar_desc=desc, return_cmd=return_cmd,
                                                   silent=silent)
        elif self.transfer_mode == "os":
            # use copy_treee
            raise NotImplementedError
        else:
            raise exceptions.TransferFailed("'{}' is not a valid transfer mode".format(self.transfer_mode))

        if return_cmd:
            # the tool above should have returned the command
            assert isinstance(transfer_success, list), "Transfer tool has not returned a command list"
            return transfer_success 
        else:
            if transfer_success:
                # tell the db we're now not in staging
                # currently different from transfer up since the we call update after this
                if not keep_staged:
                    self.set_db(in_staging=False)
                    self.in_staging = False
            else:
                raise exceptions.TransferFailed("Transfer failed")


    def rsync_transfer(self, src, dst, options=None, bar_desc=None, 
                       src_is_file=False, pbar_leave=True, return_cmd=False,
                       silent=False):
        """ 
        Execute an rsync transfer between src and dst 
        options is an optional list of commands to pass to rsync

        If return_cmd is true then the function returns the correct rsync command. 
        """
        import re
        
        command = []
        if bar_desc is None:
            bar_desc = " -> Transfering files"
        
        rsync = shutil.which('rsync')
        if rsync is None:
            msg = "rsync not found or installed"
            self.logger.error(msg)
            self.console.error(msg)
            return False

        # --> add rsync and options to cmd
        command.append(rsync)
        command.append('-a')
        if return_cmd:
            command.append('-v')
        
        if options is not None:
            assert isinstance(options, list)
            command.extend(options)
        else:
            options = []
        
        command.append('--stats')
        command.append('--dry-run')

        # add trailing slash to src if its not a file
        if not isinstance(src, list):
            src = [src]

        for s in src:
            if not src_is_file:
                _s = os.path.join(s, '')
            command.append(_s)
        
        command.append(dst)
        if return_cmd:
            # we're likely running in the background
            command.remove("--dry-run")
            return command

        # we're going to run it ourselves 
        import subprocess
        from tqdm import tqdm
        
        # get total files from rsync
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        if proc.returncode != 0:
            self.logger.error("Rsync command '%s'", " ".join(command))
            self.logger.error(stderr.decode())
            raise exceptions.TransferFailed(str(stderr.decode()))
        total_to_transfer = re.findall(r'Total transferred file size: (.+) ', stdout.decode())[0]
        total_to_transfer = int(total_to_transfer.replace(',',''))
        if total_to_transfer == 0:
            # we don't have anything to transfer so we leave
            self.logger.info("Nothing to transfer")
            
            # only leave if we don't need to remove source files
            if "--remove-source-files" not in options:
                return True
        
        # --> remove dry-run and stats
        command.remove("--dry-run")

        # --> replace --stats with --info=progress2
        index = command.index('--stats')
        command[index] = "--info=name,progress2"
        
        self.logger.debug("Running rsync, command '%s'", " ".join(command))

        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # launch Asynchronous Readers
        stdout_queue = utils.get_queue()
        stdout_reader = utils.AsynchronousFileReader(proc.stdout, stdout_queue)
        stderr_queue = utils.get_queue()
        stderr_reader = utils.AsynchronousFileReader(proc.stderr, stderr_queue)
        # example output 
        # ['4,012,118', '100%', '4.49MB/s', '0:00:00', '(xfr#6,', 'to-chk=0/7)']

        if silent:
            pbar = None
        else:
            pbar = tqdm(total=total_to_transfer, unit='B', ascii=True, unit_divisor=1024, unit_scale=True, leave=pbar_leave)
            previous = 0
            
            #with tqdm(total=total_to_transfer, unit='B', ascii=True, unit_divisor=1024, unit_scale=True, leave=pbar_leave) as pbar:
            #    previous = 0
                #_total = 0
        while not stdout_reader.eof() or not stderr_reader.eof():
            # stderr
            while not stdout_queue.empty():
                line = stdout_queue.get()
                
                if pbar:
                    # we decode the stdout to update the bar
                    try:
                        output = line.decode().split()
                        if len(output) == 1:
                            # if the output is 1 long, it's the filename
                            filename = bar_desc + " ... " + output[0]

                            pbar.set_description(desc=filename)
                            continue
                        else:
                            # we should have stats
                            transferred = output[0]
                            try:
                                transferred = int(transferred.replace(',',''))
                            except ValueError:
                                # we've got something that isn't an int
                                continue
                            # ignore if the amount hasn't changed
                            if transferred == previous:
                                continue
                            # tqdm takes the delta so we need to compute it
                            #_total = _total + delta
                            delta = transferred - previous
                            previous = transferred
                            pbar.update(delta)
                    except IndexError:
                        pass
                else:
                    self.logger.info(line.rstrip())
            while not stderr_queue.empty():
                line = stderr_queue.get()
                self.logger.error(line.rstrip())


        # join the threads
        stderr_reader.join()
        stdout_reader.join()
        # wait for the process to finish
        proc.wait()
        if proc.returncode != 0:
            if pbar:
                pbar.set_description(bar_desc + " " + utils.FAILED)
            # the subprocess has ended
            raise exceptions.TransferFailed("An unknown error occured during rsync transfer. See log")

        if pbar:
            pbar.set_description(bar_desc + " " + utils.OK)
            pbar.close()
        
        return True


    def copy_job_from(self, job):
        """ Set a new job based on the one passed """
        from cfmt.batch_systems.controller import Batch
        from cfmt.entities.jobs import Job
        
        assert isinstance(job, Job)

        # we need to generate a new batch object
        batch = Batch(job.batch_system)

        # tell it what we can
        batch.mode

        

        # copy_from is the old job
        #self.job = jobs.Job(
        #    db=self.db,
        #    run=self,
        #)
        print(copy_from.batch)

    def parse_db_run_command(self):
        """ Parses the provided string command and sets the command list """
        assert isinstance(self.job.sys_command, str)

        self.command_list = self.job.sys_command.split()

    def submit(self):
        """ 
        Submits the run
        Since we need to capture output from each batch system, we delegate this 
        """
        # need to delegate the reading of the output to the batch system
        assert self.job is not None, "Job not generated"

        # start the subprocess
        # --> submit handles the job_id etc
        self.job.submit()

        # save the job
        self.job.save()

        # save the run
            
    def on_status_change(self, new_status, old_status):
        """ Called when the job status changes """

        if old_status == self.TRANSFERRING:
            # we need to read the previous status from the run dir
            try:
                _old_status = self.read_status_file()
            except Exception as err:
                utils.print_exception_info(str(err))
            else:
                new_status = _old_status
                # update it, avoiding recursion
                self.set_status(new_status, bypass_status_change=True)

        if old_status == self.DELETED:
            return 
        
        if new_status == self.TRANSFERRING:
            # ensure the old status is written, so we can copy it back
            self.update_status_file(old_status)
        elif new_status != self.DELETED:
            self.update_status_file(new_status)

    def update_status_file(self, status):
        """
        Writes the run status to a file called .cfmt.status
        """
        status_string = str(self._status_name(status)).lower()
        run_dir = self.get_run_dir(local_only=True)
        fname = self.STATUS_FNAME
        fpath = os.path.join(run_dir, fname)

        self.logger.debug("Updating status file %s with status %s", fpath, status_string) 
        with open(fpath, 'w') as f:
            f.write(status_string + "\n")
    
    def read_status_file(self):
        """
        Reads the status file from the run_dir
        """
        run_dir = self.get_run_dir(local_only=True)
        fname = self.STATUS_FNAME
        fpath = os.path.join(run_dir, fname)

        with open(fpath, 'r') as f:
            line = f.readline()

        # check that the line is correct
        status_string = line.rstrip()
        if status_string in self.valid_status_names:
            return self._name_to_level[status_string]
        else:
            # return None
            raise ValueError("Invalid status read")
    
    def update(self, **kwargs):
        """
        Updates the status of the run
        """
        # if we have kwargs, we set these and then save
        for arg, value in kwargs.items():
            # handle straight database fields
            if arg in self.db_fields:
                if arg == 'status':
                    assert value in self.valid_status_names
                    value = self._name_to_level[value]
                
                if arg == "is_archived":
                    if value != 0:
                        # we need to check the given int is valid
                        self.load_archives()
                        if value not in [a.id for a in self.archives]:
                            raise exceptions.UpdateFailed("Archive '{}' either does not exist or is not associated with this run".format(str(value)))
                
                if arg == "tags":
                    # we need to parse the tags
                    value = self._get_tag_str(value)
                
                # try to set the value
                try:
                    setattr(self, arg, value)
                except AttributeError:
                    raise exceptions.UpdateFailed("Failed to set attribute '%s' on run '%s", arg, self.name)
            
            # handle tags
            if arg == "add_tags":
                # add the tags to the list
                for tag in value:
                    self.add_tag(tag)
                # get the correct string
                self.tags = self._get_tag_str()
                
            if arg == "remove_tags":
                # remove the tags from the list
                for tag in value:
                    if tag not in self.tag_list:
                        raise exceptions.UpdateFailed("Run '{}' does is not tagged with '{}'".format(self.name, tag))
                    self.remove_tag(tag)
                # get the correct string
                self.tags = self._get_tag_str()
            
        for field in self.db_fields:
            try:
                new = getattr(self, field)
                old = getattr(self.db_record, field)
                if new != old:
                    self.logger.debug("Updating run '%s', %s: '%s' -> '%s'", self.name, field, old, new)
                    # handle status change 
                    if field == "status":
                        self.set_status(new)
                    else:
                        setattr(self.db_record, field, new)
            except AttributeError:
                raise

        # save the record
        self.save()
    
    
    @staticmethod 
    def get_status_headers(job_id=True, dates=False, src_ref=False, columns=None, 
                           sizes=False, tags=True, stats=False):
        """
        Returns the headers for the output table
        """
        if columns is None:
            columns = [
                'status_symbol',
                'name',
                'status',
            ]
        
        if job_id:
            columns.append('job_number')
        if dates:
            columns.append('submitted')
            columns.append('finished')
            columns.append('wallclock')
        if src_ref:
            columns.append('src_ref')
        if sizes:
            columns.append('size')
        if tags:
            columns.append('tags')

        if stats:
            job_id = False
            columns.append('total_wallclock')
            columns.append('total_cpu')
        

        columns.append('reason')

        header_mapping = {
            'status_symbol': '',
            'name': utils.cstr('Run Name', fg=248),
            'status': utils.cstr('   Status', fg=248),
            'job_number': utils.cstr('Job #', fg=248),
            'submitted': utils.cstr('Submit/Start', fg=248),
            'finished': utils.cstr('Finished', fg=248),
            'size': utils.cstr('Size (S)(A)', fg=248),
            'src_ref': utils.cstr('SHA1', fg=248),
            'reason': utils.cstr('Reason', fg=248),
            'tags': utils.cstr('Tags', fg=248),
            'wallclock': utils.cstr('Wallclock', fg=248),
            'total_wallclock': utils.cstr('Total Wallclock', fg=248),
            'total_cpu': utils.cstr('Total CPU', fg=248)
        }
        
        status_headers = []       # for symbol
        columns = utils.make_list_unique(columns)
        
        for key in columns:
            if key in header_mapping:
                header = header_mapping[key]
            else:
                header = 'N/A'
            
            status_headers.append(header)
        
        return status_headers
    
    def get_status_row(self, job_id=True, dates=False, src_ref=False, 
                       columns=None, status_symbol=None, sizes=False,
                       tags=True, stats=False):
        """
        Returns a list of items to print in a table
        """
        from colored import stylize
        from cfmt.entities.jobs import Job
        
        row = []
        
        # job number
        foreign = None
        if job_id:
            if not self.jobs:
                # get the last job
                self.set_last_job()
            # determine if foreign
            # load submit hosts supported by the profile (if any)
            hosts_supported = config.get_supported_submithost()
            if hosts_supported is not None:
                hosts_supported = hosts_supported if isinstance(hosts_supported,list) else [hosts_supported]
            else:
                import socket
                current_host = socket.getfqdn()
                hosts_supported = [current_host]

            if len(self.jobs) > 0:
                job_submit_host = self.jobs[-1].submit_host
                if job_submit_host not in hosts_supported:
                    foreign = True
                else:
                    foreign = False

        # arrow '&#x25CF';
        #row.append(utils.cstr("   ->", fg='blue'))
        if status_symbol is None:
            symbol = u"\u25CF"
        else:
            symbol = status_symbol

        if foreign is not None:
            if foreign: symbol += "f"
        
        row.append(stylize(symbol.ljust(3), self._level_to_color[self.status]))
        # name
        row.append(self.name)
        
        # staging/archive
        if self.is_archived:
            status_string = utils.cstr("A".center(3), attr=['bold'], fg=200)
        elif self.status == self.TRANSFERRING:
            status_string = utils.cstr("T".center(3), attr=['bold'], fg=183)
        else:
            if self.staging_mode != 'none':
                if self.in_staging:
                    status_string = u"\u25CF".center(3)
                else:
                    status_string = u"\u25D2".center(3)
            else:
                status_string = u"\u25CB".center(3)

        status_string = utils.cstr(status_string, fg=248) + self.color_status(self.status)


        row.append(status_string)




        if columns is None:
            columns = set()
            columns.add('reason')
        
        if stats:
            job_id = False
            stats_jobs = self.get_jobs(status=Job.COMPLETED)
            columns.add('total_wallclock')
            columns.add('total_cpu')

        if job_id:
            if not self.jobs:
                self.set_last_job()
            columns.add('job_number')
                #

        if dates:
            columns.add('submitted')
            columns.add('finished')
            columns.add('wallclock')
            # fetch the last job if one hasn't been set
            if not self.jobs:
                self.set_jobs()
        if sizes:
            columns.add('size')
        if src_ref:
            columns.add('src_ref')
        if tags:
            columns.add('tags')
        

        if 'job_number' in columns:
            if self.jobs and isinstance(self.jobs[0],Job):
                job = self.jobs[0]
                if job.job_id is None:
                    row.append("-")
                else:
                    row.append(job.job_id)
            else:
                row.append("N/A")
        
        if 'submitted' in columns: 
            # submitted of last (or selected job)
            submitted = utils.cstr("N/A", fg=238)
            if self.jobs and isinstance(self.jobs[0],Job):
                job = self.jobs[0]
                if isinstance(job.started, datetime):
                    submitted = utils.print_datetime(job.started)
                elif isinstance(job.submitted, datetime):
                    submitted = utils.print_datetime(job.submitted)
        
            row.append(submitted)

        if 'started' in columns: 
            # started
            started = utils.cstr("N/A", fg=238)
            if self.jobs and isinstance(self.jobs[0], Job):
                job = self.jobs[0]
                if isinstance(job.started, datetime):
                    started = utils.print_datetime(job.started)
        
            row.append(started)

        if 'finished' in columns: 
            # finished
            finished = utils.cstr("N/A", fg=238)
            if self.jobs and isinstance(self.jobs[0], Job):
                job = self.jobs[0]
                if isinstance(job.finished, datetime):
                    finished = utils.print_datetime(job.finished)
        
            row.append(finished)

        if 'wallclock' in columns:
            # wall clock time for last job
            wallclock = utils.cstr("N/A", fg=238)
            if self.jobs and isinstance(self.jobs[0], Job):
                job = self.jobs[0]
                if isinstance(job.started, datetime) and isinstance(job.finished, datetime):
                    # compute the difference
                    wallclock = utils.diff_datetime_str(job.finished - job.started)
            
            row.append(wallclock)
        
        if 'src_ref' in columns:
            raise ValueError("src_ref not supported for runs. Try -j")
            if self.code_src_ref:
                src_refs = utils.cstr(self.code_src_ref)
            else:
                src_refs = utils.cstr("N/A", fg=238)
            row.append(src_refs)

        if 'size' in columns:
            if self.status == self.DELETED:
                line = "N/A"
            else:
                try:
                    self.local_size = utils.get_dir_size(self.abspath)
                except OSError as err:
                    self.logger.error(str(err))
                    line = "N/A"
                else:    
                    line = utils.bytes2human(self.local_size)

                if self.staging_mode != 'none':
                    try:
                        size = utils.get_dir_size(self.staging_dir)
                        line += " " + "(" + utils.bytes2human(size) + ")"
                    except OSError as err:
                        utils.print_exception_info(err)
                        size = 0
                        line += " " + "(N/A)"
                    self.remote_size = size
                else:
                    line += " " + "()"
                    size = 0
            
            # load the archives
            self.load_archives()
            if len(self.archives) > 0:
                asize = sum(a.size for a in self.archives)
                line += " " + "(" + utils.bytes2human(asize) + ")"
            else:
                asize = 0
                line += " " + "()"
            self.archive_size = asize
           
            row.append(line)
        
        if 'tags' in columns:
            import textwrap
            if self.tag_list:
                line = ", ".join(self.tag_list)
                line = textwrap.fill(line, width=18)
                line = line.replace(" ","")
            else:
                line = utils.cstr("N/A", fg=238)
            row.append(line)

        if 'total_wallclock' in columns:
            # get all jobs
            if stats_jobs:
                wallclock = Job.get_total_wallclock(stats_jobs)
                row.append(utils.diff_datetime_str(wallclock))
            else:
                row.append('-')


        if 'total_cpu' in columns:
            # get all jobs
            if stats_jobs:
                cputime = Job.get_total_cputime(stats_jobs)
                row.append(str(cputime))
            else:
                row.append('-')


        if 'reason' in columns:
            # reason
            import textwrap
            if self.reason:
                line = textwrap.fill(str(self.reason), width=40)
                row.append(line)
            else:
                row.append("-")

        # apply to the entire row
        if self.status == self.CLOSED:
            # remove existing formatting
            for item in row:
                base_string = utils.str_remove_ansi(str(item))
                row[row.index(item)] = utils.cstr(base_string, fg=240)
        elif self.status == self.DELETED:
            # remove existing formatting
            for item in row:
                base_string = utils.str_remove_ansi(str(item))
                row[row.index(item)] = utils.cstr(base_string, fg=235)
        elif self.status == self.OBSOLETE:
            for item in row:
                base_string = str(item)
                row[row.index(item)] = utils.cstr(base_string, fg=237)

        # indent first row
        row[0] = utils.cstr("   ", attr=['hidden']) + row[0]

        return row

    def get_job_table(self, dates=False, src_ref=False):
        """ Returns a table of jobs """
        from cfmt.entities.jobs import Job
        
        data = []
        
        # get the headers
        headers = Job.get_status_headers(dates=dates, src_ref=src_ref)
        data.append(headers)

        # get the data
        if self.jobs:
            for job in self.jobs:
                row = job.get_status_row(dates=dates, src_ref=src_ref)
                data.append(row)
        return data
    
    def get_archive_table(self):
        """ Returns a table of jobs """
        from cfmt.archives.base import Archive
        data = []
        
        # get the headers
        headers = Archive.get_status_headers()
        data.append(headers)

        # get the data
        for archive in self.archives:
            x = None
            if archive.id == self.is_archived:
                x = lambda r: utils.cstr(r, attr=['bold'], fg=200)
            
            row = archive.get_status_row(format_row_with=x)
            
            data.append(row)
        return data
    
    def load_archives(self, **kwargs):
        """ Loads the selected archives """
        assert hasattr(self, "db_record")

        # remove existing archives
        self.archives = self.get_archives(**kwargs)


    def get_archives(self, **kwargs):
        """ Returns the selected archives """
        from cfmt.entities.archives import load_archive
        archives = []

        # only interested in archives associated with this run
        query = self.db_record.has_archives.select()

        if "run" in kwargs:
            if kwargs["run"] != self.db_record:
                msg = "get_archives can only be used to retrive archives for the current run"
                raise ValueError(msg)
        
        query = self.db.get_archives(query=query, **kwargs)
            
        for record in query:
            archives.append(load_archive(self, record))
        
        return archives 
    
    def has_archives(self):
        """ Returns whether or not he run has any archives """
        if len(self.db_record.has_archives.select()) > 0:
            return True
        
        return False
        
    def get_archive(self, id):
        """ Get an archive with the selected id """
        assert isinstance(id, int)

        archives = self.get_archives(ids=[id])

        # warn if we find more than one
        if len(archives) > 1:
            self.console.warning("More than 1 archive found with the same id")
        elif len(archives) == 0:
            return None
        else:
            return archives.pop()
        
        # return next((a for a in self.archives if a.id == id), None)


    def withdraw_archive(self, args):
        """ Withdraws an archive from the repository """
        
        assert self.is_archived != 0

        # get the marked archive
        archive = self.get_archive(self.is_archived)

        if archive is None:
            raise exceptions.ArchiveFailed("Withdraw archive not found")

        # set some options
        archive.set_option(
            force=args.force,
            delete=args.delete,
        )
        
        # do the withdraw
        archive.withdraw()
        
        # set the database
        self.set_db(is_archived = 0)
        self.save()


    
    def create_archive(self, remote_path, host, args, pbar=None):
        """
        Archives a study to the remote path. 

        This:
        - Locally tars the run directory
        - Executes an rsync transfer
        - Stores a file in the run directory as a reminder
        """
        from cfmt.entities.archives import new_archive
        
        assert isinstance(remote_path, str)

        if args.deposit:
            assert self.is_archived == 0

        # get a new archive
        archive = new_archive(self, archive_type=args.type)

        # if --put then we're archiving the run
        # set some options
        archive.set_option(
            compress=args.compress,
            keep_source=not args.deposit,
        )
        # add the description if we have one
        if args.description is not None:
            assert isinstance(args.description, list)
            archive.description = str(args.description[0])

        # create the archive
        try:
            archive.create(remote_path,host)
        except Exception as err:
            raise exceptions.ArchiveFailed(err)
        
        if not archive.archive_created:
            raise exceptions.ArchiveFailed("Archive not marked as created")
        
        if args.deposit:
            self.set_db(is_archived = archive.id)
            self.save()
            #self.set_db(is_archived)
        
    def fetch_tout(self, args):
        """
        Fetches tout
        """
        import glob
        # get all tout by default
        all = True
        
        # if we've specified an argument for args.get_tout 
        # then we fetch that
        if args.mode == 'sync':
            if args.get_tout and None not in args.get_tout:
                print(args.get_tout)
                all = False
        
        base_src = self.staging_dir
        src_list = []

        src_size = 0
        if all:
            # get the tout mask and glob
            mask_list = config.configtool.get(self.code.name, "tout_mask").split(" ")
            for m in mask_list:
                files = glob.glob(os.path.join(base_src, m))
                for fname in files:
                    src_size += utils.get_dir_size(fname)
                    src_list.append(os.path.basename(fname))
        else:
            for m in args.get_tout:
                fname = os.path.join(base_src, m)
                if os.path.isdir(fname):
                    src_size += utils.get_dir_size(fname)
                    src_list.append(os.path.basename(fname))
                else:
                    raise exceptions.TransferFailed("TOUT directory does not exist: {}".format(fname))
                    
        if not src_list:
            # we have no timeoutput
            return

        # if we have a host, prefer ssh over nfs
        host = config.configtool.get('staging', "host", fallback=None)
        
        # directory to change to 
        cd = self.abspath
        # directory to run command in
        cwd = base_src

        base_desc = " --> Transferring tout "
        final_desc = " -> Run '{}' tout".format(self.name)

        utils.tarpipe(src_list, host=host, cwd=cwd, cd=cd, compression=False, src_size=src_size,
                      base_desc=base_desc, final_desc=final_desc)


    def move(self, dest_case):
        """
        Moves the run to a new case, moving the staging directory as well.
        """
        self.logger.info("Moving run '%s' from case '%s' to case '%s'", self.name, self.case.name, dest_case.name)
        case = self.case
        study = case.study
        
        # cannot move a case to itself
        if case == dest_case:
            msg = f"Run '{self.name}' is already associated with case {case.name}"
            raise exceptions.CaseExists(msg)

        # cannot move to a case which is not using the same code
        if case.code != dest_case.code:
            msg = f"Destination case '{dest_case.name}' for run '{self.name}' uses different code ({dest_case.code.name})"
            raise exceptions.InvalidCode(msg)

        old_case_path = case.path
        old_case_name = case.name 
        new_case_path = dest_case.path
        new_case_name = dest_case.name 
        
        # new run name and directory
        c, d = self.name.split('#')
        new_name = new_case_name + "#" + d

        paths = self.path.split(os.path.sep)
        paths[0] = new_case_path
        new_path = os.path.join(*paths)

        # check there's no name clash (possible, but unlikely)
        new_path_abs = os.path.join(study.path,new_path)
        if os.path.exists(new_path_abs):
            msg = f"Destination run path '{new_path_abs}' already exists"
            raise exceptions.RunExists(msg)
        fruns = study.get_runs(names=new_name)
        if len(fruns) > 0:
            msg = f"Destination run '{new_name}' already exists in database"
            raise exceptions.RunExists(msg)

        # good to go        
        
        # -> modify the database record. Items:
        # - path
        # - name 
        db_record = self.db_record
        db_record.path = new_path
        db_record.name = new_name
        db_record.case_id = dest_case.db_record
        self.logger.debug("Changing run '%s' name to '%s'",self.name, new_name)
        self.logger.debug("Changing run '%s' path to '%s'",self.name, new_path)
        self.logger.debug("Changing run '%s' case id to '%d'", self.name,dest_case.id)

        mv_staging = False
        # -> modify the database
        try:
            if self.in_staging:
                self.logger.debug("Run is '%s' is currently in staging, working out new staging directory",self.name)
                mv_staging = True
                
                old_staging_path = db_record.staging_dir
                #old_staging_dirs.append(old_staging_path)
                split_path = utils.splitpath(old_staging_path)
                try:
                    # we reverse the list, so that index returns the right most instance
                    ix = len(split_path) - 1 - split_path[::-1].index(old_case_path)
                except ValueError as err:
                    msg = "Failed to update staging directory for run '{}'. Old case name not found in directory.".format(self.name)
                    self.logger.error(msg)
                    raise Exception(msg) from err

                old_case_staging_dir = "/" + os.path.join(*split_path[:ix+1])
                split_path[ix] = new_case_path
                new_staging_path = "/" + os.path.join(*split_path)
                #new_staging_dirs.append(new_staging_path)
                if os.path.isdir(new_staging_path):
                    msg = "Failed to copy run '{}', staging directory already exists".format(self.name)
                    self.logger.error(msg)
                    raise FileExistsError(msg)
                else:
                    new_case_staging_dir = "/" + os.path.join(*split_path[:ix+1])
                    db_record.staging_dir = new_staging_path
                    self.staging_dir = db_record.staging_dir
                    self.logger.debug("Changing run '%s' staging path to '%s'",self.name, new_staging_path)

            # -> loop through jobs
            jobs = self.get_jobs()
            for job in jobs:
                record = job.db_record

                # change name
                self.logger.debug("Changing job '%d' name to '%s'",record.id, new_name)
                record.name = new_name

                # change the working directory
                old_wd = record.working_dir
                split_path = utils.splitpath(old_wd)
                try:
                    # find the right most index
                    ix = len(split_path) - 1 - split_path[::-1].index(old_case_name)
                except ValueError as err:
                    print(old_case_name)
                    print(split_path)
                    msg = "Failed to change wd for job '{}".format(job.id)
                    raise Exception(msg) from err 
                else:
                    split_path[ix] = new_case_path
                    new_wd = "/" + os.path.join(*split_path)
                    self.logger.debug("Changing job '%d' working directory to '%s'",record.id, new_wd)
                    job.working_dir = new_wd
                    record.working_dir = new_wd
            
            if mv_staging:
                src = old_staging_path
                dst = new_staging_path
                # check the old run staging directory exists 
                if not os.path.exists(src):
                    msg = f"Cannot find run staging directory '{src}'"
                    raise FileNotFoundError(msg)
                
                # check the new case staging directory exists
                self.logger.debug("Creating new case staging directory '%s' if requireds", new_case_staging_dir)
                os.makedirs(new_case_staging_dir, exist_ok=True)
                
                # move the data
                self.logger.debug("Moving '%s' to '%s", src, dst)
                os.rename(src, dst)
            
            # hooks for any code actions
            self.code.move_run_hook(self)

            # move the case level run directory
            src = self.abspath
            dst = new_path_abs
            self.logger.debug("Moving run directory from '%s' to '%s'", src, dst)
            os.rename(src, dst)

        except Exception as err:
            # clear any tdmq
            print("")
            if isinstance(err, KeyboardInterrupt):
                print("Caught SIGINT ...")
            
            utils.print_exception_info(str(err), no_log=True)
            
            expected_exc = (
                KeyboardInterrupt,
                exceptions.DatabaseException,
                FileExistsError,
                FileNotFoundError,
                IOError,
            )
            if not isinstance(err, expected_exc):
                # reraise
                raise Exception from err
            
            exit(1)
        else:
            self.logger.info("Saving new database record for run '%s'", self.name)
            self.save()
            for job in jobs:
                job.save()

            self.logger.info("Move for run '%s' completed", self.name)
        return

    def move_staging(self, dest, args, batch=None, preserve_source=False, rfilter=None, silent=False):
        """
        Moves the staging directory to dest, using the provided 
        batch system if requested
        """

        submit_batch = True if batch is not None else False

        src = self.staging_dir

        # make sure the new staging directory exists
        if not os.path.exists(dest):
            os.makedirs(dest, exist_ok=True)
        
        # if the run is not staged, then we can simply update the staging dir
        if not self.in_staging:
            self.update(staging_dir=dest)
            self.logger.info(f"Updating staging directory for run '{self.name}' to '{dest}'")
            return
        
        options = []
        if not preserve_source:
            options.append('--remove-source-files')
            
            if rfilter:
                for fil in rfilter:
                    line = " ".join(fil)
                    options.append("--filter="+line)
            
        if submit_batch:
            exclude = []
            name = "rsync-staging"
            
            # exclude the stdout/stderr themselves
            exclude.append(batch.get_stdout_script_name())
            exclude.append(batch.get_stderr_script_name())

            for e in exclude:
                options.append('--exclude')
                options.append(e)

            silent = True

        cmd = self.rsync_transfer(
            src,
            dest,
            options=options,
            return_cmd=submit_batch,
            silent=silent,
        )

        
        if submit_batch:
            # ask the batch system to generate a job

            # fail if we're already marked as transferring
            if self.status == self.TRANSFERRING:
                raise exceptions.TransferFailed("Run already marked as transferring") 
            
            
            # set the wd as the dest directory
            self.working_dir = dest
            
            type_ = "move_staging"
            
            job = batch.create_job(self, type_=type_, task_cmd=cmd, name=name)
            batch.submit(job)

            job.save()
            self.save()
        else:
            # cmd will be a bool
            if cmd:
                # update the staging directory of the run
                self.update(staging_dir=dest)
            else:
                raise exceptions.TransferFailed("Rsync failed, see log")

    
    def fetch(self, args, batch=None, preserve_source=False, keep_staged=False, 
              with_tout=False, exclude=None):
        """
        Fetches a study from staging

        If return_cmd is true then this returns a list of commands which can be run
        by a batch system
        """
        if exclude is None:
            exclude = []
        # we add tout to the exclude list by default
        if not with_tout:
            # build the filter list from the code
            rfilter = self.code.get_tout_filter(self)
        else:
            rfilter = []
        
        submit_batch = True if batch is not None else False

        # if batch, we exclude the stdout and stderr files
        if submit_batch:
            # set the name to the transfer mode
            name = self.transfer_mode
            
            # exclude the stdout/stderr themselves
            exclude.append(batch.get_stdout_script_name())
            exclude.append(batch.get_stderr_script_name())

        cmd = self.transfer_down(
            preserve_source=preserve_source, 
            keep_staged=keep_staged,
            exclude=exclude,
            filter=rfilter,
            return_cmd=submit_batch
        )
        # save any changes
        self.save()

        if submit_batch:
            # ask the batch system to generate a job

            # fail if we're already marked as transferring
            if self.status == self.TRANSFERRING:
                raise exceptions.TransferFailed("Run already marked as transferring") 
            # set the working directory 
            self.working_dir = self.get_run_dir()
            
            if args.func == "sync":
                type_ = "sync"
            elif args.func == "fetch":
                type_ = "fetch"
            else:
                raise ValueError("Invalid argument function")
            
            job = batch.create_job(self, type_=type_, task_cmd=cmd, name=name)
            
            batch.submit(job)
            
            job.save()
            self.save()

            if job.status == job.ERROR:
                raise exceptions.TransferFailed("Failed to submit batch job")


    def print_tout_info(self):
        """
        Prints out information about time-output
        """
        from cfmt import tables
        
        template = """
            === CFMT Run ===
            --------------------
            STATUS    |  {status_name}
            --------------------
            Run directory       : '{run_dir}'
            Reason for run      : {reason}
            --------------------
        """
        # format the base template
        data = {
            'status_name': self.color_status(self.status),
            'study_path': self.study.path,
            'run_dir': self.abspath,
            'reason': self.reason
        }
        if not self.code.supports("tout"):
            # the code used to compute the run does not support this function
            return "Code does not support tout"
        
        # ask the code for the tout info
        try:
            tout_info = self.code.get_tout_info(self)
            data.update(tout_info)
        except NotImplementedError as err:
            utils.print_exception_info(err)
            return ""
        
        # print run info
        print(utils.remove_left_margin(template.format(**data)))

        # print tavg summary info
        print("  tout sets : " + utils.cstr("{num_sets}".format(**tout_info),attr=1))
        table_data = []
        if tout_info['num_sets'] == 0:
            return
        
        i=1
        table_headers = [
            utils.cstr('No', fg=248),
            utils.cstr('Name', fg=248),
            utils.cstr('NT', fg=248),
            utils.cstr('Start Time', fg=248),
            utils.cstr('End Time', fg=248),
            utils.cstr('Duration', fg=248),
            utils.cstr('Mean Freq.', fg=248),
            utils.cstr('Total size.', fg=248)
        ]

        for tout_name, tout_set in tout_info['sets'].items():
            nt = tout_set['nt']
            start = tout_set['start_time']
            end = tout_set['end_time']
            duration = float(end-start)
            frequency = duration/float(nt)
            size = utils.bytes2human(tout_set['size'])
            row = []
            row.append(i)
            row.append(tout_name)
            row.append(nt)
            row.append(start)
            row.append(end)
            row.append(duration)
            row.append(frequency)
            row.append(size)
            table_data.append(row)
            i += 1
        
        tables.print_table(table_data, table_headers, tablefmt="plain", floatfmt=".4e")


    def print_tavg_info(self):
        """
        Prints out information about time-averaged results
        """
        from cfmt import tables
        template = """
            === CFMT Run ===
            --------------------
            STATUS    |  {status_name}
            --------------------
            Run directory       : '{run_dir}'
            Reason for run      : {reason}
            --------------------
        """

        tavg_headers = set([
            'Set #',
            'folder',
            'start NT',
            'end NT',
            'NT',
            'start time',
            'end time',
            'averaged time',
        ])

        # format the base template
        data = {
            'status_name': self.color_status(self.status),
            'study_path': self.study.path,
            'run_dir': self.abspath,
            'reason': self.reason
        }
        if not self.code.supports("tavg"):
            # the code used to compute the run does not support this function
            return "Code does not support tavg"

        # ask the code for the tavg info
        try:
            tavg_info = self.code.get_tavg_info(self)
            data.update(tavg_info)
        except NotImplementedError as err:
            utils.print_exception_info(err)
            return ""

        # print run info
        print(utils.remove_left_margin(template.format(**data)))

        # print tavg summary info
        print("  tavg sets : " + utils.cstr("{num_sets}".format(**tavg_info),attr=1))
        table_data = []
        if tavg_info['num_sets'] == 0:
            return
        
        i=1
        table_headers = [
            utils.cstr('No', fg=248),
            utils.cstr('Name', fg=248),
            utils.cstr('NT', fg=248),
            utils.cstr('Start Time', fg=248),
            utils.cstr('End Time', fg=248),
            utils.cstr('Duration', fg=248)
        ]

        for tavg_name, tavg_set in tavg_info['sets'].items():
            row = []
            row.append(i)
            row.append(tavg_name)
            row.append(tavg_set['nt'])
            row.append(tavg_set['start_time'])
            row.append(tavg_set['end_time'])
            row.append(float(tavg_set['end_time']-tavg_set['start_time']))
            table_data.append(row)
            i += 1
        
        tables.print_table(table_data, table_headers, tablefmt="plain", floatfmt=".4e")
        
        print("")
        table_headers = [
            utils.cstr('Dir', fg=248),
            utils.cstr('NT', fg=248),
            utils.cstr('End Time', fg=248),
            utils.cstr('Duration', fg=248)
            ]
        for tavg_name, tavg_set in tavg_info['sets'].items():
            table_data = [] 
            start_time = tavg_set['start_time']
            for snapshot, info in tavg_set['snapshots'].items():
                row = []
                row.append(snapshot)
                row.append(info['nt'])
                row.append(info['end_time'])
                row.append(float(info['end_time'] - start_time))
                table_data.append(row)

            # sort the table data
            table_data = sorted(table_data, key=lambda x: int(x[0]))
            print(utils.cstr("Set: ", fg=248) + utils.cstr("{}".format(tavg_name), attr=1))
            tables.print_table(table_data, table_headers, tablefmt="plain", floatfmt=".4e")

        
        
        # print details information about each tavg set

        return utils.remove_left_margin(template.format(**data))

    def info(self):
        """
        Prints out information about the run
        """
        from cfmt.entities.jobs import Job

        template = """
            === CFMT Run ===
            --------------------
            STATUS    |  {status_name}{is_archived}
            --------------------
            Part of Case        : {case_name}
            Part of Study       : {study_name} 
            Case path           : '{study_path}'
            Run directory       : '{run_dir}'
            Created             : {created}
            Last modified       : {last_modified}
            Reason for run      : {reason}
            Tags                : {tags}

            Completed Jobs      : {jobs_complete}
            Total Wallclock     : {jobs_wallclock}
        """
        _jobs = self.get_jobs(status=Job.COMPLETED)
        wallclock = Job.get_total_wallclock(_jobs)

        # format the base template
        data = {
            'case_name': self.case.name,
            'study_name': self.study.name,
            'status_name': self.color_status(self.status),
            'study_path': self.study.path,
            'created': utils.print_datetime(self.created),
            'last_modified': utils.print_datetime(self.last_modified),
            'reason': self.reason,
            'run_dir': self.abspath,
            'is_archived': ", archived" if self.is_archived else "",
            'tags': ",".join(self.tag_list) if self.tag_list else "N/A",
            'jobs_complete': str(len(_jobs)),
            'jobs_wallclock': utils.diff_datetime_str(wallclock),
        }

        if self.staging_mode == "none":
            template += """
                Staging mode        : {staging_mode}
            """
            data.update({
                'staging_mode': self.staging_mode
            })
        else:
            template += """
                Staging mode        : {staging_mode}
                Staging directory   : {staging_dir}
                Transfer mode       : {transfer_mode}
                In staging          : {in_staging}
            """
            data.update({
                'staging_mode': self.staging_mode,
                'staging_dir': self.staging_dir,
                'transfer_mode': self.transfer_mode,
                'in_staging': self.in_staging
            })
        
        if self.has_archives():
            self.load_archives()
            # loop over the archives
            for archive in self.archives:
                archive_template = """
                    Archive {id}
                    -------------------------------------
                    Host                : {archive_host}
                    Name                : {archive_name}
                    Path                : '{archive_path}'
                    Description         : {archive_description}
                    Size                : {archive_size}
                    Created             : {archive_created}
                    Last modified       : {archive_modified}
                    md5sum              : {archive_md5sum}
                """
                template += archive_template.format(
                    id=archive.id,
                    archive_name=archive.name,
                    archive_path=archive.path,
                    archive_host=archive.hostname,
                    archive_size=utils.bytes2human(archive.size),
                    archive_created=utils.print_datetime(archive.created),
                    archive_modified=utils.print_datetime(archive.last_modified),
                    archive_md5sum=str(archive.hash) if archive.hash is not None else "n/a",
                    archive_description=str(archive.description) if archive.description is not None else "n/a"            
                )

        # TODO Notes
        #if self.status == self.COMPLETED:
        #    template += """
        #        Run statistics
        #        CPU Time            : {cpu_time}
        #        Wall Clock Time     : {wall_clock}
        #        Memory              : {memory}
        #    """
        #    data.update({
        #        'cpu_time': 100,
        #        'wall_clock': 100,
        #        'memory': 100
        #    })
        #self.has_notes = True
        #if self.has_notes:
        #    # we print the last note
        #    template += """
        #        Last log entry
        #        User: {log_user}
        #        Date: {log_date}
        #        
        #            {last_log_text}
        #    """
        #    data.update({
        #        'log_user': "mbgm7dw2",
        #        'log_date': utils.print_datetime(datetime.now()),
        #        'last_log_text': "this is a very long note"
        #    })

        return utils.remove_left_margin(template.format(**data))

    def delete(self, keep_files=False, prompt=True, keep_database=True, staging_only=False):
        """
        Deletes the run
        If keep_files is True, we only remove the entry from the database
        If keep_database is True, we only mark the run as deleted.
        """
        dirs = []
        
        if staging_only:
            # run must have a staging directory

            if self.staging_dir is None:
                print(f"Not processing run '{self.name}' as it does not have a staging directory")
                self.logger.info("Not processing run '%s' as it does not have a staging directory", self.name)
                return

            self.console.warning("Removing contents of staging directory '%s'", self.staging_dir)
            dirs.append(self.staging_dir)

            if prompt:
                _prompt = utils.confirm(f" -> Delete contents of staging directory '{self.staging_dir}'")
            else:
                _prompt = True
            
            if _prompt:
                from tqdm import tqdm
                desc = f" ---> Deleting contents of staging directory '{self.staging_dir}'"
                with tqdm(unit=' files', desc=desc, leave=False) as pbar:

                    def callback(item):
                        pbar.update()
                        desc = f" ---> Removing {os.path.basename(item)}"
                        pbar.set_description(desc=desc)

                    utils.delete_dir_contents(self.staging_dir, remove_symlinks=True, callback=callback)
            return

        if not keep_files:
            self.console.warning("Removing directory '%s'", self.abspath)
            dirs.append(self.abspath)

            if self.in_staging:
                self.console.warning("Removing staging directory '%s'", self.staging_dir)
                dirs.append(self.staging_dir)
        
        if prompt:
            _prompt = utils.confirm(" -> Delete run '{}'".format(self.name))
        else:
            print(" -> Deleting run '{}'".format(self.name))
            _prompt = True

        if _prompt:
            # --> database
            if keep_database:
                self.set_status(self.DELETED)
                self.save()
                self.logger.info("Run '%s' marked as deleted.", self.name)
            else:
                self.db.delete(db_record=self.db_record, cascade=True)
            
            self.logger.info("Deleted run '%s' ...", self.name)
            # --> files
            if not keep_files:
                for directory in dirs:
                    shutil.rmtree(directory, ignore_errors=True)
                    self.logger.info("Removed directory '%s'",directory)

    def delete_staging(self, prompt=True):
        """
        Deletes the staging directory of the run only
        """
        dirs = []
        if self.in_staging:
            self.console.warning("Removing staging directory '%s'", self.staging_dir)
            dirs.append(self.staging_dir)
        else:
            return

        if prompt:
            _prompt = utils.confirm(" -> Delete run '{}' staging directory".format(self.name))
        else:
            print(" -> Delete run '{}' staging directory".format(self.name))
            _prompt = True
        
        if _prompt:
            for directory in dirs:
                shutil.rmtree(directory, ignore_errors=True)
                self.logger.info("Removed directory '%s'",directory)
    
    def select_monitors(self, local_only=False):
        """
        Prompt the user to select a monitor 
        TODO: Merge this with select plot
        """ 
        run_dir = self.get_run_dir(local_only=local_only)
        
        # get the list of monitors
        try:
            monitors = self.code.get_monitor_files(run_dir)
        except FileNotFoundError:
            print("No files found")
            sys.exit(0)

        # sort the list
        monitors.sort(key=str.lower)
        
        # add an option to do nothing
        monitors.append(os.path.join(run_dir, "Exit"))
        
        # map the found names
        fmap = lambda d: os.path.relpath(d, start=run_dir)
        
        selection = self._select_files(monitors, fmap)
        
        if selection == len(monitors)-1:
            return False
        else:
            return monitors[selection]

    def select_plots(self, local_only=False):
        """
        Prompt the user to select a monitor 
        TODO: Merge this with select monitors
        """ 
        run_dir = self.get_run_dir(local_only=local_only)
        # get the list of monitors
        try:
            plots = self.code.get_plot_files(run_dir)
        except FileNotFoundError:
            print("No files found")
            sys.exit(0)

        # sort the list
        plots.sort(key=str.lower)
            
        # add an option to exit
        plots.append(os.path.join(run_dir, "Exit"))
        
        # map the found names
        fmap = lambda d: os.path.relpath(d, start=run_dir)

        selection = self._select_files(plots, fmap)
        if selection == len(plots)-1:
            return False
        else:
            return plots[selection]


    def _select_files(self, flist, fmap=None, prompt=None):
        """
        Prompt the user to select a file from the flist
        """
        assert isinstance(flist, list)
        fno = len(flist)
        files = []
        print("")
        if fno > 0:
            print(" -> Found {} files to edit".format(fno))

            if prompt is None:
                prompt="Please select a file: "
            
            selection = utils.list_select(flist, title=prompt, options_map_func=fmap)
            
            return selection
    
    
    def display_plot(self, fpath, file_sequence=False):
        """ Plots a file """
        from cfmt import plotting

        # get a reader
        if file_sequence:
            # we're passed a dir, load the sequence
            reader = self.code.file_sequence_reader(fpath)
        else:
            fpath = fpath.pop()
            reader = self.code.plot_reader(fpath)

        if file_sequence: 
            app = plotting.FileSequencePlot(reader)
            app.run()
        else:
            app = plotting.LivePlot( 
                reader=reader, 
                live=False, 
                terminate=reader.terminate,
                title="Plotting {} for run '{}'".format(os.path.basename(fpath),self.name),
                standalone=True,
                xlabel="",
                ylabel="",
                logmode=(False, False)
            )
            if app:
                # wrap with a wakup handler
                plotting.SignalWakeupHandler(app.app)

                app.create(None)
                app.run()

    
    
    def display_monitors(self, live=False, timeres=False, residuals=False, 
                               mass_flow=False, monitors=None, compare_with=None,
                               maxlen=None, local_only=False):
        """ Displays residuals for the run """
        from cfmt import plotting
        # how we display them is code dependent to some extent
        if residuals:
            # get the plot widget
#            residual_reader = self.code.residual_reader(self.get_run_dir())
            idx = 0
            waiting = False
            try:
                while True:
                    try:
                        fpath = self.get_run_dir(local_only=local_only)
                        residual_reader = self.code.residual_reader(fpath, maxlen=maxlen, live=live)
                    except exceptions.EmptyFile:
                        if idx == 0:
                            msg = "File is empty: '{}'".format(fpath)
                            print(msg)

                        if live:
                            waiting = True
                            waiting_msg = "Waiting for data ..."
                            utils.waiting(idx, waiting_msg)
                            idx += 1 
                            time.sleep(0.8)
                            continue
                        else:
                            # stop the data reader
                            sys.exit(1)
                    else:
                        if waiting:
                            print(waiting_msg + " OK")
                        break
            except KeyboardInterrupt:
                sys.exit(0)
            app = plotting.LivePlot( 
                reader=residual_reader, 
                live=live, 
                terminate=residual_reader.terminate,
                title=self.name + " Residuals",
                standalone=True,
                xlabel="",
                ylabel="Residual",
                logmode=(False, True)
            )
        elif monitors is not None:
            assert isinstance(monitors, list)

            monitor = monitors.pop()

            # check the file exists
            fpath = os.path.join(self.get_run_dir(local_only=local_only), monitor)
            if not os.path.isfile(fpath):
                self.logger.error("Monitor '%s' does not exist as location '%s'",monitor, fpath)
                raise FileNotFoundError("Monitor '{}' not found".format(monitor))
            
            idx = 0
            waiting = False
            try:
                while True:
                    try:
                        reader = self.code.monitor_reader(fpath, maxlen=maxlen, live=live)
                    except exceptions.EmptyFile:
                        if idx == 0:
                            msg = "File is empty: '{}'".format(fpath)
                            print(msg)

                        if live:
                            waiting = True
                            waiting_msg = "Waiting for data ..."
                            utils.waiting(idx, waiting_msg)
                            idx += 1 
                            time.sleep(0.8)
                            continue
                        else:
                            # stop the data reader
                            sys.exit(1)
                    else:
                        if waiting:
                            print(waiting_msg + " OK")
                        break
            except KeyboardInterrupt:
                sys.exit(0)
            
            app = plotting.LivePlot( 
                reader=reader, 
                live=live, 
                terminate=reader.terminate,
                title=self.name + " Monitor: " + os.path.basename(fpath),
                standalone=True,
                xlabel="",
                ylabel="",
                logmode=(False, False)
            )
        elif mass_flow:
            # generate readers for all files
            freaders = []
            try:
                fpath = self.code.get_massflow_file(self.get_run_dir(local_only=local_only))
                freaders.append(self.code.monitor_reader(fpath, maxlen=None, live=live))
                
                if compare_with is not None:
                    for run in compare_with:
                        fpath = run.code.get_massflow_file(run.get_run_dir(local_only=local_only))
                        freaders.append(run.code.monitor_reader(fpath))
                    pass
                    # we're going to attempt to plot two files
            except FileNotFoundError as err:
                utils.print_exception_info(str(err))
                sys.exit(1)

            assert isinstance(freaders, list)
            
            app = plotting.LivePlot( 
                reader=freaders[0], 
                live=live, 
                terminate=freaders[0].terminate,
                title=self.name + " Monitor: " + os.path.basename(fpath),
                standalone=True,
                xlabel="",
                ylabel="MassFlow",
                logmode=(False, False)
            )
        elif timeres:
            idx = 0
            waiting = False
            try:
                while True:
                    try:
                        fpath = self.code.get_timeres_file(self.get_run_dir(local_only=local_only))
                        reader = self.code.timeres_reader(fpath, maxlen=maxlen, live=live)
                    except exceptions.EmptyFile:
                        if idx == 0:
                            msg = "File is empty: '{}'".format(fpath)
                            print(msg)

                        if live:
                            waiting = True
                            waiting_msg = "Waiting for data ..."
                            utils.waiting(idx, waiting_msg)
                            idx += 1 
                            time.sleep(0.8)
                            continue
                        else:
                            # stop the data reader
                            sys.exit(1)
                    else:
                        if waiting:
                            print(waiting_msg + " OK")
                        break
            except KeyboardInterrupt:
                sys.exit(0)

            app = plotting.LivePlot( 
                reader=reader, 
                live=live, 
                terminate=reader.terminate,
                title=self.name + " Time Residuals",
                standalone=True,
                xlabel="",
                ylabel="Residual",
                logmode=(False, False)
            )
        
        if app:
            # wrap with a wakup handler
            plotting.SignalWakeupHandler(app.app)
            app.create(None)
            app.run()
            
        

        # start the signal handler
        #signal_handler = utils.SignalHandler(residual_reader.terminate, residual_reader.thread)
        #signal_handler.set_handler()

    def get_job(self):
        """ Loads a job """
        if self.job:
            job = self.job
        elif len(self.jobs) > 0:
            job = self.jobs[0]

        return job
    
    def display_stdout(self, live=False, local_only=False):
        """ Displays the stdout """
        # do we have a job pre-loaded
        job = self.get_job()

        # get the correct file
        # if the job is currently running, then use its working directory
        if job.status == job.RUNNING and not local_only:
            _dir = job.working_dir
        else:
            _dir = self.get_run_dir(local_only=local_only)

        f = os.path.join(_dir, job.get_stdout_fname(wd=_dir))
        #f = self.get_stdout_path(job=job)

        self.logger.info("Displaying output file '%s'",f)
        self.display_output(f, live=live)

    def display_stderr(self, live=False, local_only=False):
        """ Displays the stderr """
        # do we have a job pre-loaded
        job = self.get_job()
        
        # get the correct file
        # if the job is currently running, then use its working directory
        if job.status == job.RUNNING and not local_only:
            _dir = job.working_dir
        else:
            _dir = self.get_run_dir(local_only=local_only)
        #f = self.get_stderr_path(job=job)
        # get the correct file
        f = os.path.join(_dir, job.get_stderr_fname(wd=_dir))

        self.logger.info("Displaying output file '%s'",f)
        self.display_output(f, live=live)
    
    def display_output(self, path, live=False):
        """ Runs a live data reader and follows the file """
        # check the file exists
        if not os.path.isfile(path):
            # check for possible compression
            if path := utils.check_for_compressed(path):
                pass
            else:
                self.logger.error("File '%s' not found for live output", path)
                raise FileNotFoundError("Cannot find '{}' for live output".format(path))

        # get a data reader
        reader = utils.file_reader(path)
        try:
            if live:
                while True:
                    for line in reader.readlines():
                        print(line)

                    time.sleep(0.2)
            else:
                for line in reader.readlines():
                    print(line)

                # data is in the databuffer
                #try:
                #    line = reader.databuffers[0].popleft(
                #    print(line)
                #except IndexError:
                #    time.sleep(0.2)
        except KeyboardInterrupt:
            print("Caught SIGINT ... terminating")
            pass
        finally:
            reader.close()

        #plot.run()
    def live_monitor_app(self):
        """ Runs a live monitor app """
        # get the stdout of the running process
        from cfmt import plotting

        # texts
        texts = []
        stdout_reader = utils.file_reader(self.get_stdout_path())
        stdout_widget = plotting.LiveFile(
            reader=stdout_reader,
            live=True,
            terminate=stdout_reader.terminate,
            title="stdout",
        )
        texts.append(stdout_widget)

        stderr_reader = utils.file_reader(self.get_stderr_path())
        stderr_widget = plotting.LiveFile(
            reader=stderr_reader,
            live=True,
            terminate=stderr_reader.terminate,
            title="stderr",
        )
        texts.append(stderr_widget)


        # get the residuals
        monitors = []
        residual_reader = self.code.residual_reader(self.get_run_dir())
        residual_widget = plotting.LivePlot( 
            reader=residual_reader, 
            live=True, 
            terminate=residual_reader.terminate,
            title=self.code.name + " Residuals",
            xlabel="Residual",
            ylabel="IT",
        )
        
        app = plotting.PlotApp(live=True, residuals=residual_widget, monitors=monitors, texts=texts)


        # start the signal handler
        #signal_handler = utils.SignalHandler(residual_reader.terminate, residual_reader.thread)
        #signal_handler.set_handler()
        app.run()


    def convert_output(self, args, fmt=None):
        """ Converts data output from the code """
        print(" -> Converting {} output to {} ...".format(self.code.name, fmt), end='\r')
        self.logger.info("Converting %s output to %s", self.code.name, fmt)
        try:
            post_args = self.code.convert_output(
                self.get_run_dir(), 
                args, 
                fmt=fmt,
                run=self,
            )
        except (exceptions.PostFailure, NotImplementedError) as err:
            print(" -> Converting {} output to {} ... FAILED".format(self.code.name, fmt), end='\n')
            utils.print_exception_info(str(err))
            sys.exit(1)
        else:
            print(" -> Converting {} output to {} ... OK".format(self.code.name, fmt), end='\n')

        return post_args

    def get_post_args(self, post_name, args=None, fmt=None):
        """
        Returns the command arguments to pass to post_prog
        """
        # does the code provide the post arguments
        post_args = []

        if self.code.provides_post_args(post_name):
            post_args = self.code.get_post_args(post_name, self, args)
        
        return post_args
    
    def prepost(self, post_prog, args):
        """ 
        Prepares the run to be loaded into the pass post_prog 
        """
        # ask the code to handle results 
        self.code.select_results(self)
        
        # ask the code what format is present
        output_format = self.code.get_output_format(self)
        
        if output_format not in post_prog.supported_input_formats:
            utils.print_exception_info("Output format '{}' is not supported by '{}'".format(output_format, post_prog.name))
            sys.exit(1)

        cwd = self.get_run_dir()
        
        post_args = self.get_post_args(post_prog.name, args=args, fmt=output_format)
        post_prog.set_args(post_args, args=args)
        post_prog.set_cwd(cwd)
    
    def get_run_dir(self, local_only=False):
        """ 
        Return the current run directory 
        local_only shortcircuits
        """
        # are we in staging
        if self.in_staging and not local_only:
            directory = self.staging_dir
        else:
            directory = self.abspath

        return directory

    def get_stdout_path(self, job=None, local_only=False):
        """ Return the location of the stdout file if it exists """
        if job is None:
            # attempt to get the last one
            job = self.get_job()
        
        # get the filename from the job 
        stdout = job.get_stdout_fname()

        return os.path.join(self.get_run_dir(local_only=local_only), stdout)
    
    def get_stderr_path(self, job=None, local_only=False):
        """ Return the location of the stdout file if it exists """
        if job is None:
            # attempt to get the last one
            job = self.get_job()

        stderr = job.get_stderr_fname()

        return os.path.join(self.get_run_dir(local_only=local_only), stderr)

    def set_last_job(self):
        """ Fetch and set the most recent job """
        job = self.get_last_job()
        if job:
            self.jobs = []
            self.jobs.append(job.pop())
            self.job = self.jobs[0] 
        
            # load the specific job
            #self.job.load()

    def get_last_job(self):
        """ Fetch and attach the most recent job """
        from cfmt.entities.jobs import Job
        
        return self.get_jobs(records=self.db.get_last_job(run=self.db_record, type_=Job.SOLVER))

    def set_jobs(self, **kwargs):
        """ Assigns the jobs """
        self.jobs = self.get_jobs(**kwargs)
    
    def get_jobs(self, status=None, records=None, ids=None, type_=None):
        """ Queries the database for jobs matching the passed parameters """
        from cfmt.entities.jobs import Job
        
        if type_ is not None:
            # argparse provides these in a list
            if isinstance(type_, list):
                type_ = type_[-1]
            
            if isinstance(type_, str):
                assert type_ in Job._name_to_type, "Invalid Job type" 
                type_ = Job._name_to_type[type_]
            elif isinstance(type_, int):
                assert type_ in Job._type_to_name, "Invalid Job type"

            # convert back to list
            type_ = [type_]
        
        if status is not None:
            # query by status
            if isinstance(status, int):
                status = [status]

            assert isinstance(status, list)
            records = self.db.get_jobs(run=self.db_record, status=status, type_=type_)
        elif records is not None:
            pass
        elif ids is not None:
            if isinstance(ids, int):
                ids = [ids]

            assert isinstance(ids, list)
            records = self.db.get_jobs(run=self.db_record, ids=ids, type_=type_)
        else:
            records = self.db.get_jobs(run=self.db_record, type_=type_)
        _jobs = []
        if records:
            for record in records:
                # make sure the right run is set
                if record.run_id != self.db_record.id:
                    run = Run(study=self.study, db_record=record.run)
                else:
                    run = self
                job = Job(run=run, db=self.db, db_record=record)
                _jobs.append(job)
        
        return _jobs

    def compress(self,
                 stdout=False,
                 stderr=False,
                 monitors=False,
                 residuals=False,
                 logs=False,
                 local_only=False,
                 algorithm=None,
                 decompress=False):
        """
        Compress elements of a run directory
        """
        if not any([stdout, stderr, monitors, residuals, logs]):
            print("No run elements selected for compression")
            sys.exit(0)

        # check if jobs were set
        if not self.jobs:
            # then we work on all jobs
            self.set_jobs()

        # working directory
        run_dir = self.get_run_dir(local_only=local_only)

        # compression algorithm
        if algorithm is None:
            algorithm = config.configtool.get("utils", "compression", fallback="lzma")

        print(f"Processing run {self.name} ... ")

        paths = []

        for j in self.jobs:
            if j.status == j.RUNNING:
                self.logger.debug("Job %d not compressed as it has status running", j.id)
                print(utils.WARNING + f"Job {j.id} skipped as it has status running")
                continue

            if stdout:
                self.logger.info("Compressing stdout for job %d", j.id)

                # get the stdout fname bypassing the code
                fname = os.path.join(run_dir, j.get_stdout_fname(wd=run_dir, bypass_code=True))
                paths.append(fname)

            if stderr:
                self.logger.info("Compressing stderr for run %s", self.name)

                # get the stdout fname bypassing the run
                fname = os.path.join(run_dir, j.get_stderr_fname(wd=run_dir, bypass_code=True))
                paths.append(fname)

            if logs:
                self.logger.info("Compressing logs for run %s", self.name)

                # ask the code for log files that should be compressed
                fnames = self.code.get_log_files(j)
                paths += [os.path.join(run_dir, f) for f in fnames]

        # monitors are per run, not per job
        if monitors:
            self.logger.info("Compressing monitors for run %s", self.name)

            # get the monitor mask
            mfiles = self.code.get_monitor_files(run_dir)
            for fname in mfiles:
                paths.append(fname)

        # residuals are per run, not per job
        if residuals:
            self.logger.info("Compressing residuals for run %s", self.name)

            # get the main residual file
            fname = os.path.join(run_dir, self.code.get_residual_file())
            paths.append(fname)
            
            # try for a timeres (only a few codes)
            try:
                paths = self.code.get_timeres_file(run_dir)
            except (NotImplementedError,FileNotFoundError):
                pass
            else:
                for fname in paths:
                    paths.append(fname)

        from tqdm import tqdm

        if decompress:
            base_desc = " --> Decompressing "
        else:
            base_desc = " --> Compressing "
        bar_fmt = "{l_bar}{bar}{r_bar}"
        with tqdm(total=len(paths), unit='files', ascii=False, bar_format=bar_fmt) as pbar:
            for p in paths:
                fname = os.path.basename(p)
                desc = base_desc + f"{fname:<20}"
                pbar.set_description(desc=desc)
                try:
                    utils.compress(p, algorithm=algorithm, decompress=decompress, pbar=pbar)
                except FileNotFoundError as exc:
                    self.logger.warning("File not found '%s'", p, exc_info=exc)
                    msg = utils.WARNING + f": File not found '{p}' ... skipping"
                    pbar.write(msg)
                    pbar.update(1)
                else:
                    pbar.update(1)
    
    def delete_post(self,
                    ens=False,
                    local_only=False):
        """
        Delete/tidy post
        """
        if not any([ens]):
            print("No post elements selected for deletion")
            sys.exit(0)

        # check if jobs were set
        if not self.jobs:
            # then we work on all jobs
            self.set_jobs()

        # working directory
        run_dir = self.get_run_dir(local_only=local_only)

        if ens:
            # only for STREAM runs
            if not self.code.name == "stream":
                print(utils.ERROR + " Only valid for stream runs, skipping ...")
                return
            bmsg = f" -> Deleting ensight files for run '{self.name}' ... "
            print(bmsg, end="\r")
            # ask the code to do it
            try:
                paths = self.code.delete_ensight(run_dir)
            except OSError as exc:
                self.logger.error("Failed to delete ensight for run %s", self.name, exc_info=True)
                msg = bmsg + utils.FAILED + ":" + str(exc)
                print(msg)
            else:
                msg = bmsg
                msg += f"{len(paths)} files deleted "
                msg += utils.OK
                print(msg)

    def delete_tout(self, local_only=False, dry_run=False, include_scratch=False):
        """
        Deletes tout folders

        include_scratch: Run actions in the scratch directory even if
          the run is not staged
        """
        import glob
        
        # working directory
        dirs = []
        dirs.append(self.get_run_dir(local_only=local_only))

        if include_scratch and not self.in_staging:
            dirs.append(self.get_staging_path())

        print(dirs)


        # check for tout directories

    
    def delete_uncompressed(self, strict=False, local_only=False, dry_run=False):
        """
        Checks for files that have compressed and uncompressed versions.
        """
        import glob
        # working directory
        run_dir = self.get_run_dir(local_only=local_only)

        # return a list of known compression ext to create a glob mask
        for alg, ext in utils.SUPPORTED_COMPRESSION.items():
            mask = "*" + ext
            paths = glob.glob(os.path.join(run_dir, mask))
            for p in paths:
                # remove compression extension from filename
                pu = p.rstrip("." + ext)

                # check if the file exists, continuing if not
                if not os.path.exists(pu):
                    continue

                if not strict:
                    # check the dates
                    ctime_p = os.path.getctime(p)
                    ctime_pu = os.path.getctime(pu)
                    if ctime_p > ctime_pu:
                        print(utils.cstr(" ->",fg=2) + f" Deleting '{pu}'")
                        if not dry_run:
                            self.logger.info("Deleting file %s", pu)
                            os.remove(pu)
                    else:
                        print("Compressed is older, refusing to delete", pu)
                        print(utils.cstr(" ->",fg=1) + f" Refusing to delete '{pu}': compressed file is older")
                else:
                    if utils.compare_compressed(p,pu,alg):
                        print(utils.cstr(" ->",fg=2) + f" Deleting '{pu}'")
                        if not dry_run:
                            self.logger.info("Deleting file %s", pu)
                            os.remove(pu)
                    else:
                        print(utils.cstr(" ->",fg=1) + f" Refusing to delete '{pu}': compressed file do not match")


