"""
    cft: A management tool for Computational Fluid Dynamics
"""
import os,sys
import shutil
from datetime import datetime

import logging
import uuid
import cfmt.config as config
import cfmt.utils as utils
from cfmt.entities.base import BaseEntity
from cfmt.codes import codes
from cfmt import exceptions
from cfmt import logging as customLog

def load_study(path=None, profile=None, db_bypass=False):
    """
    Read a project from the directory passed and return the Project object
    If no argument is provided, then the project is read from the cwd
    """
    from cfmt.db import controller as dbc
    
    console = logging.getLogger('console')
    console.info('Loading study')
    if not path:
        study_path = os.getcwd()
    else:
        study_path = os.path.abspath(path)

    requested_path = study_path

    while not os.path.isdir(os.path.join(study_path, config.CFMT_DIR_NAME)):
        oldpath, study_path = study_path, os.path.dirname(study_path)
        if study_path == oldpath:
            raise exceptions.NotAStudy("No cfmt study exists in directory '{}' or above it".format(requested_path))

    # we have a valid directory, check the database file exists
    # attempt to load the database
    try:
        db = dbc.load_database(study_path)
    except IOError:
        raise
    except:
        raise
        #raise IOError("Cannot load database file")

    # perform a version check
    db_version = db.get_version()
    
    if db_version != config.DBVERSION and not db_bypass:
        # database version mismatch
        msg = "Database version: {}; CFMT requires: {}\nDatabase requires updating. Please run 'cfmt upgrade'".format(db_version, config.DBVERSION)
        raise exceptions.DBVersionError(msg)
    
    # fetch the study from the db
    study_db = db.get_study()

    # load the study and profile config if available
    try:
        config.load(study_path=study_path, profile=profile)
    except FileNotFoundError as err:
        console.error(str(err))
        exit(1)

    # feed to our Study class
    study = Study(db_record=study_db, db=db, path=study_path)
    
    console.info('Study loaded')
    return study

def delete_study(path=None, files=False):
    """ Deletes a study """
    if not path:
        path = os.getcwd()
    else:
        path = os.path.abspath(path)
    
    if os.path.exists(path):
        cft_path = os.path.join(path, config.CFMT_DIR_NAME)
        if os.path.exists(cft_path):
            resp = utils.confirm("Remove cmft folder in '{0}'".format(path), True)
            if resp:
                utils.delete_dir_contents(cft_path)
                os.rmdir(cft_path)
            if files:
                msg = "Remove all files and folders in '{0}'"
                resp = utils.confirm(msg.format(path), True)
                if resp:
                    utils.delete_dir_contents(path)
        else:
            msg = "Directory '{0}' is not managed by cfmt"
            raise IOError(msg.format(path))
    else:
        msg = "Directory '{0}' does not exist"
        raise IOError(msg.format(path))

class Study(BaseEntity):
    """
    The Study class
    """
    info_fields = [
        'name',
        'description',
        'project_name',
        'created',
        'last_modified',
        'creator',
        'path',
        'case_number',
        'codes_associated',
        'uuid'
    ]
    db_fields = [
        'name',
        #'path',
        'created',
        'last_modified',
        'description',
        'project_name',
        'creator',
        'uuid'
    ]
    template = """
        === CFMT Study ===
        Study name          : {name}
        Description         : {description}
        Project name        : {project_name}
        Number of cases     : {case_number}
        Associated codes    : {codes_associated}
        Study path          : '{path}'
        Created             : {created}
        Last modified       : {last_modified}
        Created by          : {creator}
        UUID                : {uuid}
        """
    status_template = """
        === CFMT Status ===
        Study name          : {name}
        Description         : {description}
        Number of cases     : {case_number}
        Last modified       : {last_modified}
        UUID                : {uuid}
        """
    """ A dictionary of codes associated with the study """
    codes = {}

    def __init__(self, db_record=None, db=None, path=None, 
                 name=None, empty=False, project_name=None, 
                 description="", code_names=None):
        """ Constructor 
        db_record The database record as returned by pw
        db The database object
        path The path in which to create the study
        name The name associated with the study
        empty Whether or not to create an empty study
        project_name The name associated with a project
        description A description of the project
        codes A list of codes to assoicate with the project
        
        """
        assert path is not None, "Path required when constructing a study"

        if db_record is not None:
            # we're loading an existing study
            self.db_record = db_record
            for db_field in self.db_fields:
                record = getattr(db_record,db_field)
                setattr(self, db_field, record)

            # --> set the path
            self.path = path

            if db is not None:
                self.db = db
                
                # --> get the number of cases
                self.case_number = self.get_number_of_cases()

                # --> store the code names for output
                self.codes_associated = self.get_code_names()

                # --> create a dictionary of codes 
                for code in self.get_codes():
                    _code = codes.get(code)
                    _code_db = db.get_code(identifier=code)
                    self.codes[code] = _code(study=self, db_record=_code_db)

            else:
                self.case_number = "N/A"
                self.codes_associated = "N/A"

            

        else:
            # we're creating a new object from passed information
            if name is None:
                name = os.path.basename(path)
            self.name = name
            self.path = path
            self.empty = empty
            self.db = db
            self.db_record = db_record
            self.description = "" if description is None else description

            if project_name is None:
                self.project_name = name
            else:
                self.project_name = project_name
        
            # uuid (will be in database)
            self.uuid = uuid.uuid1()
            
            # create a dictionary of available codes
            for code in code_names:
                _code = codes.get(code)
                self.codes[code] = _code(study=self)

        self.dirs = {} if empty else config.STUDY_DIRS

        self.config_dir = os.path.join(self.path, config.CFMT_DIR_NAME)

        # replace the logger file handler
        self.logger = logging.getLogger('file')
        self.log_fname = os.path.join(self.config_dir, config.LOG['filename'])

        # logger
        log_handler = customLog.getLogHandler(self.log_fname) 
        customLog.replaceHandler(self.logger, log_handler)
        self.logger.info("CFMT version: %s", config.PROGVERSION)
        self.logger.info("Loaded study: '%s'", self.name)
        
        # console logger
        self.console = logging.getLogger('console')

        # notes
        self.notes = None


    def init(self, args):
        """ Initializes the requried folder structure for a study"""
        
        self.logger.info("Initializing study '%s' in '%s'...", self.name, self.path)
         
        # define the database record
        self.db_record = self.db.new_study(
            name=self.name,
            path=self.path,
            created=datetime.today(),
            last_modified=datetime.today(),
            description=self.description,
            project_name=self.project_name,
            creator=utils.get_username(),
            uuid=self.uuid
        )

        # create the directory structure
        if not self.empty:
            for folder, options in self.dirs.items():
                new_dir = os.path.join(self.path, folder)
                if not os.path.exists(new_dir):
                    os.mkdir(new_dir)

                # initialize vcs repositories
                if 'vcs' in options:
                    from cfmt.vcs.controller import VCSController
                    
                    vcs = VCSController.get_vcs(options['vcs'])
                    vcs.init(new_dir)
        
        
        # handle code specific initialization
        for code in self.codes:
            self.codes[code].init(db=self.db, args=args)

        # note if there are no codes
        if not self.codes:
            self.logger.warning("No codes to attach")
        
        # create the database
        self.db.create()

        self.logger.info("Study initialized")



    def save(self):
        """ Writes the study state to the database """
        # populate the database
        if self.db_record is not None:
            self.logger.debug("Saving study database record")
            self.db_record.save()
        else:
            raise IOError("Study not loaded")
        
        for code in self.codes:
            if self.codes[code].db_record is not None:
                self.logger.debug("Saving database for code '%s'", code)
                self.codes[code].db_record.save()
            else:
                raise AttributeError("Code does not have database record")
            

    def get_rel_path(self, path):
        """ 
        Given an absolute path, return the relative path to the studies 
        absolute path
        """
        return os.path.relpath(path, self.path)

    def get_number_of_cases(self):
        """ Returns the number of cases associated with the study """
        if self.db_record is None:
            # should raise an error here
            return 0

        return self.db.get_number_of_cases(self.db_record)

    def get_all_cases(self):
        """ Returns a list of case db_records """
        assert self.db is not None
        from cfmt.entities.cases import Case
        
        case_records = self.db.get_all_cases(self.db_record)

        _cases = []
        for record in case_records:
            case = Case(study=self, db_record=record)
            _cases.append(case)

        return _cases

    def add_code(self, code_name, args=None):
        """ Given a code, add it to the database """
        if code_name in self.codes:
            raise ValueError("Code already attached to study")
            
        # create a dictionary of available codes
        _code = codes.get(code_name)
        self.codes[code_name] = _code(study=self)

        # handle code specific initialization
        self.codes[code_name].init(db=self.db, args=args)

        # append the config
        config.append_config(code_name, directory=self.config_dir)


    def get_code_names(self):
        """ Returns a csv list of codes associated with the study"""
        if self.db_record is None:
            raise exceptions.NoDB("No Database record")
        
        return ", ".join(self.db.get_code_names(self.db_record))
    
    def get_codes(self):
        """ Returns an array codes associated with the study"""
        if self.db_record is None:
            raise exceptions.NoDB("No Database record")
        
        return self.db.get_code_names(self.db_record)

    def get_runs(self, names=None, status=None, from_case=None, recent=None, has_archives=None):
        """ Returns an array of runs, with their respective cases attached """
        
        from cfmt.entities.runs import Run
        from cfmt.entities.cases import Case
        
        records = None
        runs = []
        cases_to_add = {}
        
        # sanitize 
        if names is not None:
            if isinstance(names, str):
                names = [names]
        if status is not None:
            if isinstance(status, str):
                status = [status]

        # hand off query
        runs = self.db.get_runs(names=names, status=status, from_case=from_case, recent=recent,
                                has_archives=has_archives)

        # get the cases associated with the runs
        order = []
        for run in runs:
            order.append(run.name)
            cases_to_add.setdefault(run.case, []).append(run)

        _runs = []
        for record, run_records in cases_to_add.items():
            # if we've been passed a case, check if it's from the same run
            if from_case is not None and record.id == from_case.db_record.id:
                case = from_case
            else:
                case = Case(study=self, db_record=record)

            # get the runs
            for run in run_records:
                _run = Run(study=self, case=case, db=self.db, db_record=run)
                _runs.append(_run)
        
        if (recent):
            # preserve original order
            _runs = sorted(_runs, key=lambda x: order.index(x.name))
            #__runs = sorted(_runs, key=lambda x: runs.index(x))

        return _runs
    
    def get_cases(self, names=None, paths=None, with_runs=None, with_jobs=None, 
                        return_empty=False, in_staging=None, last_job=False, 
                        with_run_status=None, with_run_tags=None, select=None, 
                        order='ASC', order_by="name"):
        """ Queries the database for cases matching passed parameters """
        records = []
        queries = []
        runs_to_add = {}
        jobs_to_add = {}

        # split queries according to whether it makes more sense to 
        # query JOBS, RUNS or CASES
        # ==========================
        # Query CASES
        # ==========================
        # sanitize
        if names is not None:
            if isinstance(names, str):
                names = [names]
        
        if paths is not None:
            # we query by name
            if isinstance(paths, str):
                paths = [paths]
            # get the relative path to the study
            paths = [self.get_rel_path(os.path.abspath(path)) for path in paths]
        
        if order_by is not None:
            assert isinstance(order_by, str)

        # for select, we do not run the general query
        if select is None:
            query = self.db.get_cases(names=names, paths=paths, order_by=order_by)
            queries.append(query)
        
        # ==========================
        # Query RUNS
        # ==========================
        # run centric queries
        # -> note we don't run the query unless one option is selection
        do_query = False
        if with_run_status is not None:
            assert isinstance(with_run_status, list)
            do_query = True
        
        if with_runs is not None:
            assert isinstance(with_runs, list)
            do_query = True
        
        if with_jobs is not None:
            # we expect a list of ints
            assert isinstance(with_jobs, list)
            assert all(isinstance(j, int) for j in with_jobs), "Job list must be integers"
            do_query = True
        
        if with_run_tags is not None:
            assert isinstance(with_run_tags, list)
            do_query = True
        
        if in_staging is not None:
            assert isinstance(in_staging, bool)
            do_query = True
        
        if with_run_status is not None:
            assert isinstance(with_run_status, list)
            from cfmt.entities.runs import Run
            # status must be a number
            with_run_status = [getattr(Run, s.upper()) for s in with_run_status]
            do_query = True
        
        run_query = []
        if do_query:
            run_query = self.db.get_runs(in_staging=in_staging,
                                         names=with_runs,
                                         status=with_run_status,
                                         has_job_ids=with_jobs,
                                         tags=with_run_tags) 
            records = []
            # grab the unique cases to get
            for run in run_query:
                runs_to_add.setdefault(run.case.id, []).append(run)
                if run.case not in records:
                    records.append(run.case)
            
            # grab add the jobs
            if with_jobs is not None:
                jobs = self.db.get_jobs(ids=with_jobs)
                for job in jobs:
                    jobs_to_add.setdefault(job.run.id, []).append(job)

        
            queries.append(records)
        
        # ==========================
        # Query SELECT (either RUN or JOB)
        # ==========================
        # -> arguments of select can be either a run list or a job number (int)
        # -> select is an AND operation, since we're being specific, so we treat it 
        #    seperately heere
        if select is not None:
            assert isinstance(select, list)

            # attempt to split between job ids and run_names
            job_ids = []
            run_names = []
            case_records = []
            run_records = []
            # -> assume we have an int
            for job_id in select:
                try:
                    split_id = job_id.split('#')
                    is_id = int(split_id.pop())
                except ValueError:
                    # get the run string
                    run_names.append(job_id)
                else:
                    job_ids.append(is_id)
            
            
            if job_ids:
                # get the jobs (and associated runs)
                jobs = self.db.get_jobs(ids=job_ids)
                run_records = []

                if jobs:
                    # grab the unique runs to get
                    for job in jobs:
                        jobs_to_add.setdefault(job.run.id, []).append(job)
                        if job.run not in run_records:
                            runs_to_add.setdefault(job.run.case.id, []).append(job.run)
                            run_records.append(job.run)
            
            if run_names:
                # get the runs
                run_query = self.db.get_runs(names=run_names)
                for run in run_query:
                    runs_to_add.setdefault(run.case.id, []).append(run)
                    run_records.append(run)
            
            # add get all unique cases 
            for run in run_records:
                if run.case not in case_records:
                    case_records.append(run.case)

            queries.append(case_records)

        #if with_job_id is not None:
        #    assert isinstance(with_job_id, list)
        #    job_ids = []
        #    run_names = []
        #    for job_id in with_job_id:
        #        try:
        #            split_id = job_id.split('#')
        #            is_id = int(split_id.pop())
        #        except ValueError:
        #            pass
        #        else:
        #            job_ids.append(is_id)
        #            # get the run string
        #            run_names.append('#'.join(split_id))

        #    # get the jobs (and associated runs)
        #    jobs = self.db.get_jobs(ids=job_ids)
        #    run_records = []

        #    if jobs:
        #        # grab the unique runs to get
        #        for job in jobs:
        #            jobs_to_add.setdefault(job.run.id, []).append(job)
        #            if job.run not in run_records:
        #                run_records.append(job.run)
        #        
        #    if run_records:
        #        # grab the unique cases to get
        #        records = []
        #        for run in run_records:
        #            runs_to_add.setdefault(run.case.id, []).append(run)
        #            if run.case not in records:
        #                records.append(run.case)
        #        queries.append(records)
        
        _cases = []
        
        if queries:
            from cfmt.entities.cases import Case
            # -> if any query returns a case, that should be present in all
            #    queries (AND)

            # ensure all queries have been executed
            _queries = [] 
            for query in queries:
                _queries.append([r for r in query])
            
            # compute the intersection
            _records = set(_queries[0]).intersection(*_queries)

            # sort back according to the first list
            _records = sorted(_records, key=_queries[0].index)

            for record in _records:
                # use existing case if we already have it
                case = Case(study=self, db_record=record)
                
                if case not in _cases:
                    _cases.append(case)

                if names and case.name in names:
                    names.remove(case.name)

                # add the runs
                if case.id in runs_to_add:
                    case.runs = case.get_runs(records=runs_to_add[case.id], last_job=last_job)

                    # add the jobs
                    for run in case.runs:
                        if run.id in jobs_to_add:
                            run.jobs = run.get_jobs(records=jobs_to_add[run.id])
        else:
            raise exceptions.CaseDoesNotExist("No cases found")
        
        if return_empty:
            for name in names:
                _cases.append(name)

        return _cases

    def update(self, args):
        """ 
        Updates the study

        args - argparser object of arguments
        Expected

        args.runs : list of run names 
        
        """
        from cfmt.entities.jobs import Job
        from tqdm import tqdm
        
        cases = None
        try:
            if args.case_names is not None:
                # we have a list of cases to get 
                cases = self.get_cases(names=args.case_names)
            elif args.directory is not None:
                directory = args.directory
                if isinstance(directory, str):
                    directory = [directory]
                cases = self.get_cases(paths=directory)
            elif args.runs is not None:
                # we're going to fetch the case which contains the runs requested
                # first hit the db to get the run, records,
                cases = self.get_cases(with_runs=args.runs)
        except exceptions.CaseDoesNotExist as err:
            utils.print_exception_info(err)

        if not cases:
            # check if a job has been passed
            if args.runs:
                # see if we have a job
                _jobs = self.get_jobs(ids=args.runs)
            else:
                # we're updating all
                # -> fetch all jobs with the defined status
                statuses = [   
                    Job.SUBMITTED,
                    Job.QUEUING,
                    Job.RUNNING,
                    Job.HOLDING
                ]
                _jobs = self.get_jobs(status=statuses)
            # group the jobs by batch systems
            if _jobs:
                from cfmt.batch_systems.controller import Batch
                
                jobs_batch = {}
                for job in _jobs:
                    self.logger.debug("Found job '%d' to update", job.id)
                    jobs_batch.setdefault(job.batch_system, []).append(job)
                
                jobs_to_save = []
                # load batch systems support by the profile (if any)
                batch_supported = config.get_supported_batch()

                if batch_supported is None:
                    batch_supported = list(jobs_batch.keys())

                # filter the list by supported
                job_count = 0
                for batch_system in list(jobs_batch.keys()):
                    if batch_system not in batch_supported:
                        del jobs_batch[batch_system]
                    else:
                        job_count += len(jobs_batch[batch_system])

                
                # load submit hosts supported by the profile (if any)
                hosts_supported = config.get_supported_submithost()
                if hosts_supported is not None:
                    hosts_supported = hosts_supported if isinstance(hosts_supported,list) else [hosts_supported]
                    job_count = 0 
                    for batch_system, jobs in jobs_batch.items():
                        fjobs = [j for j in jobs if j.submit_host in hosts_supported]
                        jobs_batch[batch_system] = fjobs
                        job_count += len(fjobs)

                print(f"Found {job_count} jobs to update ...")
                
                with tqdm(total=job_count, unit='jobs', ascii=True) as pbar:
                    msg = "OK"
                    for batch_system, __jobs in jobs_batch.items():
                        # for each batch system we call the update
                        batch = Batch(batch_system)

                        try:
                            batch.update(__jobs)
                        except exceptions.UpdateFailed as err:
                            # if the batch operation fails, we skip the batch system
                            self.console.warning("Batch system: '%s' failed update", batch_system)
                            self.logger.warning("Batch system: '%s' failed update", batch_system)
                            self.logger.warning(str(err))
                            msg = "PARTIAL"
                            continue
                        else:
                            # add the jobs to those to update
                            jobs_to_save += __jobs
                    
                    # save each job/case
                    for job in jobs_to_save:
                        self.logger.info("Updating job '%s'", job.id)
                        job_name = " -> Case '{}' ... ".format(job.run.case.name) + str(job.job_id)
                        pbar.set_description(desc=job_name)
                        try:
                            job.update()
                            job.save(cascade=True)
                        except exceptions.UpdateFailed:
                            msg = "PARTIAL"
                        finally:
                            pbar.update(1)
                
                    job_name = ""
                    job_name = " -> Case '{}' ... ".format(job.run.case.name) + msg
                    pbar.set_description(desc=job_name)
            else:
                print("Nothing to update")
        else:

            # handle stuff we've set on the command line
            run_kwargs = {}
            if args.set_status is not None:
                run_kwargs['status'] = args.set_status[0]
            if args.set_reason is not None:
                run_kwargs['reason'] = args.set_reason[0]
            if args.set_staging is not None:
                run_kwargs['in_staging'] = args.set_staging[0]
            if args.set_staging_mode is not None:
                run_kwargs['staging_mode'] = args.set_staging_mode[0]
            if args.set_archived is not None:
                run_kwargs['is_archived'] = args.set_archived[0]
            if args.add_tags is not None:
                run_kwargs['add_tags'] = args.add_tags
            if args.remove_tags is not None:
                run_kwargs['remove_tags'] = args.remove_tags
            if args.set_tags is not None:
                run_kwargs['tags'] = args.set_tags

            if run_kwargs: 
                # loop over and update cases
                for case in cases:
                    try:
                        self.logger.info("Updating case '%s'", case.name)
                        print(" -> Case '{}' ...".format(case.name), end="\r")
                        case.update(run_kwargs=run_kwargs)
                    except exceptions.UpdateFailed as err:
                        utils.print_exception_info(str(err), True)
                        print(" -> Case '{}' ... FAILED".format(case.name))
                        raise
                
            if args.move_to_case is not None:
                # we are moving the run to a new case
                # -> check the destination is a valid case
                from cfmt.entities.cases import load_case
                dest = args.move_to_case[0]
                dest_abs = os.path.abspath(dest)
                
                path = dest_abs
                while path != self.path:
                    try:
                        dest_case = load_case(path, self)
                    except exceptions.CaseDoesNotExist:
                        msg = "Directory '{}' is not a valid case directory"
                        msg = msg.format(dest)
                        valid_case = False
                        print(msg)
                        break
                    except IOError as err:
                        utils.print_exception_info(str(err))
                        sys.exit(0)
                    else:
                        case_abs_path = os.path.join(self.path, dest_case.path)
                        if dest_abs == case_abs_path:
                            valid_case = True
                            break
                        else:
                            valid_case = False
                            continue
                    finally:
                        oldpath, path = path, os.path.dirname(path)
                
                if not valid_case:
                    return
        
                # make a backup of the database
                #try:
                #    print(" -> Backing up database ... ", end='\r')
                #    self.db.backup_db()
                #except IOError as err:
                #    print(" -> Backing up database ... FAILED", end='\n')
                #    print(str(err))
                #    print("Copy halted")
                #    exit(1)
                #else:
                #    print(" -> Backing up database ... OK", end='\n')

                # count runs
                n_runs = len([r for case in cases for r in case.runs])

                with tqdm(total=n_runs, unit='runs', ascii=True) as pbar:
                    # do the move
                    for case in cases:
                        for run in case.runs:
                            msg = f" -> Run '{run.name}' ... "
                            pbar.set_description(desc=msg)

                            try:
                                run.move(dest_case)
                            except (exceptions.CaseExists, exceptions.InvalidCode) as err:
                                pbar.write(utils.WARNING + ": " + str(err) + " ... " + utils.SKIPPING)
                                continue
                            finally:
                                pbar.update(1)
                exit(0)

    
    def get_jobs(self, status=None, records=None, ids=None, recent=None, limit=None, 
                 with_dev=False, type_=None, local_only=False):
        """ 
        Queries the database for jobs matching the passed parameters 
        Jobs are returned with the correct job.run set
        """
        from cfmt.entities.cases import Case
        from cfmt.entities.runs import Run
        from cfmt.entities.jobs import Job

        if type_ is not None:
            # argparse provides these in a list
            if isinstance(type_, list):
                type_ = type_[-1]
            
            if isinstance(type_, str):
                assert type_ in Job._name_to_type, "Invalid Job type" 
                type_ = Job._name_to_type[type_]
            elif isinstance(type_, int):
                assert type_ in Job._type_to_name, "Invalid Job type"

            # convert back to list
            type_ = [type_]
        
        if status is not None:
            # we query by status
            if isinstance(status, int):
                status = [status]
            
            assert isinstance(status, list)
            #records = self.db.get_jobs(status=status)
        elif ids is not None:
            # we query by id
            if isinstance(ids, int):
                ids = [ids]

            assert isinstance(ids, list)
            #records = self.db.get_jobs(ids=ids)

        #elif recent:
            #records = self.db.get_jobs(recent=True, limit=limit, with_dev=with_dev)
        #    pass
        elif records is not None:
            pass
        #else:
            #records = self.db.get_jobs()

        submit_host=None
        if local_only:
            hosts_supported = config.get_supported_submithost()
            # either current submithost, or the one in the config
            if hosts_supported is not None:
                hosts_supported = hosts_supported if isinstance(hosts_supported,list) else [hosts_supported]
            else:
                import socket
                current_host = socket.getfqdn()
                hosts_supported = [current_host]
            
            submit_host=hosts_supported


        if records is not None:
            pass
        else:
            records = self.db.get_jobs(recent=recent, limit=limit, with_dev=with_dev, status=status,
                                       type_=type_, ids=ids, submit_host=submit_host)


        _jobs = []
        sort_order = []
        if records:
            run_ids = {}
            job_orphaned = []
            # gather unique run_ids from the jobs
            for record in records:
                # make sure the right run is set
                # collect run ids
                # store job ids for ordering
                sort_order.append(record.id)
                run_ids.setdefault(record.run, []).append(record)
            # gather unique case_records 
            case_ids = {}
            for run in run_ids:
                if run is None:
                    # we've got some orphaned jobs
                    for job in run_ids[run]:
                        job_orphaned.append(job)
                    continue
                case_ids.setdefault(run.case, []).append(run)

            # check for orphans
            if job_orphaned:
                msg = "Orphaned jobs found"
                utils.print_exception_info(msg)
                print(job_orphaned)
                print("Might wanna fix that")
                exit(1)
            # fetch the cases
            for _case in case_ids:
                case = Case(study=self, db_record=_case)
                for _run in case_ids[_case]:
                    # fetch the runs
                    run = Run(study=self, case=case, db_record=_run)
                    for _job in run_ids[_run]:
                        # fetch the jobs
                        job = Job(run=run, db_record=_job)
                        _jobs.append(job)

            # fix order
            __jobs = sorted(_jobs, key=lambda job: sort_order.index(job.id))
            _jobs = __jobs
        
        return _jobs

    def print_status(self, args):
        """ Prints the status of the study """

        from cfmt import tables
        
        # mutually exclusive
        if args.list_jobs and args.list_archives:
            self.console.error("Cannot list jobs and archvies at the same time")
            sys.exit(1)
        
        print_summary = config.configtool.getboolean('defaults', 'study_summary_with_status', fallback=False)        

        if print_summary:
            data = {
        
            'name': self.name,
            'description': self.description,
            'case_number': self.case_number,
            'last_modified': utils.print_datetime(self.last_modified),
            'uuid': self.uuid
            }
            print(utils.remove_left_margin(self.status_template.format(**data)))
            print()
        
        cases = None
        runs = None
        _jobs = None
        # if we've asked for something specific, turn off all

        # modes are for things that need to be true in order to do something different
        modes = [
            args.recent,
            args.list_archives,
        ] 
        
        # specifiers just need to not be None
        specifiers = [
            args.directory, 
            args.case_names, 
            args.jobs, 
            #args.recent, 
            #args.list_archives,
            args.with_tags,
            args.with_status,
            args.with_staging,
            args.local
        ]

        if specifiers.count(None) != len(specifiers):
            args.update_all = False
        
        if any(modes) :
            args.update_all = False


        if args.update_all:
            # we're going to fetch all the cases and loop through
            #cases = self.get_all_cases()
            #runs = self.get_runs()
            cases = self.get_cases()
        elif args.recent: 
            # special list of the most recent runs (jobs)
            _jobs = self.get_jobs(recent=True, limit=args.limit, with_dev=args.with_dev, type_=args.job_type,
                                  local_only=args.local)
        else:
            # try the cases
            try:
                cases = self.get_cases(names=args.case_names, 
                                       paths=args.directory,
                                       with_jobs=args.jobs,
                                       with_run_tags=args.with_tags,
                                       with_run_status=args.with_status,
                                       in_staging=args.with_staging,
                                       )
            except exceptions.CaseDoesNotExist as err:
                utils.print_exception_info(err)

        if cases:
            # loop over and collect data for tabulation
            print_list = []
            for case in cases:
                if isinstance(case,str):
                    # a string implies a null result, see return_empty flag to get_cases
                    print(" " + utils.cstr("==>", fg='red', attr=['bold']) + " case: " + utils.cstr(case, fg='white', attr=['bold']) + " does not exist")
                    continue
                
                # determine what runs to print, if not already set by query
                if case.runs is None:
                    case.set_runs(last_job=False)

                if args.machine_readable:
                    # add the run to a list to print 
                    
                    print_list += [str(r.name) for r in case.runs]
                    continue

                # skip if the case has no runs 
                if len(case.runs) < 1:
                    print(" " + utils.cstr("==>", fg='green') + " case: " + utils.cstr(case.name + " --no runs--", fg=248))
                    continue
                print()
                print(" " + utils.cstr("==>", fg='green', attr=['bold']) + " case: " + utils.cstr(case.name, fg='white', attr=['bold']))
                case.print_status(
                    dates=args.dates, 
                    notes=args.notes, 
                    details=args.details, 
                    src_ref=args.src_ref,
                    job_list=args.list_jobs, 
                    stats=args.stats, 
                    sizes=args.sizes,
                    archive_list=args.list_archives,
                    with_dev=args.with_dev,
                    job_type=args.job_type,
                    with_deleted=args.with_deleted,
                )
            if args.machine_readable:
                print(" ".join(print_list))
        elif runs:
            from cfmt.entities.runs import Run

            # print list of runs
            #columns=set(['finished'])
            columns=None
            headers = Run.get_status_headers(dates=args.dates, src_ref=args.src_ref, columns=columns)
            data = []
            
            # loops over and collect data for tabulation
            for run in runs:
                data.append(run.get_status_row(dates=args.dates, src_ref=args.src_ref, columns=columns))
            # formats = plain, simple grid, pipe, orgtbl
            tables.print_table(data, headers, tablefmt="plain")
        elif _jobs:
            from cfmt.entities.jobs import Job
            # print list of jobs
            columns=['id','name','status', 'run_status', 'tags', 'finished_h', 'wallclock']
            headers = Job.get_status_headers(dates=args.dates, src_ref=args.src_ref, columns=columns)
            data = []
            for job in _jobs:
                data.append(job.get_status_row(dates=args.dates, src_ref=args.src_ref, columns=columns, color_row=False))
            tables.print_table(data, headers, tablefmt="plain")
        else:
            print(utils.cstr("-- No results --", fg=248))
            

    def format_notes(self):
        """ Format notes for view """
        # return all notes for the study
        if self.notes is None:
            self.get_notes()

        # sort my most recent first
        _notes = sorted(self.notes, key=lambda x: x.created, reverse=True)
        
        text = ""
        if _notes:
            for note in _notes:
                # case
                text += utils.cstr("Case:  {}\n".format(note.case.name), fg=3)
                
                # date
                text += utils.cstr("Date:  {}\n".format(utils.print_datetime(note.created)), fg=250)
                
                # id
                text += utils.cstr("ID:    {}\n".format(note.id), fg=250)

                # new line
                text += "\n"

                # note text
                text += utils.indent_string(note.text)

                # new line
                text += "\n\n"
        else:
            text = utils.cstr("No notes for study: '{}'".format(self.name), fg=248)

        return text

    def edit_note(self, ids=None, editor=None):
        """
        Edits a note with id ids
        """
        from cfmt.entities.notes import Note
        
        # make sure it is a list
        if isinstance(ids, int):
            ids = [ids]
        # make sure there's only 1
        assert len(ids) == 1

        # get the notes
        self.get_notes(ids=ids)

        # turn them into objects
        if not self.notes:
            msg = "No notes with id '{}'".format(ids.pop())
            self.logger.info(msg)
            print(msg)
            return

        for note in self.notes:
            _note = Note(db_record=note, db=self.db)
                    
        initial_message =  str(_note)
        initial_message += "#-----------------\n"
        initial_message += "# vim: ft=markdown"
        self.logger.info("Editing note '%s'", str(_note.id))

        if editor is None:
            editor = config.get_editor()

        # we request an editor
        text = utils.editor_prompt(initial_msg=initial_message,
                                   strip_comments=True, 
                                   remove_initial_msg=True,
                                   editor=editor)

        if len(text) != 0:
            # save the new note
            _note.set_text(text)
            self.logger.info("Note '%s' edited", _note.id)
            print("Edited note '{}'".format(str(_note.id)))
            _note.save()

    
    def get_notes(self, last_n=None, ids=None):
        """ 
        Returns the last_n notes 
        Note this returns a query. NOT A notes object
        
        """
        self.notes = self.db.get_notes(limit=last_n, ids=ids)

    def delete_notes(self, ids, prompt=False):
        """
        Deletes the notes with ids provided in ids
        """
        # if we have a single integer, put into list
        if isinstance(ids, int):
            ids = [ids]

        assert isinstance(ids, list)

        # get the notes
        self.get_notes(ids=ids)

        # work out which ones are valid
        valid_ids = [x.id for x in self.notes if x.id in ids]
        invalid_ids = [x for x in ids if x not in valid_ids]

        if invalid_ids:
            self.console.warning("Note ids '%s' not found",", ".join(str(x) for x in invalid_ids))

        # get confirmation
        if valid_ids:
            if prompt:
                msg = "Do you wish to delete notes '{}'".format(", ".join(str(x) for x in valid_ids))
                do_delete = utils.confirm(prompt=msg, default=False)
            else:
                do_delete = True
        
            if do_delete:
                try:
                    self.db.delete_notes(ids=valid_ids)
                except exceptions.DeleteFailed as err:
                    utils.print_exception_info(str(err))
                else:
                    msg = "Deleted notes '{}'".format(", ".join(str(x) for x in valid_ids))
                    print(msg)
                    self.logger.info(msg)
        else:
            print("Nothing to delete")

    
    def archive(self, args, cwd=None):
        """
        Archives runs
        """ 
        from tqdm import tqdm
        from cfmt.entities.archives import init_archive_repo
        
        # confirm we have required config
        path = config.configtool.get('archive', 'path', fallback=None)
        host = config.configtool.get('archive', 'host', fallback=None)

        if path is None:
            utils.print_exception_info("Please set the archive path in configuration")
            sys.exit(1)

        if host is not None:
            # if a host provided, we assume a remote, possibly shared repo
            archive_dir = self.name + "-" + self.uuid.hex
            assert len(archive_dir) <= 255, "Archive directory name too long"
            path = os.path.join(path, self.name + "-" + self.uuid.hex)
        #assert hasattr(args, "RUNS"), "RUNS argument missing"
            
        if host is None and not os.path.isabs(path):
            path = os.path.join(self.path, path)

        cases = []
        runs = []
        archives = []

        # we'll either have RUNS, IDS or none (all)

        if args.RUNS:
            # we're getting all the cases associate
            if args.archive_mode in ["create", "withdraw", "deposit"]:
                try:
                    cases = self.get_cases(with_runs=args.RUNS)
                except exceptions.CaseDoesNotExist:
                    print("No cases found with runs '{}'".format(",".join(args.select)))
            else:
                # we're operating on a list of archives
                runs = self.get_runs(names=args.RUNS)

                if runs:
                    for run in runs:
                        run.load_archives()
                        if not run.archives:
                            print("Run {} does not have any archives".format(run.name))
                            sys.exit(0)
                        else:
                            [archives.append(a) for a in run.archives]


        elif args.IDS:
            # we're getting a specific archive
            assert hasattr(args, "IDS")
            # ids should be in 
            ids = args.IDS.copy()
            runs = self.get_runs(has_archives=ids)

            if runs:
                # attached the archives
                for run in runs:
                    run.load_archives(ids=ids)

                    # check we found them all
                    for a in run.archives:
                        ids.remove(a.id)
                        archives.append(a)

                if ids:
                    print("Cannot find archives with ids '{}'".format(",".join(str(i) for i in ids)))
                    sys.exit(0)
            else:
                print("No archives found")
                sys.exit(0)

        elif args.all:
            # we're getting all the cases associated with the current working directory
            if cwd == self.path:
                cases = self.get_all_cases()
            else:
                cases = self.get_cases(paths=cwd)
            
            # confirm we want to operate on all!
            if not utils.confirm("{} cases found. Are you sure you want to continue?".format(len(cases)), default=False):
                sys.exit(0)
        elif args.init:
            # we're initialising
            init_archive_repo(host, path, archive_type=args.type)
            sys.exit(0) 
        else:
            return

        if cases:
            # if a case doesn't have any runs, we fetch them all
            [c.get_runs() for c in cases if c.runs is None]

            for c in cases:
                print("Processing case '{}'".format(c.name))
                try:
                    c.archive(args, path, host)
                except exceptions.ArchiveFailed as err:
                    msg = "Failed to archive case '{}'".format(c.name)
                    utils.print_exception_info(msg + str(err))
                    continue
        
        if archives:
            op = args.archive_mode

            # warn for delete
            if op == "delete":
                check = utils.confirm("{} archives found. Are you sure you want to delete?".format(len(archives)), default=False)
                if not check:
                    sys.exit(0)

            if op == "restore":
                # we know we should only have 1 archive
                rdir = archives[0].run.path
                check = utils.confirm("Restoring will delete ALL data in run directory {}. Continue?".format(rdir))
                if not check:
                    sys.exit(0)

            if op == "list_contents":
                # we don't loop, just work
                a = archives.pop()
                try:
                    getattr(a, op)()
                except exceptions.ArchiveFailed as err:
                    utils.print_exception_info(err)

                sys.exit(0)

            status = utils.OK
            base_desc = " -> {}".format(op.title())
            with tqdm(total=len(archives), unit=' archive', ascii=True) as pbar:
                for a in archives:
                    a.set_option(
                        force=args.force,
                    )
                    pbar.set_description(base_desc)
                    try:
                        _op = getattr(a, op)
                        _op()
                    except exceptions.ArchiveFailed as err:
                        pbar.write(str(err))
                        utils.print_exception_info(err, no_print=True)
                        status = utils.PARTIAL
                    except (AttributeError, NotImplementedError) as err:
                        utils.print_exception_info(err)
                        pbar.write("Operation not implemented")
                        desc = base_desc + " " + utils.ERROR
                        pbar.set_description(desc=desc)
                        pbar.close()
                        sys.exit(1)
                    
                    pbar.update(1)
                
                desc = base_desc + " " + status
                pbar.set_description(desc=desc)


            # we're doing operations that don't require the case

    
    def fetch(self, args, sync=False):
        """
        Fetches staged data
        """
        keep_staged = False
        cases = None
        if args.select is not None:
            # we've been passed something specific to look for
            try:
                cases = self.get_cases(with_runs=args.select)
            except exceptions.CaseDoesNotExist:
                print("No cases found with runs '{}'".format(",".join(args.select)))
        elif args.all:
            try:
                cases = self.get_cases(in_staging=True)
            except exceptions.CaseDoesNotExist:
                print("No cases found with runs in staging")
        
        # preserve source
        if sync:
            preserve_source = True
            keep_staged = True
        else:
            preserve_source = args.preserve_source

        
        if cases:
            # special mode - moving staging to a different device
            if args.mode == "sync" and args.move_staging_to is not None:
                staging_dest = args.move_staging_to[-1]
                
                # default is not to preserve source
                preserve_source = False

                # loop and print the new case destinations before submitting
                data = []
                runs = []
                dests = []
                headers = ["Run", "Source", "Destination"]
                staging_root = config.configtool.get('staging', 'location', fallback=None)
                if staging_root is None:
                    raise exceptions.ConfigError("Staging location missing")
                lead_p = len(utils.splitpath(staging_root))

                for case in cases:
                    for r in case.runs:
                        # get common path between the two
                        if r.staging_mode == "none":
                            row = [r.name, "SKIPPING - run not staged", "-"]
                        else:
                            # get the common root
                            p = utils.splitpath(r.staging_dir)
                            rp = p[lead_p:]
                            rp = os.path.join(*rp)
                            dest = os.path.join(staging_dest, rp)
                            if os.path.exists(dest) and os.path.samefile(r.staging_dir, dest):
                                row = [r.name, "SKIPPING - new directory is the same", "-"]
                            else:
                                runs.append(r)
                                dests.append(dest)
                                row = [r.name, r.staging_dir, dest]
                        
                        data.append(row)
                from cfmt.tables import print_table
                print("")
                print_table(data, headers=headers)
                print("")
                msg = "Please confirm the above path changes are correct"
                if not utils.confirm(msg, default=False):
                    exit()
            
                # determine if we're submitting batch jobs
                if args.batch_system is not None:
                    from cfmt.batch_systems.controller import Batch, get_batch_cls

                    batch_system = args.batch_system[-1]

                    batch_class = get_batch_cls(batch_system) 
                    # get options from config
                    system_name = batch_class.system_name
                    default_queue = config.configtool.get(system_name, "sync_queue", fallback=None)
                    default_extra = config.configtool.get(system_name, "sync_extra", fallback=None)

                    batch_options = {
                        "script_name": config.configtool.get(system_name, "move_staging_script_name", fallback="move-staging.sh"),
                    }
                    if default_queue is not None:
                        batch_options["queue"] = default_queue

                    if default_extra is not None:
                        batch_options["extra"] = default_extra

                    batch = batch_class(batch_options=batch_options, parallel=False, np=1, cmdline_args=args)
                else:
                    batch = None

                for run, dest in zip(runs,dests):
                    try:
                        run.move_staging(
                            dest,
                            args, 
                            batch=batch,
                            preserve_source=preserve_source,
                        )
                    except:
                        pass

            else:

                for case in cases:
                    self.logger.info("Fetching data for case '%s'", case.name)
                    print("Processing case '{}'".format(case.name))
                    try:
                        case.fetch(
                            args,
                            preserve_source=preserve_source, 
                            keep_staged=keep_staged,
                            with_tout=args.with_tout,
                            exclude=args.exclude
                        )
                    except exceptions.TransferFailed as err:
                        utils.print_exception_info("Failed to fetch data for case '{}': ".format(case.name) + str(err))
                    else:
                        self.logger.info("Successfully fetched data for case '%s'", case.name)

        
    def upgrade_db(self):
        """
        Upgrades the database
        DEPRECATED
        """
        self.db.upgrade_db()

    
    def delete(self, args):
        """
        Deletes elements of a study
        """
        jobs = None
        delete_case = False
        if args.case_names is not None:
            # we have a list of cases to get
            delete_case = True
            try:
                cases = self.get_cases(names=args.case_names, return_empty=True)
                
                # fetch all the runs
                for case in cases:
                    case.runs = case.get_runs(last_job=False)
            except exceptions.CaseDoesNotExist:
                # we found nothing
                print("No cases found with name(s) '{}'".format(", ".join(args.case_names)))
                cases = None
        elif args.case_paths is not None:
            delete_case = True
            try:
                cases = self.get_cases(paths=args.case_paths)
                
                # fetch all the runs
                for case in cases:
                    case.runs = case.get_runs()

            except exceptions.CaseDoesNotExist:
                print("No cases found with path(s) '{}'".format(", ".join(args.case_paths)))
                # we found nothing
        elif args.jobs is not None:
            # we have some jobs to delete
            # get cases from job number
            try:
                jobs = self.get_jobs(ids=args.jobs)
                cases = None
            except exceptions.JobDoesNotExist:
                # we found nothing
                pass
            
            if not jobs:
                print("No jobs found with job id(s) '{}'".format(", ".join([str(x) for x in args.jobs])))

            
        elif args.runs is not None:
            # get runs from run name
            try:
                cases = self.get_cases(with_runs=args.runs)
            except exceptions.CaseDoesNotExist:
                # we found nothing
                print("No cases found with run name(s) '{}'".format(", ".join(args.jobs)))
        else:
            # attempt to load the case from the cwd
            cases = None

        if cases:
            # we have some cases to action
            for case in cases:
                case.delete(
                    delete_runs=True,
                    delete_case=delete_case,
                    keep_files=args.keep_files,
                    prompt = not args.yes,
                    keep_database = not args.delete_db,
                    staging_only = args.staging_only,
                )
        
        if jobs:
            # we have some jobs to delete
            for job in jobs:
                job.delete(prompt=not args.yes)
        
        if not (cases or jobs):
            print("Nothing to delete...")
    
    def move(self, destination, copy_files=True):
        """ Moves study """
        print("Moving study '{}' to '{}'".format(self.name, destination))
        
        abs_destination = os.path.abspath(destination)
        # check the destination doesn't exist
        if os.path.exists(abs_destination):
            raise FileExistsError("Destination '{}' exists".format(destination))

        # make a backup of the database
        try:
            print(" -> Backing up database ... ", end='\r')
            self.db.backup_db()
        except IOError as err:
            print(" -> Backing up database ... FAILED", end='\n')
            print(str(err))
            print("Move halted")
            exit(1)
        else:
            print(" -> Backing up database ... OK", end='\n')

        ## CATCH KEBOARD HERE
        try:
            # modifiy the database
            print(" -> Modifying database ... ", end="\r")
            self.db.move_study(abs_destination, db_record=None)
            print(" -> Modifying database ... OK", end="\n")

            # copy the files
            if copy_files:
                print(" -> Copying files ... ", end="\r")
                shutil.copytree(self.path, abs_destination, symlinks=True, ignore_dangling_symlinks=True)
                #shutil.copytree(self.path, abs_destination)
                print(" -> Copying files ... OK", end="\n")

        except (KeyboardInterrupt, exceptions.DatabaseException,shutil.Error) as err:
            if isinstance(err, KeyboardInterrupt):
                print("Caught SIGINT ...")
            utils.print_exception_info(str(err), no_log=True)
            # restore backup
            print(" -> Restoring backup database ... ", end="\r")
            self.db.restore_db_backup()
            print(" -> Restoring backup database ... OK", end="\n")
            print("Move halted")
            exit(1)
        else:
            if copy_files:
                print(" -> Deleting original files ... ", end='\r')
                shutil.rmtree(self.path)
                print(" -> Deleting original files ... OK", end='\n')
            print("Move complete")
            exit(0)

    def export(self, args):
        """
        exports readable lists
        """
        
        if args.item:
            # we've been passed something by name or id
            _cases = self.get_cases(select=args.item)
            #_cases = self.get_cases(names=args.item,with_runs=args.item, with_job_id=args.item, last_job=False)
        elif args.status:
            # we're after run with select status
            _cases = self.get_cases(with_run_status=args.status)
        else:
            # we get all..
            raise NotImplementedError()
        
        data = {}
        
        # handle the study fields
        study_db_fields = []
        if args.add_study_db_field:
            study_db_fields = [x for sublist in args.add_study_db_field for x in sublist]
        study_fields = []
        if args.add_study_field:
            study_fields = [x for sublist in args.add_study_field for x in sublist]
        study_field_value = {}
        if args.add_study_field_value:
            for entry in args.add_study_field_value:
                study_field_value[entry[0]] = entry[1]
        
        if study_fields:
            for field in study_fields:
                data[field] = ""
        
        if study_field_value:
            for key, value in study_field_value.items():
                data[key] = value
        
        if study_db_fields:
            for field in study_db_fields:
                data[field] = str(getattr(self, field))
            
        # Handle case/run specific
        if _cases:
            data["cases"] = []
        
        db_fields = []
        fields = []
        field_value = {}
        if args.add_db_field:
            # we have database fields to add
            db_fields = [x for sublist in args.add_db_field for x in sublist]
        
        if args.add_field:
            fields = [x for sublist in args.add_field for x in sublist]
        
        if args.add_field_value:
            for entry in args.add_field_value:
                field_value[entry[0]] = entry[1]
        
        for case in _cases:
            if case.runs is None:
                # we're just adding the case
                _data = {
                    "name": case.name
                }
                for field in fields:
                    _data[field] = ""
                
                for key, value in field_value.items():
                    _data[key] = value
                
                data["cases"].append(_data)
            else:
                for run in case.runs:
                    _data = {
                        "name": run.name
                    }
                    for field in fields:
                        _data[field] = ""
                    for key, value in field_value.items():
                        _data[key] = value
                    for field in db_fields:
                        if field == "status":
                            _data[field] = run._level_to_name[getattr(run, field)]
                        else:
                            _data[field] = str(getattr(run, field))
                    data["cases"].append(_data)

        utils.write_output(data, form=args.format.pop())
        

    def comparison_plot(self, runs, fname, live=False, local_only=False):
        """
        For comparing two runs

        NOTE: The fpath must be common to all runs
        """
        assert isinstance(fname, str), "comparison_plot requires a string argument for fpath"
        
        # currently we only support comparing up to 3 differenr runs
        if len(runs) > 5:
            raise NotImplementedError("Cannot compare more than 3 runs")
        
        # check that fpath exists in all runs
        for r in runs:
            fpath = os.path.join(r.get_run_dir(local_only=local_only), fname)
            if not os.path.isfile(fpath):
                raise FileNotFoundError(f"Requested plot file '{fpath}' does not exist in run '{r.name}'")


        from cfmt.plotting import PlotComparisonApp
        
        app = PlotComparisonApp(runs, fname, local_only=local_only)
        
        sys.exit(app.run())
