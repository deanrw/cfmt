"""
   cft
"""
from distutils.core import setup

setup(
    name='Computational Fluids Management Tool',
    url='https://wilsonlx.mace.manchester.ac.uk/git/cfmt.git',
    author='Dean Wilson',
    author_email='dean.wilson@manchester.ac.uk',
    version='0.2',
    packages=['cfmt',],
    license='GPL 3.0',
    long_description=open('README.md').read(),
    entry_points = {
        'console_scripts': ['cfmt=cfmt.cfmt:main'],
    },
)
